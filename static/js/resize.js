function resize_footer() {
    var footer = document.getElementById('site_footer');
    footer.style.width = "";
    footer.style.bottom = "";
    footer.style.position = "relative";
    if (footer.offsetTop < (window.innerHeight - footer.clientHeight)) {
        footer.style.bottom = "0px";
        footer.style.position = "absolute";
        footer.style.width = "100%";
    }
}

if ('attachEvent' in window) {
    window.attachEvent('onload', resize_footer);
    window.attachEvent('onresize', resize_footer);
}
else {
    window.addEventListener('load', resize_footer, false);
}
window.addEventListener('resize', resize_footer, false);
