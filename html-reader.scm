(define-module (html-reader)
  #:use-module (htmlprag)
  #:use-module (haunt post)
  #:use-module (haunt reader)
  #:use-module (srfi srfi-26)
  #:use-module (ice-9 match)
  #:export (html-reader*))

(define (read-html-post port)
  (values (read-metadata-headers port)
          (catch #t
            (lambda ()
              (match (html->shtml port)
                (('*TOP* sxml ...) sxml)))
            (lambda (err)
              (display "WHOOPS: something went wrong when rendering HTML!\n")
              `((doctype "html")
                (html
                 (head
                  (title "whoops"))
                 (body
                  (h1 "Haha whoops lollerskates")
                  (p "Hey, something went wrong when you were writing HTML"))))))))

(define html-reader*
  (make-reader (make-file-extension-matcher "html")
               (cut call-with-input-file <> read-html-post)))
