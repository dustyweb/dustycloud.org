title: Announcing FOSS and Crafts
date: 2020-07-14 11:20
author: Christine Lemmer-Webber
tags: fossandcrafts, podcast
slug: announcing-foss-and-crafts
---

I wrote recently about
[departing Libre Lounge](https://dustycloud.org/blog/departing-libre-lounge/)
but as I said there, "This is probably not the end of me doing
podcasting, but if I start something up again it'll be a bit different
in its structure."

Well!  Morgan and I have co-launched a new podcast called
[FOSS and Crafts](https://fossandcrafts.org/)!
As the title implies, it's going to be a fairly interdisciplinary
podcast... the title says it all fairly nicely I think: "A podcast
about free software, free culture, and making things together."

We already have the [intro episode](https://fossandcrafts.org/) out!
It's fairly intro-episode'y... meet the hosts, hear about what to expect
from the show, etc etc... but we do talk a bit about some background
of the name!

But more substantial episodes will be out soon.
We have a lot of plans and ideas for the show, and I've got a pretty
good setup for editing/publishing now.
So if that sounds fun, subscribe, and more stuff should be hitting
your ears soon!

(PS: we have a nice little community growing in
[#fossandcrafts on irc.freenode.net](https://webchat.freenode.net/?channels=fossandcrafts)
if you're into that kind of thing!)
