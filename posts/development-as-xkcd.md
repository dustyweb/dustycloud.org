title: Development As XKCD
date: 2009-06-07 22:14
tags: economics, xkcd
author: Christine Lemmer-Webber
slug: development-as-xkcd
---
As some of you may know, Morgan Lemmer (now Morgan Lemmer-Webber) and I
recently [got married](http://wedding.dustycloud.org) and are now on our
honeymoon in Montreal. More on that later, probably.

Anyway, between things I have been rereading *Development As Freedom* by
Amartya Sen, which I originally consumed as part of a class on the
ethics of globalization. It's a remarkably good book that I think I
appreciate much more having aged a few years. Anyway, a good portion of
the beginning of the book encompasses a general overview and evaluation
of different ethical systems. At one point Sen is advocating for the
value of using a large range of ethical systems rather than just using a
static set of rules (like libertarianism) or a particular framework
(like utilitarianism or John Rawls' "Theory of Justice" approach), and
that the use of human rationality to evaluate ethical situations should
be viewed positively rather than as a sign of failure. There's this
particular paragraph:

> There's an interesting choice here between "technocracy" and
> "democracy" in the selection of weights, which may be worth discussing
> a little. A choice procedure that relies on a democratic search for
> agreement or a consensus can be extremely messy, and many technocrats
> are sufficiently disgusted by its messiness to pine for some wonderful
> formula that would simply give us ready-made weights that are "just
> right." However, no such magic formula does, of course, exist, since
> the issue of weighting is one of valuation and judgment, and not one
> of some impersonal technology. \[p. 79\]

I'm pretty sure the net Sen was casting here aimed a bit wider than just
the ethics and rules of sex, and yes... I'm aware that nerds relating
everything to XKCD is such a goddamned cliche, but I can't help but
think that Monroe [summarized that paragraph pretty well in comic
form](http://xkcd.com/592/).
