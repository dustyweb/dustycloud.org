title: Firebug for Firefox3
date: 2008-06-18 10:11
tags: firefox, firebug
author: Christine Lemmer-Webber
slug: firebug-for-firefox3
---
A lot of people have been complaining about not being able to install,
or know where to install, [Firebug](http://getfirebug.com) for firefox
3. Thought I'd spread the news that, yes, there is [a release
available](https://addons.mozilla.org/en-US/firefox/addon/1843). It is a
beta version, but I haven't seen any problems so far, and it's a bit
nicer than the previous version too.

Ok, I'll stop slacking and get back to work now. ;p
