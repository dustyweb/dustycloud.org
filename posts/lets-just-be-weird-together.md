title: Let's Just Be Weird Together
date: 2019-06-25 13:10
author: Christine Lemmer-Webber
tags: life, anniversary
slug: lets-just-be-weird-together
---
![](/etc/images/blog/ljbwt.gif)

Approximately a month ago was [Morgan](https://mlemmer.org/) and I's 10
year wedding anniversary. To commemorate that, and as a surprise gift, I
made the above ascii art and animation.

Actually, it's not just an animation, it's a program, and one [you can
run](https://gitlab.com/dustyweb/dos-hurd/blob/master/dos-hurd/examples/ljbwt.rkt).
As a side note, I originally thought I'd write up how I made it, but I
kept procrastinating on that and it lead me to putting off writing this
post for about a month. Oh well, all I'll say for now is that it lead to
a [major rewrite](https://gitlab.com/spritely/goblinoid) of one of the
[main components of Spritely](https://gitlab.com/spritely/goblins). But
that's something to speak of for another time, I suppose.

Back to the imagery! Morgan was surprised to see the animation, and yet
the image itself wasn't a surprise. That's because the design is
actually built off of one we collaborated on together:

![](/etc/images/blog/ljbwt-embroidery-scaled.jpg)

I did the sketch for it and Morgan embroidered it. The plan is to put
this above the tea station we set up in the reading area of our house.

The imagery and phrasing captures the philosophy of Morgan and I's
relationship. We're both weird and deeply imperfect people, maybe even
in some ways broken. But that's okay. We don't expect each other to
change or become something else... we just try to become the best weird
pairing we can together. I think that strategy has worked out for us.

Thanks for all the happy times so far, Morgan. I look forward to many
weird years ahead.
