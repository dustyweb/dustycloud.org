title: Why games matter to free software and free culture
date: 2012-06-26 10:20
author: Christine Lemmer-Webber
slug: why-faif-games-matter
---
<p><i>(Note: this started out as a longer post about the history and
rationale of the <a href="http://lpc.opengameart.org">Liberated Pixel
Cup</a> under a subheading called "where games go, technology
follows".  But I found that this section got so long it merited its
own post, so I decided to break it out.)</i></p>

<p>
I've heard it stated before that "games aren't important" or aren't a
priority by multiple people in free software (I'm not sure I've heard
the same in free culture communities).  Most notably, I've heard this
said by <a href="http://ebb.org/bkuhn/">Bradley Kuhn</a>, for example in <a href="http://ebb.org/bkuhn/blog/2010/07/07/producing-nothing.html#footnote-entertainment-proprietary-software">this blogpost</a>:
</p>

<blockquote>
You might be wondering, "Ok, so if it's pure entertainment software,
is it acceptable for it to be proprietary?". I have often said: if
all published and deployed software in the world were guaranteed
Free Software except for video games, I wouldn't work on the cause
of software freedom anymore. Ultimately, I am not particularly
concerned about the control structures in our culture that exist for
pure entertainment. [...]
</blockquote>
<p>
Bradley is someone I couldn't admire more for his devotion to free
software, so don't misinterpret this statement; if anything the fact
that I agree so much in general with Bradley is why this exception
bothers me so greatly.  But it does bother me: I think games <i>are</i>
important for cultural and software freedom issues, and I feel that
ignoring them is something we do in the movement at our own risk.  (By
the way, Bradley has <a href="http://identi.ca/conversation/92162782#notice-92533746">asked me</a> to further explain my position on why
free software games matter, so that's partly why I'm writing
this... I'm not just picking on him.)
</p>

<p>
There are several reasons for this, but the first and foremost of
these are that where games go, the rest of technology follows.  I mean
this both in the sense that games are an indicator (of both the
exciting opportunities and dangers of) where technology will go.
</p>

<p>
Here's an example: DRM
(<a href="http://www.defectivebydesign.org/what_is_drm">Digital
Restrictions Management</a>) is an issue of great concern for both
free software and free culture people alike.  In the late 1980s and
early 1990s, if you bought a proprietary game for your MS-DOS running
PC, Commodore 64 computer, or et cetera, you may remember the rise of
copy protection software coming with games.  Many of these early copy
protection methods were even fairly silly: many games would have a
screen which would ask you to ask you to enter a word from a page,
paragraph, and word number within that paragraph from the instruction
manual before it would start the game.  The phenomenon
of <a href="http://en.wikipedia.org/wiki/Demoscene">demoscene</a>
culture, including a large amount of beautiful artwork and music, came
largely out of breaking early forms of DRM copy protection... for
all sorts of software of course, but most especially games.
</p>

<p>
Even now, we see <a href="http://www.engadget.com/2012/04/25/steam-for-linux/">DRM is coming to GNU/Linux operating systems through Steam</a>, a games distribution platform (and disturbingly enough for many
free software operating system users who worry about DRM, much of the
reaction is celebration).  And the rise of the "app store" model came
with the rise of mobile computing as game platforms.  (I realize that
in this post I don't have any hard evidence associating the rise of
app stores or DRM with games, but observationally at least I've found
this to be true, and it appears that
<a href="http://www.informationweek.com/news/personal-tech/mobile-apps/232300190">games
make up the largest category of "app store" downloads</a>.)  I think
we will see these trends continue to get worse, and games will
continue to lead the way.
</p>

<p>
Not all "indicators of the future" are necessarily foretelling of
things that are bad.  One of the smartest things I think Mozilla ever
invested money and time into was <a href="http://browserquest.mozilla.org/">Browser Quest</a> (which was released
shortly after Liberated Pixel Cup was announced with a very similar
style... we didn't know about it, but welcomed its release).  Browser
Quest was a great example that hey, this HTML5 stuff is actually
<i>happening</i>, and here's a tangible thing you can see to prove that
(not to mention it put Mozilla at the forefront of many minds as an
innovator in that space).
</p>

<p>
Aside from being an indicator of the future, people <i>want</i> games.
I spent a good portion of the 2000s surviving off of a sparse diet of
kobo-deluxe, tuxracer, supertux, and nethack.  This managed to be
enough for me (well, kind of... okay, not really), but it isn't
enough for everyone.  There's another bit to this: sure, you don't
actually need games to have a working system.  But "you also don't
really need to live to live" either: you could go through life with
the most minimal forms of food, clothing, shelter but absolutely no
culture, and you'd still be living in a literal sense... but it
would be a fairly miserable life.  Likewise, people want
entertainment, and video games are the most computer-centric of all
forms of entertainment on a computer.  If we don't provide them,
people will move elsewhere.  So I'd actually argue that an operating
system that does not provide games is actually an incomplete system.
(Actually, I'm not the only one who thinks this;
RMS <a href="http://www.gnu.org/gnu/linux-and-gnu.html">wrote in an
essay</a> that "a complete system needs games too".)
</p>

<p>
There's one more major reason why free software/culture games matter,
and it's definitely a major point of thinking behind the Liberated
Pixel Cup: games are a great motivation to get people to start hacking
and authoring things.  Almost every hacker my age that I know cites
video games as a source of inspiration to get into programming.
(Speaking personally, the first major programming I ever did was
extending a [proprietary!] game.  It's fair enough to say that I
wouldn't be a programmer today if it weren't for an interest in game
programming, and that is true of several of my friends as well.)  But
if that is true, why then do we have so few finished and polished free
software games?  Answering that question actually deserves of a post
of its own (and indeed, solving that riddle is a good portion of the
motive behind Liberated Pixel Cup), but it's enough to say for now
that we are missing opportunities of encouraging future hackers by not
making free software a welcoming playground for game development.
</p>

<p>
So, games are significant for a couple of reasons: they point to the
general future direction of technology, good or bad, so we should pay
attention to them.  Furthermore we should make sure we are providing
and building games, if for no other reason than to make the future we
want viable (I still think that a system that doesn't address games,
as I've outlined above, is an incomplete system, and one that most
people ultimately will not use... or, you know, we could just sit
aside and let the games continue to push the DRM'ed app store model
along and watch our digital freedoms erode).  But if none of the above
reasons were insufficient, games are something people get excited
about building.  And helping people get excited about hacking and making
things should be reason enough!
</p>

<p>
<i>
Still no comments working on my blog; but feel free to
<a href="http://identi.ca/conversation/94427295">discuss on identi.ca</a>
(or ostatus federated equivalent).
</i>
</p>
