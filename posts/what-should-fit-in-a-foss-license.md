title: What should fit in a FOSS license?
date: 2020-03-09 17:07
author: Christine Lemmer-Webber
tags: foss, licensing
slug: what-should-fit-in-a-foss-license
---

*Originally sent in [an email to the OSI license-discuss mailing list](https://lists.opensource.org/pipermail/license-discuss_lists.opensource.org/2020-March/021466.html).*

What terms belong in a free and open source software license?  There
has been a lot of debate about this lately, especially as many of us
are interested in expanding the role we see that we play in terms of
user freedom issues.  I am amongst those people that believe that FOSS
is a movement thats importance is best understood not on its own, but
on the effects that it (or the lack of it) has on society.  The
purpose of all this work is to advance user freedom and human rights
with respect to computing.

I also believe there are other critical issues that FOSS has a role to
play in: diversity issues (both within our own movement and empowering
people in their everyday lives) are one, environmental issues (the
intersection of our movement with the right-to-repair movement is a good
example) are another.  I also agree that the trend towards "cloud
computing" companies which can more or less entrap users in their
services is a major concern, as are privacy concerns.

Given all the above, what should we do?  What kinds of terms belong in
FOSS licenses, especially given all our goals above?

First, I would like to say that I think that many people in the FOSS
world, for good reason, spend a lot of time thinking about licenses.
This is good, and impressive; few other communities have as much legal
literacy distributed even amongst their non-lawyer population as ours.
And there's no doubt that FOSS licenses play a critical role... let's
acknowledge from the outset that a conventionally proprietary license
has a damning effect on the agency of users.

However, I also believe that user freedom can only be achieved via a
multi-layered approach.  We cannot provide privacy by merely adding
privacy-requirements terms to a license, for instance; encryption is key
to our success.  I am also a supporter of code of conducts and believe
they are important/effective (I know not everyone does; I don't care for
this to be a CoC debate, thanks), but I believe that they've also been
very effective and successful checked in as CODE-OF-CONDUCT.txt
*alongside* the traditional COPYING.txt/LICENSE.txt.  This is a good
example of a multi-layered approach working, in my view.

So acknowledging that, which problems should we try to solve at which
layers?  Or, more importantly, which problems should we try to solve in
FOSS licenses?

Here is my answer: the role of FOSS licenses is to undo the damage that
copyright, patents, and related intellectual-restriction laws have done
when applied to software.  That is what should be in the scope of our
*licenses*.  There are other problems we need to solve too if we truly
care about user freedom and human rights, but for those we will need to
take a multi-layered approach.

To understand why this is, let's rewind time.  What is the "original
sin" that lead to the rise proprietary software, and thus the need to
distinguish FOSS as a separate concept and entity?  In my view, it's the
decision to make software copyrightable... and then, adding similar
"state-enforced intellectual restrictions" categories, such as patents
or anti-jailbreaking or anti-reverse-engineering laws.

It has been traditional FOSS philosophy to emphasize these as entirely
different systems, though I think Van Lindberg put it well:

  Even from these brief descriptions, it should be obvious that the term
  "intellectual property" encompasses a number of divergent and even
  contradictory bodies of law.  [...] intellectual property isn't really
  analagous to just one program.  Rather, it is more like four (or more)
  programs all possibly acting concurrently on the same source
  materials.  The various IP "programs" all work differently and lead to
  different conclusions.  It is more accurate, in fact, to speak of
  "copyright law" or "patent law" rather than a single overarching "IP
  law."  It is only slightly tongue in cheek to say that there is an
  intellectual property "office suite" running on the "operating system"
  of US law.
    -- Van Lindberg, Intellectual Property and Open Source (p.5)

So then, as unfortunate as the term "intellectual property" may be, we
do have a suite of state-enforced intellectual restriction tools.  They
now apply to software... but as a thought experiment, if we could rewind
time and choose between a timeline where such laws did not apply to
software vs a time where they did, which would have a better effect on
user freedom?  Which one would most advance FOSS goals?

To ask the question is to know the answer.  But of course, we cannot
reverse time, so the purpose of this thought experiment is to indicate
the role of FOSS licenses: to use our own powers granted under the scope
of those licenses to undo their damage.

Perhaps you'll already agree with this, but you might say, "Well, but we
have all these other problems we need to solve too though... since
software is so important in our society today, trying to solve these
other problems inside of our licenses, even if they aren't about
reversing the power of the intellectual-restriction-office-suite, may be
effective!"

The first objection to that would be, "well, but it does appear that it
makes us addicted in a way to that very suite of laws we are trying to
undo the damage of."  But maybe you could shrug that off... these issues
are too important!  And I agree the issues are important, but again, I
am arguing a multi-layered approach.

To better illustrate, let me propose a license.  I actually considered
drafting this into real license text and trying to push it all the way
through the license-review process.  I thought that doing so would be an
interesting exercise for everyone.  Maybe I still should.  But for now,
let me give you the scope of the idea.  Ready?

"The Disposable Plastic Prevention Public License".  This is a real
issue I care about, a lot!  I am very afraid that there is a dramatic
chance that life on earth will be choked out within the next number of
decades by just how much non-degradeable disposable plastic we are
churning out.  Thus it seems entirely appropriate to put it in a
license, correct?  Here are some ideas for terms:

 - You cannot use this license if you are responsible for a significant
   production of disposable plastics.

 - You must make a commitment to reduction in your use of disposable
   plastics.  This includes a commitment to reductions set out by (a UN
   committee?  Haven't checked, I bet someone has done the research and
   set target goals).

 - If you, or a partner organization, are found to be lobbying against
   laws to eliminate disposable plastics, your grant of this license is
   terminated.

What do you think?  Should I submit it to license-review?  Maybe I
should.  Or, if someone else wants to sumbit it, I'll enthusiastically
help you draft the text... I do think the discussion would be
illuminating!

Personally though, I'll admit that something seems wrong about this, and
it isn't the issue... the issue is one I actually care about *a lot*,
one that keeps me up at night.  Does it belong in a license?  I don't
think that it does.  This both tries to both fix problems via the same
structures that we are trying to undo problems with and introduces
license compatibility headaches.  It's trying to fight an important
issue on the wrong layer.

It is a FOSS *issue* though, in an intersectional sense!  And there
are major things we can do about it.  We can support the fight of the
right-to-repair movements (which, as it turns out, is a movement also
hampered by these intellectual restriction laws).  We can try to design
our software in such a way that it can run on older hardware and keep it
useful.  We can support projects like the MNT Reform, which aims to
build a completely user-repairable laptop, and thus push back against
planned obsolescence.  There are things we can, and *must*, do that are
not in the license itself.

I am not saying that the only kind of thing that can happen in a FOSS
license is to simply waive all rights.  Indeed I see copyleft as a valid
way to turn the weapons of the system against itself in many cases (and
there are a lot of cases, especially when I am trying to push standards
and concepts, where I believe a more lax/permissive approach is better).
Of course, it is possible to get addicted to those things too: if we
could go back in our time machine and prevent these intellectual
restrictions laws from taking place, source requirements in copyleft
licenses wouldn't be enforceable.  While I see source requirements as a
valid way to turn the teeth of the system against itself, in that
hypothetical future, would I be so addicted to them that I'd prefer that
software copyright continue just so I could keep them?  No, that seems
silly.  But we also aren't in that universe, and are unlikely to enter
that universe anytime soon, so I think this is an acceptable reversal of
the mechanisms of destructive state-run intellectual restriction machine
against itself for now.  But it also indicates maybe a kind of maxima.

But it's easy to get fixated on those kinds of things.  How clever can
we be in our licenses?  And I'd argue: minimally clever.  Because we
have a lot of other fights to make.

In my view, I see a lot of needs in this world, and the FOSS world has a
lot of work to do... and not just in licensing, on many layers.
Encryption for privacy, diversity initiatives like Outreachy, code of
conducts, software that runs over peer to peer networks rather than in
the traditional client-server model, repairable and maintainable
hardware, thought in terms of the environmental impact of our
work... all of these things are critical things in my view.

But FOSS licenses need not, and should not, try to take on all of them.
FOSS licenses should do the thing they are appropriate to do: to pave a
path for collaboration and to undo the damage of the "intellectual
restriction office suite".  As for the other things, we must do them
too... our work will not be done, meaningful, or sufficient if we do not
take them on.  But we should do them hand-in-hand, as a multi-layered
approach.
