title: OpenLieroX
date: 2008-07-19 17:07
author: Christine Lemmer-Webber
slug: openlierox
---
From eighth grade all the way through high school, there was a single,
late-dos era two-player game that all my friends and I knew and loved.
Upon many occasions that we got together, we would play it. I'm talking
about [Liero](http://en.wikipedia.org/wiki/Liero), that fantastic game
that's somewhere between a two dimensional game of
[Quake](http://en.wikipedia.org/wiki/Quake) or a real-time
[Worms](http://en.wikipedia.org/wiki/Worms_(series)). It was great,
though a little buggy, but *damn* did the physics felt nice (in a
wonderfully unrealistic way), and the ninja rope was a freaking blast to
use.

But it was closed source, and apparently the author lost the source code
due a disk crash, and for years we were stuck with playing the same
tired old binary. Then Windows XP came out, and sound stopped working
entirely. And eventually it moved from a game we played to a game we
reminisced.

Over the years I've checked on the number of clones which have come
about, but none of them really had the same feel as the original Liero.
But now that's changed, for a new free and open source Liero clone is
out, called [OpenLieroX](http://openlierox.sourceforge.net/), based off
the [Liero Extreme](http://iworx5.webxtra.net/~lxallian/LXRS/) codebase.
And it's awesome. It supports the original Liero weaponpack, as far as I
can tell, flawleslly, and has a vast wealth of mods and levelpacks
available for download. On top of that, it has network play... which
isn't perfect, and I often find myself running into my own bullets in
ways that would never happen offline, but it's still pretty good.

Installing it on Linux actually isn't that hard. There's a .deb file
available on the [sourceforge download
page](http://sourceforge.net/project/showfiles.php?group_id=180059&package_id=208133&release_id=584470),
if you happen to be running Debian or Ubuntu, as well as windows builds.
But I'd recommend running from subversion, which currently has auto
level and mod download support (the latest stable releases don't). To
compile, run these commands:

    svn co https://openlierox.svn.sourceforge.net/svnroot/openlierox openlierox
    sudo apt-get install libsdl1.2-dev libsdl-mixer1.2-dev libsdl-image1.2-dev libgd2-noxpm-dev zlib1g-dev libxml2-dev
    ./compile.sh

...then you can run either 'sudo ./install.sh' './start.sh', depending
on whether you want to install it or just run it locally without
installing.

I'm actually interested in playing a few games online with people I
know... maybe even having a small local openlierox lan party. Anyone
interested?
