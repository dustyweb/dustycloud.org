title: On Hackers and Depression
date: 2013-01-16 19:10
author: Christine Lemmer-Webber
slug: on-hackers-and-depression
---
Depression is a background but ever-present part of my life. Most people
who know me closely know this, but probably most people who know me for
the type of work and projects I do do not. I'm not against people
knowing, but it isn't something I really talk about because it seems
like something that's mostly personal and thus isn't really something
that there's reason to talk about.

But recently that's changed; by now, weirdly almost everyone on the
internet knows about [Aaron Swartz's
suicide](http://en.wikipedia.org/wiki/Aaron_Swartz#Death_and_aftermath).
I say weirdly because... well I never knew Aaron personally, we never
spoke, and he was not my friend. But there was a large overlap in our
lives in more ways than I can probably count (affiliation with Creative
Commons, various kinds of informational freedom activism, programming
language preferences, et cetera), and while I did not know him, many of
my friends did. News of Aaron's death did affect me personally, partly
for empathy for friends, partly for sadness at losing someone who's kind
of a kindred spirit, partly because I [lost one of my closest friends to
suicide](http://dustycloud.org/blog/in-memoriam-matt-despears.html) over
a year ago, and partly because my own depression is a regular background
issue for me. Aaron Swartz's death was not quite like losing a friend to
me because he wasn't a friend, but for the reasons above and many others
not listed, it did stir up a strong emotional reaction in me. But there
has been enough written about Aaron Swartz, his suicide, and a whole
crock of issues that have been stirred about it by a good very many
people who are far more qualified than I am to write about such things.
So I assumed I had nothing to say on the subject that was worth reading.
And strictly on the subject of Aaron Swartz, I really don't.

But there was a well written post by Evan Prodromou titled ["On hackers
and
suicide"](http://evanprodromou.name/2013/01/14/on-hackers-and-suicide/)
which echoed a lot of my own thoughts recently, and he seemed to be
[calling for others to join the
conversation](http://identi.ca/conversation/98042181#notice-99041263),
and I feel like I do have quite a bit of thoughts on it, so I should
maybe write them down while the moment seems clear.

The first thing I guess to be said has already been said: I have
depression, it is a part of my life, and probably will be for the rest
of my life. I have been depressed for a long time, since my preteen or
early teen years at least. I remember strongly the first times I started
thinking about how I might be able to kill myself with a dirty knife on
the kitchen counter or a (unlikely to work) strategy of sticking my head
under a running faucet and holding it there until I drowned. I haven't
had a serious suicide attempt but suicide is something I think of often,
almost or maybe even daily. There was only once where I nearly did it...
I had been fighting to keep my small college Barat open from being
closed by the larger university DePaul which had bought it, had been
engaged in a long campaign to keep it open, it closed, and I felt like
my world was simply over, there was nothing left for me to do. I went
out into the woods to slit my wrists with a pocketknife. When I got into
the woods I realized I didn't have the knife on me anyways so I just
walked back and felt mostly tired and defeated for awhile. And so I'm
still alive to this day, I suppose, due to my absent-mindedness. But
aside from that time, depression wavers between being mostly something I
know is there but isn't very present to being something in the back of
my mind to something tearing at and lacerating my thoughts.

I really don't see how this is something that will change. I remember
being in college and thinking that if I could somehow carve out a life
for myself where I was doing important work that I believed in, if I was
doing things I wanted to do, I wouldn't have reason to be depressed. And
I had a hard time understanding why people who I admired so greatly, who
did so much, could really continue to be depressed. It didn't seem
logical. But here I am... I think I have actually carved out as much of
a "I am doing important, ethical work that I believe in and that I enjoy
as is actually possible at all ever" situation as I will ever be able
to... and depression, somehow, has not gone away.

Why? To look at it logically, it doesn't make sense. It's very
troubling: I have surrounded myself with a lot of people I really admire
who are doing great work, things that I think are *important
world-changing things*, people I couldn't possibly put an ounce more
admiration into, and somehow the majority of them seem to have
depression also. Not only that, but there's a way higher correlation
between the number of people who seem to be doing good things and who I
care about and the number of people who have deep, serious depression
issues. And most of my community *is* the free software community. Why
do hackers (and I mean that more broadly than programmers, include any
participant in these kinds of spaces) seem to have so much trouble with
depression?

It's hard to know without being highly self-reflective (and indeed, I've
already been more self-reflective in this post than maybe you ever
wished to know), so I'm not going to shy away from being
self-reflective. Yes, I'm projecting my own life onto you. But I think
there are some reasons why I and and other people like me seem to
struggle regularly from depression. And here are some of them.

First of all, a lot of hackers and smart people generally I think tend
to have had troubled childhoods. There's a nature versus nurture type
question that's really not easy to split apart at all that's one of
those "do nerds have socially difficult lives because they're nerds or
are they nerds because they have socially difficult lives?" I think the
answer is probably that it's a mutually compounding thing, but there's a
certain personality type that's already very smart but which is having a
difficult life that draws them to certain types of intellectual
activities as escapism. I didn't have many friends as a kid, I was
picked on in school, and that's all very standard narrative for nerds.
Sometimes when I read about other nerds, I hear about them having an
easy time in school academically, but that wasn't true for me because I
had such a strong case of ADD; I nearly failed out of the first two
years of high school due to a combination of difficulty keeping my
attention under control and because I had absolutely no friends in
school (I had friends outside of school, and yes, of course they were
nerds), and probably the only thing that saved me was being transferred
to a wonderful alternative school called Kradwell where I learned to
keep my ADD under control and nobody made fun of you for being a freak
because everyone was a freak. And I had some complicated family issues
and many other things. And computers were an escape. Computers don't
judge you, they don't make fun of you, they don't pressure you for
taking too long to figure out a problem, they're just there and they
have a world full of interesting problems that you can solve, and if you
take the time, you can become good at solving them. (I don't believe I
am a "born hacker" in the sense that I have naturally good computer
skills... I am just very stubborn and have basically forced my way
through with stubborn interest.)

And the more people I talk to, the more that I find that many other
people have been like me. Not everyone of course, and this may be
changing for the better: increased outreach efforts are reaching groups
of people who might not fit all the "hacker prototypes", and that's a
good thing. But for a lot of people I know, this is a common pattern. So
many hackers are depressed before they are ever hackers, and they don't
become hackers because they're depressed, but there's patterns that mean
that this is a common thing.

Compounding this is that people who work on free software issues tend to
be working on it for ethical reasons. And working on something for
ethical reasons is, I think, one of the most important things that you
can do with your life. The world does not move forward if we don't have
people working on things for ethical reasons. But this can also be
emotionally wearing: you're working on these things because you care
deeply about them. Becoming personally invested in something, believing
in something, takes a lot of emotional resources. It can be very
rewarding when things are going well. But you're also up against a lot,
and that can also be very wearing. Not to mention that there's a lot of
guilt... you will probably not do things 100% right 100% of the time. I
remember telling someone when I told someone that I was using a
proprietary driver on one of my computers (otherwise, 3d acceleration
would not work, and I am a 3d artist): "I do have this proprietary
driver installed, but at least I have the dignity to feel terrible about
it." (Yes, I do try to get around this also by running a completely free
system to the fullest extent I can... though that can be wearing in its
own way to try to do.)

This gets even harder if you're not just a user, but a hacker who is
trying to build new, challenging things. When you do build things, when
they're going well, there's nothing like it. But things can go badly
too, and it's very easy to take things personally. I remember when I
heard about [the suicide of Ilya, one of Diaspora's
founders](http://blog.diasporafoundation.org/2011/11/16/ilya-zhitormirskiy-1989-2011.html),
that frightened me. If I really did leave to do MediaGoblin fulltime
(which I now have), what would I do to myself if things did not succeed?
(Or even they may not be failing... I don't think Diaspora was failing,
but it's easy to feel like they are when you're building something you
care about and suffering from depression simultaneously.) Would I be
able to handle the feelings of failure emotionally? Worrying that I
could not was something that I struggled with when deciding whether this
was something I should do (ultimately, I decided it was too important to
not do it, not to mention the depression of not doing it would be even
worse). But it's still something I've thought and worried about.

And it's very easy to take things people say personally. A number of
years ago I used to join in the chorus of "X project sucks! This sucks!"
and generally snarking on a lot of things that I thought sucked. My
perspective on that has changed. For one thing, as a teenager on
slashdot, making comments about a project "sucking" didn't seem like it
could be hurting anyone partly because I thought that if I was tossing
insults around, I was tossing insults at giants, and such words would
just bounce off them like pebbles on their impossibly thick skin. The
irony of this is that I thought that these people were like powerful,
immortal coders, and thus impervious to any damage I could toss around,
so why not vent a little? But now I run a project myself, and I know
many of these developers who at one point seemed like mythical figures
walking high above my head, and I know the truth: they aren't magical.
They were never magical. They are just people. And that means they're
just as vulnerable too. And those words can hurt.

Furthermore, snark is fun and easy way to look awesome. It's hard to
build things, but it's easy to be an armchair pundit, throwing insults
around. And it's not just the armchair pundits either; there's some kind
of disturbing trend where plenty of people celebrate the past-time of
attacking each other, and this is even encouraged by some of our most
high profile community members. Every couple of weeks Linus Torvalds
switches desktops and thrashes the other desktop system and everyone
throws a party, except for the people who are currently working on that
project. I don't know what they do; if I was working on their project,
I'd probably go lie down on my bed for a few hours with the lights off
and not move for a while.

Early on in announcing MediaGoblin, a friend of mine told me that it was
mentioned on a podcast, and that the comments on this podcast said that
it'd never work because the name was stupid and it was a GNU project and
GNU projects never happen, or some sort of thing like that. My friend
said I should really listen to it so I could hear what they were saying.
Well, I was never able to listen to the podcast. I probably will some
day, but I am afraid to open the file. When my friend told me that, I
felt so terrible that I wondered whether or not I should even bother
working on anything anymore, especially not MediaGoblin. I'll tell you
an embarrassing truth: someone made fun of my project name and I cried
about it. Isn't that the dumbest thing you've ever heard? It's like the
kind of thing some dumb, dorky kid would do that everyone would make fun
of them for on the playground. Except oh wait, I did it, and I'm an
adult. (And I guess I was that dumb, dorky kid on the playground
anyway.)

But what am I saying? We should never criticize each others' projects?
We should just be really fucking, eggshell-steppingly nice all the time?
Well, that kind of level of niceness sounds kind of exhausting. And of
course, criticism isn't just good, it's critical. We need it to improve
things. But I guess, just realize that it's real human beings who are
running those projects. They're probably more vulnerable than you know.
Would you say the same things you're saying about this project to the
face of someone you know? Sometimes we say things on the internet that
we would never say in real life. That sometimes makes it a bit easier to
shake things off... but not always, and often not really. The person
who's reading your comments might be the person who runs that project,
and they probably run it because they care and believe in it. And they
have feelings.

At PyCon a few years ago my friend Ian Bicking gave a wonderful and
whimsical talk called simply [Topics of
Interest](http://blip.tv/pycon-us-videos-2009-2010-2011/topics-of-interest-ian-bicking-1941789)
(or maybe it was his [DjangoCon
keynote](http://pyvideo.org/video/22/djangocon-2009--a-socio-political-analysis-of-ope),
I don't really remember, it was at one of those talks; I'm intentionally
not watching so I can rephrase it in the way that it struck me, even if
that's wrong). At the time, everyone seemed to be bitching about the
state of Python's packaging, and picking on the people who were working
on it (I am guilty of this). I remember something that Ian said really
hit me, which was something along the lines of: "When people work on
hard problems, that's really difficult to do right, and it's easy to
pick on those people for doing things wrong. But if you make fun of
people who work on hard problems, then they go away. And then nobody
works on those problems." I was really struck by that.

It's also true that in the optimal world, where everything is going
well, chronic depression doesn't just go away anyway. It's always there,
though a life that *is* going well is one where dealing depression is
much easier. Even when it doesn't make sense though, even when you've
carved out the ideal world for yourself, once you've burned the paths in
your mind where depression and suicide can become their own escapes,
it's very easy to fall back on them. Sometimes little things can trigger
them. Sometimes a general buildup of anxiety. Sometimes it's hard to
know why. But it's easy to fall back on those paths. It's hard not to.

That said, having chronic depression as something that doesn't go away
isn't the same as "well, you have depression, it doesn't go away, so
there's not really anything to do." I think people really are affected
by what's happening in their life. I think that Aaron Swartz's suicide
wasn't just "he had depression, so ultimately he'd eventually take his
life anyway". If I had been [bullied to the extent that Aaron
had](http://questioncopyright.org/remembering_aaron_swartz) I think that
I would not have been able to take it either. That could drive anyone to
depression... and even worse, anyone with depression, I think it would
throw them over the edge. So it's not just a simple thing of "it's
depression's fault" or "it's the situation's fault". Depression might
not go away, but there are things we can do about it.

But what? I've talked a lot about my own depression here. I hope nobody
misreads this as a "feel bad for me" post. I don't want you to feel bad
for me, I'm actually doing pretty well right now. But now you know a bit
more also. And I've talked about depression, or I've tried to, in the
sense of causes. As for solutions, or even action items... that's a
much, much harder thing to talk about. I don't know if there are
solutions; that sounds too much like a problem that can be wrapped up
cleanly. There are things that we can do, I think, and but it's a lot
harder to identify them. But here's a short list of things that I think
are actionable:

> -   **Be welcoming of people.** If there's someone trying to get
>     involved in a project, that's an intimidating thing. If you're in
>     some sort of position of leadership, remember that it's up to you
>     to set the stage for a welcoming, friendly community. Support
>     people who are being kind, try to help along people who are new,
>     and try to help people who are acting inappropriately to improve
>     the way they communicate to be considerate.
> -   **Don't participate in a culture of bullying.** It's okay, even
>     good, to share criticisms. But try your best to be constructive.
>     It's easy and fun to be a snarky douchebag, but remember, there's
>     someone on the other end of the internet, and they have a face,
>     and they have feelings. Be nice to them. Be considerate. Give
>     criticism, but give it constructively. And don't give bullying or
>     antagonism your support, either.
> -   **Remember that activism is important, but hard and emotionally
>     draining.** We absolutely, positively need people to act
>     ethically, even [when the ethical solution isn't the "better"
>     solution](http://mako.cc/writing/hill-when_free_software_isnt_better.html).
>     But remember also that when we're asking people to try to stand up
>     for what's right, that often means going against the grain of
>     society, and that can be very wearing and require a lot of
>     patience. So be patient to people also.
> -   **Be supportive of each other, and seek support when you need
>     it.** Remember: if you're depressed, you aren't alone. There are
>     plenty of hackers out there who are depressed, just like you. And
>     that means don't be afraid to find other people who are like you
>     can be a support structure, and help other people when they need
>     their support. A lot of what is really needed for people who are
>     depressed is to have a friend to talk to about their problems. But
>     of course, there's also only so much that people can do as
>     friends: sometimes professional help is really needed. (And as
>     Evan said in his [forementioned
>     post](http://evanprodromou.name/2013/01/14/on-hackers-and-suicide/),
>     making suicide prevention resources widely available is I think an
>     important step in helping reduce risk.)
> -   **Remember that you're doing something important, and feel good
>     about that.** One problem I've discovered about myself is that I
>     tend to not evaluate myself based on the things I do, but on the
>     things I don't do. I think that's a common pattern, and both a
>     source of drive for people to do better, but also the source of a
>     lot of burnout. Remember that you're trying to do good, and feel
>     good about what you've done and are doing. It's okay to feel good
>     about yourself and what you do! And that means something else too:
>     don't give up on yourself. The world isn't better off without you
>     in it. It wasn't better off without Aaron in it either. If you
>     need help, get help. The world is hard, and that means partly that
>     it needs people in it to do important things to make it better. Do
>     good, and value yourself for it.

Beyond that, I don't know. It's a hard situation to figure out how to
improve, but I think it's important that we recognize and talk about
this. I'm certainly interested in what others have to say.
