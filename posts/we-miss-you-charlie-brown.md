title: We Miss You, Charlie Brown
date: 2018-06-28 11:50
author: Christine Lemmer-Webber
tags: matt despears, suicide, peanuts
slug: we-miss-you-charlie-brown
---
Morgan was knocking on the bathroom door. She wanted to know why I was
crying when I was meant to be showering.

I was crying because my brain had played a cruel trick on me last night.
It conjured a dream in which all the characters from the comic strip
"Peanuts" represented myself and friends. Charlie Brown, the familiar
but awkward everyman of the series, was absent from every scene, and in
that way, heavily present.

I knew that Charlie Brown was absent because Charlie Brown had committed
suicide.

I knew that Charlie Brown was my friend [Matt
Despears](https://dustycloud.org/blog/in-memoriam-matt-despears/).

The familiar Peanuts imagery passed by: Linus (who was me), sat at the
wall, but with nobody to talk to. Lucy held out the football, but nobody
was there to kick it. Snoopy sat at his doghouse with an empty bowl, and
nobody was there to greet him. And so on.

Then the characters, in silence, moved on with their lives, and the
title scrolled by the screen... "We Miss You, Charlie Brown".

And so, that morning, I found myself in the shower, crying.

Why Peanuts? I don't know. I wouldn't describe myself as an overly
energetic fan of the series. I also don't give too much credit for dream
imagery as being necessarily important, since I think much tends to be
the byproduct of the cleanup processes of the brain. But it hit home
hard, probably because the imagery is so very familiar and repetitive,
and so the absence of a key component amongst that familiarity stands
out strongly. And maybe Charlie Brown just a good fit for Matt: awkward
but loveable.

It has now been over six years since Matt has passed, and I find myself
thinking of him often, usually when I have the urge to check in with him
and remember that he isn't there. Before this recent move I was going
through old drives and CDs and cleaning out and wiping out old junk, and
found an archive of old chat logs from when I was a teenager. I found
myself reliving old conversations, and most of it was utter trash... I
felt embarrassed with my past self and nearly deleted the entire
archive. But then I went through and read those chat logs with Matt. I
can't say they were of any higher quality... my conversations with Matt
seemed even more absurd on average than the rest. But I kept the chat
logs. I didn't want to lose that history.

I felt compelled to write this up, and I don't entirely know why. I also
nearly didn't write this up, because I think maybe this kind of writing
can be dangerous. That may sound absurd, but I can speak from my
experience of someone who frequently experiences suicidal ideation that
the phrase "would anyone really miss me when I'm gone" comes to mind,
and maybe this reinforces that.

I do think that society tends to romanticize depression and suicide in
some strange ways, particularly this belief that suffering makes art
greater. A friend of mine pointed this out to me for the first time in
reference to John Toole's "A Confederacy of Dunces", often advertised
and sold to others by, "and the author committed suicide before it was
ever published!" But it would have been better to have more books by
John Toole instead.

So as for "will anyone miss me if I'm gone", I want to answer that
without romanticizing it. The answer is just "Yes, but it would be
better if you were here."

A group of friends and I got together to play a board game recently. We
sat around the table and had a good time. I drew a picture of "Batpope",
one of Matt's favorite old jokes, and we left it on an empty spot at the
table for Matt. But we would have rathered that Matt was there. His
absence was felt. And that's usually how it is... like in the dream, we
pass through the scenes of our lives, and we carry on, but there's a
missing space, and one can feel the shape. There's no romance to that...
just absence and memories.

We miss you, Matt Despears.
