title: Activipy v0.1 released!
date: 2015-11-03 10:00
author: Christine Lemmer-Webber
tags: activipy, foss, federation
slug: activipy-v0.1
---
Hello all! I'm excited to announce [v0.1 of
Activipy](http://activipy.readthedocs.org/en/v0.1/). This is a new
library targeting [ActivityStreams
2.0](http://www.w3.org/TR/activitystreams-core/).

If you're interested in building and expressing the information of a web
application which contains social networking features, Activipy may be a
great place to start.

Some things I think are interesting about Activipy:

:   -   It wraps ActivityStreams documents in pythonic style objects
    -   Has a nice and extensible method dispatch system that even works
        well with ActivityStreams/json-ld's composite types.
    -   It has an "Environment" feature: different applications might
        need to represent different vocabularies or extensions, and also
        might need to hook up entirely different sets of objects.
    -   It hits a good middle ground in keeping things simple, until you
        need complexity. Everything's "just json", until you need to get
        into extension-land, in which case json-ld features are
        introduced. (Under the hood, that's always been there, but users
        don't necessarily need to understand json-ld to work with it.)
    -   Good docs! I think! Or I worked really hard on them, at least!

As you may have guessed, this has a lot to do with our work on
federation and the [Social Working Group](http://www.w3.org/Social/WG).
I intend to build some interesting things on top of this myself.

In the meanwhile, I spent a lot of time on the docs, so I hope you find
[reading them](http://activipy.readthedocs.org/) to be enjoyable, and
maybe you can build something neat with it? If you do, I'd love to hear
about it!
