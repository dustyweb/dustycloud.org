title: Racket is an acceptable Python
date: 2019-07-09 09:27
author: Christine Lemmer-Webber
tags: racket, lisp, python, foss
slug: racket-is-an-acceptable-python
---
A little over a decade ago, there were some popular blogposts about
whether [Ruby was an acceptable
Lisp](http://www.randomhacks.net/2005/12/03/why-ruby-is-an-acceptable-lisp/)
or whether even [Lisp was an acceptable
Lisp](https://steve-yegge.blogspot.com/2006/04/lisp-is-not-acceptable-lisp.html).
Peter Norvig was also writing at the time [introducing Python to Lisp
programmers](https://norvig.com/python-lisp.html). Lisp, those in the
know knew, was the right thing to strive for, and yet seemed
unattainable for anything aimed for production since the AI Winter
shattered Lisp's popularity in the 80s/early 90s. If you can't get Lisp,
what's closest thing you can get?

This was around the time I was starting to program; I had spent some
time configuring my editor with Emacs Lisp and loved every moment I got
to do it; I read some Lisp books and longed for more. And yet when I
tried to "get things done" in the language, I just couldn't make as much
headway as I could with my preferred language for practical projects at
the time: Python.

Python was great... mostly. It was easy to read, it was easy to write,
it was easy-ish to teach to newcomers. (Python's intro material is
better than most, but [my spouse](https://mlemmer.org/) has talked
before about some major pitfalls that the Python documentation has which
make getting started unnecessarily hard. You can [hear her talk about
that](https://www.youtube.com/watch?v=pv0lLciMI24&t=4220s) at this talk
we co-presented on at last year's RacketCon.) I ran a large free
software project on a Python codebase, and it was easy to get new
contributors; the barrier to entry to becoming a programmer with Python
was low. I consider that to be a feature, and it certainly helped me
bootstrap my career.

Most importantly of all though, Python was easy to pick up and run with
because no matter what you wanted to do, either the tools came built in
or the Python ecosystem had enough of the pieces nearby that building
what you wanted was usually fairly trivial.

But Python has its limitations, and I always longed for a lisp. For a
brief time, I thought I could get there by contributing to the [Hy
project](https://hy.readthedocs.io/en/stable/), which was a lisp that
transformed itself into the Python AST. "Why write Python in a syntax
that's easy to read when you could add a bunch of parentheses to it
instead?" I would joke when I talked about it. Believe it or not though,
I do consider lisps easier to read, once you are comfortable to
understand their syntax. I certainly find them easier to write and
modify. And I longed for the metaprogramming aspects of Lisp.

Alas, Hy didn't really reach my dream. That macro expansion made
debugging a nightmare as Hy would lose track of where the line numbers
are; it wasn't until that when I really realized that without line
numbers, you're just lost in terms of debugging in Python-land. That and
Python didn't really have the right primitives; immutable datastructures
for whatever reason never became first class, meaning that functional
programming was hard, "cons" didn't really exist (actually this doesn't
matter as much as people might think), recursive programming isn't
really as possible without tail call elimination, etc etc etc.

But I missed parentheses. I longed for parentheses. I *dreamed in*
parentheses. I'm not kidding, the only dreams I've ever had in code were
in lisp, and it's happened multiple times, programs unfolding before me.
The structure of lisp makes the flow of code so clear, and there's
simply nothing like the comfort of developing in front of a lisp REPL.

Yet to choose to use a lisp seemed to mean opening myself up to eternal
yak-shaving of developing packages that were already available on the
Python Package Index or limiting my development community an elite group
of Emacs users. When I was in Python, I longed for the beauty of a Lisp;
when I was in a Lisp, I longed for the ease of Python.

All this changed when I discovered Racket:

-   Racket comes with a full-featured editor named DrRacket built-in
    that's damn nice to use. It has all the features that make lisp
    hacking comfortable previously mostly only to Emacs users:
    parenthesis balancing, comfortable REPL integration, etc etc. But if
    you want to use Emacs, you can use racket-mode. Win-win.
-   Racket has intentionally been built as an educational language, not
    unlike Python. One of the core audiences of Racket is middle
    schoolers, and it even comes with a built-in [game
    engine](https://docs.racket-lang.org/teachpack/2htdpuniverse.html)
    for kids. (The [How to Design Programs
    prologue](https://htdp.org/2019-02-24/part_prologue.html) might give
    you an introductory taste, and [Realm of
    Racket](https://www.realmofracket.com/) is a good book all about
    learning to program by building Racket games.)
-   My spouse and I even taught classes about how to learn to program
    for [humanities
    academics](https://dustycloud.org/misc/digital-humanities/) using
    Racket. We found the age-old belief that "lisp syntax is just too
    hard" is simply false; the main thing that most people lack is
    decent lisp-friendly tooling with a low barrier to entry, and
    DrRacket provides that. The only people who were afraid of the
    parentheses turned out to be people who already knew how to program.
    Those who didn't even praised the syntax for its clarity and the way
    the editor could help show you when you made a syntax error
    (DrRacket is very good at that). "Lisp is too hard to learn" is a
    lie; if middle schoolers can learn it, so can more seasoned
    programmers.
-   Racket might even be *more* batteries included than Python. At least
    all the batteries that come included are generally nicer; [Racket's
    GUI library](https://docs.racket-lang.org/gui/) is the only time
    I've ever had fun in my life writing GUI programs (and they're cross
    platform too). Constructing pictures with its
    [pict](https://docs.racket-lang.org/pict/index.html) library is a
    delight. Plotting graphs with
    [plot](https://docs.racket-lang.org/plot/index.html) is an
    incredible experience. Writing documentation with
    [Scribble](https://docs.racket-lang.org/scribble/index.html) is the
    best non-org-mode experience I've ever had, but has the advantage
    over org-mode in that your document is just inverted code. I could
    go on. And these are just some packages bundled with Racket; the
    [Package repository](https://pkgs.racket-lang.org) contains much
    more.
-   Racket's documentation is, in my experience, unparalleled. The
    [Racket Guide](https://docs.racket-lang.org/guide/index.html) walks
    you through all the key concepts, and the [Racket
    Reference](https://docs.racket-lang.org/reference/index.html) has
    everything else you need.
-   The tutorials are also wonderful; the [introductory
    tutorial](https://docs.racket-lang.org/quick/index.html) gets your
    feet wet not through composing numbers or strings but by building up
    pictures. Want to learn more? The next two tutorials show you how to
    [build web
    applications](https://docs.racket-lang.org/continue/index.html) and
    then [build your own web
    server](https://docs.racket-lang.org/more/index.html).
-   Like Python, even though Racket has its roots in education, it is
    more than ready for serious practical use. These days, when I want
    to build something and get it done quickly and efficiently, I reach
    for Racket first.

Racket is a great Lisp, but it's also an acceptable Python. Sometimes
you really can have it all.
