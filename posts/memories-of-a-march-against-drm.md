title: Memories of a march against DRM
date: 2016-03-23 09:11
author: Christine Lemmer-Webber
tags: drm, eme, rally
slug: memories-of-a-march-against-drm
---
![Above image [CC BY 3.0](http://creativecommons.org/licenses/by/3.0/),
[originally
here](https://media.libreplanet.org/u/zakkai/m/protesting-microsoft-s-push-for-drm-in-web-standards-f68e/),
and here's a [whole gallery of
images](https://media.libreplanet.org/u/zakkai/tag/no-drm-in-web-standards/).](/etc/images/blog/protesting_eme_before_microsoft.png){#Protesting EME before the Microsoft building; CC BY 3.0}

I participated in a rally against the W3C endorsing DRM last Sunday. I
know it was recorded, but I haven't seen any audio or video recordings
up yet, and some friends have asked what really happened there. I
thought I'd write up what I remembered.

First, some context: the rally (and subsequent roundtable discussion)
wasn't officially part of LibrePlanet, but it *did* happen right after
it. This was [one of the busiest free software focused weeks of my
life](https://identi.ca/cwebber/note/-FOQ-GXwTw-YaIxRXUHjcw), and just
earlier in the week I had been participating in the [Social Web Working
Group](https://www.w3.org/wiki/Socialwg) at the W3C, trying to hammer
out our work on federation and other related standards. I'm *so excited*
about this work, that it stands out in an interesting contrast to my
feelings on a different "standards in the W3C" issue: the real danger
that the W3C will endorse DRM by recommending the [Encrypted Media
Extensions specification](https://www.w3.org/TR/encrypted-media/).

Before I get to the rally itself, I want to dispel what I think has been
some intentional muddying of the issue by advocates of the
specification. Let's turn to the words of the specification itself:

> This specification does not define a content protection or Digital
> Rights Management system. Rather, it defines a common API that may be
> used to discover, select and interact with such systems as well as
> with simpler content encryption systems. Implementation of Digital
> Rights Management is not required for compliance with this
> specification: only the Clear Key system is required to be implemented
> as a common baseline.

Whew! That doesn't sound so bad does it? Except, oh wait, reading this
you might think that this isn't about DRM at all, and that's an
intentional piece of trickery by the authors of this specification. As
Danny O'Brien later said at the panel (I'm paraphrasing here): "While
it's true that the specification doesn't lay out a method for
implementing DRM, it instead lays out all the things that surround this
hole. The thing is, it's a DRM shaped hole, and indeed DRM is the only
thing that fits in that hole."

So once you look at it that way, yes, it's a DRM-enabling specification.
We have other programs and standards for handling "encryption".
Encryption is good, because it empowers users. The goal of this
specification is to make space for something to fit onto your computer
to strip you of your computing power and freedom.

With that said, onto the memories of the evening.

The march started outside MIT’s Ray and Maria Stata Center, where the
W3C offices are. There were a good number of people there, though I
didn't count them. I'm guessing it was 50 or so people, which is not bad
turnout at all for a post-busy-conference everyone-is-probably-exhausted
march. Despite anticipating being totally exhausted, I was surprised to
find that I wasn't, and neither was anyone around me. Everyone seemed
super fired up.

There were some speeches from Harry Halpin and Zak Rogoff and myself to
kick things off. I don't remember Harry or Zak's speeches at this stage,
though I remember thinking they were pretty good. (Harry made clear that
he was a W3C staff member but was acting in his own capacity.)

As for what I said, here's my rough memory:

> I started MediaGoblin from the goal and vision of preserving the
> decentralized nature of the World Wide Web in the growing area of
> media publishing, through audio, video, images, and so on. Thus I was
> proud to join the W3C in the standards work on our work formalizing
> federation through ActivityPub and by participating in the Social Web
> Working Group. But if the W3C enables EME, it enables DRM, and this
> threatens to undermine all that. If this were to apply to video only,
> this would be threat enough to oppose it. But once that floodgate
> opens, DRM will quickly apply to all types of documents distributed
> through the web, including HTML and JavaScript. The W3C's lasting
> legacy has been to build a decentralized document distribution network
> which enables user freedom. We must not allow the W3C to become an
> enemy of itself. Don't let the W3C lower its standards, oppose DRM
> infecting the web!

Anyway, something like that!

A lot of things happened, so let me move on to memory from what happened
from there in bulleted list form:

-   We marched from MIT to Microsoft. There were quite a few chants, and
    "rm DRM" was the most fun to chant, but notably probably the least
    clear to any outsiders.
-   Danny O'Brien from the EFF gave a speech in front of the Microsoft
    building giving a history of DRM and why we must oppose it. He noted
    that one of the most dangerous parts of DRM in the United States is
    that the DMCA makes explaining how DRM works a crime, thus talking
    about the issue can become very difficult.
-   After the march we went to the roundtable discussion / panel, hosted
    at the MIT Media Lab. It was a full room, with even more people than
    the march (maybe 80-100 people attending, but I'm bad at counting
    these things). Everyone ate some pizza, which was great times. Then
    Richard Stallman, Danny O'Brien, Joi Ito, and Harry Halpin all took
    turns speaking.
-   Richard Stallman started with an introduction to free software
    generally. He then went through a detailed explanation about how DRM
    makes user freedom impossible. He then said something funny like "I
    was allowed 30 minutes, but I notice I only used 15; I will use the
    other 15 minutes to follow up to others if necessary." (He used a
    good portion of them to correct people on their terminology.)
-   Danny O'Brien gave a detailed explanation of the history of the
    fight against DRM. He also gave his "EME is a standard with a DRM
    shaped hole" comment. He then gave a history of the fight of
    something he considered similar, the fight against software patents,
    and how the W3C had decided to fight patents by including rules that
    W3C members could not sue using patents for concepts covered by
    these specifications.
-   This lead into what was probably the biggest controversy among the
    panel members: a proposal by the EFF of a "covenant" surrounding
    DRM. The proposal was something like, "if the W3C *must* adopt EME,
    it should provide rules protecting the public by making members
    promise that they will *never* sue free software developers and
    security researchers for violating DRM." Richard Stallman had a
    strong response to this, saying that this is not really a compromise
    (Danny clarified that this proposal did not mean giving up fighting
    DRM) and while it could make things a little bit less dangerous, it
    would still be very dangerous. It could be easily circumvented as
    the party suing might not be a W3C member (and indeed, you could
    imagine many record companies or Hollywood studios who are not W3C
    members suing in such a scenario).
-   A W3C staff employee at one point said that if the general public
    was to comment on EME, it should be disagreeing on technical points,
    and be careful not to raise confused technical issues, as that will
    lead comments to being dismissed. Danny gave a nice response, saying
    that while he agreed that technical issues should be correctly
    engaged, that technical decisions are made within a policy context,
    so we should be careful to not tell people to limit themselves to
    technical-issue-only comments.
-   Joi Ito gave various anecdotes about his understanding of what lead
    DRM to its current rise in prominence today. He also used
    "intellectual property" several times, leading predictably to a
    terminology-correcting response from RMS.
-   One audience member suggested that if the W3C adopts EME, it shows
    that it can not be trusted with the responsibility of managing the
    web's standards. Interestingly, this seemed to be met with a good
    deal of approval from the crowd. It also was an interesting
    counter-point to the "well if the W3C doesn't do it, someone will
    just set up another standards body to support DRM." This "risk" to
    the W3C might be just as or more likely of other standards bodies
    emerging to replace it if it moves forward with adopting EME (but in
    this case, by individuals motivated by preserving the decentralized
    integrity of the web).
-   Harry Halpin ended the panel with a bang... first, he reiterated
    that in participating in this panel, he was acting independently and
    not as a W3C employee. (And again, to paraphrase:) "However, I will
    say that there are some lines that must be drawn. Permitting DRM to
    enter into the web is a line that must not be crossed. And if the
    W3C moves to recommend EME, I will resign."

Bam!

And so, that was my Sunday evening. If you were going to tell me that I
would end the last evening of the last day of the week even more
energized than when I began it (especially after a week as busy as
that!), I would not have believed you. But there it is! I'm glad I got
participate.

For more coverage, read up at [Defective By
Design](http://www.defectivebydesign.org/from-the-web-to-the-streets-protesting-drm),
[Motherboard](https://motherboard.vice.com/read/we-marched-with-richard-stallman-at-a-drm-protest-last-night-w3-consortium-MIT-joi-ito),
and
[BoingBoing](https://boingboing.net/2016/03/22/anti-drm-demonstrators-picket.html).
Oh yeah, and [sign the anti-DRM petition while you're at
it](http://www.defectivebydesign.org/show-them-the-world-is-watching-stop-drm-in-html)!
