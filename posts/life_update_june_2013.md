title: Life Update: June 2013
date: 2013-06-26 21:50
author: Christine Lemmer-Webber
tags: personal, life update
slug: life_update_june_2013
---
So I haven't done one of these in a while... the [last one I
did](http://dustycloud.org/blog/life_update_october_2012/) was right
when I was [leaving CC to work on
MediaGoblin](http://dustycloud.org/blog/leaving-cc-to-work-on-mediagoblin/).
I think they're pretty good to get out of my system. I tend to have a
lot of things accrue that I'd like to talk about, and I just don't get
to them. It's nice to kind of reflect all at once.

# MediaGoblin

The most publicly visible thing that's changed in my life over the last
many months is my shifting to being fulltime on MediaGoblin. I've
written about this a bit, but honestly not enough. How is it going? I
think given the circumstances, I could hardly ask for more: we have an
active community that is a joy to work with each and every day, I love
working on the codebase, and I feel like I'm doing something important.

And things have certainly busy. We've [put
out](http://mediagoblin.org/news/mediagoblin-0.3.2-goblinverse.html)
[three](http://mediagoblin.org/news/mediagoblin-0.3.3-pixel-party.html)
[releases](http://mediagoblin.org/news/mediagoblin-0.4.0-hall-of-the-archivist.html)
since the last "life update" post I did. We've got [six summer
interns](http://mediagoblin.org/news/summer-of-awesome.html)
participating in [Google Summer of Code and GNOME Outreach Program for
Women](http://mediagoblin.org/news/opw-gsoc-2013.html). And 4 out of 6
of these participants are women... that's affected by outreach, but it's
also by merit. We got a lot of super strong applicants, and I feel that
we did a good job picking the best proposal for each task. Diversity is
something I really believe matters, and I feel like we're doing well
here. It's good to see that that message is caught on and understood by
our community too (see [this post by OPW participant Emily
O'Leary](http://blog.lotusecho.net/2013/06/08/inclusivity-in-free-software/)).
Things keep churning forward, and in a good way, in a community that's
strong and functional in ways I feel proud of. (We just reached 65
people in the AUTHORS file.... how cool is that?)

Most of the time it's a lot of fun. It's also generally fairly tiring.
Not that it isn't worth it, it's totally worth it! But there's always
more to do, and I constantly feel bad about that. But I think often it's
best to not be feeling bad so much and just keep working forward
generally.

One thing I also feel bad about is I don't take the time to write enough
about things. I don't give weekly or biweekly updates partly because I
don't have a good place to put them, and blogging used to be a painful
setup (although [I've improved that a bit](/blog/now-on-pelican/))...
but maybe I should make a sub-blog for it. Or I could just spam this
blog a lot more. We do have a list managed by the FSF from the
MediaGoblin campaign where I sometimes put out notices and of course
there's the MediaGoblin blog. Anyway, it's a situation I'm not super
happy with. Joey Hess does an awesome job of [blogging on a daily basis
about his git-annex
work](http://git-annex.branchable.com/design/assistant/blog/). I talked
to him about it, he said he often copies stuff from git logs into there.
That wouldn't be hard for me to do. I've also been writing out invoices
to the FSF since I am getting paid for MediaGoblin work as a contractor,
and I detail most (but not by any means all) work there.

Speaking of Joey Hess, I talked with him when I was at LibrePlanet. (I'm
a huge Joey Hess fan, by the way!) It was nice to compare our two
projects given we're both people who were paid to work on free software
for a year from crowdfunding. One thing I've thought about is the
distribution of time... I think Joey spends more time directly on coding
than I do, and for his project, I think that makes sense. But git-annex
is, I think, mostly Joey's work (which is not to say there aren't other
contributors). MediaGoblin is structured differently, and thus my
allocation of time is different. Neither of these approaches are better
I think, and I think the workflows we have really are probably best for
our separate projects.

I write more MediaGoblin code than anyone, but the majority of
MediaGoblin code is not written by me. Mostly what I do is write the
"core infrastructure" of the project, then people build on top of that.
I help coordinate with people on the right direction of things, help
build the core bits people need, help people find what they need, do
code review on what they've written, and... well, I do a lot of guidance
day to day. But this makes sense I think... MediaGoblin really is a
community project. I provide a lot of the vision of the project, and I
do tons of work on it, but it's not all my decision making. Outside of
the general vision, a lot of what happens comes from negotiation on IRC.

How much do I spend on what tasks? I have a rough "guideline schedule":

-   Monday: administrative work (I do this at the start of the week to
    get it out of the way)
-   Tuesday through Thursday: programming
-   Friday through Saturday: code review / community management
-   Sunday: personal/retooling day

I probably work a little over 8 hours every day (but not too much more
so I don't hit total burnout or aggravate my RSI). But how much do I map
to the above schedule, really? The truth of the matter is that *usually*
there's some kind of immediate task that takes precedence over this
"general schedule", but it's useful for when there isn't. But I think
proportionally this comes close to the distribution of workload I have,
except probably I spend a bit more time on community management / code
review stuff (especially because when you add community management to
this, it becomes a much broader category of things I do) than on coding
directly. And that's just fine, actually... having a lot of code to
review and an active community is one of those things you can't complain
about.

I continue to believe that the work we're doing with MediaGoblin is
important. If there's some frustration involved it's that it's a
long-journey process. But I guess most things worthwhile are. People
have asked me whether or not I'll continue to work on MediaGoblin after
this year, and the answer is that I intend to if I can (yes, that does
require figuring out funding; yes, I am thinking about it; yes I am
[open to suggestions](/contact/)). One way or another, I have felt a
heightened sense of purpose recently. PRISM, Google announcing closing
Google Reader and it looking like they'll stop supporting federated IM
via XMPP, Google Glass coming out soon and set up by default to stream
your life through Google's datacenters... these things continue to
reinforce my feelings that working on issues of user freedom in
networked applications is an area that critically needs work.

# Visiting family and re-contextualizing my work

So things are busy with MediaGoblin, but very recently I took a break to
visit some of my dad's side of the family in New Mexico. It was good to
see everyone. I had some conversations with one of my aunts, a couple of
my uncles, my father, one of my cousins, and one of my younger brothers
about all sorts of things ranging from philosophy to religion to social
justice issues... it felt like the better side of academic debates,
which I miss a little.

Sitting in these conversations and talking to my family members gave me
an opportunity to appreciate my family for who they are in a way I
haven't thought about as much as I should. My uncle John is a great
thinker, has a clearer and sharper vision on the construction of society
than anyone I've ever met. He and my aunt Barbara both worked on
projects to aid those in poverty, and at one point they lived both in
Madison (where we are now, and that's part of the reason my parents
moved to the upper midwest and why I grew up there) on a cooperative. My
aunt at one point ran a rape crisis and counseling center, and now she
is working on issues of helping bring heath care to the disenfranchised.
My uncle Bill is working on starting a coffee shop and is trying to
figure out how to use it to support local artists and promote ethical
trade. My father taught and studied theology from the standpoint of
greater inter-cultural understanding and finding common ground and peace
between religions. My cousin Wendy is an atheist with a degree in
theology and it's interesting to see just how close she and my father
think. Just now she's leaving on a humanitarian mission to bring
sustainable water solutions to areas that need it most (part of this is
grounded in a purpose to show that actually atheists are moral people
too). I'm proud to come from a family that is grounded in both thinking
and acting upon issues of social justice.

And this lead to something interesting... the news of PRISM broke while
I was on this trip and some of that conversation shifted toward the work
I was doing, but it also contextualized some things because it was just
one part of many conversations. We talked about issues of user freedom,
both of PRISM and of the work I am trying to do, interwoven in many
issues of social justice and human rights. And this felt significant to
me, both in that I'm following in a family tradition of working on
social justice issues, but even more importantly, that issues of user
freedom *are* issues of social justice (and furthermore, that this was
just very well understood by people who have worked on issues that are
much more clearly seen as such).

But there is a flip side to this: if this is true, why have those of us
who work on user freedom issues so generally failed to contextualize the
issues we are working on in those terms? Why have we failed to describe
issues of software freedom (or user freedom concerns more generally) as
issues of social justice or contextualize them within human rights? A
lot of thinking around this really congealed for me while on this trip,
and I think I need to write about it more clearly while it is still
fresh in my mind.

I left for this trip feeling like I was taking a semi-vacation from
work, but also feeling bad about doing so. I left the trip with a
renewed sense of purpose. It was certainly worth it for that.

# Talks, conferences, and other projects

One odd thing about this year is that you'd think I'd be giving more
talks than ever being fulltime on MediaGoblin, but I have actually
avoided going to conferences for the most part. I have gone to two
conferences in 2013: FOSDEM and LibrePlanet. I spoke at both, and I am
convinced it was worth it. But every time I travel to speak it throws me
off from the work I need to be doing and it feels hard to give
MeidaGoblin's community its full attention. Right now I think we need
more work done than speaking publicity, so after LibrePlanet, I decided
to avoid conference-going for some time so I can focus. (Sadly this
meant even missing PyCon... the first time I have missed PyCon since I
started going in 2008!) I will probably resume some conference-going
soon, as I have a collection of things I want to talk about.

But about FOSDEM and LibrePlanet: they were both great conferences,
maybe some of the best conferences I have ever attended. I wish I had
more time to write about them (or rather, I really should have written
more about them closer to when they happened since this post is long
enough already) but I will say that they were both amazing experiences.
(One thing about both is that they both involved being on panels with
people I really admire and have looked up to... it's strange to be taken
somewhere near the same level of seriousness as them. Guess I'm doing a
good job of tricking people into thinking I'm relevant!)

Oh yeah, I did [give a
talk](http://pyvideo.org/video/1869/hy-a-lisp-that-transforms-itself-into-the-python)
about something unrelated recently, and it's the one project I've been
helping with a bit: [Hy, a pythonic lisp](http://hylang.org/)! You
should check it out, it's pretty cool. I've been helping with the docs
and I made the logo and I gave a talk at [ChiPy](http://chipy.org/) and
some other things. Mostly I just pester Paul Tagliamonte though. :)

# Miscellaneous

Well this post is more than long enough, so here's an attempt to wrap it
up.

Life is good. I like Madison. I'm slowly meeting friends here. Slowly,
but it's happening. I love where we live. Generally, I love life. I'm
not the absolute greatest at any of the fields that I'm in, but I seem
to be doing well, I think I'm working on the right things, I have the
fortune of working on them with amazing people, I'm trying, and I think
we have a good chance of making it. At the moment, I'm giving myself
permission to feel good about that.
