title: Free software password manager roundup
date: 2013-10-06 16:45
author: Christine Lemmer-Webber
tags: foss, security
slug: password-managers-roundup
---
So, I've had a goofy system that I homerolled for storing randomly
generated passwords that I keep encrypted. Let's just say that it's...
not ideal and doesn't scale. Really I should be using something that
other people have written. So I decided to look around at my options.
Here seems to be the best survey of things I could find:

-   **lastpass:** Irony of putting this first is also the reason in some
    ways it's first: this is NOT an option, because it's proprietary.
    That's a non-starter for me already, but it should be an extra
    non-starter post PRISM. I don't care if the LastPass people say they
    haven't been contacted/forced to hand over stuff; there's good proof
    that they could be forced into it. See LavaBit. And if they were
    forced to do so, you're essentially handing over all your stuff.
    Also, there's every potential of user stuff all being leaked at
    once. That's not security.

    Still, people seem to really like the feature set, so this gets a
    double mention here: it's something that I think is
    unacceptable/worthless to use, but maybe could be a source of
    inspiration to free software packages. I've been shown the way the
    program looks, and it does look and seem to function nicely. That's
    something free software packages should try to live up to: browser
    integration with auto form-filling, and a nice, friendly looking UI.

-   **FireFox Sync:** [FireFox
    Sync](https://support.mozilla.org/en-US/kb/firefox-sync-take-your-bookmarks-and-tabs-with-you)
    is a really cool project; a "least authority" approach to storing
    passwords, and the fact that you can set up server-side storage of
    your passwords and have all your machines sync together seems pretty
    neat, especially because Mozilla can't even read your data. That's
    pretty exciting.

    However, what advantage would I get of this over setting up my own
    password sync with something like git-annex? And does it really do
    much useful for non-browser-things? It's hard for me to tell.

    Still, even though I think it's not for me, I'm glad the project
    exists. I'm glad that Mozilla took the right way of doing the "even
    we can't see your data" thing, and I hope that post-PRISM they see
    the value of this work and keep it as-is!

-   **spd:** Let's face it, as a plaintext junkie,
    [spd](http://spd.sourceforge.net/) is more or less what I want in a
    sense... a simple single-file gpg-encrypted password manager. Seems
    perfect! I could just sync it across machines with git-annex. (And
    syncing with git-annex, actually, is probably how I want to sync
    across everything.) It's minimal, and what's extra cool is that it's
    a good fit for an organization that needs to share passwords; maybe
    the sysadmins can access X and Y, and the PR people can access Y
    and Z. spd handles that, and with a simple file format... I think
    that's pretty awesome. And check out the screenshot on the site.
    It's so cool and terminal'y, and I like that you can copy-pasta from
    a terminal, someone can be sitting behind you, and not see what your
    password... while it still being terminal based! Nice.

    There's just one problem: there's no browser integration. I've been
    coming to think that browser integration is probably pretty
    necessary these days to keep up with the *massive* number of
    passwords we have to have without reusing the same shitty ones over
    and over. So, there's that.

    Maybe a browser extension could be added; I don't have the time to
    write it sadly. Still, the format seems very simple, and probably
    this is the closest to the kind of system I want on a technical
    level.

-   **pass:** Okay so [pass](http://zx2c4.com/projects/password-store/)
    is similar to spd, simple, and probably a good solution if you're a
    command line nerd. I'm still sayin' it could use browser
    integration.

    It's also apparently written in bash, and just mostly wraps gpg,
    which I suppose makes it the "git porcelain" of password managers.

    And speaking of git, it does have nice integration with git, though
    committing passwords (even encrypted) seems a bit weird to me
    (git-annex seems to make more sense though I have a hard time
    explaining why I want to drop my history). Maybe more troublesome is
    if someone gets access to your repo, they can see *where* all your
    passwords are, since the usernames / places are just directories.
    But maybe you don't care about that part being leaked?

-   **keepass / keepass2:** [keepass](http://keepass.info/) is free
    software, and it's had quite a bit of adoption. It seems well used,
    tested, and liked, and best of all, there's a few browser extensions
    available... [keefox](http://keefox.org/) looking the nicest of all
    of them. Also, it has a single-file db extension system, so that
    makes it fairly appealing.

    So what's the downsides? It's written in C# for one. Okay, it's
    still free software, and Mono does work, shut up Chris Webber, don't
    be ridiculous. But it really feels very windows-y and out of place,
    not least of all because one of the major UI pieces says "Windows"
    on it and all of the UI components kind of look like they don't fit
    totally on my GNOME desktop. The UI also just feels very
    cumbersome/kludgey so far (it feels a bit like a GNOME 1 or Windows
    95 "power user" UI application, if you get my drift), though
    admittedly I haven't given it much time. Still, of all of these, it
    probably has the closest to all of the features I've said I want /
    asked for.

-   **KeePassX:** [KeePassX](http://www.keepassx.org/) seems to be the
    crowd favorite amongst GNU/Linux users. It's much like KeePass but
    written in QT and C++. So I guesss that reduces my anti-C# bigotry.
    However, there's no browser extensions. Why not use spd at that
    point?

    The UI does feel much nicer in GNOME though (and certainly it would
    be in KDE too). Apparently there's an "autotype" feature, but it's
    based on the window's title... that seems like a hack... but better
    than nothing?

-   **KeePassC / kppy:** Okay, looks pretty cool, a curses based tool
    using the KeePass 1.X database scheme, Python 3 based, even has a
    server? No browser integration, but looks promising, as it does
    [have a server](http://raymontag.github.io/keepassc/server.html)...
    maybe one could be implemented from there.

    However there's some weird code smells in KeePassC, like changing
    directory to [/var/empty/]{.title-ref} even if it doesn't exist.
    There's also a [kppy](http://raymontag.github.io/kppy/) which
    KeePassC uses, which is a general purpose python library to edit
    such things.

    Maybe a decent base to build things from?

-   **GNOME SeaHorse:** So, GNOME provides integrated encryption support
    via a program called SeaHorse. I like GNOME integration, thus I
    think I'd like this. However, there's also no browser extensions
    here, and I have a hard time figuring out whether or not I could
    nicely sync things across machines via git-annex and friends, so...
    hm.

-   **Encrypted plaintext files:** Okay, plaintext files plus GPG. It
    works, right? Except, also no browser integration, and also anyone
    sitting behind you can read your passwords. Let's stop pretending
    this is an option.

-   **Encrypted org-mode files:** [Several ways to do
    it](http://orgmode.org/worg/org-tutorials/encrypting-files.html) and
    actually it is probably a little bit less terrible than a
    plaintext + gpg file: the expansion of sections means you can
    navigate a bit better, maybe not expose all at once.... hm, you
    could maybe even hide the passwords with some custom elisp + font
    locking!

    Okay, except wait, still no browser integration, and I need to stop
    building systems that work just for me and nobody else in the
    universe in Emacs + OrgMode. Heh.

There's other options too, but they all seem to have the same problems
as the above, or worse.

It really looks like keepass2 + keefox is the best solution that exists
yet, but let's be honest... it's not a good solution! It speaks totally
to the traditional complaint of encryption tools in free software: they
work, we know how to use them in theory, and yet wheen you try to bring
them to the end user, they aren't a very pleasant UI experience.

That said, I'd be willing to take a pleasant experience that wasn't
really good for everyone, a-la spd, if I could get browser
integration... but that's probably admitting I'm not part of the general
solution!

**EDITS:** Added KeePassC and pass. Toned down the KeePassC exuberance
after I actually tried it.

**EDIT AGAIN:** After trying a bunch of things, I'm currently happy with
something completely not on this list at all:
[assword](http://finestructure.net/assword/). The name makes it hard to
take seriously, but it's great and elegant. Bind "assword gui" to
shift-ctrl-p, and it's the simplest system possible: give it a string,
and it either makes a new password, which it pastes, or it pastes
whatever string you had associated with that string. So. Great. And the
technology couldn't be simpler.
