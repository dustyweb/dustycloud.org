title: Life update: Late November 2013
date: 2013-11-26 17:23
author: Christine Lemmer-Webber
tags: life update, personal, mediagoblin, xudd
slug: life-update-late-november-2013
---
I thought I'd give a brief "life update" post. In some ways, this is a
more me-centric version of a "state of the goblin" post. Life is pretty
intertwined with that these days.

I gave my [block o' conferencing
reflections](/blog/block-o-conferencing-reflections/) already, so we'll
consider that out of the way. We're also about to put out a new release
of MediaGoblin. Stay tuned to the [MediaGoblin
blog](http://mediagoblin.org/news/)... it'll be an exciting one I think.

What can I say about this last year though? We're nearly at the end of
it. For this last year, I ate, breathed and lived MediaGoblin. This has
been simultaneously the greatest thing ever, and also super exhausting.
I really have not had much as in terms of breaks, role-wise I have worn
more hats than I thought I could fit on my head (among other things,
this includes writing core architecture, code review, promoting and
speaking about the project, plenty of behind the scenes communication,
plenty of management and project administration, budgeting things, the
project's "art identity", some system administration (though thankfully
simonft is helping), grant writing, all the many roles that went into
running the crowdfunding campaign and producing the associated video).
I'm glad I was an Interdisciplinary Humanities major; it couldn't have
been a more interdisciplinary year. I'm also glad I use
[Org-Mode](http://orgmode.org/); it will sound silly, but *MediaGoblin
could not exist* without that program.

And as tiring as it may have been, I am hoping I can continue with it.
The MediaGoblin community is... dare I say while admitting tons of
bias... one of the best communities I have seen in free software. (Maybe
even the best? Again, I am admitting bias! ;))

But [Joar Wandborg summarized the situation
well](http://lists.mediagoblin.org/pipermail/devel/2013-November/000706.html):

> The challenge at the moment, at least from what I see, is time.
> MediaGoblin would greatly benefit from more resources, having either
> one or more funded MediaGoblin developers would greatly benefit the
> project, as it is now, we have a lot of separate volunteers
> contributing code, thus putting a lot of work on the lead developer to
> review code. If we could increase the throughput on reviewing by
> assigning more people to review it would make the lead developer able
> to concentrate on increasingly keeping the project coherent and
> flexible while moving forward.

Well said. :)

On that note, I am simultaneously working on trying to get more
resources on board and growing MediaGoblin upward and outward. This is
achievable, I believe, and if we can get enough resources in front of
ourselves, I think MediaGoblin can easily be sustainable. But to get
there, we need to split my role into multiple people. That's hard to do
because splitting my role into multiple people requires more resources,
but it's hard to do the work to get more resources in while I am the
only full time person, even with the amazing, amazing community we have
(which is, again, super amazing!). This is solvable, but as a friend of
mine accurately described it over dinner, it's a "bootstrapping
problem". In the meanwhile, I am also playing a role of trying to
bootstrap things just so, but that means actively wearing another hat,
one that the MediaGoblin community does not usually see. It's hard not
to feel bad while I'm doing that kind of work, because I feel like I am
neglecting other things I want to move forward. But it needs to be done.
And I think we can and will get there.

On that note, we will be running another crowdfunding campaign. I won't
go into details here, but I have elsewhere, and if you're interested,
you can read a [relevant IRC
log](http://mediagoblin.org/irclogs/irc_meeting-2013-11-02.log.html).
There will be more to say soon, and of course you will hear about it
here.

Another way to summarize things: next year I want to wrap up the
features we need to get MediaGoblin 1.0 out the door (and that includes
federation work) and then work on pushing forward MediaGoblin adoption.
Plans are moving ahead on those fronts, and I am feeling optimistic.
(One way to advance those plans is, if you or an organization you are
working with are interested in running an instance, do it! And even
better, if you are interested in funding either us developing relevant
features or helping you run an instance, by all means [contact
me](/contact/)!

By the way, have I mentioned [XUDD](http://xudd.readthedocs.org)? I
don't get that much time to talk about it, but the very rare times I get
to work on code that isn't MediaGoblin (sadly, it's pretty rare) I have
been spending on XUDD. In short, I think the way we're writing a lot of
asynchronous network applications is wrong, and I think we can massively
improve the situation. XUDD is an attempt to show how I think that could
happen through an implementation of the actor model in Python. The
architecture is shaping up nicely, and I feel good about the ideas and
directions of the project. It's too bad it's so hard to allocate time
for it. As you may have guessed, this may tie back into MediaGoblin some
day, but if it does it will be some time in the future.

Anyway, that's enough of me yammering on for now. I think we've got an
exciting year head. Now, back to working on this release!
