title: Interviewed on Ryno the Bearded
date: 2015-04-07 20:04
author: Christine Lemmer-Webber
tags: ryno the bearded, podcast, interview, mediagoblin
slug: rynothebearded-interview
---
I was [interviewed on Ryno the
Bearded](http://rynothebearded.com/2015/04/my-origin-story/) in an
episode with the curious title "My Origin Story". I'm not sure whose
that refers to, maybe both of us, because we both talked about our
backstories. (Though, I think if I was going to lay out my "free
software origin story", it would probably include some other things...
but maybe it would get to be fairly rambly. I've thought about trying to
write up what that is before, but I guess there were a lot of "moments"
for my free software origins, not any one moment like "I was bitten by a
libre radioactive spider".)

I really enjoyed doing this one, and maybe you'll enjoy listening to it.
It's kind of rambly and conversational and we came in with very little
as in terms of questions, but I think I tend to do fairly well in that
format.

I did cut off the end of the interview by saying I had to go to the
bathroom though. Not really the most dignified of exits on my part. Oh
well.
