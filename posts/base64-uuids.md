title: Base64 UUIDs in Python
date: 2013-07-30 14:55
author: Christine Lemmer-Webber
tags: python, base64, uuid
slug: base64-uuids
---
Hardly even worth writing about, but maybe it's useful to someone. Ever
want a base 64 encoded UUID4 in python? I ported the uuid.uuid4() code
over for base64 encoding, with a slight cleanup function to make it URL
safe.

**UPDATE:** Making this the most useless blogpost I've already

:   written, there's already a urlsave_b64encode method (also, I thus
    removed the rest of the post above):

``` python
>>> base64.urlsafe_b64encode(uuid.uuid4().bytes).strip("=")
```
