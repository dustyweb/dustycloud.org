title: Invest in what you believe in
date: 2009-09-13 12:06
tags: miro, foss, money, advocacy
author: Christine Lemmer-Webber
slug: invest-in-what-you-believe-in
---
I just got back from [Djangocon](http://www.djangocon.org/), which was
pretty awesome. I was once again on the video team, much like at
[PyCon](http://pycon.blip.tv/). Now that I've got traveling and such out
of the way I can return to working on personal projects in my "spare
time".

And hey, one of those spare projects turned out to be making some
contributions to Miro. Pretty much just minor GTK-X11 specific fixes or
enhancements thus far. I'm hoping to return to more Miro hacking in a
serious way in the future, but of course I'm not working for the PCF
full-time any more, and I notice that the kind of things I'll likely be
working on will be a bit different: it really will be more
scratch-an-itch style development. Working on serious projects would
probably require more full-time dedication than I'm able to give at the
moment.

Which actually leads me to another point. Free software and free culture
projects all require funding. I tend to think that if you reap the
benefits of these kinds of projects, and especially if you *really
believe in them*, then you should consider putting your money toward
them. Think of it in terms of the [Lessig
Challenge](http://luke.francl.org/lessig-challenge/): how much money do
you put toward media distribution companies, proprietary software
vendors, etc whose policies and actions you object to? We do live within
a capitalist system, and that means the best way to vote toward change
is often to vote with your dollar. (There are other ways to vote of
course, you can vote with your effort and time too. Generally the best
option is to do both.) So putting your money toward things projects you
believe in, even when that "purchase" won't result in an immediate
result, is something I think everyone should do.

One such project is subtitle translations in Miro. The PCF is trying to
raise funds toward this, and I think it's a great opportunity to tackle
accessibility in open video, which hasn't really been covered yet... I'd
really like to see this bar make it all the way:

![Kickstarter](http://www.kickstarter.com/projects/1116615214/subtitles-in-miro-translations-and-support-for-t/widget/card.jpg)\_

I wouldn't stop there either. What organizations do you really believe
in? Various groups could use your support, in especially what has been a
terribly difficult year for nonprofits. A sample of groups that I think
are important and worth joining or donating to in the free culture /
free software sphere: [Creative Commons](https://creativecommons.net/),
[GNOME](http://www.gnome.org/friends), the [Free Software
Foundation](http://www.fsf.org/associate), the [Electronic Frontier
Foundation](http://www.eff.org)... these are all important groups that
need your help.

As for media, support independent artists, especially those that use
free culture licenses like [Jim's Big Ego](http://bigego.com),
[Professor Kliq](http://www.professorkliq.com), [Brad
Sucks](http://www.bradsucks.net), or any one of the many awesome artists
on [Jamendo](http://www.jamendo.com/) or
[Magnatune](http://magnatune.com). The Blender Foundation is creating a
new Open Movie, [Project Durian](http://durian.blender.org). They're
very close to meeting their pre-order goals... of course, they could
still use some help, and the more orders the better (at the moment, if
you preorder, you can get your name in the credits). That's a great
project in particular because it funds [Blender](http://blender.org)
development, helps create an awesome movie, and even releases all the
source files under free licenses. They have other items in their
[E-Shop](http://www.blender3d.org/e-shop), too. When you buy hardware,
try to buy devices that are free software friendly. There's loads you
can do in the realm of media and technology.

There's tons you can do outside of technology, too. Morgan and I get all
our groceries from the local farmers' market, from [our local
CSA](http://www.newleafnatural.net/), and from independent grocers. When
we go out to eat, we go to independent restaurants instead of chains.
The [Eat Well Guide](http://www.eatwellguide.org) is a fantastic
directory for finding ethical sources of food near you (especially
consider joining a Community Supported Agriculture program... it's a
cheap and easy way to get fresh, local and organic food at your door
every week).

Maybe not everything I've listed here meets what you believe in, but
probably something does. Just remember that your time, effort and money
are all incredibly important resources, and how you use them *will*
change the world, either in ways you believe in or ways you don't. So
invest wisely.
