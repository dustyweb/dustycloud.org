title: CRISPR drive as a Thompson hack?
date: 2017-03-04 16:00
author: Christine Lemmer-Webber
tags: crispr, genes, thompson hack
slug: crispr-driver-as-thompson-hack
---
I listened to [this episode of Radiolab on
CRISPR](http://www.radiolab.org/story/update-crispr/) last night (great
episode, everyone should listen), and I couldn't stop thinking about
this part discussed at the end of the episode about a [CRISPR gene
drive](https://en.wikipedia.org/wiki/Gene_drive#CRISPR.2FCas9)... the
idea is, you might want to replace some gene in a population, so you
might use CRISPR to edit the gene of a single parent. Then that parent
might reproduce, and there's a chance that its child might have it in
the population. Natural selection whether it stays or not... it could,
very well, peter out of a population.

But then they talked about this idea, which apparently worked on yeast
"on the first try", which was to have the parent modify the yeast of the
child during reproduction. The parent includes the instructions so that
in reproduction, it goes through and edits its new child's DNA, and
inserts the instructions on how to have that editor in the child's DNA
too.

Holy crap, am I wrong or is that a [Thompson
hack](http://wiki.c2.com/?TheKenThompsonHack) in DNA form?
