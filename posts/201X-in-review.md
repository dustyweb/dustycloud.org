title: 201X in review
date: 2019-12-31 22:46
author: Christine Lemmer-Webber
tags: new years, personal
slug: 201X-in-review
---
Well, this has been a big decade for me. At the close of 200X I was
still very young as a programmer, had just gotten married to Morgan, had
just started my job at Creative Commons, and was pretty sure everyone
would figure out I was a fraud and that it would all come crashing down
around me when everyone found out. (Okay, that last part is still true,
but now I have more evidence I've gotten things done despite apparently
pulling the wool over everyone's eyes.)

At work my boss left and I temporarily became tech lead, and we did some
interesting things like kick off CC BY-SA and GPL compatibility work
(which made it into the 4.0 license suite) and ran [Liberated Pixel
Cup](http://lpc.opengameart.org/) (itself an interesting form of
success, but I would like to spend more time talking about what the
important lessons of it were... another time).

In 2011 I started [MediaGoblin](https://mediagoblin.org/) as a side
project, but felt like I didn't really know what I was doing, yet people
kept showing up and we were pushing out releases. Some people were even
running the thing, and it felt fairly amazing. I left my job at Creative
Commons in late 2012 and decided to try to make working on network
freedom issues my main thing. It's been my main thing since, and I'm
glad I've stayed focused in that way.

What I didn't expect was that the highlight of my work in the decade
wasn't MediaGoblin itself but the standard we started participating in,
which became [ActivityPub](https://www.w3.org/TR/activitypub/). The work
on ActivityPub arguably caused MediaGoblin to languish, but on the other
hand ActivityPub was successfully ratified by the W3C as a standard and
now has over 3.5 million registered users on the network and is used by
dozens (at least 50) pieces of (mostly) interoperable software. That's a
big success for all of it that worked on it (and there were quite a
few), and in many ways I think is the actual legacy of MediaGoblin.

After ActivityPub becoming a W3C Recommendation, I took a look around
and realized that other projects were using ActivityPub to accomplish
the work of MediaGoblin maybe even better than MediaGoblin. The speed at
which this decade passed made me conscious of how short time is and made
me wonder how I should best budget it. After all, the most successful
thing I worked on turned out to not be the networked software itself but
the infrastructure for building networks. That lead me to reconsider
whether my role was more importantly as trying to advance the state of
the art, which has lead me to more recently start work on the federation
laboratory called [Spritely](https://gitlab.com/spritely/), of which
[I've written a bit about
here](https://dustycloud.org/blog/tag/spritely/).

My friend Serge Wroclawski and I also launched a podcast in the last
year, [Libre Lounge](https://librelounge.org/). I've been very proud of
it; we have a lot of great episodes, so check [the
archive](https://librelounge.org/archive/).

Keeping this work funded has turned out to be tough. In MediaGoblin
land, we ran two crowdfunding campaigns, the first of which paid for my
work, the second of which paid for Jessica Tallon's work on federation.
The first campaign got poured entirely into MediaGoblin, the second one
surprisingly resulted in making space so that we could do ActivityPub's
work. (I hope people feel happy with the work we did, I do think
ActivityPub wouldn't have happened without MediaGoblin's donors'
support. That seems worth celebrating and a good outcome to me
personally, at least.) I also was fortunate enough to get [accepted into
Stripe's Open Source
Retreat](http://dustycloud.org/blog/goodbye-2015-hello-2016/) and more
recently [my work on Spritely has been funded by the Samsung Stack Zero
grant](http://dustycloud.org/blog/samsung-stack-zero-grant/). Recently,
people have been helping by donating on
[Patreon](https://www.patreon.com/cwebber) and both my increase in
prominence from ActivityPub and Libre Lounge have helped grow that. That
probably sounds like a lot of funding and success, but still most of
this work has had to be quite... lean. You stretch that stuff out over
nearly a decade and it doesn't account for nearly enough. To be honest,
I've also had to pay for a lot of it myself too, especially by
frequently contracting with other organizations (such as [Open Tech
Strategies](https://opentechstrategies.com/) and [Digital
Bazaar](https://digitalbazaar.com/), great folks). But I've also had to
rely on help from family resources at times. I'm much more privileged
than other people, and I can do the work, and I think the work is
necessary, so I've devoted myself to it. Sometimes I get emails asking
how to be completely dedicated to FOSS without lucking out at a
dedicated organization and I feel extra imposter-y in responding because
I mean, I don't know, everything still feels very hand-to-mouth. A
friend pointed to a [blogpost from Fred Hicks at Evil
Hat](http://www.deadlyfredly.com/2018/06/how-i-got-a-full-time-job-in-tabletop-games/)
about how behind the scenes, things don't feel as sustainable sometimes,
and that struck a chord with me (it was especially surprising to me,
because Evil Hat is one of the most prominent tabletop gaming
companies.) Nonetheless, I'm still privileged enough that I've been able
to keep it working and stay dedicated, and I've received a lot of great
support from all the resources mentioned above, and I'm happy about all
that. I just wish I could give better advice on how to "make it work"...
I'm in search of a good answer for that myself.

In more personal reflections of this decade, Morgan and I went through a
number of difficult moves and some very difficult family situations, but
I think our relationship is very strong, and some of the hardest stuff
strengthened our resolve as a team. We've finally started to settle
down, having bought a house and all that. Morgan completed one graduate
program and is on the verge of completing her next one. A decade into
our marriage (and 1.5 decades into our relationship), things are still
[wonderfully
weird](http://dustycloud.org/blog/lets-just-be-weird-together/).

I'm halfway through my 30s now. This decade made it clearer to me how
fast time goes. In the book A Deepness in the Sky, a space-trading
civilization is described that measures time in seconds, kiloseconds,
megaseconds, gigaseconds, etc. Increasingly I feel like the number of
seconds ahead in life are always shorter than we feel like they will be;
time is a truly precious resource. How much more time do I have to do
interesting and useful things, to live a nice life? How much more time
do we have left to get our shit together as a human species? (We seem to
be doing an awful lot to shorten our timespan.)

I will say that I am kicking of 202X by doing something to hopefully
contribute to lengthening both the timespan of myself and (more
marginally individually, more significantly if done collectively) human
society: 2020 will be the "Year of No Travel" for me. I hate traveling;
it's bad for myself, bad for the environment. It's seemed most
importantly to be the main thing that continues to throw my health out
of whack, over and over again.

But speaking of time and its resource usage, a friend once told me that
I had a habit in talks to "say the perfect thing, then ruin it by saying
one more thing". I probably did something similar above (well, not
claiming anything I write is perfect), but I'll preserve it anyway.

Everything, especially this blog, is imperfect anyway. Hopefully this
next decade is imperfect and weird in a way we can, for the most part,
enjoy.

Goodbye 201X, hello 202X.
