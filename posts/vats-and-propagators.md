title: Vats and Propagators: towards a global brain
date: 2020-02-16 15:25
author: Christine Lemmer-Webber
tags: network, ocaps, propagators
slug: vats-and-propagators
---

(This is a writeup for future exploration; I will be exploring a small
amount of this soon as a side effect of some UI building I am doing,
but not a full system.  A full system will come later, maybe even by
years.  Consider this a desiderata document.  Also a forewarning that
this document was originally written for an ocap-oriented audience,
and some terms are left unexpanded; for instance, "vat" really just
means a one-turn-at-a-time single-threaded event loop that a bunch of
actors live in.)

We have been living the last couple of decades with networks that are
capable of communicating ideas.  However, by and large it is left to the
humans to reason about these ideas that are propagated.  Most machines
that operate on the network merely execute the will of humans that have
carefully constructed them.  Recently neural network based machine
learning has gotten much better, but merely resembles intuition, not
reasoning.  (The human brain succeeds by combining both, and a
successful system likely will too.)  Could we ever achieve a network
that itself reasons?  And can it be secure enough not to tear itself
apart?


Near-term background
====================

In working towards building out a demonstration of petname systems in
action in a social network, I ran into the issue of changes to a petname
database automatically being reflected through the UI.  This lead me
back down a rabbit hole of exploring reactive UI patterns, and also lead
me back to exploring that section, and the following propagator section,
of SICP again.  This also lead me to rewatch one of my favorite talks:
[We Don't Really Know How to Compute!](https://www.youtube.com/watch?v=Rk76BurH384)
by Gerald Sussman.

[At 24:54](https://youtu.be/Rk76BurH384?t=1494) Sussman sets up an
example problem: specifically, an expert in electrical systems having
a sense of how to be able to handle and solve an electrical wiring
diagram.  (The kind of steps explained are not dissimilar to the kind
of steps that programmers go through while reasoning about debugging a
coding problem.)  Sussman then launches into an exploration of
propagators, and how they can solve the problem.  Sussman's
explanation is better than mine would be, so I'll leave you to watch
the video to see how it's used to solve various problems.


Okay, a short explanation of propagators
=========================================

Well, I guess I'll give a little introduction to propagators and why I
think they're interesting.

Propagators have gone through some revisions since the [SICP days](https://sarabander.github.io/sicp/html/3_002e3.xhtml#g_t3_002e3_002e5);
relevant reading are the
[Revised Report on the Propagator Model](https://groups.csail.mit.edu/mac/users/gjs/propagators/revised-html.html),
[The Art of the Propagator](https://dspace.mit.edu/handle/1721.1/44215),
and to really get into depth with the ideas,
[Propagation networks: a flexible and expressive substrate for computation](https://dspace.mit.edu/handle/1721.1/54635)
(Radul's PhD thesis).

In summary, a propagator model has the following properties:

 - There are cells with accumulate information about a value.  **Note!
   This is a big change from previous propagator versions!** In the modern
   version of a propagator model, a cell doesn't hold a value, it accrues
   information about a value which must be non-contradictory.
 - Such cell information may be complete (the number 42 is all there is
   to know), whereas some other information may be a range of
   possibilities (hm, could be anywhere between -5 to 45...).  As more
   information is made available, we can "narrow down" what we know.
 - Cells are connected together with propagators.
 - Information is (usually) bidirectional.  For example, with the slope
   formula of y = (m * x) + b, we don't need to just solve for y... we
   could solve for m, x, or b given the other information.  Similarly,
   partial information can propagate.
 - Contradictions are not allowed.  Attempting to introduce
   contradictory information into the network will throw an exception.
 - We can "play with" different ideas via a Truth Maintenance System.
   What do we believe?  Changes in our beliefs can result in changes
   to the generated topology of the network.
 - Debugging is quite possible.  One of the goals of propagator networks
   is that you should be able to investigate and determine blame for a
   result.  Relationships are clear and well defined.  As Sussman says
   (roughly paraphrased), "if an autonomous car drives off the car of
   the road, I could sue the car manufacturer, but I'd rather sue the
   car... I want to hold it accountable for its decision making".  The
   ability to hold accountability and determine blame stands in contrast
   to squishier systems like neural nets, genetic programs, etc (which
   are still useful, but not as easy to interrogate).

There are a lot of things that can be built with propagators as the
general case of constraint solving and reasoning; functional reactive
UIs, type checkers, etc etc.


Bridging vats and propagators
=============================

The prototype implementations are written in Scheme.  The good news is,
this means we could implement propagators on top of something like
[Spritely Goblins](https://gitlab.com/spritely/goblins).

However (and, granted, I haven't completed it) I think there is one
thing that is inaccurately described in Radul's thesis and Sussman's
explanations, but which I think actually is no problem at all if we
apply the vat model of computation (as in E, Agoric, Goblins): how
distributed can these cells and propagators be?  Section 2.1 of Radul's
thesis explains propagators as asynchronous and completely autonomous,
as if cells and their propagators could live anywhere on the computer
network with no change in effectiveness.  I think this is only partially
true.  The reference implementation actually does not fully explore this
because it uses a single-threaded event loop that processes events until
there are no more to process, during which it may encounter a
contradiction and raise it.  However I believe that the ability to "stop
the presses" as it were is one of the nicest features of propagators and
actually should not be lost... if we introduced asynchronous events
coming in, there may be multiple events that come in at the same time
and which try making changes to the propagator network in parallel.
Thankfully a nice answer comes in form of a the vat model: it should be
possible to have a propagator network within a single vat.
[Spritely Goblins' implementation of the vat model is transactional](https://dustycloud.org/blog/goblins-time-travel-micropreview/),
so this means that if we try to introduce a contradiction, we could
roll back immediately.  This is the right behavior.  As it turns out,
this is very close to the propagator system in the way it's
implemented in the reference implementation... I think the reference
implementation did something more or less right while trying to do the
simplest thing.  Combined with a proper ocap vat model this should
work great.

Thus, I believe that a propagator system (here I mean a propagator
network, meaning a network of propagator-connected cells) should
actually be vat-local.  But wait, we talked about network (as in
internet) based reasoning, and here I am advocating locality!  What
gives?

The right answer seems to me that propagator networks should be able to
be hooked together, but a change to a vat-contained propagator system
can trigger message passing to another vat-contained propagator system,
which can even happen over a computer network such as the internet.  We
will have to treat propagator systems and changes to them as vat-local,
but they can still communicate with other propagator systems.  (This is a
good idea anyway; if you communicate an idea with me and it's
inconsistent with my worldview, it should be important for me to be able
to realize that and use that as an opportunity to correct our
misunderstandings between each other.)

However, cells are still objects with classic object references.  This
means it is possible to hold onto one and use it as either a local or
networked capability.  Attenuation also composes nicely; it should be
possible to produce a facet of a cell that only allows read access or
only allows adding information.  It's clear and easily demonstrated that
ocaps can be the right security model for the propagator model simply by
realizing that both the propagator prototype system is written in
scheme, and so is
[Jonathan Rees' W7 security kernel](http://mumble.net/~jar/pubs/secureos/secureos.html).

This is all to say, if we built the propagator model *on top of*
an ocap-powered vat model, we'd already have a good network
communication model, a good security model, and a transactional model.
Sounds great to me.

Best of all, a propagator system can live alongside normal actors.  We
don't have to choose one or the other... a multi-paradigm approach can
work great.


Speaking the same language
==========================

One of the most important things in a system that communicates is that
ideas should be able to be expressed and considered in such a way that
both parties understand.  Of course, humans do this, and we call it
"language".

Certain primitives exist in our system already; for optimization
reasons, we are unlikely to want to build numbers out of mere tallying
of numbers (such as in Peano arithmetic); we instead build in primitives
for integers and a means of combination for them.  So we will of course
want to have several primitive data types.

But at some point we will want to talk about concepts that are not
encoded in the system.  If I would like to tell you about a beautiful
red bird I saw, where would I even begin?  Well obviously at minimum, we
will have to have ways of communicating ideas such as "red" and "bird".
We will have to build a vocabulary together.

Natural language vocabulary has a way of becoming ambiguous fast.  A
"note" passed in class versus a "note" in a musical score versus that I
would like to "note" a topic of interest to you are all different things.

Linked data (formerly "semantic web") folks have tried to use full URIs
as a way to get around this problem.  For instance, two ActivityPub
servers which are communicating are very likely speaking about the same
thing if they both use "https://www.w3.org/ns/activitystreams#Note",
which is to say they are talking about some written note-like message
(probably a (micro)blog post).  This is not a guarantee; vocabulary
drift is still possible, but it is much less likely.

Unfortunately, http(s) based URIs are a poor choice for hosting
vocabulary.  Domains expire, websites go down, and choosing whether to
extend a vocabulary in some namespace is (in the author's experience) a
governance nightmare.  A better option is "content-addressed
vocabulary"; instead of "https://www.w3.org/ns/activitystreams#Note"
we could instead simply take the text from the standard:

   "Represents a short written work typically less than a single
   paragraph in length."

Hash that and you get
"urn:sha256:54c14cbd844dc9ae3fa5f5f7b8c1255ee32f55b8afaba88ce983a489155ac398".
No governance or liveness issues required.  (Hashing mechanism upgrades,
however, do pose some challenge; mapping old hashes to new ones for
equivalence can be a partial solution.)

This seems sufficient to me; groups can collaborate somewhere to hammer
out the definition of some term, simply hash the definition of it, and
use that as the terminology URI.  This also avoids hazards from choosing
a different edge of Zooko's Triangle for vocabulary.

Now that we have this, we can express advanced new ideas across the
network and experiment with new terms.  Better yet, we might be even
able to use our propagator networks to associate ideas with them.  I
think in many systems, content-addressed-vocabulary could be a good way
to describe beliefs that could be considered, accepted, rejected in
truth maintenance systems.


Cerealize me, cap'n!
====================

One observation from Agoric is that it is possible to treat systems that
do not resemble traditional live actor'y vats still as vats (and
"machines") and develop semantics for message passing between them (and
performing promise resolution) nonetheless, for instance blockchains.

Similarly, above we have observed that propagator systems can be built
on top of actors; I believe it is also possible to describe propagator
networks in terms of pure data.  It should be possible to describe
changes to a propagator network as a standard serialized ledger that can
be transferred from place to place or reproduced.

However, the fact that interoperability with actors is possible is good,
desirable, and thankfully a nice transitional place for experimentation
(porting propagator model semantics to Spritely Goblins should not be
hard).


Where to from here?
===================

That's a lot of ideas above, but how likely is any of this stuff to be
usable soon?  I'm not anticipating dropping any current work to try to
make this happen, but I probably will be experimenting in my upcoming UI
work to try to have the UI powered by a propagator system (possibly even
a stripped down version) so that the experimental seeds are in place to
see if such a system can be grown.  But I am not anticipating that we'll
see anything like a fully distributed propagator system doing something
interesting from my own network soon... but sometimes I end up
surprised.


Closing the loop
================

I mentioned before that human brains are a combination of faster
intuitive methods (resembling current work on neural nets) and slower,
more calculating reasoning systems (resembling propagators or some logic
programming languages).  That's also to say nothing about the giant
emotional soup that a mind/body tends to live in.

Realistically the emergence of a fully sapient system won't involve any
of these systems independently, but rather a networked interconnection
of many of them.  I think the vat model of execution is a nice glue
system for it; pulling propagators into the system could bring us one
step closer, maybe.

Or maybe it's all just fantastical dreaming!  Who knows.  But it could
be interesting to play and find out at some point... perhaps some day we
can indeed get a proper brain into a vat.
