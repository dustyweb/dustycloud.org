title: ActivityPub Conf 2019
date: 2019-07-22 11:00
author: Christine Lemmer-Webber
tags: apconf, activitypub, conference, foss
slug: activitypub-conf-2019
---
![This flier also available in
[PDF](https://dustycloud.org/misc/APConfFlier.pdf) and
[ODT](https://dustycloud.org/misc/APConfFlier.odt)
formats.](/etc/images/blog/APConfFlier.png){.align-center}

**UPDATE:** As of August 5th, registrations have now filled up! See all
of you who registered at ActivityPub Conf!

That's right! We're hosting the first ever ActivityPub Conf. It's
immediately following [Rebooting Web of
Trust](https://www.weboftrust.info/) in Prague.

There's no admission fee to attend. (Relatedly, the conference is kind
of being done on the cheap, because it is being funded by organizers who
are themselves barely funded.) The venue, however, is quite cool: it's
at the [DOX Centre for Contemporary Art](https://www.dox.cz/en/), which
is itself [exploring the ways the digital world is affecting our
lives](https://www.dox.cz/en/exhibitions/datamaze).

If you plan on attending (and maybe also speaking), you should get in
your application soon (see the
[flier](https://dustycloud.org/misc/APConfFlier.pdf) for details). We've
never done one of these, and we have no idea what the response will be
like, so this is going to be a smaller gathering (about 40 people). In
some ways, it will be somewhere between a conference and a gathering of
people-who-are-interested-in-activitypub.

As said in the [flier](https://dustycloud.org/misc/APConfFlier.pdf), by
attending, you are agreeing to the [code of
conduct](https://www.contributor-covenant.org/version/1/4/code-of-conduct),
so be sure to read that.

The plan is that the first day will be talks (see the
[flier](https://dustycloud.org/misc/APConfFlier.pdf) above for details
on how to apply as a speaker) and the second day will be an
unconference, with people splitting off into groups to work through
problems of mutual interest.

Applications for general admission are first-come-first-serve.
Additionally, we have reserved some slots for speakers specifically; the
application to get in submissions for talks is 1 week from today (July
29th). We are hoping for and encouraging a wide range of participant
backgrounds.

Hope to see you in Prague!
