title: Life Update part 2: Leaving CC to focus on MediaGoblin
date: 2012-06-13 10:30
author: Christine Lemmer-Webber
slug: leaving-cc-to-work-on-mediagoblin
---
I said in my [last
update](http://dustycloud.org/blog/june-2012-life-update) that I was
partly clearing my life update news queue because I had something much
bigger to announce. So here it is:

I just gave my notice at Creative Commons... I'm leaving so I can spend
a greater amount of time focusing on my pet project,
[MediaGoblin](http://mediagoblin.org).

This should be a pretty smooth and gradual transition if all goes as
planned... I care deeply about the success of CC and the tech team, so
we've agreed on me staying part-time to ease the transition of the team
and to help wrap up the projects I've been working on. So I feel good
about that. One thing is for sure: working at Creative Commons these
last three years as a software engineer has been a rewarding,
fulfilling, and unique life opportunity, and I'm glad to have had it.
I'm glad that I'll be leaving in a way that I can feel good about both
for myself and for Creative Commons.

I've never had a project that I've cared about as deeply as MediaGoblin
before, nor one that I thought was as important socially as MediaGoblin
is. So I am planning to shift focus so I can more carefully help the
project along. MediaGoblin is also much more than just my project; we
have a lot of contributors who have invested much time and energy into
it. Yet, at the same time, I've observed that every time I step away
from the project it seems to grind to a halt, and whenever that happens
I get depressed. For the last year, I've dumped 100% of my weekend,
vacation, holiday time into MediaGoblin. I need a way to be able to
spend even more time on MediaGoblin than I already am while avoiding
burnout.

You might be wondering: how am I going to fund this? I have some ideas,
and will be working on it, but in the meanwhile I don't have anything
concretely set up. I have the part time CC work, and I have some
possible contracting opportunities lined up, but I'd also like to make
MediaGoblin development funding sustainable.

It'll be an interesting and exciting year ahead. There are a lot of
transitions going on in my life, including the move to Madison, but I
think this is the most interesting and exciting one for me.

On to the next chapter!
