title: Empathy for PHP + Shared Hosting (Which is Living in the Past, Dude)
date: 2014-03-30 11:45
author: Christine Lemmer-Webber
tags: deployment, mediagoblin, php, python, tech privilege, shared hosting
slug: empathy-for-php
---
After I wrote my [blogpost yesterday about
deployment](http://dustycloud.org/blog/configuration-management-for-the-people/)
it generated [quite a bit of discussion on the
pumpiverse](https://identi.ca/cwebber/note/7ZKVvXCeQ1m4T4o87hPr6Q). Mike
Linksvayer pointed out (and correctly) that "anti-PHP hate" is a poor
excuse for why the rest of us are doing so bad, so I edited that bit out
of my text.

After this though, maiki made a great series of posts, first asking
["Should a homeless person be able to 'host'
MediaGoblin?"](https://pump.jpope.org/maiki/note/CIn6ZNKIR5GFrhZsx00jMQ)
and then [talking about their own
experiences](https://pump.jpope.org/maiki/note/Ss-6T6EgQZm2rIt4J7flqA).
Go read it and then come back. It's well written and there's lots to
think about. (Read the whole thread, in fact!) The sum of it though is
that there's a large amount of tech privilege involved in installing a
lot of modern web applications, but maiki posts their own experiences
about why having access to free software with a lower barrier to entry
was key to them making changes in their life, and ends with the phrase
"aim lower". (By the way, maiki is actually a MediaGoblin community
member and for a long time ran an instance.)

So, let's start out with the following set of assertions, of which I
think maiki and I both agree:

-   Tech privilege is a big issue, and that lowering the barrier to
    entry is critical.
-   PHP + shared hosting is probably the lowest barrier to entry we
    have, assuming your application falls within certain constraints.
    This is something PHP does right! (Hence the "empathy for PHP"
    above.)
-   "Modern" web applications written in Python, Ruby, Node, etc, all
    require a too much tech privilege to run and maintain, and this is a
    problem.

So given all that, and given that I "fixed up" my previous post by
removing the anti-PHP language, the title I chose for this blogpost
probably seems pretty strange, or like it's undoing all that work. And
it probably seems strange that given the above, I'll still argue that
the choices around MediaGoblin were actively chosen to tackle tech
privilege, and that tackling these issues head-on is critical, or free
software network services will actually be in a *worse* place,
especially in a tech privilege sense.

That's a lot to unpack, so let's step back.

I think there's an element of my discussion about web technology and
even PHP that hasn't been well articulated, and that fault is my own...
but it's hard to explain without going into detail. So first of all,
apologies; I *have* been antagonistic towards PHP, and that's unfair to
the language that currently powers some of the
[most](http://wordpress.org/)
[important](http://www.mediawiki.org/wiki/MediaWiki) software on earth.
That's lame of me, and I apologize.

So that's the empathy part of this title. Then, why would I include that
line from my slides, that "PHP is [Living in the past,
Dude](http://ebb.org/bkuhn/blog/2011/08/05/living-in-the-past.html)", in
this blogpost? It seems to undo everything I'm writing. Well, I want to
explain what I meant about the above language. It's not about "PHP
sucks". And it does relate to free software's future, and also 5factors
into conversations about tech privilege. (It also misleading in that I
do not mean that modern web applications can't be written in PHP, or
that their communities will be bad for such a choice, but that PHP +
shared hosting as a deployment solution assumes constraints insufficient
for the network freedom future I think we want.)

Consider the move to GNOME 3, the subject of [Bradley's "living in the
past"
blogpost](http://ebb.org/bkuhn/blog/2011/08/05/living-in-the-past.html):
during the move to GNOME 3, there were really two tech privilege issues
at stake. One is that actually you're requiring newer technology with
OpenGL support, and *that's* a tech privilege issue for people who can't
afford that newer technology. (If you volunteered at a FreeGeek center,
you'd probably hear this complaint, for example.) But the other one is
that GNOME 3 was also trying to make the desktop easier for people, and
in a direction of usability that people expect these days. That's also a
tech privilege issue, and actually closer to the one we're discussing
now: if the barrier to entry is that things are too technical and too
foreign to what users know and expect, you're still building a privilege
divide. I think GNOME made the right decision on addressing privilege,
and I think it was a forward-facing one.

Thus, let me come back around to why, knowing that Python and friends
are much harder, I decided to write MediaGoblin in Python anyway.

The first one is functionality. MediaGoblin probably could never be a
good video hosting platform on shared hosting + PHP only; the celery
component, though it makes it harder to deploy, is the whole reason
MediaGoblin can process media in the background without timing out. So
in MediaGoblin's case (where media types like video were always viewed
as a critical part of the project), Celery *does* matter. More and more
modern web applications are being written in ways that PHP + Shared
Hosting just can't provide: they need websockets, they need external
daemons which process things, and so on.

And let's not forget that web applications are not the only thing. PHP +
shared hosting does not solve the email configuration problem, for
example. More and more people are moving to GMail and friends; this is a
huge problem for user freedom on the net. And as someone who maintains
their own email server, I don't blame them. Configuring and running this
stuff is just too hard. And it's not like it's a new technology... email
is the oldest stable federated technology we have.

Not to mention that I've argued previously that [shared hosting is not
user freedom
friendly](https://identi.ca/cwebber/comment/9M-XUW86SxyZiYmy4kHhUg).
That's almost a separate conversation, though.

I also disagree that things like encryption certificates, which are also
hard, don't matter. I think peoples' privacy does matter immensely, and
I think we've only seen more and more reason to believe that this is an
area we must work on over the last few years. (You might say that "SSL
is doing it wrong" anyway, and I agree, though that's a separate
conversation. Proably something that does things right will be just as
hard to set up signing-wise if it's actually secure, though.)

Let's also come back to me being a Python programmer. Even given all the
above, there are a lot of people out there like me who are just not
interested in programming in PHP. This doesn't mean there aren't good
PHP communities, clearly there are. But I do think more and more web
applications are being written in non-PHP languages, and there's good
reason for that. But yes, that means that these web applications are
hard to deploy.

What's the answer to that? Assuming that lots of people want to write
things in non-PHP languages, and that PHP + shared hosting is
insufficient for a growing number of needs anyway, what do we do?

For the most of the non-PHP network services world, it has felt like the
answer is to not worry about the end user side of things. Why bother,
when you aren't releasing your end web application anyway? And so we've
seen the rise of devops coincide with the rise of "release everything
but your secret sauce" (and, whether you like it or not, with the
decline of PHP + shared hosting).

I was fully aware of all of this when I decided MediaGoblin would be
written in Python. Part of it is because I *like* Python, and well, I'm
the one starting the project! But part of it is because the patterns I
described above are not going away. In order for us to engage the future
of the web, I think we need to tackle this direction head-on.

In the meanwhile, it's hard. It's hard in the way that installing and
maintaining a free software desktop was super hard for me back in 2001,
when I became involved in free software for the first time. But
installers have gotten better, and the desktop has gotten better. The
need for the installfest has gone away. I think that we are in a similar
state with free network services, but I believe things *can* be
improved. And that's why I [wrote that piece yesterday about
deployment](http://dustycloud.org/blog/configuration-management-for-the-people/),
because I am trying to think about how to make things better. And I
believe we need to, to build web applications that meet the needs of
what people expect, to make free network services comparable to the
devops-backed modern architected proprietary network services of today.

So, despite what it might appear at the moment, tech privilege has
always been on my mind, but it's something that's forward-looking.
That's hard to explain though when you're stuck in the present. I hope
this blogpost helps.
