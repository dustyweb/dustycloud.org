title: More Eee PC impressions
date: 2008-03-21 01:04
author: Christine Lemmer-Webber
slug: more-eee-pc-impressions
---
So, first post written from the Eee. As usual, I'm using
[ViewSourceWith](https://addons.mozilla.org/en-US/firefox/addon/394)
with emacsclient to type it up.

There hasn't been a person whom I've shown it to today who hasn't been
both impressed with (and jealous of) the device. I must say that it
manages to fit expectations totally and completely... hardware-wise
anyway.

Yes, the keyoard is still really small and feels pretty cramped. But I'm
typing with both hands pretty well and with a fairly low margin of
error. There actually are two problems I've been having with the
keyboard, but they aren't the ones you'd expect. I end up pressing the
enter key a lot when I actually intend to press the apostrophe key.
Also, and this is what actually irritates me more than anything, the
keyboard doesn't feel totally fastened on the right side very well. It
sounds fairly springy when I type on that side in a way that it doesn't
on the left. Does anyone else have this problem, I wonder? It's annoying
me a lot, especially when I press the arrow keys. Maybe I'd feel less
annoyed with it if someone confirmed that this was normal.

The resolution's pretty low, and the screen's pretty tiny. The next
version of the device looks like it's going to improve on that greatly,
though actually... it's still a lot nicer than I expected even in its
present incarnation. You forget just how much of the web is still pretty
doable at a tiny width of 800 pixels. Yes, [Penny
Arcade](http://www.penny-arcade.com/) fits nice and snugly within the
browser window, framed so close and perfectly that you'd almost think
that maybe they designed their comics with this resolution in mind. And
maybe they did. But probably not.

I'm actually rather surprised at how nice the speakers are, as I'm
fairly certain they are quite better than my thinkpad's.

So, as for the hardware, its all pretty good excepting that keyboard
springy noise, which is getting to me a little. Funny thing is that I
had this same issue with another laptop I owned, and it irritated the
hell out of me then, too. I'm not sure if this justifies a replacement.
I'm thinking about it.

As for software, the version of Linux that's installed on here, some
Xandros derivative (which is in turn a derivative of Debian), is pretty
lame. Some people have actually complimented the desktop, but I think
it's an irritation. Ubuntu, from all I've read, runs just fine on here,
and I plan to install that shortly. But why install a stripped-down
Linux in the first place? I've found that Ubuntu is really easy to use
already, even for people who have had no prior experience with Linux.

That being said, its not terrible, and once you know that the terminal
can be started with Ctrl-Alt-T, it's survivable. It comes with
OpenOffice, FireFox, and Pidgin, and really for most people that's all
they really need. However, I'm not most people, and I'll be installing
Ubuntu on this thing shortly.

One last note, which is really a curiosity more than anything. I was a
bit surprised to see that the system came with some games, but all of
them (with the exception of Sudoku, which is such a lame game it almost
doesn't count) are games that have been around since I first started
running Linux. I'm not sure if that's a commentary on the state of Open
Source gaming or not, but it was kind of disappointing to realize.

Speaking of which, I tried installing [Wesnoth](http://wesnoth.org) on
here out of the Xandros repositories and it segfaulted. And that's
almost reason enough to switch this thing over to Ubuntu. When I get
around to doing that, I'll have more details here.
