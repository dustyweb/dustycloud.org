title: Why I Am Pro-GPL
date: 2015-07-21 07:29
author: Christine Lemmer-Webber
tags: gpl, copyleft, apache, permissive, licensing, foss
slug: why-i-am-pro-gpl
---
<p>
Last night at OSCON I attended the lightning talks (here called
"Ignite Talks").  Most of them were pretty good (I especially loved
Emily Dunham's "First Impressions (the value of the 'noob')" talk),
but the last talk of the night was titled "Why I don’t use the GPL" by
Shane Curcuru, "VP of Brand Management at the Apache Software
Foundation" (the association of which he invoked during his talk last
night, which made me wonder if he was speaking on behalf of the ASF,
which seemed surprising). <i>(<b>Edit:</b> this was confirmed to not
have been intended to be speaking on behalf of the ASF, which is good
to hear.  I don't have a recording so I'm not sure if Shane invoked
his association or if the person doing the introducing did.)</i>
</p>

<p>
It was a harsh talk.  It was also the last talk of the night, and
there was really no venue to respond to it (I looked to see if there
would be future lightning talk slots at this conference, but there
aren't).  Though the only noise from the audience was applause, I know
that doesn't mean everyone was happy, just polite... a number of my
friends got up and left in the middle of the talk.  But it needs a
response... even if the only venue I have at the moment is my blog.
That'll do.
</p>

<p>
So let me say it up front: my name is Chris Lemmer-Webber, and I
am pro-GPL and pro-copyleft.  Furthermore, I'm even pro-permissive (or
"lax") licensing; I see no reason our sides should be fighting, and I
think we can work together.  This is one reason why this talk was so
disappointing to me.
</p>

<p>
There's one particular part of the talk that really got to me though:
at one point Shane said something along the lines of "I don't use
copyleft because I don't care about the source code, I care about the
users."  My jaw dropped open at that point... wait a minute... that's
<i>our</i> narrative.  I've <a href="http://dustycloud.org/blog/field-guide-to-copyleft/">written on this before</a> (indeed, at the time I
thought that was all I had to say on this subject, but it turns out
that's not true), but the <i>most common</i> version of anti-copyleft
arguments are a "freedom to lock down" position (see how this is a
freedom to remove freedoms position?), and the most common form of
pro-copyleft arguments are a "freedom for the end-user" position.
</p>

<p>
Now there <i>is</i> an anti-copyleft position which does take a stance
that copyleft buys into a nonfree system -- you might see this from
the old school BSD camps especially -- a position that copyright
itself is an unjust system, and to use copyright at all, even to turn
the mechanisms of an evil machine against itself as copyleft does, is
to support this unjust system.  I can respect this position, though I
don't agree with it (I think copyleft is a convenient tactical move to
keep software and other works free).  One difficulty with this
position though is to really stay true to it, you logically are
against proprietary software <i>far more</i> than you are
against copyleft, and so you had better be against all those companies
who are taking permissively licensed software and locking it down.
This is decidedly <i>not</i> the position that Shane took last night: he
explicitly referenced that the main reason you want to use lax
licensing and avoid copyleft is it means that businesses are more
willing to participate.  Now, there are a good number of businesses
which do work with copyleft, but I agree that anti-copyleft sentiments
are being pushed from the business world.  So let me parse that
phrasing for you: copyleft means that everyone has to give back the
changes that build upon your work, and not all businesses want to do
this.  The "businesses are more willing to participate" means that
businesses can use your project as a stepping stone for something they
can lock down.  Some businesses are looking for a "proprietary
differentiation point" to lock down software and distinguish
themselves from their competitors.
</p>

<p>
As I said, I am not only pro-copyleft, I am also pro-permissive
licensing.  The difference between these is tactics: the first tactic
is towards guaranteeing user freedom, the second tactic is toward
pushing adoption.  I am generally pro-freedom, but sometimes pushing
adoption is important, especially if you're pushing standards and the
like.
</p>

<p>
But let's step back for a moment.  One thing that's true is that over
the last many years we've seen an explosion of free and open source
software... at the same time that computers have become more locked
down than ever before!  How can this be?  It seems like a paradox; we
know that free and open source software is supposed to free users,
right?  So why do users have less freedoms than ever?  Mobile
computing, the rise of the executable web, all of this has FOSS at its
core, and developers seem to enjoy a lot of maneuverability, but
computers seem to be telling us more what we can and can't do these
days than we tell them.  And notice... the rise of the arguments for
permissive/lax licensing have grown simultaneously with this trend.
</p>


<p class="centered">
  <a href="">
    <img src="/etc/images/blog/free_speech_zone_by_mustafa_and_aziza.jpg" alt="Free Speech Zone by Mustafa and Aziza" />
  </a>
  <br />
  <i><a href="https://www.flickr.com/photos/mus/3457967/">Free Speech Zone</a> by <a href="https://www.flickr.com/photos/mus/">Mustafa and Aziza</a>, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC BY-SA 2.0</a></i>
</p>

<p>
This is no coincidence.  The fastest way to develop software which
locks down users for maximum monetary extraction is to use free
software as a base.  And this is where the anti-copyleft argument
comes in, because copyleft may effectively force an entity to give
back at this stage... and they might not want to.
</p>

<p>
In Shane's talk last night, he argued against copyleft because
software licenses should have "no strings attached".  But the very
strategy that is advocated above is all about attaching strings!
Copyleft's strings say "you can use my stuff, as long as you give back
what you make from it".  But the proprietary differentiation
strategy's strings say "I will use your stuff, and then add terms
which forbid you to ever share or modify the things I build on top of
it."  Don't be fooled: both attach strings.  But which strings are
worse?
</p>

<p>
To return to the arguments made last night, though copyleft defends
source, in my view this is merely a strategy towards defending users.
And indeed, as in terms of where freedoms lie between those who make
use of the source and code side of things vs the end-user-application
side of things, one might notice a trend: there are very few
permissively licensed projects which aim at end users.  Most of them
are stepping stones towards further software development.  And this is
great!  I am glad that we have so many development tools available,
and it seems that permissive/lax licensing is an excellent strategy
here.  But when I think of projects I use every day which are programs
I actually run (for example, as an artist I use Blender, Gimp and
Inkscape regularly), most of these are under the GPL.  How many truly
major end-user-facing software applications can you think of that are
under permissive licenses?  I can think of many under copyleft, and
very few under permissive licenses.  This is no coincidence.  Assuming
you wish to fight for freedom of the end user, and ensure that your
software remains free for that end user, copyleft is an excellent
strategy.
</p>

<p>
I have heard a mantra many times over the last number of years to
"give away everything but your secret sauce" when it comes to software
development.  But I say to you, if you really care about user freedom:
give away your secret sauce.  And the very same secret sauce that
others wish to lock down, that's the kind of software I tend to
release under a copyleft license.
</p>

<p>
There is no reason to pit permissive and copyleft licensing against
each other.  Anyone doing so is doing a great disservice to user
freedom.
</p>

<p>
My name is Chris Lemmer-Webber.  I fight for the users, and I'm
standing up for the GPL.
</p>

<p>
<i><b>Addendum:</b>
<a href="https://twitter.com/webmink/status/623540836328812544">Simon Phipps points out</a>
that all free licenses are "permissive" in a sense.  I agree that
"permissive" is a problematic term, though it is the most popular term
of the field (hence my inclusion also of the term "lax" for
non-copyleft licenses).  If you are writing about non-copyleft
licenses, it is probably best to use the term "lax" licenses rather
than "permissive".
</i>
</p>
