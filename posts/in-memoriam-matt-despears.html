title: In Memoriam: Matt DeSpears
date: 2011-12-06 15:00
tags: personal
author: Christine Lemmer-Webber
slug: in-memoriam-matt-despears
---
<p>
  Today I received a call from my friend Miles telling me that one of
  our mutual close friends, Matt DeSpears, passed away by taking his
  own life.  It's hard to pick the appropriate words to describe how I
  feel upon hearing this news.  "Shattered" and "devastated" are words
  that come to mind, and yet even though I feel like I am both of
  those things I don't feel that I have fully processed it yet.  How
  do you describe losing one of those people who you just take for
  granted as a constant in your life?
</p>

<p>
  I don't really feel like I am thinking completely clearly, but I
  feel compelled to write.  And I feel like Matt deserves a proper
  tribute.  And maybe the most appropriate time to give that tribute
  is right now, when I am most overwhelmed with emotion.
</p>

<p>
  Matt, like many of the people I know and love, was a misfit.  I
  don't mean this in a negative way: the people I love most in life
  are misfits (and I consider myself one of the biggest misfits of
  all).  Being misfit means your character is likely to develop into
  something unusual, and the most unusual people are often the most
  beautifully interesting.  Matt was even more misfit than most, and I
  loved him dearly for his unique character.  There will never be
  another Matt.  But being a misfit also means that it's harder to fit
  into the rest of the world, and that was especially true for
  Matt.
</p>

<p>
  I met Matt at an alternative school called Kradwell, which I
  transferred to in my junior year of high school (as I was nearly
  failing out of school due to my social issues and trouble coping
  with my attention deficit disorder).  I quickly came to love
  Kradwell.  At Kradwell, my social problems vanished.  I used to joke
  with people: nobody makes fun of you for being a freak at Kradwell
  because at Kradwell, <i>everyone</i> is a freak.  Instead of
  resenting how weird I was, I came to embrace it.  I met many friends
  there, all strange in their own wonderful ways.  One of the friends
  I met was Matt.
</p>

<p class="centered">
  <a href="/etc/images/blog/matt_despears_yearbook.png">
    <img src="/etc/images/blog/matt_despears_yearbook-scaled.png" alt="Matt DeSpears photo from Kradwell yearbook" /></a>
  <br />
  <i>Photo of Matt DeSpears from the Kradwell yearbook</i>
</p>

<p>
  It took a while for Matt and I to become friends.  Matt had a
  certain amount of awkwardness that was high even by Kradwell
  standards.  But eventually I did come to know him, and one time I
  invited him over to a get-together at my house.  I don't really
  remember the details, but I remember that day becoming a day when he
  became more integrated into the group of friends I was meeting at
  Kradwell.  It was also around the time that my group of friends at
  Kradwell became integrated with my older group of friends from
  childhood.  Without any realization, we formed a close knit group of
  friends that had continued to be strong even until this day, which
  my wife and I now call the Milwaukee Crew.  I came to love this
  group of people, not just individually, but as a group: bonded
  through a mutuality of friendship and antagonism.  It had that kind
  of dynamic to it that you can't force into being, that just
  develops, and you come to enjoy.  Whenever I've come into Milwaukee,
  the Milwaukee Crew would assemble... or whoever of us were around.
  Morgan would sometimes remark about how resilient our group of
  friends were, still meeting together and making the same old jokes,
  expecting the same quirks... I thought it would last forever...
</p>

<p>
  As I said, Matt was more awkward than most, but underneath that
  shell of awkwardness was a true warmness.  Matt had a very fuzzy and
  puppy-dog like personality to him when you knew how to reach it.
  The truth of the matter was, Matt just wanted to be loved.  I guess
  that's true for everyone, but even more so than for most people,
  that's how I think of Matt.
</p>

<p>
  Matt also loved to antagonize people.  We all did, but there was
  this certain flavor of antagonism in Matt that's really hard to
  describe.  I remember Jay driving Matt and saying "Which way should
  we turn, Matt?" and Matt said, "Pope."  "No seriously, which way do
  we turn, Matt?"  "Pope."  "No seriously, we're about to pass the
  light, which way do we turn?"  "Pope."  <i>Maniacal laughter ensues,
  as Jay drives through the intersection yelling with frustration.</i>
  (Edit: Apparently it was a highway off-ramp, the next intersection
  wasn't for miles, and I wasn't even in the car.  But the memory has
  been strong enough in our group that I've remembered it as if I was.)
</p>

<p>
  It might be hard to understand how that could be lovable, or even
  entertaining, but maybe you just weren't there.  For years, we've
  groaned about this story, but behind the groan was a fondness for
  the memory, for the dynamic that unrolled between our friends.
</p>

<p>
  There are other things, too, that I remember fondly, that seem like
  they will never be able to happen in the same way again.  All night
  LAN parties playing CounterStrike, with Jay and Matt getting angry
  at each other over some tidbit.  Playing a prank on Matt so we made
  it so that every action on his computer elicited the noise
  "whoop-da-doop-da-doop!" and watching him getting angry as he
  couldn't turn the noise off.  Later (after it was disabled), he
  became obsessed and in love with that noise, as we all did.  It's
  just one of those inside jokes that all of us became obsessed with
  that nobody else could understand the meaning behind.
</p>

<p>
  I also remember that Matt became obsessed, as a few of us were, with
  an MMORPG that existed before that term existed called Graal, which
  was basically Legend of Zelda in 2d online.  Believe it or not, this
  proprietary game partly led me to become obsessed with the idea of
  free software as it was completely scriptable, and for a time,
  anyone could run their own server which meant that anyone could
  build their own universe.  Then they took the ability to run your
  own server away and I became angry and... hm, that's a topic for
  another blogpost.  The real point is, after a good number of us
  stopped playing, Matt continued playing the game, caught up in that
  online social world.  We used to antagonize him about it, and then
  he asserted that he stopped playing the game altogether.  Later,
  Miles and Jay would sneak across the side of his house and take a
  photo of him playing the game.  He was furious.  But eventually he
  came to laugh about it, as we all did.  Filed into another memory of
  fondness in the Milwaukee crew.  And there was something about that
  game that seemed to reflect something interesting about Matt, maybe
  about all of us.  The promise that you could build your own
  universe, and the ability to escape into another one that wasn't as
  painful as your own.  Matt became an administrator on the server.
  One day Matt had shown me that he had built an entire trading card
  minigame inside the game itself that had a bit of a following, even
  a fansite.  He had never programmed anything else in his life.  He
  just did it.  I don't remember Matt ever doing anything else like
  that before.
</p>

<p class="centered">
  <a href="/etc/images/blog/miles_versus_jay_robotsuit.png">
    <img src="/etc/images/blog/miles_versus_jay_robotsuit-scaled.png" alt="Miles or me vs Jay in the robot suit" /></a>
  <br />
  <i>This is either Miles or me chasing Jay in a cardboard robot
  suit.  I think it's Miles.  Regardless, it captures the spirit of
  the times and the crew.</i>
</p>

<p>
  I could go on into infinity listing off fond memories, and I'm a bit
  tempted to, but I think maybe I shouldn't.  But here are a few of
  the ones I remember the most fondly:
  <ul>
    <li>Creating cardboard robot suits and beating each other with
    plastic bats to the confusion of our neighbors.</li>
    <li>Hanging out at The Node, a coffee shop for nerds we all loved
    (until sadly they shut it down) and playing Risk.  One day after a
    particularly serious defeat, I "whoop-de-doop-de-doop"'ed Matt
    while doing a dance.  Nobody else in the coffee shop knew what I
    was doing or why I was making a fool of myself.  But we all
    understood.</li>
    <li>Driving around and talking about our relationships, or
    sometimes lack thereof.  Cheering each other up as friends,
    dropping by to say hello.</li>
    <li>Jay and Miles stopping by to order the most complex sandwich
    they could order at the sandwich shop Matt was working at.</li>
    <li>Matt's wonderfully strange vocal intonation somehow becoming
    a manner of speaking that everyone in the crew adopted.</li>
    <li>Various shenanigans at Kradwell.</li>
    <li>Matt's bizarre obsession with the Pope, Battle Pope, and
    BatPope.</li>
    <li>After I had moved away out to Barat College, Matt, Miles and
    Jay driving all the way down to my dorm completely unannounced,
    picking me up and making me come along with them on some
    adventure.  At the time, I acted like it was an inconvenience
    even though I truly appreciated it.  And of course I really appreciate
    the memory now.</li>
    <li>Hanging out on our private web forum which was entirely full
    of inside jokes and shenanigans.  "Nice" and "Shut up Wesley"
    being inside jokes that were tossed everywhere on there.</li>
    <li>As members of the group began to disperse, doing various
    get-togethers in Milwaukee or Appleton nonetheless.  Beating each
    other senseless with styrofoam pool noodles even though that
    doesn't make any sense because we're supposed to be adults
    here.</li>
    <li>My bachelor party, which was also a LAN party.  We were hoping
    it wouldn't be one of the last time we ever got to do one of these
    types of things again, but knew that it probably was.</li>
    <li>Matt being one of the ushers at my wedding, with Jay as my man
    of honor, Miles as one of the groomsmen (the others were
    siblings).  Several other members of the Milwaukee crew (Claire,
    Dani, Fatch (Jon), Corinna, Jeni... and I am probably missing a
    couple others) weren't in the wedding party but were in the
    audience.</li>
  </ul>
</p>

<p class="centered">
  <a href="/etc/images/blog/matt_miles_chris_jay_chriswedding.jpg">
    <img src="/etc/images/blog/matt_miles_chris_jay_chriswedding-scaled.jpg" alt="Matt, Miles, myself, and Jay at my wedding" /></a>
  <br />
  <i>Matt, Miles, myself, and Jay (in order) at my wedding</i>
</p>

<p>
  I think, more than anything, I am sad that in all of our get
  togethers there just won't be a Matt anymore to recollect with.
  There will just be an empty seat and a memory of Matt.  It seemed
  like it would last forever, be a constant in my life, and yet now it
  will never be the same.
</p>

<p>
  A couple weeks ago I was going stir crazy from
  <a href="/blog/moved-to-dekalb">living in DeKalb</a>.  Those of you
  who know me personally know that DeKalb is not exactly my favorite
  place to be, but we're here because of Morgan's grad school program
  which I am supportive of (and we can do because I have the good
  fortune of working remotely as a programmer).  I decided to go to
  Milwaukee to visit friends and family for Thanksgiving and even take
  a few days off to work on some of my own projects.  Right before I
  left the car broke down... but we decided that it was important enough
  to blow the money on a rental car so I could get out of town.
</p>

<p>
  While I was there, I tried to assemble the crew, but it didn't
  happen as it usually did.  I had the chance to meet most people
  individually, at least.  Matt and I had scheduled to go out and meet
  one night, but for reasons I won't go into I did something I never
  did: I got so frustrated over some detail that I canceled.
  Thankfully, we agreed to go out and get breakfast at George Webbs
  the next morning.
</p>

<p>
  We talked about relationships, life, work, the usual.  It was a good
  catch up.  Matt wasn't upset that I had refused to meet with him the
  previous night, or he didn't show any animosity.  After we got
  breakfast we went to American Science and Surplus and walked around
  and looked at various things.  I considered buying several things
  but didn't.  Matt bought a keychain container for his medication.
  It was a nice and quiet walk through someplace that we both
  cherished.  I was a glad we had that opportunity to get together for
  it.  Afterwards Matt suggested I meet his father, as I had never met
  his parents strangely in the more than ten years I'd known him.  We
  walked in, his father was preoccupied but said hello, Matt and I
  shrugged, he grabbed an iced tea from the fridge, and I drove him
  home.  Prairie Home Companion played on the radio, which I love, and
  Matt had never heard.  It unusually wasn't a good episode, not even
  the part with Guy Noir, Private Eye, and I felt bad that I didn't
  give Matt the opportunity to appreciate this show I really enjoyed.
  But he didn't mind.  I dropped him off, we cheerily waved goodbye,
  and I drove home thinking I was glad we had that opportunity to hang
  out before I left.
</p>

<p>
  Today I received a call from my friend Miles.  He asked me to make
  sure I was sitting, which I was, and then told me that he had just
  heard from Matt's nephew.  Matt had gotten into a fight with his
  girlfriend, swallowed a bottle of pills, and passed away.
</p>

<p>
  Matt was a strange and wonderful person.  Like many of us strange
  people, he suffered from depression and various other issues.  I
  also in many times in my life, have suffered from depression, and
  have come close to attempting suicide on several occasions.  I am
  glad I have not done so, as I probably would have become the same
  thing to others that Matt will be now: a dearly loved friend who is
  no longer there, an empty chair at a coffee table in a gathering of
  friends and family.  I will miss Matt dearly.  He was a great
  friend.
</p>

<p>
  I want to say one more thing in this post before I close it out.
  Matt had a son whom he rarely got to meet in this life.  If that
  person ever reads this, I want you to know several things about your
  father.  First: he regretted not being able to be a better father
  and blamed a lot of this on himself.  Family issues can become
  complex, as they were here, and at one point Matt thought he should
  finish school before he was more supportive, and then that never
  seemed to finish, he wished he could move down and be with you, and
  that never seemed to pan out.  But he wanted to be there for you, he
  just didn't know how to get to that point.  Second: your father
  loved you.  He would show me pictures of you, he would talk about
  you, and he wanted nothing more than to figure out how to be there
  for you, and the fact that he wasn't was a great source of sadness
  and guilt for him.  But when he spoke of you there were moments of
  inner pride and happiness that I never saw otherwise.  And I wish
  that things could have worked out so you could have gotten to know
  your father and loved him as I did.  He was a wonderful man, in his
  own curious way.  Third: he would have wanted you to be happy, to do
  good things, to enjoy life.  There's nothing he would have wanted
  more than that.
</p>

<p>
  And to all others: if you know someone who is awkward or strange, be
  nice to them, embrace them; they need it.  And if you yourself are
  strange or weird, don't be afraid of this, learn to love it.
</p>

<p>
  Matt, I will miss your strangeness, your wonderfulness, your
  kindness, your friendship.  Around the coffee table of my heart,
  there will always be a seat for you.
</p>

<p>
  <i><b>Edit:</b> I tried to incorporate some text earlier into this post
  to make it clear that I don't think Matt's girlfriend was to blame.
  I honestly don't think she was.  I haven't always gotten along with
  Matt's girlfriend in the past, but when I met with Matt for
  breakfast he expressed to me, "I know you guys don't like hanging
  out with her, but I love her, and she makes happy.  I'm happier now
  than I have been for a long time."  And I agreed... he did seem
  happier than he had been in a long time.  This is partly added to
  the surprise of this news.  I spoke with Matt's girlfriend and heard
  her side of the story.  I didn't think she was to blame before, and
  I especially don't now.  Couples' fights happen... and Matt was
  close to the edge for a long time.  This event is going to be hard
  on a lot of people, but probably especially it'll be hard on Jackie.
  Please don't put any more grief on her shoulders than she is already
  going to have to bear.</i>
</p>
