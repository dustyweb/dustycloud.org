title: Terminal Phase in Linux Magazine (Polish edition)
date: 2020-08-12 12:54
author: Christine Lemmer-Webber
tags: terminal phase, magazine
slug: terminal-phase-in-linux-magazine-pl
---

![Terminal Phase featured in Polish version of Linux Magazine](https://dustycloud.org/misc/terminal-phase-linux-magazine-pl.jpg)

Hey look at that!
My terminal-space-shooter-game [Terminal Phase](https://gitlab.com/dustyweb/terminal-phase)
made an appearance in the [Polish version of Linux Magazine](https://linux-magazine.pl/).
I had no idea, but [Michal Majchrzak](https://twitter.com/m_a_j_ch_rz_a_k)
both tipped me off to it and took the pictures.  (Thank you!)

I don't know Polish but I can see some references to Konami and SHMUP
(shoot-em-up game).
The screenshot they have isn't the one I published, so I guess the
author got it running too... I hope they had fun!

Apparently it appeared in the June 2020 edition:

![June 2020 edition of Polish Magazine](https://dustycloud.org/misc/linux-magazine-pl-june-2020-cropped.jpg)

I guess because print media coverage is smaller, it feels cooler to
get covered these days in it in some way?

I wonder if I can find a copy somewhere!
