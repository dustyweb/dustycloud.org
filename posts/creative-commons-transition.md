title: Creative Commons Transition
date: 2009-08-11 09:25
tags: miro, pcf, creative-commons
author: Christine Lemmer-Webber
slug: creative-commons-transition
---
If an unchanging life is a boring life, then I certainly haven't had
much time for boredom as of late. I've [microblogged about these changes
plenty](http://identi.ca/cwebber/), but a blog post of normal length is
probably in order.

I've mentioned on here already about getting married and etc. That
hasn't been too huge of a change moving from unmarried to married life
though. When you live together for long enough, the kind of changes that
marriage brings are more in the realm of civil benefits and how you sign
documents than anything else. There is, of course, the promise of
permanence. Yet that's mostly a seal on a document it felt like we
signed a long time ago. It is comforting to have, though.

The other big change has been a change in employment. A budget shortfall
means I'm no longer working at the Participatory Culture Foundation. No
animosity there... while I was sad to go, I understand that it needed to
happen, and I am still on great terms with everyone there. I expect to
continue to work on the Miro family of projects. I'm in a bit of a
transition period, but I suspect I will simply be returning to the work
capacity of a volunteer as I was before being hired at the PCF. Things
need to settle again before I can do anything though, so we'll see. (In
the meanwhile in PCF land, Paul Swartz took over the work I was doing on
Miro Community TV, and it's coming out great. I expect to run a
Blender-related instance of it myself soon.)

So, between jobs I did a small amount of contracting. I got positive
feedback for the work I did. It is nice to know then that I have the
capacity to do this if it is necessary, but I think I would have enjoyed
it more if I wasn't worrying so much. At this point in my life at least,
I prefer being employed as part of an organization. Maybe at some point
in the future that will change. In the meanwhile, I'm extremely giddy to
report that *am* employed at an organization... and an incredible one at
that! I accepted a position as a software engineer at Creative Commons!
How awesome is that?

Last week I spent doing "homework", reading up on the various
technologies used at CC, the numerous projects in place, and so on. This
week I have actually flown in to San Francisco (I am writing this from
my hotel right now, actually). I spent yesterday going over this stack
with [Nathan](http://yergler.net) in person and reviewing what the
"glorious future" is supposed to be (read: a cleanup in code and
architecture). Today I will begin working toward that glorious future,
to which much work has already been done. Exciting!

Anyway, I'm grateful for the fact that I have been able to move from one
incredibly awesome and socially positive organization to another without
too much of a difficult transition period. To be able to put your daily
effort toward something you truly believe in is rather rare, so I
consider myself quite lucky. I come mostly from a free software
background as in terms of these philosophical issues, but I said often
when giving talks on Miro that I am especially interested in the areas
where free software and free culture intersect. And now I'll be able to
directly work to progress the free culture movement by working with and
on free software. Which is several levels of fantastic.

Oh, and in case you were wondering, no, we are not moving to San
Francisco... not in the immediate future, anyway. Early Saturday morning
I will be flying back to Chicago so I can do work from our messy, messy
apartment.

Speaking of which, I thought San Francisco was going to be all
unbearably warm and etc. I was rather incorrect. It appears that in
flying out I ducked the worst of a very modest heatwave in Chicago. So,
fancy that.
