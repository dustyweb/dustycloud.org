title: Chicago GNU/Linux talk on Guix retrospective
date: 2015-10-01 19:50
author: Christine Lemmer-Webber
tags: guix, chicago, foss, userops, deployment
slug: chicagolug-guix-talk-retrospective
---
[![GuixSD logo](/etc/images/blog/guixsd-logo-scaled.png)](https://www.gnu.org/software/guix/)

Friends... friends! I gave a talk on
[Guix](https://www.gnu.org/software/guix/) last night in Chicago, and it
went amazingly well. That feels like an undersell actually; it went
*remarkably* well. There were 25 people, and apparently there was quite
the waitlist, but I was really happy with the set of people who were in
the room. I haven't talked about Guix in front of an audience before and
I was afraid it would be a dud, but it's hard to explain the reaction I
got. It felt like there was a general consensus in the room: Guix is
taking *the right approach* to things.

I didn't expect this! I know some extremely solid people who were in the
audience, and some of them are working with other deployment
technologies, so I expected at some point to be told "you are wrong",
but that moment didn't come. Instead, I met a large amount of enthusiasm
for the subject, a lot of curious questions, and well... there was some
criticism of the talk, though it mostly came to presentation style and
approach. Also, I had promised to give two talks, both about federation
and about Guix, but instead I just gave the latter and splatted over the
latter's time. Though people seemed to enjoy themselves enough that I
was asked to come back again and give the federation talk as well.

Before coming to this talk, I had wondered whether I had gone totally
off the yaks in heading in this direction, but giving this talk was
worth it in at least that the community reaction has been a huge
confidence booster. It's worth persuing!

So, here are some things that came out of the talk for me:

-   The talk was clear, and generally people said that though I went
    into the subject to quite some depth, things were well understood
    and unambiguous to them.
-   This is important, because it means that once people understood the
    details of what I was saying, it gave them a better opportunity to
    evaluate for whether it was true or not... and so the general sense
    of the room that this is the right approach was reassuring.
-   A two tier strategy for pushing early adoption with "practical
    developers" probably makes sense:
    -   Developers seem really excited about the ["universal
        virtualenv"](http://dthompson.us/ruby-on-guix.html) aspect of
        Guix (using "guix environment") and this is probably a good
        feature to start gaining adoption.
    -   Getting GuixOps working and solidly demonstrable
-   The talk was too long. I think everything I said was useful, but I
    literally filled two talk slots. There are some obvious things that
    can be cut or reduced from the talk.
-   In a certain sense, this is also because the talk was not one, but
    multiple talks. Each of these items could be cut to a brief slide or
    two and then expanded into its own talk:
    -   An intro to functional programming. I'm glad to see this this
        intro was very clear, and though concise, could be reduced
        within the scope of this talk to two quick slides rather than
        four with code examples.
    -   An "Intro to Guile"
    -   Lisp history, community, and its need for outreach and diversity
    -   "Getting over your parenthesis phobia"
-   I simply unfolded an orgmode tree while presenting the talk, and
    while this made things easy on me, it's not very interesting for
    most audience members (though my friend Aeva clearly enjoyed it)

Additionally, upon hearing my talk, my friend [Karl
Fogel](http://www.red-bean.com/kfogel/) seemed quite convinced about
Guix's direction (and there's few people whose analysis I'd rate
higher). He observed that Guix's fundamentals seem solid, but that what
it probably needs is institutional adoption at this point to bring it to
the next level, and he's probably right. He also pointed out that it's
not too much for an organization to invest themselves in Guix at this
point, considering that developers are using *way* less stable software
than Guix to do deployments. He suggested I try to give this talk at
various companies, which could be interesting... well, maybe you'll hear
more about this in the future. Maybe as a trial run I should submit some
podcast episodes to [Hacker Public Radio](http://hackerpublicradio.org/)
or something!

Anyway, starting next week I'm putting my words to action and working on
doing actual deployments using Guix. Now that'll be interesting to write
about! So stay tuned, I guess!

PS: You can download the [orgmode file of the
talk](http://dustycloud.org/misc/talks/guix/chicagolug_2015/guix_talk.org)
or [peruse the html rendered
version](http://dustycloud.org/misc/talks/guix/chicagolug_2015/guix_talk.html)
or even better [check out my talks
repo](https://gitlab.com/dustyweb/talks)!
