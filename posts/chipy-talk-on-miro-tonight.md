title: ChiPy Talk on Miro Tonight
date: 2008-10-09 16:46
author: Christine Lemmer-Webber
slug: chipy-talk-on-miro-tonight
---
I'm giving a talk tonight on [Miro](http://getmiro.org), which I've
[mentioned before](/blog/view_post/miro-volunteering/), at
[ChiPy](http://chipy.org). Details are on teh wiki, but might as well
duplicate them here:

> Chipy's October Meeting will be our best ever.
>
> Location: [Skinny Corp](http://www.skinnycorp.com/), 4043 N.
> Ravenswood Ave. Suite 106
>
> Date: Thursday, Oct 9th, \~7pm
>
> Topics:
>
> :   -   Chris Webber: [Miro](http://getmiro.org), a free, open source
>         internet tv & video player
>     -   KumarMcMillan - [freebase](http://www.freebase.com/), a free
>         collaborative database with a rich API

I know of a few other people who will probably be giving talks but
didn't update the wiki or announce on list. Also, you should come since
I've been hired by the [Participatory Culture
Foundation](http://pculture.org/) to work on Miro full-time, so you can
find out all about it!

What's that? I forgot to mention that I've changed jobs? Well, if you
come to the talk you'll find out more! I'll update my blog to talk about
it this weekend, but if you come tonight you won't have to wait as long!
:)

**Edit:** Also, SkinnyCorp people are the cool people who run
[Threadless](http://www.threadless.com/), which I know a couple people
who read this blog are fans of. So visiting their office is one more
reason to come!
