title: Block o' conferencing reflections
date: 2013-10-29 06:13
author: Christine Lemmer-Webber
tags: conference, foss, personal, mediagoblin
slug: block-o-conferencing-reflections
---
So this last month and a half I've done much more conferencing than I
normally do; first was [GNU 30th](https://www.gnu.org/gnu30/celebration)
where we ran a MediaGoblin hackathon, the Google Summer of Code Mentor
Summit 2013 where I ran a federation session and helped Karen Sandler
run the Outreach Program for Women session, and the [Blender
Conference](http://www.blender.org/conference/) where I gave a talk
(which was [recorded and uploaded to
YouTube](https://www.youtube.com/watch?v=ZdtDY6xY7lM), which I suppose I
should mirror to MediaGoblin when I get time, or something).

There were a lot of good things that came out of these conferences for
me (though I am always worrying while at conferences whether or not I am
making the best use of time as it is near impossible to do "normal"
tasks there). Spreading news about MediaGoblin was good, and I think I
was successful there. More importantly though, I queried a lot of people
for advice. I pestered a lot of people to try to get a sense of what's
ahead, and I'm grateful to everyone, but especially to Deb Nicholson (as
always, as MediaGoblin co-conspirator), Aeva Palecek (who puts up with
hearing me think through nearly everything), Karen Sandler, Mike
Linksvayer, Bradley Kuhn, John Sullivan, Leslie Hawthorne, Asheesh
Laroia, and Ton Roosendaal, who all sat down with me at some point and
gave me useful perspectives on how to take various strategies forward
for MediaGoblin and related things. Given I'm now nearing the end of
MediaGoblin's year of paid work from the MediaGoblin campaign, this was
really important for me to figure out how to move ahead with the next
year. I feel like I have a good sense of direction now and a set of
loose plans that will (hopefully!) work out, and that's really
important. Thank you, all, and thanks also to the many people I didn't
even list because it would be too long.

I already called them out, but it was really great on this trip
especially to talk to and observe Karen Sandler and Ton Roosendaal on
the way they organize and plan their respective organizations. Karen
gave me a lot of personal advice that I will not repeat here but which
gave me some confidence and sense of capability (thanks Karen). I also
really admire both what Karen has done with the GNOME Foundation
(especially in how it's been branching out to other things with Outreach
Program for Women), and I also admire how much of a large universe of
things Ton Roosendaal has helped cultivate with the Blender Foundation
and Blender Institute. By the way (maybe it helps to know a bit about
the Blender community?) I highly recommend [watching Ton Roosendaal's
"foundation feedback"](https://www.youtube.com/watch?v=Huh8Din7Exc)
talk. Maybe one of the most impressive things about Ton's approach to
the Blender Foundation (which really is very minimal and just handles
being a steward for the code side of things and etc) and the Blender
Institute (which is much more ambitious, has a large studio, employs
many developers, funds open movie and game projects, and does large and
bold things). One thing he talks about is that as soon as the community
takes over an activity, such as doing training, the Blender Institute
hands it off to the community and stops doing it so it can focus on
other things that are *not* being worked on as strongly. A bold move;
many organizations seem to have a super difficult time letting go of
something once it's in their domain. But it works well; the Blender
Institute focuses on growing the community into new areas, but when
those areas are well established to be sustainable externally, just let
them be! It's a surprising and refreshing approach in a world where even
nonprofits seem to want to establish empires. Additionally, other
interesting things happened in that talk and Ton's keynote; UI
conversations have been strong and while construtive, somewhat divisive
in the community. Ton wrote an article (also discussed in the talk)
called [(Re)defining
Blender](http://code.blender.org/index.php/2013/10/redefining-blender/)
and I think it does a good job of reframing the issue in a way that's
constructive for everyone.

Related to that, there was quite a bit of racial diversity, but sadly
not much gender diversity, at the Blender conference. Sadly I think
Blender falls in the intersection of free software communities and 3d
graphics, both of which really struggle with gender diversity.
Nonetheless, the women who were there all were doing amazing, powerful,
and well-respected things, from coding of important Blender tooling, to
the authorship of amazing short films, to the use of Blender for fine
art, to 3d printing, to anthropological reconstruction, to I am sure
some more things from people I did not talk to. But one form of
diversity that Aeva and I discussed that we were both impressed by was
the large diversity of types of things people are doing; from heart
surgery training to animation to games to programming to fine art,
people were really all over the place in the things they were
accomplishing. That felt really great to see.

Speaking of Blender, it was pretty incredible to hang out in the same
room of some of my "childhood heroes"... and by that I mean, I really
didn't have many of those since I didn't watch a lot of non-animated
television and that seems to be where people pick up their majority of
celebrity crushes, and so the age range at which some of my largest
"heroes" developed was in late high school and early college, when I
became obsessed with free software and especially the Blender community.
I had befriended Bassam Kurdali some time ago, but it was also really
great to hang out in the room as people such as [Andy
Goralczyk](http://www.artificial3d.com/) and [Pablo
Vaquez](http://venomgfx.com/) and both to talk to them and for them to
seem to take me seriously. I told them that if it weren't for being
inspired by their
[creaturey](http://www.artificial3d.com/lox-the-goblin-and-the-snog/)
[artwork](http://www.pablovazquez.org/portfolio/#characters) maybe I
wouldn't have been so encouraged to continue on with my own creaturey
stuff... maybe MediaGoblin wouldn't have a goblin and would be named
something else! They seemed appreciative when I showed them [Liberated
Pixel Cup](http://lpc.opengameart.org/), the [style
guide](http://lpc.opengameart.org/static/lpc-style-guide/index.html),
and the thing I was trying to prove (partly in response to the Open
Movie Projects that they had both participated in) that [distributed
free culture projects are indeed
possible](http://labs.creativecommons.org/2012/07/11/liberated-pixel-cup-and-distributed-free-culture-projects/)
if you do enough of the work up front (relevant maybe now due to the
nature of the newly announced Project Gooseberry which will be a lot
more distributed than previous open movie projects). They both seemed
excited and interested (we even talked about how a 3d version of the
base could be done, even in the same style), and I felt really happy
about that.

There are of course many more things that happened and plenty of other
things I can go on about too; maybe one of the more memorable things
about this stretch of time was the GNU 30th Hackathon. We had a good
turnout:

[![GNU 30th hackathon turnout](http://hipsterpunk.com/mgoblin_media/media_entries/281/DSC_0939.medium.jpg)](http://hipsterpunk.com/u/aeva/m/gnu-30th-mediagoblin-hackathon-b7a8/)

And we were even lucky enough to have all Outreach Program for Women
students attend!

[![OPW students!](http://hipsterpunk.com/mgoblin_media/media_entries/282/DSC_0946.medium.jpg)](http://hipsterpunk.com/u/aeva/m/opw-mediagoblineers-gnu-30th/)

I was really happy with this hackathon, and especially the opportunity
to have everyone together. As we said goodbye to each person, I felt a
little more sad, to the point where when saying goodbye to Jessica
Tallon and I split off at the ticketing area at the airport, I was
nearly in a funk.

And this leads into something else. At all of these conferences I was
asked: will you be coming back? Will we see you next year? Will you come
to this other conference our project is running? And there's a
temptation to do so... the [curse of the
traveler](http://www.reddit.com/r/IWantOut/comments/zykw2/getting_out_and_what_it_means_to_me/c68uit9)
raises its head, and I feel that I want to see these people again and
that I will miss them when I'm gone (and I will).

But how much conferencing should I do, really? I think the time invested
in these conferences was worth it, but I will be glad to be done
conferencing for some time. Previously in the year I had been invited to
a bunch of conferences and for half a year I avoided all of them because
I wanted to get real work done. Let's face it: there's certain kinds of
useful work that's done in making connections and
establishing/revitalizing community that can be done by
conference-going, but it's also quite difficult to do Real Work (TM). (A
certain exception: many projects have a role where someone really
*should* be attending more conferences and etc for the pupose of making
connections; it should be worked into the expectations of the position
that this kills one's capacity to do all sorts of other kinds of work
though.)

One thing Ton said in his talk (I'm paraphrasing) was "Every other day I
get invited to another conference, and I turn down every one of them.
Every conference you go to takes away a week of work, and there's too
much work to be done." Too true! While I think I did the right thing by
this set of conference attendance, it's time to hole back up in my
apartment and get stuff done.

Time to get back to work... and there's plenty to do!
