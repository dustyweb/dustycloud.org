title: Working It at the PCF
date: 2008-10-27 12:28
tags: miro, pcf
author: Christine Lemmer-Webber
slug: working-it-at-the-pcf
---
So as I mentioned briefly in my [last
post](/blog/view_post/chipy-talk-on-miro-tonight/), I started work at
the [Participatory Culture Foundation](http://pculture.org/) (PCF) this
month, mostly to work on [Miro](http://getmiro.org). Anyone who knows me
probably can guess that a job working on free and open source software,
especially related to media, and *in Python*, is a huge dream come true.

I had [mentioned](/blog/view_post/chipy-talk-on-miro-tonight/) that I
was to give a talk at [ChiPy](http://chipy.org) about Miro. And talk I
did... there's even a [recording of my
talk](http://chipy.blip.tv/file/1384376/) available to watch. (The
angle's a bit weird to look at, and you're mostly looking at my emacs
buffer, but the talk itself is interesting, I think.)

However, that talk is mostly directed at a programming audience, and
since this blog is read by some non-programming friends and family, I
figured I should write up some explanation of why I'm so hyped about
working here.

So first of all, Miro itself is awesome. It's a free and open source
internet television player. There's tons of content for it... *tons* of
content... all available on the [Miro
Guide](https://www.miroguide.com/). (The Miro Guide is itself a really
cool project. And yes, it's programmed in
[Django](http://www.djangoproject.com/).)

Part of why Miro matters so much is that it's built on open standards.
There are some other internet video players out there, but they often
rely on proprietary schemas. I like to think that Miro is kind of like
the Firefox of internet TV.

It's also really enjoyable to use. You know, there's that thing.

So for about three months primary to joining the PCF fulltime, I was a
volunteer to Miro's codebase. It's been great, partly because I've been
able to hit the ground running, but also because during that time I came
to really enjoy working on Miro's codebase. Which is part of what makes
being hired on to work at the Participatory Culture Foundation so
cool... I already knew I enjoyed working on Miro. And now I get to work
on it fulltime. Not to mention that all of the people at the PCF are
super nice, super fun to work with, super smart, and super productive
(giving me a good challenge to try and keep up...).

There's also the fact that the Participatory Culture Foundation has a
very clear and noble [mission](http://www.getmiro.com/about/mission/).
Aside from just working on technology to consume media, the PCF is
interested in helping to [inform people on how to make internet
television](http://makeinternettv.org/), as well as
[educating](http://www.getmiro.com/blog/2008/09/democracy-now-producers-violently-arrested-at-rnc/)
[people](http://www.getmiro.com/blog/2008/09/bandwidth-caps-comcasts-silver-bullet/)
[about](http://www.getmiro.com/blog/2008/07/best-practices-in-fair-use-for-online-video/)
[issues](http://www.getmiro.com/blog/2008/10/mccain-campaign-witnesses-dark-side-of-dmca/)
related to [Open
Video](http://www.getmiro.com/blog/2008/09/what-we-mean-when-we-say-open-video/)
(something the PCF takes seriously). So overall, this is a very morally
fulfilling organization to work for, and they've got other cool things
in the works. So, what can I say? I'm super happy to be where I am now.

By the way, I'm now syndicated on [Planet
Miro](http://planet.getmiro.com). Hello, Planet Miro! I guess [Will
already beat
me](http://pculture.org/devblogs/wguaraldi/2008/10/23/chris/) to
introducing me to the Planet Miro scene, or whatever :).
