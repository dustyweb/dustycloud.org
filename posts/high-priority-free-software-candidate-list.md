title: Unofficial candidate list for high priority free software projects
date: 2014-12-20 15:05
author: Christine Lemmer-Webber
tags: high priority, freesw, foss
slug: high-priority-free-software-candidate-list
---
I'm happy to see the FSF has
[revitalized](https://www.fsf.org/news/fsf-commences-review-of-high-priority-free-software-projects-list-your-input-is-needed)
the [high priority free software projects
list](https://www.fsf.org/campaigns/priority-projects/).

The FSF is calling for input, so consider the following my "high
priority wishlist": these are all things I wish I had nicer
representations in free software, and the lack thereof is hindering user
freedom in some urgent way.

# Server deployability tools

Deployment needs to become easier if we expect people to have any amount
of digital autonomy. Processes need to become generalized, abstracted. I
have [written some ideas on
this](http://dustycloud.org/blog/configuration-management-for-the-people/).
I am happy to talk to anyone interested in improving this space... :)

# Federation tools

Obviously somewhat [self-serving](http://mediagoblin.org/), though my
very reason for working on federation tools is due to my belief that a
lack of federation tooling leads to many problems. Those arguments
already well understood by anyone reading this site, so I won't go into
details.

Implementation of the [Social WG
standards](https://www.w3.org/wiki/Socialwg) would be a good direction
to take. Making this implementation reusable would be helpful.

# "Unity competitor" level game engine

I've also written before on why I believe that [games matter to free
software and free culture](/blog/why-faif-games-matter/).

But I'll particularly emphasize here: 1) proprietary games are bringing
DRM to otherwise free software operating systems via steam, 2) games are
currently the world's most popular medium, so if expression here is
particularly hard, this is not good and 3) games are a great way to
recruit new hackers, as this is the dream of many.

Assuming you buy those arguments, we are missing out on many
opportunities of expression by the sheer amount of work required to get
the basics of most games running. In the proprietary world, there is a
game engine called Unity that seems popular not just for developers but
also for artists. What is free software's answer?

Though it will surely come up, I'm not confident in the Blender Game
Engine. [Godot](http://www.godotengine.org/wp/) looks promising.

(As a side note, both of the following systems are fairly low-level at
the moment and thus do not fulfill the above "full featured and easy to
use", but some interesting things are happening in the lands of both
[Sly](https://gitorious.org/sly) and
[m.grl](https://github.com/Aeva/m.grl) (both by friends of mine)... I
hope more happens with these.)

# Password management tooling

Managing passwords is hard but important, and the [current state of
affairs in free software](/blog/password-managers-roundup/) is a sorry
one. Many users are turning to "LastPass", which astounds me in a
[post-Snowden,
post-Lavabit](http://www.theguardian.com/commentisfree/2014/may/20/why-did-lavabit-shut-down-snowden-email)
world. How to provide a way to store passwords safely and easy to use
way?

KeePassX and others which require manually copying around passwords I
think are simply not usable enough given the massive volume of times
users need to enter passwords.

I currently use [assword](https://finestructure.net/assword/), as
"assword gui", when bound to a key in your window manager, gives a
simple-ish prompt, with passwords dumped into whatever textbox
previously had focus in X11. It also pleasantly stores passwords in a
simple format: gpg encrypted json. Not bad, but there's no nice
"management" interface, and there's no "mobile" (read:
Android/Replicant) client.

# Easier encryption

It's great that we have [GnuPG](https://gnupg.org/), but usage is hard.
How to make easer? Maybe something like
[monkeysign](http://web.monkeysphere.info/monkeysign/) could help web of
trust things, but not only web of trust things are hard. SSL is also a
great way to [lose
hair](http://wingolog.org/archives/2014/10/17/ffs-ssl).

Relatedly, GPG could use your help, funding-wise! Consider making a
[donation](https://gnupg.org/donate/index.html).

# Free phones and mobile devices

Is this software? Sort of, it also ties in with hardware.

[Replicant](http://www.replicant.us/) is the best thing we have for
phones. (I would love to see a resurrection of Maemo 5, but I realize I
am in the minority here.)

# Wearable computing

Where is free software's competitor to Google Glass? Even if it isn't
cool right now, some competitor will make pervasive computing popular.
Do we really want to wait for the iEye before we start worrying that we
don't have a solution? This, like free phones, requires both hardware
and software.

# Circuit printing pipeline

Libre hardware which can print circuits could possibly get us well on
our way towards building hardware that's safe, reproducible, verifiable.
It won't help with chips, still will require lots of soldering, but
maybe it could help.

# Easy, peer to peer filesharing between friends

The return of peer to peer? Yes indeed. I see this one is [already on
the
list](https://www.fsf.org/campaigns/priority-projects/priority-projects/highpriorityprojects#sync)
though. :)
