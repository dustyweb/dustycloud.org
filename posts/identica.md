title: On Identi.ca
date: 2009-01-15 11:13
tags: foss, identica
author: Christine Lemmer-Webber
slug: identica
---
I just joined [Identi.ca](http://identi.ca), a microblogging service
(similar to [Twitter](http://twitter.com), but open source and better).

Currently using it so that people can track my work updates, probably?
So if you're curious about updates to the [Miro
Guide](http://miroguide.com), I'm going to be publishing them pretty
regularly until the new release. I'm also going to be talking about my
totally boring life. So, you know, if you wanted to know about those
things, feel free to [stalk me](http://identi.ca/cwebber).
