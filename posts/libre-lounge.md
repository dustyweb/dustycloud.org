title: Libre Lounge
date: 2019-02-08 08:13
author: Christine Lemmer-Webber
tags: libre lounge, foss
slug: libre-lounge
---
Did I somehow not blog here that I started co-hosting a podcast named
[Libre Lounge](https://librelounge.org/) with my friend [Serge
Wroclawski](https://blog.emacsen.net/)? We're talking about all sorts of
topics facing user freedom. Take a look at the
[archive](https://librelounge.org/archive/) and you might find something
you like!

At the time of writing we've also [had on one guest, Karen
Sandler](https://librelounge.org/episodes/episode-5-karen-sandler-and-software-freedom-conservancy.html).
This was a really nice treat since the show [Free as in
Freedom](http://faif.us/), which Karen co-hosts, is clearly a big
influence on Libre Lounge. Anyway, we plan to have on lots more guests
in the future.

Hope you enjoy!
