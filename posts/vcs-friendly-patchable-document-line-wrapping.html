title: VCS friendly, patchable, document line wrapping
date: 2015-12-17 10:30
author: Christine Lemmer-Webber
tags: writing, hacking
slug: vcs-friendly-patchable-document-line-wrapping
---
<p>
  If you do enough work in any sort of free software environment, you get used
    to doing lots of writing of documentation or all sorts of other things in
    some plaintext system which exports to some non-plaintext system.
  One way or another you have to decide: are you going to wrap your lines with
    newlines?
  And of course the answer should be "yes" because lines that trail all the way
    off the edge of your terminal is a sin against the plaintext gods, who are
    deceptively mighty, and whose wrath is to be feared (and blessings to be
    embraced).
  So okay, of course one line per paragraph is off the table.
  So what do you do?
</p>

<p>
  For years I've taken the lazy way out.
  I'm an emacs user, and emacs comes with the `fill-paragraph' command,
    so conveniently mapped to M-q.
  So day in and day out I'm either whacking M-q now and then, or
    I'm being lazy and letting something like `auto-fill-mode' do the job.
  Overall this results in something rather pleasing to the plaintext-loving
    eye.
  If we take our first paragraph as an example, it would look like this:
</p>

<blockquote style="font-family: monospace">
<pre>If you do enough work in any sort of free software environment, you get used to
doing lots of writing of documentation or all sorts of other things in some
plaintext system which exports to some non-plaintext system.  One way or
another you have to decide: are you going to wrap your lines with newlines?
And of course the answer should be "yes" because lines that trail all the way
off the edge of your terminal is a sin against the plaintext gods, who are
deceptively mighty, and whose wrath is to be feared (and blessings to be
embraced).  So okay, of course one line per paragraph is off the table.  So
what do you do?</pre>
</blockquote>

<p>
  But my friends, you know as well as I do: this isn't actually good.
  And we know it's not good because one of the primary benefits of plaintext
    is that we have nice tools to diff it and patch it and check it into
    version control systems and so on.
  And the sad reality is, if you make a change at the start of a paragraph
    and then you re-fill (or re-wrap for you non-emacs folks) it,
    <i>you are going to have a bad time!</i>
  Why?
  Because imagine you and your friends are working on this document together,
    and you're working in some branch of your document, and then your friend
    Sarah or whoever sends you a patch and you're so excited to merge it,
    and she does a nice job and edits a bunch of paragraphs and re-wraps it or
    re-fills them because why <i>wouldn't</i> she do that, it's the best
    convention you have, so you happily merge it in and say thanks, you look
    forward to future edits, and then you go to merge in your own branch you've
    been working on privately, but oh god oh no you were working on your own
    overhaul which re-wrapped many of the same paragraphs
    <i>and now there are merge conflicts everywhere.</i>
</p>

<p>
  That's not an imaginary possibility; if you've worked on a documentation
    project big enough, I suspect you've hit it.
  And hey, look, maybe you haven't hit it, because maybe most of your writing
    projects aren't so fast paced.
  But have you ever looked at your version control log?
  Ever done a `git/svn/foo blame', `git/svn/foo praise', or whatever
    convention?
  Eventually you can't figure out what commit anything came from, and my
    friends, that is a bad time.
</p>

<p>
  In trying to please the plaintext gods, we have defiled their temple.
  Can we do better?
</p>

<p>
  One interesting suggestion I've heard, but just can't get on board with,
    is to keep each sentence on its own line.
  It's a nice idea, and I want to like it, because the core idea is good:
    each sentence doesn't interfere with the one before or after it,
    so if you change a sentence, it's easy for both you and the computer
    to tell which one.
  This means you can check things in and out of version control,
    send and receive patches, and from that whole angle, things are great.
</p>

<p>
  But it's a <i>sin to the eye</i> to have stuff scrolling off the edge of
    your terminal like that, and each sentence on its own line, well...
    it just confuses me.
  Let's re-look at that first paragraph again in this style:
</p>

<blockquote style="font-family: monospace">
<pre>If you do enough work in any sort of free software environment, you get used to doing lots of writing of documentation or all sorts of other things in some plaintext system which exports to some non-plaintext system.
One way or another you have to decide: are you going to wrap your lines with newlines?
And of course the answer should be "yes" because lines that trail all the way off the edge of your terminal is a sin against the plaintext gods, who are deceptively mighty, and whose wrath is to be feared (and blessings to be embraced).
So okay, of course one line per paragraph is off the table.
So what do you do?</pre>
</blockquote>

<p>
  Ugh, it's hard to put into words why this is so offensive to me.
  I guess it's because each sentence can get so long that it looks like
    the separation between sentence is a bigger break than the separation
    between paragraphs.
  And I just hate things scrolling off to the right like that.  I don't want
    to be halfway through reading a word on my terminal and then have to jump
    back so I can keep reading it.
</p>

<p>
  So no, this is not good either.
  But it <i>is</i> on the right track.
  Is there a way to get the best of both worlds?
</p>

<p>
  Recently, when talking about this problem with my good friend
    <a href="https://dthompson.us/">David Thompson</a>, I came to realize
    that there is a potentially great solution that makes a hybrid of the
    technical merits of the one-sentence-per-line approach and the visually
    pleasing merits of the wrap/fill-your-paragraph approach.
  And the answer is: put each sentence on its own line, and wrap each
    sentence!
</p>

<p>
  This is best seen to be believed, so let's take a look at that first
    paragraph again... this time, as I typed it into my blogging system:
</p>

<blockquote style="font-family: monospace">
<pre>If you do enough work in any sort of free software environment, you get used
  to doing lots of writing of documentation or all sorts of other things in
  some plaintext system which exports to some non-plaintext system.
One way or another you have to decide: are you going to wrap your lines with
  newlines?
And of course the answer should be "yes" because lines that trail all the way
  off the edge of your terminal is a sin against the plaintext gods, who are
  deceptively mighty, and whose wrath is to be feared (and blessings to be
  embraced).
So okay, of course one line per paragraph is off the table.
So what do you do?</pre>
</blockquote>

<p>
  Yes, yes, yes!
  This is what we want!
  Now it looks good, and it merges good.
  And we still can preserve the multi-line separation between paragraphs.
  Also, you might notice that I continue each sentence by giving two spaces
    before its wrapped continuation, and I think that's an extra nice touch
    (but you don't have to do it).
</p>

<p>
  This is how I'm writing all my documentation, and the style in which I will
    request all documentation for projects I start be written in, from now
    on.
  Now if you're writing an email, or something else that's meant to be read
    in plaintext as-is (you do read/write your email in plaintext, right?),
    then maybe you should just do the traditional fill paragraph approach.
  After all, you want that to look nice, and in many of those cases,
    the text doesn't change too much.
  But if you're writing something where the plaintext version is just
    intermediate, and you have some other export which is what people
    mostly will read, I think this is a rather dandy approach.
</p>

<p>
  I hope you find it useful as well!
  Happy documentation hacking!
</p>
