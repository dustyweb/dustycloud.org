title: Now on Pelican
date: 2013-06-14 18:30
author: Christine Lemmer-Webber
tags: dustycloud, pelican
slug: now-on-pelican
---
I guess I've switched my blog around plenty in the last number of years.
Not long ago I was running [Zine](/blog/switched-blog-to-zine), then
[PyBlosxom](/blog/switched-blog-to-pyblosxom/) with some [Jinja2
templating hacks I wrote](https://gitorious.org/pyblosxom-jinja2-plugin)
(which had all sorts of unicode problems), and now,
[Pelican](http://docs.getpelican.com/en/3.2/).

I'm happy with the switch: Pelican seems sane, very cleanly built, and
things are working. I took a bit to clean up some other things about the
site. And ah yeah, there's a new [self
portrait](/etc/images/libreplanet_toon_chris.png) on the homepage.

I guess that's not terribly exciting, but I've cleaned it up and made
pushing changes as easy as a "make" command.

You can also check the site's contents out [via
git](https://gitorious.org/dustycloud/) if you like. Not sure anyone
would care, but hey! There it is.
