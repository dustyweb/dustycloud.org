title: Miro on Lifehacker
date: 2008-08-11 13:02
tags: miro, foss
author: Christine Lemmer-Webber
slug: miro-on-lifehacker
---
I was [just
talking](http://dustycloud.org/blog/view_post/miro-volunteering/) about
the work I've been doing on [Miro](http://getmiro.org), and now there's
an article [featuring Miro on
LifeHacker](http://lifehacker.com/400157/miro-is-your-tivo-for-internet-video).
Nice! It's a pretty good overview as well.

You might consider [digging the
article](http://digg.com/software/Online_Video_Miro_is_Your_TiVo_for_Internet_Video),
if you're into that kind of stuff.
