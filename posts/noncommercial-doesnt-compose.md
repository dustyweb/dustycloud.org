title: Noncommercial Doesn't Compose (and it never will)
date: 2019-12-27 18:46
author: Christine Lemmer-Webber
tags: nc, foss
slug: noncommercial-doesnt-compose
---
**NOTE:** I actually [posted this some time ago on
license-discuss](http://lists.opensource.org/pipermail/license-review_lists.opensource.org/2019-April/004024.html)
and some people suggested that I blog it. A good idea, which I never
did, until now. The topic has't become any less relevant, so...

It's sad to see history repeat itself, but that's what history does, and
it seems like we're in an awfully echo'y period of time. Given the
volume of submissions in favor of some sort of noncommercial style
license, I feel I must weigh in on the issue in general. Most of my
thoughts on this developed when I worked at Creative Commons (which
famously includes a "noncommercial" clause that can be mixed into a few
licenses), and it took me a while to sort out why there was so much
conflict and unhappiness over that clause. What was clear was that
Non-Commercial and No-Derivatives were both not considered "free
culture" licenses, and I was told this was drawn from the lessons of the
free software world, but here we are hashing it out again so anyway...

(I'm not suggesting this is a CC position; Creative Commons hasn't to my
knowledge taken an official stance on whether NonCommercial is right,
and not everyone internally agreed, and also I don't work there anymore
anyhow.)

I thank Rhea Myers for a lot of clarity here, who used to joke that NC
(the shorthand name for Non-Commercial) really stood for "No Community".
I think that's true, but I'll argue that even more so it stands for "No
Composition", which is just as much or more of a threat, as I hope to
explain below.

As a side note, I am of course highly empathetic to the motivations of
trying to insert a noncommercial clause; I've worn many hats, and
funding the software I've worked on has by far been the hardest. At
first glance, an NC approach appears to be a way to solve the problem.
Unfortunately, it doesn't work.

The first problem with noncommercial is that nobody really knows for
sure what it means. [Creative Commons made a great effort to gather
community
consensus](https://wiki.creativecommons.org/wiki/Defining_Noncommercial).
But my read from going through that is that it's still very "gut feel"
for a lot of people, and while there's some level of closeness the
results of the report result in nothing crisp enough to be considered
"defined" in my view. Personally I think that nothing will ever hit that
point. For instance, which of these is commercial, and which is
noncommercial?

-   Playing music at home
-   Playing music overhead, in a coffee shop
-   A song I produced being embedded in a fundraising video by the Red
    Cross
-   Embedding my photo in a New York Times article
-   Embedding my photo in a Mother Jones article
-   Embedding my photo on Wikipedia (if you think this is a clear and
    easy example btw, perhaps you'd like to take a selfie with this
    monkey?)

But this actually isn't the most significant part of why noncommercial
fails, has always failed, and will always fail in the scope of FOSS: it
simply doesn't compose.

Using the (A)GPL as the approximate maxima (and not saying it's the only
possible maxima) of license restrictions, we still have full composition
from top to bottom. Lax and copyleft code can be combined and reused
with all participants intending to participate in growing the same
commons, and with all participants on equal footing.

Unfortunately, NC destroys the stack. NC has the kind of appeal that a
lottery does: it's very fun to think about participating when you
imagine yourself as the recipient. The moment you have to deal with it
underneath, it becomes a huge headache.

I had an argument about this with someone I tend to work closely with,
they began arguing for the need to insert NC style clauses into code,
because developers gotta eat, which is at any rate a point I don't
disagree with. But recently several of the formerly FOSS infrastructure
switched to using an NC license, and they began to express that this
opened up a minefield underneath them. If it felt like a minefield with
just one or two libraries or utilities following the NC path, what will
happen once it's the whole system?

What would using Debian be like if 1/4 of the packages were under NC
licenses? Would you deploy it on your home machine? Would you deploy it
on your personal VPS? Would you deploy it at your corporate datacenter?
Even if I am a "noncommercial" user, if my VPS is at Linode, would
Linode have to pay? What about Amazon? Worse yet... *what if some of the
package authors were dead or defunct*?

To me it's no coincidence that we're seeing an interest in NC right at
exactly the same time that faith in proprietary relicensing of copyleft
code as a business strategy has begun to wane. If you were at my talk at
CopyleftConf, you may have heard me talk about this in some detail (and
some other things that aren't relevant to this right now). You can see
the original table on slide 8 from [my
presentation](https://dustycloud.org/misc/boundaries-on-network-copyleft.pdf),
but here it is reproduced:

  ------------------------------------------------------------------
                   Libre Commoner           Proprietary Relicensor
  ---------------- ------------------------ ------------------------
  **Motivation**   Protect the commons      Develop income

  **Mitigating**   Tragedy of the commons   Free rider problem

  **Wants**        Compliance               Non-compliance
  ------------------------------------------------------------------

(By "tragedy of the commons", here I mean "prevent the commons from
being eaten away".)

The difference in the "wants" field is extremely telling: the person I
will call the "libre commoner" wants everyone to be able to abide by the
terms of the license. The "propretary relicensor" actually hopes and
dreams that some people *will not* comply with the license at all,
because their business strategy depends on it. And in this part, I agree
with Rob's "No-Community" pun.

Let me be clear: I'm not arguing with the *desire* to pay developers in
this system, I'm arguing that this is a non-solution. To recap, here are
the problems with noncommercial:

-   What commercial/noncommercial are is hard to define
-   NC doesn't compose; a tower of noncommercial-licensed tools is a
    truly brittle one to audit and resolve
-   The appeal of NC is in non-compliance

Noncommercial fails in its goals and it fails the community. It sounds
nice when you think you'll be the only one on top, but it doesn't work,
and it never will.
