title: The Beginning of A Grueling Diet
date: 2022-07-13 12:30
author: Christine Lemmer-Webber
tags: food, gruel, pottage
slug: a-grueling-diet
---
Today I made a couple of posts on the social medias, I will repost
them in their entirety:

> FUCK eating "DELICIOUS" food.  I'm DONE eating delicious food.
>
> What has delicious food ever done for me?  Only made me want to eat more of it.
>
> From now on I am only eating BORING-ASS GRUELS that I can eat as much as I want of which will not be much because they are BORING -- [fediverse post](https://octodon.social/@cwebber/108640769909556965) [birdsite post](https://twitter.com/dustyweb/status/1547243546750713856)

And then:

> I am making a commitment: I will be eating nothing but boring GRUELS until the end of 2022.
>
> Hold me to it. [fediverse post](https://octodon.social/@cwebber/108640904343292509) [birdsite post](https://twitter.com/dustyweb/status/1547251755120984068)

I am hereby committing to "a grueling diet" for the rest of 2022, as
an experiment.  Here are the rules:

 - [Gruel](https://en.wikipedia.org/wiki/Gruel), [Pottage](https://en.wikipedia.org/wiki/Pottage), and soup, in unlimited quantities.  But gruel is preferred.
 - Fresh, steamed, pickled, and roasted fruit, vegetables, and tofu may be eaten in unlimited quantity.  Unsweetened baking chocolate, cottage cheese are also permitted.
 - Tea, seltzer water, milk (any kind), and coffee are fine to have.
 - Pottage (including gruel) may be adorned as deemed appropriate, but not too luxuriously.  Jams and molasses may be had for a nice breakfast or dessert, but not too much.  Generally, only adorn pottages at mealtime; pottages in-between mealtime should be eaten with as sparing of additions as possible.
 - Not meaning to be rude to visitors, guests, hosts, and other such good company, exceptions to the diet may be made when visiting, being visited, or going on dates.

I will provide followups to this post throughout the year, justifying the diet, describing how it has affected my health, putting it in historical context, providing recipes, etc.

In the meanwhile, to the rest of 2022: may that it be grueling indeed!

**Edit (2022-07-15):** Added tofu to the list of acceptible things to additionally eat.  Clarified gruel/pottage adornment advice.  Added roasting as an acceptable processing method for fruit/vegetables/tofu.
