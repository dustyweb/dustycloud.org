title: Guix package manager without "make install"
date: 2015-02-08 17:50
author: Christine Lemmer-Webber
tags: guix, package manager, foss, guile
slug: guix-package-manager-without-make-install
---
I've been interested in [Guix](https://www.gnu.org/software/guix/), the
GNU functional package manager based on Guile and drawing much from
[Nix](http://nixos.org/nix/) (though, I think, as a complete system,
Guix is much more interesting... maybe a topic of its own post). After a
great FOSDEM 2015, where amongst other things I got to speak to and hear
from the super nice Guix developers (and even communicated much about
the challenges with deploying libre webapps, a topic of one of the
presentations I gave), I decided to finally install the thing.

Trouble is, if I'm mostly interested in playing with the package manager
(there's now a complete Guix distro called the "Guix System
Distribution", but it's a bit too alpha yet, and I'd like to play around
in the system first anyhow), and I run [Debian](http://debian.org/)...
but Guix isn't yet packaged for Debian (though I would like it to be!)
But I hate... hate hate hate... doing a [make install]{.title-ref} of a
package to my system. [make uninstall]{.title-ref} is so unreliable, and
I like to keep my system as a combination of system packages (so, stuff
through apt/dpkg) and stuff I've built but keep in my user's home
directory. (Though, adding Guix to the mix now adds a third middle
ground!) Plus, what if I want to hack on Guix, it seems like being able
to have Guix use the code straight from my dev checkout is best, right?

[Dave Thompson](http://dthompson.us/) suggested that I do what he do:
run Guix on top of Debian *without* a [make install]{.title-ref},
straight from the git repo. This was indeed what I wanted to do, but it
took me a while to figure out the exact process to get that to work
(especially in getting the emacs mode to work with this), so I'm
documenting that here... maybe it can be helpful to someone else as
well?

So we're mostly going to follow the [install
docs](http://www.gnu.org/software/guix/manual/guix.html#Installation) up
until the point where we do a [make install]{.title-ref}, where we
obviously won't. So:

    # git clone the repo and cd into it
    $ git clone git://git.savannah.gnu.org/guix.git
    $ cd guix

    # Install appropriate dependencies... insert your distro package manager here
    # Not actually sure if -dev needed on libgcrypt20 or not :)
    $ sudo apt-get install guile-2.0 libgcrypt20 libgcrypt20-dev build-essential \
        guile-gnutls guile-2.0-dev libsqlite3-dev pkg-config

    # Build the package
    $ ./bootstrap && ./configure && make

    # Make the "worker users" and their group... this allows the daemon
    # to offload package building while keeping things nicely contained
    $ sudo groupadd guix-builder
    $ for i in `seq 1 10`; do
        sudo useradd -g guix-builder -G guix-builder           \
                     -d /var/empty -s `sudo which nologin`          \
                     -c "Guix build user $i" --system          \
                     guix-builder$i;
      done

    # Make the /gnu/store directory, where packages are kept/built
    $ sudo mkdir -p /gnu/store
    $ sudo chgrp guix-builder /gnu/store
    $ sudo chmod 1775 /gnu/store

(Supposedly there's a way to run things without the extra users and
[/gnu/store]{.title-ref}, but you lose a lot of benefits... personally
it seems to me that having the store and the daemon is worth it.)

Okay, you're done! Now you should be able to run guix straight from your
checkout. But you can't run it directly, you need to use the
[./pre-inst-env]{.title-ref} script which has many environment variables
set... after all, we aren't doing a make install, but guix needs to know
where all our stuff is! First let's start the daemon... it runs as root,
but don't worry, all package building happens as the guix-builder users
we set up above:

    $ sudo ./pre-inst-env guix-daemon --build-users-group=guix-builder

So, maybe leave that running in a screen session, or daemonize it, or
something, but leave it running. For now we're just testing, so
whatever.

This is optional, but you may want to "authorize" the Guix substitutes
repository. (Thanks to [mthl]{.title-ref} in [#guix]{.title-ref} on
freenode for pointing this out!) Otherwise, Guix will spend a lot of
time rebuilding things itself that have already been built elsewhere:

    $ sudo ./pre-inst-env guix archive --authorize < hydra.gnu.org.pub

Now we can actually run guix! (Maybe from another terminal?):

    # a bunch of stuff should run by... but the package will install
    $ ./pre-inst-env guix package -i hello

    $ ~/.guix-profile/bin/hello
    Hello, world!

Great! It ran. But okay... all three of the last commands were annoying.
We probably want a way to launch these much more easily, so let's add
aliases for [guix-daemon]{.title-ref} and [guix]{.title-ref}, as well as
putting our user's guix profile on our [PATH]{.title-ref}. (Yes, that's
right... different users can have different packages installed with guix
pretty easily.) So, let's edit our [\~/.bashrc]{.title-ref} and add the
following lines:

    function guix-enable() {
        # Guix stuff

        alias guix="~/devel/guix/pre-inst-env guix"
        alias guix-daemon="sudo ~/devel/guix/pre-inst-env guix-daemon --build-users-group=guix-builder"

        # add guix's bin to the path
        export PATH=$HOME/.guix-profile/bin:$PATH
        # and others
        export PYTHONPATH="$HOME/.guix-profile/lib/python3.4/site-packages"
        export GUILE_LOAD_PATH="$GUILE_LOAD_PATH:$HOME/.guix-profile/share/guile/site/2.0/"
        export GUILE_LOAD_COMPILED_PATH="$GUILE_LOAD_PATH:$HOME/.guix-profile/share/guile/site/2.0/"
    }

You can [source \~/.bashrc]{.title-ref} if you have existing terminals
open, or start new terminals / bash sessions... whatever. :) Anyway, it
should now be much easier to run things:

    $ guix-enable   # run this whenever you want to play with guix things
    $ guix package -i xeyes
    $ xeyes

Okay, great! So, now we have a reasonable hacking setup, right?

Well, assuming you want to be using Guix as [the emacs of
distros](https://www.gnu.org/software/guix/guix-fosdem-20150131.pdf),
you probably want to take advantage of [Guix's great emacs
integration](https://savannah.gnu.org/forum/forum.php?forum_id=8081).

Unfortunately, I found [the docs on this
subject](https://www.gnu.org/software/guix/manual/html_node/Emacs-Initial-Setup.html#Emacs-Initial-Setup)
frustratingly did not work with the method used above. Luckily, I got
some help on IRC, and with the following additions to my \~/.emacs,
things work:

    (add-to-list 'load-path "/home/cwebber/devel/guix/emacs")

    (setq guix-guile-program '("/home/cwebber/devel/guix/pre-inst-env" "guile"))

    (setq guix-load-path "/home/cwebber/devel/guix/emacs")

    (require 'guix-init)

Obviously, change directory names as appropriate.

Now [M-x guix-all-available-packages]{.title-ref} should work!

Hopefully I'll have more to say on why I think Guix is pretty
interesting, but at the very least, it might be compelling to hear that
Guix can be used as a sort of [language-agnostic
virtualenv](http://dthompson.us/reproducible-development-environments-with-gnu-guix.html).
Pretty cool!

Hopefully that helps someone else out there!
