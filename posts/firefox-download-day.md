title: Firefox download day is NOW
date: 2008-06-17 13:04
tags: foss, firefox
author: Christine Lemmer-Webber
slug: firefox-download-day
---
To celebrate the release of Firefox 3, the folks at Mozilla are trying
to set a world record for most downloads ever in a 24 hour time
period... aka, [download
day](http://www.spreadfirefox.com/en-US/worldrecord/firefox3)!

Well, download day just started (though their main page, at the time of
writing, still says firefox 2). But you can get it now, while its hot,
for
[Linux](http://download.mozilla.org/?product=firefox-3.0&os=linux&lang=en-US)!
[OSX](http://download.mozilla.org/?product=firefox-3.0&os=osx&lang=en-US)!
[Windows](http://download.mozilla.org/?product=firefox-3.0&os=win&lang=en-US)!

You want Firefox 3. All those memory leaks? Haven't noticed a one since
I've switched. Plus better native deaktop integration, and.. have I
mentioned the awesome bar?

Yes. [You definitely want
this.](http://people.mozilla.com/~beltzner/overview-of-firefox3.swf)

**Update:** Firefox 3 is now properly appearing on the [Mozilla
homepage](http://mozilla.com)... when it actually loads, that is. At the
time of writing, I'm hearing on IRC that downloads are between 3000-5000
downloads/minute.

**Update 2:** Oh hai, [download stats
counter](http://downloadcounter.sj.mozilla.com/)! (Currently \> 12000
downloads a minute... holy crap!)

**Update 3:** The [world record
page](http://www.spreadfirefox.com/en-US/worldrecord/) page is showing a
lot more downloads than that other page is (by like, double). I wonder
why?
