title: Miro and ChiGlug groups on Identi.ca
date: 2009-01-23 18:12
tags: pcf, autonomous, miro, foss, identica
author: Christine Lemmer-Webber
slug: identica-groups
---
I mentioned in my [last
post](http://dustycloud.org/blog/view_post/identica/) that I am now
using [Identi.ca](http://identi.ca). I'll be honest.. when I first set
it up I didn't really think I'd use it. 140 character limit? And why
have a microblog when you can just have, you know, a *blog*? And then I
started using it and realized that it was really like public instant
messaging or a global chatroom. And now I'm [totally
hooked](http://identi.ca/cwebber/).

I've been using it to send out updates on the guide by tagging those
posts with [#miroguide](http://identi.ca/tag/miroguide) so that people
doing testing can know what kind of changes are happening. Will has also
been tagging with [#miro](http://identi.ca/tag/miro) quite a bit.

A lot of people have asked me... why [Identi.ca](http://identi.ca)? Why
not [Twitter](http://twitter.com/)? Indeed, Twitter does have a
significantly bigger community. You can also sync up your identi.ca
account with you twitter account, and indeed I do know a lot of people
who have done that. But I'm simply not too interested in collecting my
information into yet another walled garden. Identi.ca runs off of
[Laconi.ca](http://laconi.ca), which is free software that anyone can
run on their own server. And the people behind it are pushing for a
standard that will allow for even further decentralization called
[OpenMicroBlogging](http://openmicroblogging.org/) (OMB). Admittedly, I
haven't read the standard yet, but what I'm hoping is that this will
allow for a setup that is closer to what we have both with email and
[XMPP/Jabber](http://en.wikipedia.org/wiki/Xmpp), where anyone can run a
server and send messages to anyone on any other server. So, at this
point there are a lot of people being snarky and responding with, "Well,
not as many people are *using* it yet, so it really doesn't matter."
Which is what people said about XMPP/Jabber in its infancy, while AIM
was the proprietary, de-facto protocol. But now XMPP is the default
standard... if you have [LiveJournal](http://livejournal.com),
[Facebook](http://facebook.com), [GMail](http://gmail.com), you already
have an XMPP account, whether you realize it or not. It's not really
clear whether it will be the case or not, but hopefully the same will be
the case with OMB.

I still think that the 140 character limit is a bit short. Though I
could be wrong on that. I still think it would be better if it would be
a bit longer, and if it were possible to use named links instead of
having to resort to third party servers like [bit.ly](http://bit.ly) and
[tinyurl.com](http://tinyurl.com) (this is the *web* after all, and
almost any decent communications platform (including xmpp) supports web
links). I think the biggest problem is that it might break twitter
compatibility, but I guess that really doesn't bother me. If we're
thinking of microblogging as like a public chat room, we can have a bit
of a higher character limit and still be reasonable. But those are minor
complaints.

So today identi.ca released a brand new version of the site running off
of a new version of laconi.ca. It looks good, has a few bugs, but most
importantly, it now supports *groups*. Think of it this way: a !group is
like a subscribable #tag or a magical collective \@person. If you mark a
post with your !group, and all subscribers of the group will get that
message, regardless of whether they are subscribed to you specifically.
It's a great feature, and if I understand correctly, one people have
been wishing for for a while... even in twitter land. ;)

So with that in mind, I set up a few new groups...
[!miro](http://identi.ca/group/miro) (which three of the miro developers
have joined at this point already),
[!chiglug](http://identi.ca/group/chiglug) for the local [Chicago
GNU/Linux User Group](http://chicagolug.org), and of course I had to set
up a [!blender](http://identi.ca/group/blender) group. Group avatars
aren't working yet apparently, but I'll upload them once they do. Well,
what are you waiting for? Sign up! :)
