title: June 2012 Life Update (part 1)
date: 2012-06-10 16:30
author: Christine Lemmer-Webber
tags: personal, life update
slug: june-2012-life-update
---
There's a ton of things going on in my life right now. A *ton* of
things! Enough that it's been hard to do any blogging lately because
I've been just so damned busy. And in the next few days there are some
new things to announce as well, so I thought this would be a good
opportunity to dequeue a bit before I make any further announcements.

# MediaGoblin

[![Crop of mediagoblin.org screenshot](/etc/images/blog/mediagoblin_homepage_crop.png)](http://mediagoblin.org/)

First, let's start with MediaGoblin. We've passed our one year
anniversary, and I feel we have an impressive set of things to look back
on. Already we've surpassed my wildest expectations in a large set of
ways: we have not only images support, but also
[video](http://mediagoblin.org/news/mediagoblin-0.2.0-our-tubes.html)
and
[audio](http://mediagoblin.org/news/mediagoblin-0.3.0-rise-of-the-robogoblins.html)
support. We have OpenStreetMap support. We did a huge overhaul switching
from MongoDB to SQL(Alchemy). (Okay, maybe that's not an interesting
accomplishment generally, but it was a huge undertaking.) On the horizon
are several other things, including plugin support, theming support, and
hopefully federation stuff. If you're interested in such things,
consider subscribing to the [MediaGoblin news
feed](http://mediagoblin.org/news/index.xml).

There's been a number of good articles about MediaGoblin recently, but
here are two really good ones:

> -   [MediaGoblin interview with me on
>     OpenSource.com](http://opensource.com/life/12/5/mediagoblin)
> -   [MediaGoblin's One Year Anniversary: What's
>     Next?](https://www.linux.com/learn/tutorials/572812-mediagoblins-one-year-anniversary-whats-next)
>     (another interview with me)

We've a bunch of cool things, and a really awesome community. We have
somewhere around 50 people who have contributed to the project over the
last year. I think that's really impressive for a new project.

And that's actually been one of the big themes of the last year:
discovering I actually do have some fairly good leadership skills. Up
until now I've mostly assumed I'm way too disorganized and distractable
of a person to be a good leader. But I feel like the last year has
proven me wrong. In fact, I feel that my long history of learning
techniques to deal with my ADD in a sense has helped: I know how to
write out plans for things in orgmode, break them into small tasks, and
execute them. Fears of me not being capable to stay focused when working
from home actually helped me learn to have very rigorous work habits.
And having those traits has helped a lot.

Anyway, the MediaGoblin community is an *amazing* group of people, and
I'm so happy to be working on the project. A [little over a year
ago](http://dustycloud.org/blog/2011/5/5/gnu-mediagoblin.html) I said:

> We're still kind of running on hopes and dreams here, and hopefully
> those hopes and dreams can become true. It'll be a lot of hard work,
> but I think we can do it. At the very least, we need to try!

That statement is still true to some extent, but we're running on more
than just hopes and dreams at this point. We have some real sites
running the software (but we could use some more) and more importantly
we have more than just an idea of a codebase, we have something very
solid. It's been a thrilling year, and I'm looking forward to the next
one.

# Work at Creative Commons (generally)

This year at Creative Commons has been an interesting one, and somewhat
of a mixed one. About a year ago there were some changes in the org and
a number of people I care about have left. This includes my former boss,
[Nathan Yergler](http://yergler.net/), and more slowly the [stepping
back](http://creativecommons.org/weblog/entry/32601) of [Mike
Linksvayer](http://gondwanaland.com/), among others. Both of these
people especially have been both friends and mentors, and it has been
sad to see them go.

On the other side, the last year has been interesting in some other
ways. Especially, I've been glad to see some other great people join,
including [Greg Grossmeier](http://grossmeier.net/) and my good friend
[Lunpa](http://lunpa.org/). These are both people I greatly admire, and
over the last year I've seen them do some great things.

There has been another interesting thing too: in the void since Nathan
Yergler left, I became the somehow official and yet unofficial tech team
manager/representative. So, in addition to MediaGoblin, here is another
space where I've learned quite a bit about leadership, including the
fact that I seem to be decently competent at doing it. While I have done
some programming work over the last year (particularly around
simplifying CC's er... complex... translation tooling), I have more and
more spent time on representation and planning. This was an unexpected
shift for me, but one I'm glad I've had the opportunity to do. I feel
I've had a lot of growth over the last year, especially in the areas of
planning and "workplace diplomacy". I think that's one of the big
lessons I've learned this year: the best way to think of yourself in a
leadership position is that of a diplomat. There are usually a lot of
things people want out of a situation... it's your job to identify what
makes sense and to negotiate things, be a buffer, and help people do
good. I wouldn't say I've been a perfect manager in this position, but I
think overall things have been good, and I'm proud of the results I have
had in the er... quasi-position.

[![Lunpa's new chooser mockup](/etc/images/blog/cc_new_chooser_mockup.png)](http://wiki.creativecommons.org/Interactive_license_chooser)

*Interactive chooser mockup by Lunpa, not me.*

Some other interesting things have happened over the last year as well.
Part of these things are the projects that we are rolling out. One thing
is a [new, interactive
chooser](http://wiki.creativecommons.org/Interactive_license_chooser).
We also have a [legalcode errata
tool](http://wiki.creativecommons.org/4.0_License_Errata_Annotation_Tool)
ready to go, we just need sign-off on some information. Anyway, there
have been other things too, but I'm particularly proud of the work the
tech team has done, especially [Lunpa](http://lunpa.org/), on this work.
And it has been great to watch and help with... even though that help
has been partly from a different, more manager-y perspective than I am
used to.

Another interesting thing that has happened is that I've been more
involved with license and legalcode work, especially in the [4.0 license
process](http://wiki.creativecommons.org/4.0). But things actually
started with the work to declare [CC0 and GPL to be compatible and CC0
to be acceptable for free software
usage](http://creativecommons.org/weblog/entry/27081) (sadly, but
possibly for the best, we [submitted for OSI
approval](http://projects.opensource.org/pipermail/license-review/2012-February/000092.html)
then later
[withdrew](http://projects.opensource.org/pipermail/license-review/2012-February/000271.html)
for reasons having to do with patents, equitable estoppel, and really
long mailing list threads that you can read if you care to). I have been
involved in several things, including trying to raise the visibility of
[complexities involving games, 3d printing, and functional
content](http://wiki.creativecommons.org/4.0/Games_3d_printing_and_functional_content).
There were a lot of points explained through that, but the most
interesting bit is perhaps the issue of what happens in complex examples
where functional and creative works are combined in particular and
complex ways, and the former uses the GPL and the latter uses CC BY-SA,
and there's an unintended incompatibility of copyleft licenses. This has
lead to
[discussions](http://lists.ibiblio.org/pipermail/cc-community/2012-May/007019.html)
between Creative Commons, the Free Software Foundation, and the Software
Freedom Law Center about possible compatibility with CC BY\[-SA\] 4.0
and the GPL. Nothing is resolved at this time, but I am thrilled that
this conversation is moving forward. I often comment that one
semi-official role of the CC Tech Team is to be a bridge back to free
and open source software, and this is probably an even more clear and
direct example than most.

Anyway, as is probably evident above, it has been a mixed year, but a
very interesting one that I'm glad to have been part of. And anyway,
there's one more thing I haven't discussed in the above...

# Liberated Pixel Cup

[![Liberated Pixel Cup homepage screenshot crop](/etc/images/blog/lpc_homepage_crop.png)](http://lpc.opengameart.org/)

[Liberated Pixel Cup](http://lpc.opengameart.org)! This project is in
some ways simultaneously the most "unnecessary" appearing of projects
I've worked on recently, especially at CC, and yet I think it is
actually maybe one of the most important.

I guess I ought to say what Liberated Pixel Cup is. In a sense, visiting
the site should probably tell you most anything you need to know, but
here's a repeat of the tagline:

> Liberated Pixel Cup is a two-part competition: make a bunch of awesome
> free culture licensed artwork, and then program a bunch of free
> software games that use it.
>
> Liberated Pixel Cup brings together some powerful allies: Creative
> Commons, Mozilla, OpenGameArt, the Free Software Foundation, and you.

In a sense, merely bringing together these organizations on a common
purpose feels like a huge victory for me: they're organizations that
really agree on a lot of things but simply don't seem to speak to each
other much. But even more significantly it brings together several
*movements* in a tangible way: games are by necessity an overlap of
culture and functionality, and in this way free software and free
culture are directly intertwined.

The art portion of the contest is now under-way, but there was a
"pre-contest" part of the game in which we ended up working out a style
guide and set of base assets. I really encourage you to check out the
style guide, assets, and mini "demo game":

[![Liberated Pixel Cup styleguide crop](/etc/images/blog/lpc_styleguide_crop.png)](http://lpc.opengameart.org/static/lpc-style-guide/index.html)

Anyway, Liberated Pixel Cup is kind of my baby (well, I guess so is
MediaGoblin) and it's great to see it moving along so well. The art
contest has kicked off and we're already [seeing some awesome
entries](http://opengameart.org/forumtopic/progress-pic-thread-show-us-what-youre-working-on).

I have a lot more to say on Liberated Pixel Cup as to the what and why,
but I think this blogpost is already getting a bit wrong, so you can
read [this interview with me about Liberated Pixel Cup on
opensource.com](http://opensource.com/life/12/4/liberated-pixel-cup-proving-potential-free-culture-and-free-software-game-development)
instead. Hopefully I'll get to writing that a bit more soon. The
rationale might not be 100% self-evident, but I'm hoping the awesomeness
of things even so far is. :)

# Moving and my relationship

Another thing going on in my life is actually really more about my
spouse, Morgan. I'm happy to say that she finished her Masters program
here at NIU and we'll soon be moving from [teh
Deklabbs](http://dustycloud.org/blog/moved-to-dekalb) to Madison (where
Morgan will work on her PHD) in mid-August.

Not much to say on that other than that I'm very proud of Morgan. We
also hit our three year anniversary recently, so that's awesome.

# Other things

There are a few other things going on in life. I want to wrap up this
blogpost so here's a quick rundown:

> -   I volunteered at PyCon on the video team again this year.
> -   I [gave a bunch of MediaGoblin talks and ran a
>     sprint](http://mediagoblin.org/news/very-busy-goblins.html) and
>     will be [presenting at OSCON as
>     well](http://www.oscon.com/oscon2012/public/schedule/detail/23987).
> -   [Tube](http://urchn.org/) successfully met and beat out its
>     [kickstarter fundraising
>     campaign](http://www.kickstarter.com/projects/1331941187/the-tube-open-movie)!
>     Okay... I'm not really involved in this anymore, but I'm proud of
>     Bassam, Fateh, and crew. Before starting on MediaGoblin I was
>     volunteering on some python scripting on this though, and there's
>     a small shout-out to my scripting work in [Bassam's Libre Graphics
>     Meeting
>     talk](http://www.kickstarter.com/projects/1331941187/the-tube-open-movie/posts/221090)
>     ([webm version here](http://urchn.org/lgm/lgm_tube_blender.webm)).
>     That's not trying to claim any sort of credit; my participation in
>     Tube has honestly been minimal, but I'm proud to see it move
>     forward.
> -   Interestingly enough I've been mentioned in the last couple of
>     [FaiF](http://faif.us/) episodes; specifically my [field guide to
>     copyleft](/blog/field-guide-to-copyleft) blogpost in [Fontana's
>     talk](http://faif.us/cast/2012/may/22/0x29/), and vaguely in the
>     interview with [Deb Nicholson of
>     OIN](http://faif.us/cast/2012/jun/05/0x2B/) (who is actually a
>     MediaGoblin co-conspirator, and a great speaker, so you should
>     listen to that episode), but mostly indicating that I'll be on in
>     a future episode there. So, I guess that will happen.

I think there's more, but honestly I'm totally exhausted typing this all
out, so I think that'll have to be it. But life over the last year has
been a lot of things, but boring is not one of them. And the next year
looks like it will be even more interesting, as you'll hear in the next
couple of days... so keep your eye here.
