title: Another programmer proposal
date: 2008-04-15 13:10
author: Christine Lemmer-Webber
slug: another-programmer-proposal
---
A bunch of people have sent me links to an article about a programmer
who [proposed to his girlfriend through a video
game](http://www.chicagotribune.com/news/nationworld/sns-ap-odd-programmer-proposal,1,6878396.story),
and have noted the similarity to my own proposal using PyStage.

It's not quite the same thing. The system I wrote was an animation
system I wrote from scratch, and not a game. This other programmer
hacked a pre-existing game. Both are interesting feats in their own
ways. (Interestingly enough, the original plan was for me to propose
with a full game, but then I realized that the cutscene was complex
enough.)

Still, there are certainly similarities in the nerdiness of the
proposals, but they're not really surprising. Good programmers take
pride in the nerdiness and cleverness of programming, and what better
way to propose than to make full use of your craft?
