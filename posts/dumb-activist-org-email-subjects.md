title: Activist/political orgs: stop using stupid email subjects
date: 2014-12-06 13:35
author: Christine Lemmer-Webber
tags: email
slug: dumb-activist-org-email-subjects
---
I love nonprofits and activist orgs, and I think political campaigns
*should* send emails to their base asking for support.

That said, for the last few years, I've gotten angry at nearly every
email subject I've gotten from activist organizations. You've probably
gotten the same, but I've collected some samples:

-   re: The site crashed!
-   Um...
-   re: Un-freaking-believable
-   Argh!
-   Please please please
-   Forward this!
-   Forward this →
-   This is dangerous:
-   Holy moly!!!
-   re: Corrupt?
-   re: Comcast *(like a thousand of these)*
-   Pancakes and syrup *(yes, a real subject sent for
    non-breakfast-invitation reasons)*

And the list goes on, and on. My "Political Campaigns" folder is a joke.

Apparently this [started in Obama's re-election
campaign](http://abcnews.go.com/blogs/politics/2012/11/odd-obama-email-subject-lines-drew-huge-cash/)
and was found to be successful in terms of getting both eyeballs and
dollars.

Is it still successful? If it was a novel trick, it isn't anymore.
What's worse, these subject lines are awfully similar to two groups of
emails I hate to get: the classic and loathed "ha ha forward this to
friends" email from relatives, and spam. Which means many of these I
automatically mark as read or accidentally misfile as spam.

But I'm tired of getting these emails. If you're an activist org, and
you send an email to me like this, I'm unsubscribing. And as immature as
it might be to say, I'm unlikely to donate to your campaign again,
assuming I ever did.

Political/activist orgs: don't treat me like an idiot. Prove my brain
energy is worth your time.
