title: Doored, and Avoiding Doors
date: 2008-04-23 23:20
author: Christine Lemmer-Webber
slug: doored-and-avoiding-doors
---
So on my second day of biking in Chicago, I was
[doored](http://www.urbandictionary.com/define.php?term=doored) for the
first (and hopefully last) time. It wasn't that bad in my case... I saw
the door opening, yelled "Hey!", and both braked and swerved out of the
way. The driver was very apologetic, and I told him not to worry too
much as at least nothing bad happened, but that he should be more
careful. Recovering from his shock and embarrassment he said, "Man! I
thought that stuff only happened in the movies!" I found his comment
funny, but the whole situation could have been very tragic; I got away
with nothing but a few bruises on the knuckles (which bounced against
the edge of the door), but there was a bus about ten feet behind me. If
it were closer when I swerved, things could have been much worse.

But perhaps this situation is all for the best as it has served as
somewhat of a wake up call. Since then I've done a lot of research both
on how to avoid being doored and on bike safety in general, with a lot
of help from some bike-savvy friends. I now know that you should always
keep yourself at least three feet away from all car doors to avoid being
hit (four is better, five is ideal). I've also found a [nice
essay](http://www-personal.umich.edu/~riin/bikes/doorzone.htm) on the
subject (including some examples of people who have died or been
seriously injured by being doored) as well as
[advice](http://www.cityofchicago.org/Transportation/bikemap/doorzone.html)
from the City of Chicago. These are good reads, but nothing compares to
[Bicycle Safe: How to Not Get Hit by Cars](http://bicyclesafe.com/).
Seriously, it's a *must read* for any urban biker.

Anyway, I'm glad that I'm biking a bit better informed. But I'm
definitely going to keep my alertness level at its highest and stick to
neighborhood roads. My sister just told me about how she was a passenger
in a car when the driver parked and doored a bicyclist, knocking out the
guy's full set of teeth. To be honest, I don't take the best care of my
teeth, but I sure would hate to lose them all at once.
