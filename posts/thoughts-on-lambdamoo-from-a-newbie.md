title: Thoughts on LambdaMOO from a newbie
date: 2015-02-22 21:05
author: Christine Lemmer-Webber
tags: mud, moo, lambdamoo, game
slug: thoughts-on-lambdamoo-from-a-newbie
---
While working on [MediaGoblin's](http://mediagoblin.org) upcoming
release, and (dare I say) feeling a bit bored and anxious and frustrated
about the whole process, I decided that maybe it was time to look into
some sort of entertainment. But for the most part, I feel like I've
*played* all the interesting free software games out there. I've mostly
beaten even the games of my old proprietary console using past. What to
do?

I wondered what the state of good ol fashioned
[MUD](http://en.wikipedia.org/wiki/MUD) systems are. I've had dreams for
years of building such a system from scratch (hence my [interest in the
actor model](http://xudd.readthedocs.org/)). Answer: they feel like
they're in the same place they were around 2000. But maybe this isn't
bad? I tried playing some dungeon crawling'y MUDs but found myself a bit
bored. But having remembered that [Rob Myers](http://robyers.org) has
[talked plenty about
LambdaMOO](http://robmyers.org/category/lambdamoo/), so that seems like
good enough reason to connect and look around. (Note, [connecting is not
hard](http://robmyers.org/category/lambdamoo/)! I am using
[Mudlet](http://www.mudlet.org/) at the moment.)

So, in-between actual coding and fretting about this and that, I've been
getting in some downtime, poking around the LambdaMOO environment. And
at first I was fairly unimpressed... this thing is what, 20 rooms big?
What can I *do* here? But I've been finding some interesting things:

-   That LambdaMOO is both the software and one particular server is
    fairly confusing, but no matter: both are of interesting
    construction. They're both also very old, 25 years old at this point
    (egads), but particularly as to the world, that leads to much depth.
-   The main rooms of the house (see [help geography]{.title-ref}) are
    not as interesting as the places they lead to. You can enter a model
    railroad and actually walk around that imaginary city and end up in
    some interesting places, reading graffiti off of nightclub walls,
    etc. Find an old teletype and type on it, or play one of the
    functioning (!) board games sitting around. A dusty hallway leads to
    a full hack and slash style game engine, coded live inside the
    system itself.
-   Most of the world is not really a dungeon crawler, though there is
    one hidden in it. Most of walking around LambdaMOO is exploring the
    many curiosities that users have provided there.
-   Speaking of coding live inside the system, what's *really*
    interesting about LambdaMOO is that you're really manipulating [a
    big
    database](http://www.hayseed.net/MOO/manuals/LambdaCoreUserMan.html),
    and that many features are [live coded from within the game
    itself](http://www.hayseed.net/MOO/manuals/ProgrammersManual.html).
    Everyone can experiment with adding rooms, though not everyone can
    program, though you may be able to get help from a certain buffer in
    the smoking room, if you can find it. (I just got access... I'll
    have to try things.)
-   LambdaMOO is best known for a [famous
    essay](http://en.wikipedia.org/wiki/A_Rape_in_Cyberspace), but I
    think that essay is maybe not as interesting as it was in the 90s
    when online experiences were becoming a new thing? Still, the effect
    of the essay and the event it chronicles is still clear, including
    the way the world is introduced to you.
-   Interesting to think of LambdaMOO in case of copyright and
    licensing, of which... well there seems to be no clear answer what
    is going on, but sharing seems not impeded by it.
-   Being from the early 90s, it's amazing how much is here to explore,
    and how everything still lives. Since most of the world is pretty
    "low burn", that's nice for me, because I can just type in a command
    to look around somewhere every once in a rare while and leave
    LambdaMOO open in the background.
-   I'm enjoying exploring the world, but I sense that LambdaMOO had a
    heyday, and while there are users connected, the sheer size of it vs
    the volume of connected users gives me the sense that today is not
    it. There's a certain amount of feeling of exploring a secret city,
    which is wonderful and sad. (It would be nice to have some friends
    who were interested in exploring with me, maybe I should cajole some
    into joining me.) It's an interesting piece of internet history,
    still here to explore, but since it's in a "living" database, what
    will happen when it shuts down? There won't be any Internet Archive
    to preserve the LambdaMOO memories, I fear. But maybe nothing is
    forever, anyway.

I have more, maybe crazier, thoughts on MUDs and maybe a way they could
be more useful in this modern world. May I'll explore soon, but I think
that the popularity of MUDs has diminished with World of Warcraft and
Skyrim and the like, but maybe they have a place still if we can do some
new things with them? And those are fairly hack-and-slash type systems,
maybe MOO/MUD/MUSH systems have more to offer than popular MMO systems
provide? Roguelikes seem more popular than ever, if anything... things
can come back... maybe new interfaces are needed?...

In the meanwhile, if you drop by LambdaMOO, send me a message! I'm
"paroneayea" there.
