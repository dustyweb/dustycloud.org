title: Life Update: January 2015
date: 2015-01-13 22:45
author: Christine Lemmer-Webber
tags: life update, personal
slug: life-update-january-2015
---
Hey, so where did 2014 go, amirite guys? Amirite??

2014 in bulleted list form:

-   Most stressful year of my life (and that includes the years I both
    worked full time and went to school full time), but not bad: high
    peaks and low valleys even out to a good year, but turbulent. Not
    just high and low events contributing to stress, also much stress
    has been ambient from ongoing and difficult events, but much of it
    not really befitting describing on this blog.

-   [MediaGoblin campaign](http://mediagoblin.org/pages/campaign.html)
    went well, but I am tired of doing crowdfunding campaigns. Probably
    the last I will ever run... or at least the last for a long while.
    Nearly 5 months from start to wrapup of 60 hour high-stress weeks.
    But again, it went well! And hey, that video came out pretty great.

-   [Hiring
    Jessica](http://mediagoblin.org/news/welcome-jessica-tallon.html)
    was the smartest move we could have made, and I'm glad we made it.

-   MediaGoblin federation work is going well; next release should make
    that clearer I hope.

-   Both Jessica and I are on the [W3C Social Working
    Group](https://www.w3.org/wiki/Socialwg) trying to standardize
    federation, and I'm excited about that.

-   Hiring Jessica is great, but what to do about my own income? Happy
    to say I've started contracting for [Open Tech
    Strategies](http://opentechstrategies.com/) who are great. Working
    under [Karl Fogel](http://www.red-bean.com/kfogel/) is wonderful,
    and so is contracting for an org that mandates all code written
    there be free software. Plus, contracting 10 hours a week means I
    have plenty of other (including MediaGoblin) time left over.

-   Also it's great to have a boss again who is reviewing my performance
    and seems happy with my work, especially when that boss is almost
    certainly more technically capable than I am; I forgot how much
    external affirmation from that is helpful in probably some base
    human way. I had some [great](http://yergler.net/)
    [bosses](http://gondwanaland.com/) in the not too distant past, and
    while I think I'm a pretty decent boss to others, Morgan has pointed
    out that I am a really mean boss to myself.

-   Despite ambient stress for both of us, Morgan and I's relationship
    goes well, maybe this year better than ever.

-   Got nerdier, started playing tabletop role playing games with
    friends a lot. Board games too.

-   Living in Madison is good.

-   We are currently caring for a dog since another family member can't
    keep her where she is staying. Aside from temporary dog-sitting,
    I've never lived somewhere with a dog that I am caring for... it's
    interesting.

-   The first half of the year was crazy due to the MediaGoblin campaign
    (again, I think it went great, and I had lots of help from great
    friends, just stressful), the second half crazy due to... well, a
    pile of things that are too personal for a blog (yeah I know I
    already said that). But everything came to a head right at the end
    of the year. This year burnt me the hell out.

-   This made me pretty useless in December, and makes me feel terrible
    because I pushed vocally for a MediaGoblin release and failed to
    hold up my end of things to make it happen. I need to get back on
    track. This will happen, but in the meanwhile, I feel shitty.

-   Burnout recovery has been productive; an odd thing to say maybe, but
    I seem to be getting a lot done, but not always on the things I
    think I should be.

-   I feel more confident in myself as a programmer than before... I've
    always felt a large amount of impostor syndrome because I don't
    really have a computer science degree... I'm a community trained
    hacker (not to knock that, but it's hard to not feel insecure
    because of it).

    But this year I did some cool things, including getting patches in
    to a [fun language](http://hy.readthedocs.org/en/latest/), and I
    worked on [an actor model
    system](http://xudd.readthedocs.org/en/latest/) that I think has a
    hell of a lot of promise if I could just get the damned time for it.
    (If only I had time to solve error propagation and the
    inter-hive-communication demos...) I did every exercise in [The
    Little Schemer](http://www.ccs.neu.edu/home/matthias/BTLS/) (a real
    joy to work through) and I feel like hey, I finally understand how
    to write code recursively in a way that feels *natural*. And it
    turns out there is a [MELPA-installable texinfo
    version](http://melpa.org/#/sicp) of
    [SICP](https://mitpress.mit.edu/sicp/full-text/book/book-Z-H-4.html)
    and I've been slowly working my way through it when I'm too tired to
    do anything else but want to pretend to be productive (which has
    been a lot of the last month). Still so much to learn though, but I
    appreciate the bottomless well aspect of programming.

-   Aside from the MediaGoblin campaign video, not a lot of artwork done
    this year. Hrm.

-   A couple of friends this year have made the "I've been doing nothing
    but python webdev for years and I need to mix it up" and to those
    friends: I hear you. Maybe hence the above?

-   Aside from MediaGoblin I've been doing a lot more chipping away at
    tiny bits of some free software projects, but maybe nothing
    significant enough to blog about yet, but there's a deployment
    system in there and a game thing and some other stuff. Nothing
    MediaGoblin sized, though. (Whew!)

-   Enjoying learning functional reactive programming (and expanding my
    understanding of Scheme) with [Sly](https://gitorious.org/sly).
    Unfortunately still under-documented, but it's getting better, and
    [davexunit](http://dthompson.us/) is answering lots of questions for
    me. I might write a tutorial, or tutorials, soon.

-   Another ascii art logo for a FOSS project I made got vectorized and
    made way better than my original version, but that deserves its own
    post.

-   I continue to be able to work on free software full time, which is
    great.

I feel like the start of 2015 has already been moving upward, and has
been much less stressful than the end of 2014. I hope that curve can
keep moving up. And I hope I can keep up what I feel, despite me nearly
going insane from various facets of it, is a fairly productive life.
