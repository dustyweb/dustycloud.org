title: State of Spritely for February 2020
date: 2020-02-10 15:30
author: Christine Lemmer-Webber
tags: spritely
slug: state-of-spritely-2020-02
---

We are now approximately 50% of the way through the 
[Samsung Stack Zero grant](/blog/samsung-stack-zero-grant/) for Spritely,
and only a few months more since I
[announced the Spritely project](https://dustycloud.org/blog/spritely/)
at all.
I thought this would be a good opportunity to review what has happened
so far and what's on the way.

In my view, quite a lot has happened over the course of the last year:

 - [Datashards](https://datashards.net/) grew out of two Spritely
   projects, [Magenc](https://gitlab.com/dustyweb/magenc/blob/master/magenc/scribblings/intro.org)
   and [Crystal](https://gitlab.com/spritely/crystal/blob/master/crystal/scribblings/intro.org).
   This provides the "secure storage layer" for the system, and by
   moving into Datashards has even become its own project (now mostly
   under the maintainership of Serge Wroclawski, who as it turns out is
   also co-host with me of [Libre Lounge](https://librelounge.org/).
   There's external interest in this from the rest of the federated
   social web, and it was a topic of discussion in
   [the last meeting](https://socialhub.activitypub.rocks/t/2-8-socialcg-telecon/507)
   of the [SocialCG](https://www.w3.org/wiki/SocialCG).
   While not as publicly visible recently, the project is indeed
   active; I am currently helping advise and assist Serge with some of
   the ongoing work on optimizations for smaller files, fixing the
   manifest format to permit larger files, and a more robust HTTP API
   for stores/registries.
   (Thank you Serge also for taking on a large portion of this work
   and responsibility!)

 - [Spritely Goblins](https://gitlab.com/spritely/goblins), the actor
   model layer of Spritely, continues its development.
   We are now up to release [v0.5](https://gitlab.com/spritely/goblins/commit/9deede936e97ae7eeaa614e05071e7bd2de446c0).
   I don't consider the API to be stable, but it is stabilizing.
   In particular, the object/update model, the synchronous
   communication layer, and the transactional update support are
   all very close to stable.
   Asynchronous programming mostly works but has a few bugs I need
   to work out, and the distributed programming environment design
   is coming together enough where I expect to be able to demo it soon.

 - In addition, I have finally started to
   [write docs for Spritely Goblins](https://dustycloud.org/tmp/goblins-draft.html).
   I think the tutorial above is fairly nice, and I've had a good amount
   of review from various parties, and those who have tried it seem to think
   it is fairly nice.
   (Please be advised that it requires working with the
   [dev branch of Goblins](https://gitlab.com/spritely/goblins/-/tree/dev)
   at the time of writing.)
   v0.6 should the first release to have documentation after the major
   overhaul I did last summer (effectively an entire rewrite of the system,
   including many changes to the design after doing
   [research into ocap practices](http://www.erights.org/talks/thesis/)).
   I cannot recommend that anyone else write production-level code using the
   system yet, but I hope that by the summer things will have congealed
   enough that this will change.

 - I have made a couple of publicly visible demos of Goblins' design.
   Weirdly enough all of these have involved ascii art.

    - The proto-version was the
      [Let's Just Be Weird Together](http://dustycloud.org/blog/lets-just-be-weird-together/)
      demo.
      Actually it's a bit strange to say this because the LJBWT demo didn't
      use Goblins, it used a library called [DOS/HURD](https://gitlab.com/dustyweb/dos-hurd).
      However, writing this library (and adapting it from [DOS/Win](https://docs.racket-lang.org/dos/win.html))
      directly informed the
      [rewrite of Goblins, Goblinoid](https://gitlab.com/spritely/goblinoid)
      which eventually became Goblins itself, replacing all the old code.
      This is why I advocate demo-driven-development: the right design
      of an architecture flows out of a demo of it.
      (Oh yeah, and uh, it also allowed me to make a present for my 10th wedding
      anniversary, too.)
 
    - Continuing in a similar vein, I made the
      ["Season's Greetings" postcard](https://dustycloud.org/blog/conservancy-card/),
      which [Software Freedom Conservancy](https://sfconservancy.org/)
      actually used in their funding campaign this year.
      This snowy scene used the new rewrite of Goblins and allowed me
      to try to push the new "become" feature of Goblins to its limit
      (the third principle of [actor model semantics](https://en.wikipedia.org/wiki/Actor_model#Fundamental_concepts),
      taken very literally).
      It wasn't really obvious to anyone else that this was using Goblins
      in any interesting way, but I'll say that writing this really allowed
      me to congeal many things about the update layer and it also lead to
      uncovering a performance problem, leading to a 10x speedup.
      Having written this demo, I was starting to get the hang of
      things in the Goblins synchronous layer.
 
    - Finally there was the
      [Terminal Phase demo](https://gitlab.com/dustyweb/terminal-phase).
      (See the [prototype announcement blogpost](/blog/terminal-phase-prototype/) and
      the [1.0 announcement](/blog/terminal-phase-1.0/).)
      This was originally designed as a
      [reward for donors for hitting $500/mo](https://www.patreon.com/posts/terminal-phase-31308473)
      on
      [my Patreon account](https://www.patreon.com/cwebber)
      (you can still
      [show up in the credits](https://dustycloud.org/gfx/goodies/terminal-phase-credits-2020-01-19.gif)
      by donating!),
      though once 1.0 made it out the door it seems like it raised considerable
      excitement
      [on the r/linux subreddit](https://www.reddit.com/r/linux/comments/erh851/i_made_a_space_shooter_that_runs_in_your_terminal/)
      and
      [on Hacker News](https://news.ycombinator.com/item?id=22095092),
      which was nice to see.
      Terminal Phase helped me finish testing and gaining confidence in the
      transactional object-update and synchronous call semantics of Spritely Goblins,
      and I now have no doubt that this layer has a good design.
      But I think Terminal Phase was the first time that *other people* could
      see why Spritely Goblins was exciting, especially once I showed off the
      [time travel debugging in Terminal Phase demo](https://dustycloud.org/blog/goblins-time-travel-micropreview/).
      That last post lead people to finally start pinging me asking
      "when can I use Spritely Goblins"?
      That's good... I'm glad it's obvious now that Goblins is doing something
      interesting (though the most interesting things are yet to be demo'ed).

 - I participated in, keynoted, and drummed up enthusiasm for
   [ActivityPub Conference 2019](/blog/activitypub-conf-2019/).
   (I didn't organize though, that was
   [Morgan Lemmer-Webber](https://mlemmer.org)'s doing,
   alongside [Sebastian Lasse](https://mastodon.social/@sl007) and with
   [DeeAnn Little](https://mastodon.social/@chillicampari) organizing the
   video recording.)
   We had a great [speaker list](http://dustycloud.org/blog/activitypub-conf-2019-speakers/)
   and even got [Mark S. Miller to keynote](http://dustycloud.org/blog/mark-miller-at-apconf-2019/).
   [Videos of the event](https://conf.tube/video-channels/apconf_channel/videos)
   are also available.
   While that event was obviously much bigger than Spritely, the engagement
   of the ActivityPub community is obviously important for its success.

 - Relatedly, I continue to co-chair [the SocialCG](https://www.w3.org/wiki/SocialCG)
   but [Nightpool](https://cybre.space/@nightpool) has joined as co-chair
   which should relieve some pressure there, as I was a bit too overloaded
   to be able to handle this all on my own.
   The addition of the [SocialHub community forum](https://socialhub.activitypub.rocks/)
   has also allowed the ActivityPub community to be able to coordinate in a way
   that does not rely on me being a blocker.
   Again, not Spritely related directly, but the health of the
   ActivityPub community is important to Spritely's success.

 - At Rebooting Web of Trust I coordinated with a number of contributors
   (including Mark Miller) on sketching out plans for
   [secure UI designs](https://github.com/cwebber/rwot9-prague/blob/secure-uis/draft-documents/secure-user-interfaces.md).
   Sadly the paper is incomplete but has given me the framework for
   understanding the necessary UI components for when we get to the
   social network layer of Spritely.

 - Further along the lines of sketching out the desiderata of
   federated social networks, I have written a nearly-complete
   [OcapPub: towards networks of consent](https://gitlab.com/spritely/ocappub/blob/master/README.org).
   However, there are still some details to be figured out; I have
   been hammering them out on the [cap-talk mailing list](https://groups.google.com/forum/#!forum/cap-talk)
   (see [this post laying out a very ocappub-like design](https://groups.google.com/forum/#!topic/cap-talk/icey8aO5ABo)
   with some known problems, and then
   [this analysis](https://groups.google.com/d/msg/cap-talk/5Q8BM3aW0Gw/lHzTgXaQAgAJ)).
   The ocap community has thankfully been very willing to participate
   in working with me to hammer out the right security foundations,
   and I think we're close to the right design details.
   Of course, the proof of the pudding is in the demo, which has yet
   to be written.

Okay, so I hope I've convinced you that a lot has happened, and
hopefully you feel that I am using my time reasonably well.
But there is much, much, much ahead for Spritely to succeed in its goals.
So, what's next?

 - I need to finish cleaning up the Goblins documentation and do a
   v0.6 release with it included.
   At that point I can start recommending some brave souls to use it for
   some simple applications.

 - A demo of Spritely Goblins working in a primarily asynchronous
   environment.
   This might simply be a port of [mudsync](https://notabug.org/cwebber/mudsync/)
   as a first step.
   ([Recorded demo of mudsync](https://archive.org/details/feb_2017-live_network_coding_8sync)
   from a few years ago.)
   I'm not actually sure.
   The goal of this isn't to be the "right" social network design (not
   full OcapPub), just to test the async behaviors of Spritely Goblins.
   Like the synchronous demos that have already been done, the purpose of
   this is to congeal and ensure the quality of the async primitives.
   I expect this and the previous bullet point to be done within the next
   couple of months, so hopefully by the end of April.

 - Distributed networked programming in Goblins, and associated demo.
   May expand on the previous demo.
   Probably will come out about two months later, so end of June.
   
 - Prototype of the secure UI concepts from the forementioned secure
   UIs paper.
   I expect/hope this to be usable by end of third quarter 2020.

 - Somewhere in-between all this, I'd like to add a demo of being able
   to securely run untrusted code from third parties, maybe in the MUD
   demo.
   Not sure when yet.

 - All along, I continue to expect to push out new updates to Terminal
   Phase with more fun enemies and powerups to continue to reward donors
   to the [Patreon campaign](https://www.patreon.com/cwebber).

This will probably take most of this year.
What you will notice is that this does not explicitly state a tie-in
with the ActivityPub network.
This is intentional, because the main goal of all the above demos are to
prove more foundational concepts before they are all fully integrated.
I think we'll see the full integration and it coming together with the
existing fediverse beginning in early 2021.

Anyway, that's a lot of stuff ahead.
I haven't even mentioned my involvement in
[Libre Lounge](https://librelounge.org/), which I've been on hiatus
from due to a health issue that has made recording difficult, and from
being busy trying to deliver on these foundations, but I expect to be
coming back to LL shortly.

I hope I have instilled you with some confidence that I am moving
steadily along the abstract Spritely roadmap.
(Gosh, I ought to finally put together a website for Spritely, huh?)
Things are happening, and interesting ones I think.

But how do you think things are going?
Maybe you would like to leave me feedback.
If so, feel free to [reach out](/contact/).

Until next time...
