title: My PyCon talk explodes, and I buy an Eee PC
date: 2008-03-19 09:01
author: Christine Lemmer-Webber
slug: pycon-talk-explodes-i-buy-eee-pc
---
I decided after all to give that lightning talk on PyStage at PyCon. I
planned on using the same talk I did at ChiPy, just significantly
shorter. I did not plan on a key getting stuck on my keyboard halfway
through the talk, essentially grinding it to a halt.

Well, a number of people told me that I recovered really well by turning
it into a humorous situation. I ended up smacking the heck out of my old
T23 Thinkpad trying to find which key was stuck. Heck, it might have
been for the best, for at least it ended up forcing me to finish my talk
in a reasonable amount of time. I'm actually looking forward to seeing
the video of it go up on YouTube: that way I'll have one video where I
did really well and where the concept was well received, and another
where I'm literally pounding the keyboard with my fist to humorous
effect. Too bad the former was in front of the 60 or so people at ChiPy
and the latter was in front of the 1000 or so people at PyCon.

Even so, the whole situation was embarrassing, and built upon a number
of other frustrations I've had with that laptop: broken wireless and a
video card for which I haven't been able to figure out how to get direct
rendering with OpenGL. These things have made a number of frustrations
for me, particularly at nerd events like ChiPy, PyCon, or even while
sprinting at PyWeek, and in an impulsive moment I made a significant
financial decision (without consulting my fiance): I bought an [Eee
PC](http://en.wikipedia.org/wiki/Eee_pc).

There were a lot of people with these things at PyCon, and I found them
appealing to me on a number of different levels:

-   **Cost:** Pre tax and shipping, it was only \$350
-   **Portability:** It's incredibly tiny and cute, and easy to carry
    around.
-   **Functionality:** Despite being so small, it still has a fully
    functional (albeit tiny) keyboard.
-   **Power:** It's strong enough to be able to play some
    [reasonably](http://youtube.com/watch?v=r_XVzt_o1Ac)
    [impressive](http://youtube.com/watch?v=KKNRxuqmHa8)
    [games](http://youtube.com/watch?v=UixLlpMgJqA). I won't be using it
    for that, but if it's powerful enough for that, it should be able to
    handle [PyGame](http://pygame.org), [Pyglet](http://www.pyglet.org/)
    and [SuperTux](http://supertux.lethargik.org/) development.
-   **Freedom:** It ships with GNU/Linux off the bat, albeit with a
    shitty distribution. That's okay, it's able to
    [run](http://youtube.com/watch?v=ZRP8fPcaSzI) the distros
    [I](http://www.us.debian.org/) [like](http://www.ubuntu.com/).

I've been interested in a super small computer with working wireless for
a long time. I don't really need or want a laptop that can replace my
desktop, as I enjoy doing most of my work on a machine that's
persistently on. This is why I've invested money into the old Zaurus I
bought several years ago. Unfortunately, I never got the networking on
it working well (that might have been partly due to my inexperience with
networking at the time) and thus was never able to get the environment
to the state that I wanted it in. Also, the keyboard on it was fairly
decent for what it was, but it wasn't a *real* keyboard. This thing has
a *real* keyboard.

The only thing I feel bad about with purchasing this device is that I
feel like it distracts from the [One Laptop Per Child
project](http://laptop.org/). Unfortunately, neither the [Give One Get
One](http://laptopgiving.org/en/index.php) nor the developer programs
are running right now, so I don't think its possible for me to get ahold
of one before [Flourish](http://www.flourishconf.com/).

There's also a possibility that I won't end up achieving what I want
with it, but I'm quite optimistic. If nothing else, at least I'll be
able to finally have a machine with working wireless access, which seems
pretty critical for a geek in modern society.

In the meanwhile I'm refreshing the UPS shipping page constantly. It's
scheduled to arrive tomorrow, which is almost a shame because we're off
of work tomorrow and I had it ship to the office. Well, maybe I can
drive to the UPS store myself to pick it up, because honestly tomorrow
can't come soon enough.
