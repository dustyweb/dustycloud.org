title: FOSDEM 2013
date: 2013-01-31 10:00
author: Christine Lemmer-Webber
slug: fosdem-2013
---
I'll be at [FOSDEM 2013](https://fosdem.org/2013/) this year speaking on
the [AGPL Panel](https://fosdem.org/2013/schedule/event/agpl_panel/).
It's not "a MediaGoblin talk" but the reasons I'm there are entirely to
do with MediaGoblin being under the AGPL.

There will be several other MediaGoblin community members there. I'm
really interested in Deb Nicholson's [Messaging for Free
Software](https://fosdem.org/2013/schedule/event/messaging_for_free_software/)
talk. We might do a hackathon.

Will you be there? Maybe we should meet up! Or at least drop by and say
hello. I'm arriving tomorrow and leaving early Monday morning. If you're
interested, shoot me an email at cwebber AT dustycloud DOT org. Or just
come up and say hello!
