title: I've been awarded the Samsung Stack Zero Grant
date: 2019-01-31 18:26
author: Christine Lemmer-Webber
tags: foss, funding, spritely, activitypub, federation
slug: samsung-stack-zero-grant
---
Good news everyone! I've [been awarded the Samsung Stack Zero
Grant](https://samsungnext.com/whats-next/category/podcasts/decentralization-samsung-next-stack-zero-grant-recipients/).
But why not quote them?

> Chris Lemmer-Webber is the co-editor and co-author of the
> now-ubiquitous ActivityPub protocol. While it provides a great
> framework for creating, updating, and deleting content across
> applications, it doesn’t provide any standardised mechanism for secure
> authorisation. With Spritely, Webber will work on extending the
> protocol in a backward-compatible manner, while at the same time
> building tools and applications that showcase its use. This will
> enable developers to build applications that enable richer
> interactions through a federated standard.

This should fund my next couple of years of work on full time
advancement of the fediverse.

You may remember that I've [talked about Spritely
before](https://dustycloud.org/blog/spritely/). In fact I am finally in
launch-mode... I am currently [sitting in a wizard's
tower](https://www.watertorenvlissingen.com/) at a
[hackathon](https://indieweb.org/2019/Vlissingen), getting out the first
release of [Golem](https://gitlab.com/spritely/golem), a Spritely
artifact.

Anyway, I'll be at [FOSDEM 2019](https://fosdem.org/2019/) giving a
[talk](https://fosdem.org/2019/schedule/event/guileracket/) and on a
[panel](https://fosdem.org/2019/schedule/event/activitypub_panel/). And
after that I'll be
[speaking](https://2019.copyleftconf.org/schedule/presentation/2/) at
[CopyleftConf](https://2019.copyleftconf.org/). Maybe I'll see you?

More news soon...
