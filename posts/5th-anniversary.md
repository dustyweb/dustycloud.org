title: Fifth wedding anniversary, the RPG
date: 2014-05-30 23:40
author: Christine Lemmer-Webber
tags: personal, anniversary, tabletop
slug: 5th-anniversary
---
We had an awesome wedding anniversary today... 5 years! Morgan didn't
know I had planned something ahead of time. I'm not good at surprises,
but this time I managed to pull off a surprise, and I think with massive
style.

So, some context. First of all, a number of my friends and I, and I
guess me especially, have kind of become tabletop role playing game
nerds over the last year. We've been playing a wide variety of games,
especially in [Fate](http://faterpg.org/) (the basis of which is under a
free culture license!), from space adventures to crime dramas to
[German-esque fairytale
stories](http://dustycloud.org/misc/the_witch_and_the_carriage.html) to
[some rather silly holiday
adventures](http://dustycloud.org/misc/santas_little_uprising.html), to
large scale inter-kingdom dramas. Though we have played some games in
the style of \$STANDARD_FANTASY games (what most people think of when
they think of tabletop RPGs), most of the stuff we've been playing has
been more narrative even then, and less about straight up
monster-killing dungeon crawling.

We have played some games in the category of \$STANDARD_FANTASY though,
and one in particular has a character that my spouse Morgan plays in, a
sort of "professor of magic history" (yeah, people who know Morgan may
notice some overlaps) who uses jewelry to actually power up her
abilities, but isn't particularly innately magically inclined. Morgan is
not really one for fancy jewelry, but she does like the engagement ring
that I got her, which is a bit fancy. When she plays as this character,
she actually dresses up for it, and wears some matching earrings as
well.

So anyway, today we played a game in that universe, and I GM'ed (my
brother and his girlfriend also participated, they were awesome). Morgan
was arriving in a somewhat swashbuckling'y type city, so I compelled her
that she probably would have to remove her ring (the source of half her
magic) until she figured out a way to get some sort of protection from
thievery in the city. I figured this would piss off Morgan... I didn't
realize how much. "I'm not playing a game with you if you take away my
jewelry." She eventually accepted the compel after encouragement from
other players, but she was pretty fuming'ly mad about it. "You're making
me take off my *engagement ring* on our *anniversary*?" (Morgan had
thought I had accidentally planned this game on our anniversary without
realizing it. She didn't realize that for once in our lives, I was being
smooth about things.)

It was a good game generally. There was an artifact delivered, etc etc.
By the end of the game, an NPC said "Oh, thanks, I think I have a jewel
that matches yours... I can't really use it because it's for fire magic
users only. Now where did I put that?"

In real life I shuffled through some drawers looking and pulled out a
small beat up looking urn. "Careful, this thing is hot. Only fire magic
users seem to be able to open it with no problem."

Of course her character did, and of course she was able to bypass the
"smoke" (white tissue paper) no problem. And what was inside? A pendant
that perfectly matches her ring. ("Conflict-free" in real life,
supposedly, to the extent those things are true. Also, aside from her
engagement ring, this is the *only* piece of jewelry I have ever bought
Morgan. We're not really that much of jewelry-presents type people, but
I think an exception here was successful.) Oh yeah, and in-game the
pendant gave her a fate-point-activated fire shield that can give her an
offensive-defense against people trying to pickpocket her or doing melee
attacks.

It was a stunt for sure, but it worked. It extra worked because usually
I *am* such a doofus, and I *did* piss off Morgan by compelling her
earlier, and it looked like there were a bunch of bits that turned out
to be just me making stupid mistakes that then turned around and made
the end reveal awesome.

Somehow I pulled off that stunt, all the players seemed to have a good
time generally, I didn't give it away beforehand (usually I'm not so
successfully sneaky), and yes, my
intentional-though-appearing-accidental pissing-off maneuvers really did
make the final result more fun for everyone. Success!

Anyway, so that's that. Morgan told me I'm the best, and nerdiest,
husband ever, so I'll gladly take all of that.

Happy fifth anniversary, Morgan. Here's to many more.
