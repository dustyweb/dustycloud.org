title: Comments and byte-compiled Lojban
date: 2015-01-10 12:08
author: Christine Lemmer-Webber
tags: lojban, machine learning, ai
slug: comments-and-byte-compiled-lojban
---
Thought of the morning: people often say "we have variables and comments
because programming languages are for humans, not computers". But if
computers were really at the point where they were able to program
themselves, and I mean *really* do it (even invent and code new things,
and I don't mean genetic algorithm bullshit, I mean thinking about
design... so this also means code as more than just "learning", but
actually planning and programming something new), would they need
variable names and comments?

My thinking is: yes, or they'd need something like it. If you don't have
this, this means you're effectively reverse engineering "purpose" in the
codebase all the time, which can be both expensive and faulty. I think
any AI that's not some \~dumb application of known heuristics will need
to be able to "think" about the code at point, and knowing the reason
for a code change is important. So of course relevant information should
be recorded in that portion of the code.

Now, does that mean something as messy as English will be used (as the
majority of present code is written in English)? I doubt it. Probably
something like Lojban will be used. Maybe it will not even be plaintext
code: it could be machine-readable, *machine-contemplatable* code with
"byte compiled lojban", or similar.

Relatedly, in the (glorious???) future where machines can think and
design programs, assuming enough resources exist to keep said machines
running, humans will have to interface with computers on a computer's
level more often. Will Lojban as a second language be mandated in
schools?

(On that note, maybe I should really learn Lojban... :))
