title: Miro Volunteering
date: 2008-08-08 23:16
author: Christine Lemmer-Webber
slug: miro-volunteering
---
If you read [Planet Miro](http://planet.getmiro.com/), you may have
noticed that [I was recognized in Will's
blog](http://pculture.org/devblogs/wguaraldi/2008/08/08/status-trunk-4/)
for the volunteering I've been doing on Miro. I've been planning on
mentioning my participation for some time on here, so this seems like a
good time for me to do so.

It's been fun. But most of all, it's something I feel is really
important. Television is the most consumed medium in modern western
culture, and with the internet, there's a chance to shift it away from
its original place of control by just a small number of megacorporations
toward something that's as democratic as the web. There are a lot of
IP-TV systems emerging, but Miro's the only one that really takes open
and decentralized video playing seriously.

That's still a pretty vague explanation for why I think this is so
important. Hopefully I'll find some time to really flesh out this
reasoning soon, because it really does matter to me. In the meanwhile I
can tell you that there's a lot of really exciting development happening
in SVN trunk, including an entire user interface overhaul. We're
switching the codebase from a lot of embedded HTML to actual widgetry. I
guarantee that Linux/GTK has never looked better, and the code is
getting a lot cleaner too with *significantly* less fragmentation across
platforms. It's also faster and more featureful. Right now though, we're
still in the process of reimplementing a lot of the old code. I wouldn't
run from trunk right now, especially because it changes the database in
a way that makes it incompatible with the last stable release. So if you
upgrade to the development code, you're stuck with it. Don't worry, SVN
is evolving at an astonishingly fast rate. It's already more enjoyable
for me to run SVN than the last stable release, but I really should
re-emphasize the fact that there are still a lot of important features
missing.

The Miro people also sent me a [t-shirt](http://www.getmiro.com/store/)
for the volunteering I've done. I picked the pretty one with the bird,
and it's already one of my favorites in my collection. I might buy the
exploding TV one soon too.

This has been really fun. It's nice to work on an application that's not
web related and which is used by a lot of people. And everyone from the
core Miro team has been really great to work with.

2.0's gonna be awesome. You'll see.
