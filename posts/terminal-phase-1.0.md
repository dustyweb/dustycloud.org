title: Terminal Phase 1.0
date: 2020-01-19 16:10
author: Christine Lemmer-Webber
tags: terminal phase, spritely, foss
slug: terminal-phase-1.0
---

![Testing Terminal Phase image](https://dustycloud.org/gfx/goodies/terminal-phase-parallax-starfield.gif)

I'm pleased to announce that [Terminal Phase](https://gitlab.com/dustyweb/terminal-phase), a space shooter game you
can play in your terminal, has achieved [version 1.0](https://gitlab.com/dustyweb/terminal-phase/tree/v1.0).
The game is completely playable and is a fun game (well, at least a
number of playtesters told me they thought it was fun).
It includes two levels (one of which is more balanced than the other),
and more content is on its way (1.0 isn't the end!).
You can see it being played above in
[cool-retro-term](https://github.com/Swordfish90/cool-retro-term)
but it works in all sorts of terminals, including gnome-terminal
and etc.

I also [released a video recently](https://www.youtube.com/watch?v=wxt2dqqulQc) ([archive.org mirror](https://archive.org/details/terminal-phase-2020-01-14)) of me doing a
live playtest of the game and also showing off how to make new levels
and program new enemies (which serves as kind of an introduction, but
probably not the best one, to [Spritely Goblins](https://gitlab.com/spritely/goblins)).

Terminal Phase was actually a reward for hitting the [$500/mo milestone](https://www.patreon.com/posts/new-goal-based-30434803)
on [my Patreon account](https://www.patreon.com/cwebber), which we achieved [a little over a week ago](https://www.patreon.com/posts/we-made-it-phase-33006392).
I aimed to get 1.0 out the door by midnight on Wednesday but I actually
released it a couple of hours later, closer to 2:30am, because I was
trying to make the credits look cool:

![Terminal Phase Credits](https://dustycloud.org/gfx/goodies/terminal-phase-credits-2020-01-19.gif)

I think I succeeded, right?
Maybe you would like your name in there; you can still do so by
[selecting a tier on my Patreon account](https://www.patreon.com/cwebber).
I released the game as FOSS, so whether you donate or not, you can
still reap the benefits.
But I figure making the credits look cool and putting peoples' names
in there would be a good way of making people feel motivated.
And there are more releases on the way; I'll be adding to this now and
then and releasing more stuff occasionally.
In fact you may notice the cool parallax scrolling starfield in the gif
at the top of this post; I added that [after 1.0](https://gitlab.com/dustyweb/terminal-phase/commits/starfield).
I guess it's a bit sneaky to put that on top of a post labeled 1.0,
but the good news is that this means that 1.1 is not far away,
which will include some new enemies (maybe a boss?), new levels, and
yes, parallax starfields (and maybe your name in the credits if it
isn't already).

Anyway, enough self-shilling; let's talk more about the game itself.
Terminal Phase really had a number of goals:

-   Fun.  Games are fun, and making them is (well, mostly) fun and
    interesting.
    And I feel like the FOSS world could use more fun.
-   Fundraising.  I do a lot of work to enrich the commons; funding
    that stuff can be brutally hard, and obviously this was a
    fundraising tactic.
-   A litmus test.  I wanted to see, "Do people care about funding FOSS
    games, in particular?  Does this matter to people?"  My suspicion
    is that there is an interest, even if niche, and that seems to have
    been validated.  Great.
-   Pushing the medium of interactive terminal-based / ascii art
    content.  Probably because it's a retro area, it's not really one
    where we see a lot of new content.  We see a lot more
    terminal-based turn-based games, most notably roguelikes; why not
    more live action stuff?  (Note that I have done [two](https://dustycloud.org/blog/conservancy-card/) [other](http://dustycloud.org/blog/lets-just-be-weird-together/) projects
    I did this year in this same vein.)
-   Thanking donors.  I had this Patreon account and that's great that
    people were being generous, but I felt like it would be nice to
    have something that felt quasi-tactile, like you got something
    visible back from it.  I hope people feel like that succeeded.
-   But most importantly, advancing [Spritely Goblins](https://gitlab.com/spritely/goblins).  Terminal Phase
    is a program to demonstrate and test how well about half of what
    Goblins does works well, namely transactional object interactions.

I feel like all of those were a success, but I really want to write
more about that last one.
Except, well, [I already have in some detail](https://dustycloud.org/blog/terminal-phase-prototype/), and maybe I'd repeat
myself.
But I'll say that developing Terminal Phase has made me dramatically
more confident that the core parts of Spritely Goblins work well and
make sense.
That's good, and I can say that without a bunch of hand-waving; I
built something that feels nice to use and to program.

That lets me move forward with solidifying and
documenting what I have and focusing on the next layer: the
asynchronous programming and distributed networked objects layers.
The former of those two exists, the latter of those needs work, but
both will be tested in a similar way soon; I plan on building some
highly interactive demos to show off their ideas.

Anyway, I hope you enjoy the game, and thank you to everyone who
donated and made it possible!
Again, I plan to release more soon, including new levels, new enemies,
boss battles, and yes, even some powerups.
And if you like the game, consider [becoming a supporter](https://www.patreon.com/cwebber) if you aren't already!

Now back to working on Spritely Goblins itself...
