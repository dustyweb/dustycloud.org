title: In Which My Twelve Year Old Brother Reviews GNU/Linux and Ubuntu
date: 2009-01-23 19:47
tags: foss, advocacy
author: Christine Lemmer-Webber
slug: johns-linux-essay
---
So a few months ago, I installed [Ubuntu](http://ubuntu.com) on my
younger brother John's computer. A couple of days ago he sent me a rough
draft of an essay he was writing for his English class. He asked me a
few questions, and I answered those, but this writing is all his. I'll
let you read it for yourself.

> Have you found a document that is in a format that your word processor
> doesn't recognize? Are you bored of the games you have? Do you have a
> computer? If you are or have any of these things, Ubuntu Linux is the
> thing for you! It's great for computer geeks and people who just use
> the computer. It even is good for people who have little patience or
> can't tell when the computer is about to crash. It's a user friendly
> form of Linux.
>
> Linux is nearly virus free. As long as you only download open source
> programs, there is little to no chance of viruses. This is thanks to
> real computer geeks and programmers. If it is open source, programmers
> can look at a code and find any virus ware imputed on the code, they
> then delete that part of the code.
>
> How can one word processor understand so many formats like .doc or
> .odt? And how are there so many of them? Each format has its one
> unique code. If you open a .odt in .doc format, the writing will look
> like gibberish or a bunch of numbers. That's because it interprets the
> information differently. Luckily a group of people were clever enough
> to make a word processor that can type in all the formats and read all
> the formats. This is free open source and comes with Ubuntu.
>
> Ubuntu is for geeks and laypersons alike. For the geeks who know how
> to navigate the command line and through it, manipulate virtually any
> part software of the computer. You could even use Python, the very
> easy yet complex programing language of Ubuntu and really all of
> Linux, is used in the command line. It can be used to create new
> tools. These new tools can then be used to create more complex tools.
> Python is complex and flexible enough to keep geeks with many years of
> practice involved while still allowing laypersons to create a simple
> tic-tac-toe game with one or two days of brother-to-brother or
> sister-to-sister lessons.
>
> There is one special thing called a split hard drive. A split hard
> drive allows you to have four operating systems on one hard drive. You
> can download Ubuntu with a free CD. You can make this simply by going
> to Ubuntu's website going to the downloads page, pick the latest
> version, and then follow the instructions. After you copy the image on
> the CD, restart the computer. Just follow the instructions and go
> through the installation processes. Within 30 minutes to an hour
> you're ready. If you need help, ask your neighborhood geek. Printers
> are instantly installed, and it comes with a built in multi-instant
> messenger.
>
> Ubuntu Linux is the practical solution to your virus problems.
> Windows, mac, or other non-open source operating and system is still
> recommended due to certain things not being compatible with Linux.
> This, as mentioned before, can be fixed by making your hard drive into
> a split hard drive. If you're a geek there are millions of
> possibilities. If you're a layperson who likes the user friendliness
> of Windows, Ubuntu still has this with out the slowness of Windows. Go
> on and get Ubuntu free today!

As you could guess, I'm brimming with pride. There are some errors, but
I don't even live in the same city as my brother, so this has mostly
come out of his own experiences after a bit of guidance from me. And
obviously there's more to learn but for a twelve year old who has only
been running GNU/Linux for a few months I'm just plain impressed by *how
much he gets it*.

In sum, I'm excited both for my brother and for the increasing
accessibility of free and open source software, both in product and in
spirit.

**Edit:** John gave me his final version of the essay, so I replaced the
old one with this one, as promised.
