title: Prosody is a nice XMPP server
date: 2010-03-07 16:02
tags: xmpp, foss, dustycloud
author: Christine Lemmer-Webber
slug: prosody
---
Up until recently I was using [ejabberd](http://www.ejabberd.im/) for my
XMPP server on dustycloud.org. It worked pretty well, was easy enough to
set up, etc. Unfortunately it was also a total memory hog, sucking up
more than 120MB of the memory on my tiny VPS. Given I was running the
smallest Linode server possible (actually, I just upgraded to the second
smallest today, which was pretty painless) this was making it rather
difficult for me to add new services and sites.

After a [very helpful conversation on
identi.ca](http://identi.ca/conversation/24029318#notice-23950021), I
decided to make the switch to [Prosody](http://prosody.im/). I'm glad to
say it was quite painless: after installing the .deb off their site,
making a couple lines of changes to their bundled config file, and
running their ejabberd2prosody.lua script, things are running smooth.
Prosody only takes about 15 megabytes of memory (including what's
cached). Nice!

One word of advice: ejabberd2prosody.lua isn't bundled with Prosody's
.deb files currently, so you have to run it from source. The "migrated
database" that it creates is set up relative to the script's own path it
seems, so you'll have to move the database it creates out of the source
directory into /var/lib/prosody. After I figured that out, everything
was smooth!
