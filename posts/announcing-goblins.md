title: Announcing Goblins (the actor model foundation of Spritely)
date: 2018-10-31 14:40
author: Christine Lemmer-Webber
tags: goblins, actor model, foss, spritely
slug: announcing-goblins
---
In my last post I talked about [launching the Spritely
project](https://dustycloud.org/blog/spritely/) in all its
ambitiousness. As I said, I plan on releasing it as a series of "demos".
What I mean by "demo" is fairly loose: things you can try and see,
artifacts to indicate progress as I go. Convergence slowly towards a
goal.

Over the last week I released the first version of the foundation of
this work, which I'm calling
[Goblins](https://gitlab.com/spritely/goblins). v0.1, a pre-pre-alpha
release, is now out the door. Goblins is an [actor model
implementation](https://en.wikipedia.org/wiki/Actor_model) for the
[Racket programming language](https://racket-lang.org/). I think if you
know some Racket, then [the Goblins
documentation](https://docs.racket-lang.org/goblins/) should be fairly
approachable, and I'd love to hear feedback. Not everything is
documented, but it should give you a nice taste of what the core ideas
are. (Astute readers may observe that Goblins' name shares striking
resemblance to [another project I have worked
on](https://mediagoblin.org/); this is no coincidence and is indeed a
hint as to what I think the future direction of that area of work is.)

Most live distributed systems are in some way a language-level actor
model implementation, but *knowing* that you're an actor model
implementation may change how you do things.
([ActivityPub](https://www.w3.org/TR/activitypub/) is itself an actor
model protocol, though I think not all implementors realize that.)
Goblins is the third generation of an actor model I've written, the
previous ones being [XUDD](https://xudd.readthedocs.io/en/latest/) and
[8sync](https://www.gnu.org/software/8sync/). (Maybe if you count the
big rewrite I did of 8sync to run it on top of
[fibers](https://github.com/wingo/fibers), it could be the fourth
generation.)

If you read [the Spritely
blogpost](https://dustycloud.org/blog/spritely/) you'll know that
distributed games are part of the long con here. This isn't the first
time I've written a game system on top of one of these actor model
implementations; in fact if you scroll down on
[8sync](https://www.gnu.org/software/8sync/) website you'll (currently)
see a video of me [presenting on
Mudsync](https://archive.org/details/feb_2017-live_network_coding_8sync),
a multi-user dungeon written in 8sync, where in the talk instead of
slides I had rooms, and the audience was moving around in the world and
I was changing it in response to their suggestions.

But the drive of the actor model connected to social distributed games
goes back further for me. I initially got interested in an actor model
the first time I tried to work on the whole multiplayer game front about
a decade ago. A friend and I spent a couple hours experimenting with
threads and locks and after the first demo it was obvious that was never
going to work. I remembered I had been told by a former coworker that
they had used a system that had an actor model + coroutines as its
basis; I didn't know what either of those things were yet, but as I
looked into them it became clear to me that this was the right route.
But one piece since then until recently remained a mystery to me, which
was how to manage the complex web of authorization between the objects.

Last year at TPAC, the World Wide Web consortium's conference, I was
staying with some people who asked me what I would do if I had unlimited
time to work on whatever I wanted. I sheepishly admitted to my dream of
a federated social web as a distributed game, and expected to be laughed
out of the room. I was surprised to find out that nearly everyone I was
working with had some background in this same work and dream. This gave
me some hope that I wasn't completely foolish, and the observation that
even failure in persuing this ambitious dream would probably result in
useful artifacts.

And last year at Rebooting Web of Trust, I hit a big breakthrough. I had
the chance to meet Mark S. Miller in person, someone who's work I read
but barely understood at the time, and this meeting in some ways changed
the direction of my career. For one thing, it lead to us collaborating
on the foundation for the [ocap-ld
specification](https://w3c-ccg.github.io/ocap-ld/), which came out of [a
paper we collaborated on from that
event](https://github.com/WebOfTrustInfo/rebooting-the-web-of-trust-fall2017/blob/master/final-documents/lds-ocap.md)
(also available in [nicely formatted PDF
form](https://github.com/WebOfTrustInfo/rebooting-the-web-of-trust-fall2017/raw/master/final-documents/lds-ocap.pdf)).
But what I also discovered was that Mark Miller and company had actually
built the distributed *and secure* social network dream in the late
1990s and called it Electric Communities ([rare video of it being
demonstrated](https://www.youtube.com/watch?v=KNiePoNiyvE), and a [rare
video of some of the out-there
ideas](https://www.youtube.com/watch?v=kOFzisF7aNw)). What I found out
was that they had solved most of the remaining questions about
authorization and security through object capabilities, and that was
through a pile of object capability patterns built on top of the actor
model. While Electric Communities did not survive, its foundation of the
[the "E" programming language](http://erights.org/) lived on, pulling in
many of its good ideas with it. I proceeded to read as much
[documentation about E](http://www.skyhunter.com/marcs/ewalnut.html) and
surrounding [capability
patterns](http://erights.org/elib/capability/index.html) as I could, and
this has majorly influenced Goblins' design. (Don't underestimate the
depth of ideas on [erights.org](http://erights.org/) just because it
looks like an ancient Geocities page... it did come from that era after
all...)

Another key insight came from discovering the [Object-Capability
Security in Virtual
Environments](https://www.uni-weimar.de/fileadmin/user/fak/medien/professuren/Virtual_Reality/documents/publications/capsec_vr2008_preprint.pdf)
paper. I won't go into details on this except to say that it's a
must-read for this problem domain, with the caveat that it pulls in a
lot of terminology baggage from the object capability (aka ocap)
community. The ocap community is one of those groups that has so many of
the right ideas but which unfortunately is also drowning in its own
verbiage, despite true efforts to make the ideas more accessible. We
(and I guess I'm part of this group now, so "we" is appropriate) need to
do better, but [it isn't
easy](https://en.wikipedia.org/wiki/Curse_of_knowledge).

Goblins is still in its very early days. Its api is unstable and it
surely has bugs ([this one in
particular](https://gitlab.com/spritely/goblins/issues/8) I need to deal
with, and which even exposed an [underlying bug in
Racket](https://github.com/racket/racket/issues/2341), which [did get
fixed
quickly](https://github.com/racket/racket/commit/d1fe6a6e3e26df403f650ae821aa0adbd3df491d)
because the Racket community is amazing). Please don't build anything
production-oriented on top of it yet. That said, I'd love users to try
things. [Check out the docs](https://docs.racket-lang.org/goblins/), see
if they're up your alley.

If you like this kind of work and want to see more of it, [consider
donating](https://www.patreon.com/cwebber); I don't have a corporate
sponsor, so progress is funded by people like you. And keep watching
here; more Spritely demos/artifacts/releases on the horizon.
