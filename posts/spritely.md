title: Spritely: towards secure social spaces as virtual worlds
date: 2018-10-14 15:20
author: Christine Lemmer-Webber
tags: spritely, foss, federation
slug: spritely
---
If you [follow me on the fediverse](https://octodon.social/@cwebber),
maybe you [already
know](https://octodon.social/@cwebber/100866264755512782). I've sent an
announcement to my work that I am switching to doing a project named
[Spritely](https://gitlab.com/spritely/) on my own full time. (Actually
I'm still going to be doing some contracting with my old job, so I'll
still have some income, but I'll be putting a full 40 hours a week into
Spritely.)

**tl;dr:** I'm working on building the next generation of the fediverse
as a distributed game. [You can support this
work](https://www.patreon.com/cwebber) if you so wish.

# What on earth is Spritely?

"Well, vaporware currently", [has been my joke since announcing
it](https://medium.com/we-distribute/faces-of-the-federation-christopher-allan-webber-on-mediagoblin-and-activitypub-24bbe212867e),
but the plans, and even some core components, are starting to congeal,
and I have decided it's time to throw myself fully into it.

But I still haven't answered the question, so I'll try to do so in
bullet points. Spritely:

-   Aims to bring stronger user security, better anti-abuse tooling,
    stronger resistance against censorship, and more interesting
    interactions to users of the fediverse.
-   Is based on the massively popular
    [ActivityPub](https://www.w3.org/TR/activitypub/) standard (which I
    co-authored, so I do know a thing or two about this).
-   Aims to transform distributed social networks into distributed
    social games / virtual worlds. The dreams of the 90s are alive in
    Spritely.
-   Recognizes that ActivityPub is based on the [actor
    model](https://en.wikipedia.org/wiki/Actor_model), and a pure
    version of the actor model is itself [already a secure object
    capability
    system](http://www.erights.org/elib/capability/ode/index.html), so
    we don't have to break the spec to gain those powers... just change
    the discipline of how we use it.
-   Will be written in [Racket](http://racket-lang.org/).
-   Is an umbrella project for a number of modular tools necessary to
    get to this goal. The first, an object capability actor model system
    for Racket named [Goblins](https://gitlab.com/spritely/goblins),
    should see its first public release in the next week or two.
-   And of course it will be 100% free/libre/open source software.

That's a lot to unpack, and it also may sound overly ambitious. The game
part in particular may sound strange, but I'll defend it on three
fronts. First, not too many people run federated social web servers, but
a lot of people run Minecraft servers... lots of *teenagers* run
Minecraft servers... and it's not because Minecraft has the best
graphics or the best fighting (it certainly doesn't), it's because
Minecraft allows you to build a world together with your friends.
Second, players of old MUDs, MOOs, MUSHes and etc from the 90s may
recognize that modern social networks are structurally degenerate forms
of the kinds of environments that existed then, but contemporary social
networks lack the concept of a sense of place and interaction. Third,
many interesting projects (Python's Twisted library, Flickr, much of
object capability security patterns) have come out of trying to build
such massively multiplayer world systems. Because of this last one in
particular, I think that shooting for the stars means that if we don't
make it we're likely to at least make the moon, so failure is okay if it
means other things come out of it. (Also, four: it's a fun and
motivating use case for me which [I have explored
before](https://archive.org/details/feb_2017-live_network_coding_8sync).)

To keep Spritely from being total vaporware, the way I will approach the
project is by regularly releasing a series of "demos", some of which may
be disjoint, but will hopefully increasingly converge on the vision.
Consider Spritely a skunkworks-in-the-public-interest for the federated
social web.

# But why?

[Standardizing
ActivityPub](https://dustycloud.org/blog/activitypub-is-a-w3c-recommendation/)
was a much more
[difficult](https://dustycloud.org/blog/on-standards-divisions-collaboration/)
effort than anticipated, but equally or more so more successful than I
expected (partly due to Mastodon's adoption launching it past the sound
barrier). In that sense this is great news. We now have dozens of
projects adopting it, and the network has (at last I looked) over 1.5
million registered users (which isn't the same as active users).

So, mission accomplished, right? Well, there are a few things that
bother me.

-   The kind of rich interactions one can do are limited by a lack of
    authorization policy. Again, I believe object capabilities provide
    this, but it's not well explained to the public how to use it. (By
    contrast, Access Control Lists and friends are [absolutely the wrong
    approach](http://waterken.sourceforge.net/aclsdont/current.pdf).)
-   Users are currently insufficiently protected from spam, abuse, and
    harassment while at the same time [administrators are
    overwhelmed](https://nolanlawson.com/2018/08/31/mastodon-and-the-challenges-of-abuse-in-a-federated-system/).
    This is leading a number of servers to move to a whitelisting of
    servers, which both re-centralizes the system and prioritizes big
    instances over smaller instances (it shouldn't matter what instance
    size you're on; arguably we should be encouraging smaller ones
    even). There are some paths forward, and I will hint at just one:
    what would happen if instead of one inbox, we had multiple inboxes?
    If I don't know you, you can access me via my public inbox, but
    maybe that's heavily moderated or you have to pay "postage". If I do
    know you, you might have an address with more direct access to me.
-   Relatedly, contemporary fediverse interfaces borrow from
    surveillance-capitalism based popular social networks by focusing on
    breadth of relationships rather than depth. Ever notice how the
    first thing Twitter shows you when you hover over a person's face is
    how many followers they have? I don't know about you, but I
    *immediately* compare that to my own follower count, and *I don't
    even want to*. This encourages high school popularity contest type
    bullshit, and it's by design. What if instead of focusing on how
    many people we can connect to we instead focused on the depth of our
    relationships? Much of the fediverse has imported "what works"
    directly from Facebook and Twitter, but I'd argue there's a lot we
    can do if we drop the assumption that this is the ideal starting
    base.
-   The contemporary view in the fediverse is that social scoping is
    like Python scoping: locals (instance) and globals (federation).
    Instance administrators are even encouraged to set up to run
    communities based on a specific niche, which is a nice reason to
    motivate administrators but it causes problems: even small
    differences between servers' expected policies often result in
    servers banning each other entirely. (Sometimes this is warranted,
    and I'm not opposed to moderation but rather looking for more
    effective forms of it.) Yet most of us are one person but part of
    many different communities with different needs. For instance, Alice
    may be a computer programmer, a tabletop game enthusiast, a
    fanfiction author, and a member of her family. In each of those
    settings she may present herself differently and also have different
    expectations of what is acceptable behavior. Alice should not need
    multiple accounts for this on different servers, so it would seem
    the right answer for community gathering is closer to something like
    mailing lists. What is acceptable at the gaming table may not be
    acceptable at work, and what happens on the fanfiction community
    perhaps does not need to be shared with one's family, and each
    community should be empowered to moderate appropriately.
-   I'd like to bridge the gap between peer to peer and federated
    systems. One hint as to how to do this: what happens when you run
    ActivityPub servers over [Tor onion
    services](https://www.torproject.org/docs/onion-services.html.en) or
    [I2P](https://geti2p.net/en/)? What if instead of our messages
    living at http addresses that could down, they could be [securely
    addressed by their encrypted
    contents](https://github.com/WebOfTrustInfo/rwot7/blob/master/topics-and-advance-readings/magenc.md)?
-   Finally, I will admit the most urgent reason for these concerns...
    I'm very concerned politically about the state of the world and what
    I see as increasing authoritarianism and flagrant violations of
    human rights. I have a lot of worry that if we don't normalize use
    of decentralized and secure private systems, we will lose the
    ability to host them, though we've never needed them more urgently.

There are a lot of opportunities, and a lot of things I am excited
about, but I am also afraid of inaction and how many regrets I will have
if I don't try. I have the knowledge, the privilege, and the experience
to at least attempt to make a dent in some of these things. I might not
succeed. But I should try.

# Who's going to pay for all this?

I don't really have a funding plan, so I guess this is kind of a
non-answer. However, I do have a [Patreon
account](https://www.patreon.com/cwebber) you could donate to.

But should you donate? Well, I dunno, I feel like that's your call.
Certainly many people are in worse positions than I am; I have a buffer
and I still am doing some contracting to keep myself going for a while.
Maybe you know people who need the money more than I do, or maybe you
need it yourself. If this is the case, don't hesitate: take care of
yourself and your loved ones first.

That said, FOSS in general has the property of being a public good but
tends to have a [free rider
problem](https://en.wikipedia.org/wiki/Free-rider_problem). While we did
some fundraising for some of this stuff a few years ago, I gave the
majority of the money to other people. Since then I've been mostly
funding work on the federated social web myself in one way or another,
usually by contracting on unrelated or quasi-related things to keep
myself above the burn rate. I have the privilege and ability to do it,
and I believe it's critical work. But I'd love to be able to work on
this with focus, and maybe get things to the point to pull in and pay
other people to help again. Perhaps if we reach that point I'll look at
putting this work under a nonprofit. I do know I'm unwilling to break my
FOSS principles to make it happen.

Anyway... you may even still be skeptical after reading all this about
whether or not I can do it. I don't blame you... even *I'm* skeptical.
But I'll try to convince you the way I'm going to convince myself: by
pushing out demos until we reach something real.

Onwards and upwards!
