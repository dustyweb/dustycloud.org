title: Fever Dream: cryptocurrency
date: 2015-03-29 12:42
author: Christine Lemmer-Webber
tags: cryptocurrency, fever
slug: fever-dream-cryptocurrency
---
I've been sick the last two days, and for some reason, I've been
dreaming in lisp. It's not the first time I've dreamt in code, but all
times have been in lisp. Maybe lisp is not as readable as many would
like, but it seems dreamable.

Just got up from another nap, another lisp-based fever dream. This one
about a cryptocurrencty on an actor model system, built for a MUD/AR
type environment... no blockchain required (note: may be inspired by
reading Rainbows End between naps):

-   Various actors represent "reserves", like the Federal Reserve can
    print their own money, as much as they like, but there may be
    various community enforcements of this
-   You might have different central banks / reserves on different
    servers
-   The "value" or exchange rate determined by the market, like
    international currency.
-   Currency is non-divisible (you have 100 rupees, but not .1 rupees)
-   Bank has a private key, so does each actor.
-   Basically more or less passing along capabilities (I think???) but
    maybe even the central bank signing whoever has "posession". The
    bank does not know who things are transferred to, but does verify
    that the transferring owner has the right capability currently
    belonging to that unit's ID, and does issue a new capability to the
    new owner.

Does this make sense? I'm too sick to know for now. But transactions
flying everywhere, wrapped in parentheses, in my fevered mind.

If I have more crazy lisp fever dreams today I will record them here. No
guarantee of sanity.
