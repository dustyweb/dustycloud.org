title: Book Meme
date: 2008-11-13 15:17
tags: meme
author: Christine Lemmer-Webber
slug: book-meme
---
I don't normally jump onto blogging memes like this, but the "book meme"
one going on over at [Planet Gnome](http://planet.gnome.org) seems like
a pretty good one. I'm going to slack for a minute from the work I'm
doing to hop in.

-   Grab the nearest book.

-   Open it to page 56.

-   Find the fifth sentence.

-   Post the text of the sentence in your journal along with these
    instructions.

-   Don’t dig for your favorite book, the cool book, or the intellectual
    one: pick the CLOSEST.

    "The story reel becomes the core of your animation production and
    provides your first chance go get a feel for timing and action."

From [Animating with Blender: How to Create Short Animations from Start
to
Finish](http://www.amazon.com/Animating-Blender-Create-Animations-Finish/dp/0240810791),
by Roland Hess (aka [Harkyman](http://www.harkyman.com/)). I've been
finishing it over the last week. Really good stuff. A more advanced book
about [Blender](http://blender.org) that's half tech and half project
management.

And now, back to work!

**EDIT:** Oops, there was a book closer to me that I didn't see, so
bonus sentence:

> "As you can see, using these principles of relative velocity and
> acceleration allows you to calculate the resultant kinematic
> properties of any point on your rigid body at any given time by
> knowing what the center of mass of the body is doing along with how
> the body is rotating."

From [Physics for Game
Developers](http://www.amazon.com/Physics-Game-Developers-David-Bourg/dp/0596000065)
by David M Bourg. That's a pretty fancy sentence.. too bad I haven't
read this book yet. It's been festering on my shelf since I got it as a
gift from [O'Reilly](http://oreilly.com/) for competing (but not
finishing) in the web framework rumble in the [2008 Flourish
Conference](http://www.flourishconf.com/flourish2008/). A book I haven't
read for a competition I didn't finish... now I feel doubly bad.
