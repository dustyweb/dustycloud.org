title: My taste in music
date: 2013-01-29 20:20
author: Christine Lemmer-Webber
slug: taste-in-music
---
True stories in the life of me, as told on the #mediagoblin IRC channel:

    <paroneayea> a song comes on next on rhythmbox and I'm like, Oh, I love this
                 song's intro, haven't heard it in a while
    <paroneayea> turns out it was a car outside with a belt problem
    <paroneayea> how you know you have terrible taste in music, part 1

Morgan, who has previously compared the music I listen to to dial up
modems and car horns, would probably agree with this self-assesment.
