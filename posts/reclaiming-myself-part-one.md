title: Reclaiming Myself, Part One
date: 2008-05-07 09:28
author: Christine Lemmer-Webber
slug: reclaiming-myself-part-one
---
I'm working to reclaim myself. That might sound silly, but..

During the two years I was working at Google, I was going to both work
and school full-time. I had no time to do anything I liked during those
two years. I also spent most nights either trying to finish schoolwork
or work on projects for work (I would stay up all night working on
projects I was totally unpaid for, just because I wanted to learn, to
prove myself, and to do good for the company). Overall, I learned a lot,
and of course both my experience at Google and my graduation are both
invaluable. But there have been other problems.

I gained an unbelievably large amount of weight, to the point that
people who knew me several years ago often can't recognize or believe
that it's me today at first, or people who know me today can't recognize
pictures of me several years ago. I have a picture on my desk of myself
and my siblings taken three or four years ago. One woman at my work came
up to my desk, pointed at me, and said, "Who's that?" I explained,
that's me before I worked at Google. She kept coming back and staring at
the picture saying "No way..." One of my other coworkers I showed the
picture to said, "Wow, Google can do that to you?"

Of course, it wasn't as much Google that did it to me (though the daily
lunches we had seemed to increase the weight of almost everyone on our
team over time) as much as the late nights drinking energy drinks,
eating snacks, etc, just to survive the crazy schedule I was working
through. (Also, I actually had started gaining weight just the year
before they closed Barat... during that year they closed our cafeteria
but maintained that all residents on campus had to invest a mandatory
1000 dollars every 3 months into a meal plan that we could only use at
the coffee shop, where they served greasy muffins, chicken nuggets, etc.
But the weight didn't really pile on until I joined Google.) As I said
in a previous post, I also had stopped biking entirely once I got my car
and moved into the city. This certainly didn't help either, as I pretty
much was stripped of all forms of exercise, other than the daily
busywork of the datacenter.

The sad thing about all this is that, due to image issues I have with
myself that date back to my teenage years, I have a very hard time
looking at myself in the mirror. I gained most of the weight in just a
few very short months, and hadn't really realized it until I stepped
onto a scale. After that point, numberwise it appears that my weight
gain was not quite as quick and drastic, but until I got out of the work
and school cycle, I kept putting on weight.

It's been nearly a year since I graduated from school and started
working at Imaginary Landscape. During that period I haven't gained any
weight, but I really haven't lost any either.

That is, until recently. I've begun biking constantly (I have a new
schedule where I bike a little more than ten miles every morning before
work... today it was raining, I am totally soaked), and I feel much
healthier and have less desire to snack so much. I'm counting calories
and eating a lot of spinach salads. And I'm seeing the effects: the
weight is peeling off. I lost a lot of weight very quickly the first
week and a half, and it's been slower since, but its going down. I am
slowly adding more exercises to my schedule. I can feel the effects of
this. Surprisingly, I'm even enjoying it.

It's going to take at best a year for me to get back to my pre-Google
weight, and hopefully I'll succeed in that as I'll then be in shape for
our wedding.

And so begins part one of reclaiming myself: reclaiming my body. I'm now
also trying to figure out how to handle part two, reclaiming my mind.
