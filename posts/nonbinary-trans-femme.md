title: Hello, I'm Chris Lemmer-Webber, and I'm nonbinary trans-femme
date: 2021-06-28 18:13
author: Christine Lemmer-Webber
tags: gender
slug: nonbinary-trans-femme
---

![A picture of Chris and Morgan together](https://dustycloud.org/gfx/goodies/chris-and-morgan-2021-06-27.jpg)

I recently came out as nonbinary trans-femme.
That's a picture of me on the left, with my spouse Morgan Lemmer-Webber
on the right.

In a sense, not much has changed, and so much has changed.
I've dropped the "-topher" from my name, and given the common tendency
to apply gender to pronouns in English, please either use nonbinary
pronouns or feminine pronouns to apply to me.
Other changes are happening as I wander through this space, from
appearance to other things.
(Probably the biggest change is finally achieving something resembling
self-acceptance, however.)

If you want to know more,
[Morgan and I did a podcast episode](https://fossandcrafts.org/episodes/30-gender-sexuality-personal-perspective.html)
which explains more from my present standing, and also explains Morgan's
experiences with being demisexual, which not many people know about!
(Morgan has been incredible through this whole process, by the way.)

But things may change further.
Maybe a year from now those changes may be even more drastic, or maybe
not.
We'll see.
I am wandering, and I don't know where I will land, but it won't be
back to where I was.

At any rate, I've spent much of my life not being able to stand myself
for how I look and feel.
For most of my life, I have not been able to look at myself in a mirror
for more than a second or two due to the revulsion I felt at the person
I saw staring back at me.
The last few weeks have been a shift change for me in that regard...
it's a very new experience to feel so happy with myself.

I'm only at the beginning of this journey.
I'd appreciate your support... people have been incredibly kind to me
by and large so far but like everyone who goes through a process like this,
it's very hard in those experiences where people aren't.
Thank you to everyone who has been there for me so far.
