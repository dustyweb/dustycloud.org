title: Pycon 2008, day one!
date: 2008-03-14 23:34
author: Christine Lemmer-Webber
slug: pycon-2008-day-one
---
Pycon, day one! (Or that is, day one if you didn't attend tutorials
yesterday.) Best day of my life? Well, no. *One of the best days of my
life?* Oh yes, certainly.

Anyway, summary is:

-   Lots of cool swag
-   Looks like Python 3.0 and 2.6 are going to be developed nicely so
    that the transition will be really easy.
-   Django 1.0 coming soon, possibly after this upcoming sprint
-   Met a lot of interesting folks. Was surprised to meet some people
    whose writings and work I've followed in my spare time. [Kevin
    Kubasik](http://kubasik.net/blog), whose blog I've seen a bit on
    [Planet GNOME](http://planet.gnome.org) stopped by our booth.
-   That whole stalking Phil Hassey thing was a joke, and I didn't
    expect to actually spot him in the crowd, but I did and I somewhat
    rudely and awkwardly interrupted a conversation of his, and didn't
    recover. So the whole joke of me looking like a stalker probably
    actually *did* make me look like a stalker. Oh well.
-   [Brian Fitzpatrick](http://www.red-bean.com/fitz/), an acquaintance
    and head of the Open Source department in Google's downtown
    engineering office, gave me a sticker that said "My other computer
    is a data center". The irony induced by the context of my previous
    employment didn't strike me until several hours later.
-   Massimo Di Pierro, professor at DePaul's CTI department, physicist,
    web framework author, and all around cool guy, gave me a
    [Web2Py](http://mdp.cti.depaul.edu/) hat. I didn't see him give
    anyone else one of these hats, so I'm pretending it's just because
    I'm awesome.

Also, as to whether or not I'm giving a talk tomorrow, I'm not really
sure. I signed up, but I signed up in the special first-grabs sponsor
talk sheet, and I think I might have done so too late. So not sure if
I'm going to get to speak, but I'm preparing anyway.

Oh hey, and if you are there, stop by [Imaginary
Landscape](http://imagescape.com)'s booth in the break sessions! I'll
probably be around.
