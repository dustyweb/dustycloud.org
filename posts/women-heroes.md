title: When "women can be heroes" is simply an accepted premise
date: 2012-07-04 20:47
author: Christine Lemmer-Webber
slug: women-heroes
---
Morgan and I went and saw the film
[Brave](http://en.wikipedia.org/wiki/Brave_%282012_film%29) recently. I
thought it was good, maybe worth a second seeing even. It was certainly
pretty. And after twelve (twelve!) films in a row by Pixar where the
primary character was male (don't give me the "The Incredibles" had the
mom and daughter characters either... those were main characters, but
there was a decidedly "primary character" and that person was not a
woman), it was probably the right film to make. It takes a stand of
sorts: women can be heroes too, should be free to make their own
decisions and set their own life directions, and manages to say all this
cleanly without feeling at all like it beat you over the head. So I'm
glad films like this are being made, and given Pixar's long period of
negligence, it was probably the right film for them to finally make.

That said, here's my worry: so, great, Pixar made a film that's good,
and largely about how you can be a woman and a hero. Now that we've
established that, will we have other films about women heroes with that
pretext established? I'm worried that either we won't see any ("hey, we
hit our films about women quota anyway, right?"), or there will be a
long and dry spell of no films where a woman is the primary character,
and then we get another one that steps out and reminds us, "Oh hey yeah,
by the way, women can be awesome heroes too! Don't forget about that!"

What I'm trying to say here is: why aren't there more films where women
just *are* heroes? Can we get to the point where the pretext that women
*can* be heroes is established and they just... are?

This isn't the first time I've mentioned this; in fact when I first saw
the trailer for Brave, I [made similar
comments](http://identi.ca/conversation/90830818/replies):

> "Sintel beat them to it though. I also feel like the way Sintel did it
> was best: a female character who just *was* awesome, without even a
> gender battle backdrop. (Challenging patriarchy is good, but a state
> where female leads are just awesome from the get-go and we're not even
> questioning that is better.)"

Deb pointed out that the character of Sintel is maybe not the best
example as to possibly being a bit too "[shaped for dudes to look
at](http://identi.ca/notice/91068287)" in that "[it would be kind of
radical for a pudgy, awkward girl to be chosen in a fantasy story every
so often](http://identi.ca/notice/91070602)". That said, I was glad that
a free culture film beat out Pixar/Dreamworks/etc in putting out a film
where the primary character was a woman, and that she was just awesome
and had an interesting story and adventure without needing to justify
it.

It's not that these films don't get made, even in the animated world; I
can think of a few examples: Spirited Away, and The Triplets of
Belleville (probably my favorite film ever, with a great amount of
adventure and a very non-traditional "hero" of sorts in the grandmother
(tangentially the word "hero" is not really great in this article, I
really mean "primary character" or "character of focus", but those sound
a bit belabored to say)). And on the free culture end of things,
[Tube](http://urchn.org/) is not out yet, but will succeed I think here.
But anyway, these films seem sparse.

So I'd like to see more films, especially animated films, that don't fit
the lame "princess"/"damsel in distress" archetype but that have strong
female characters. And there may yet still be a role for films like
Brave (or Mulan in the 1990s) that try assert clearly that women can be
awesome. But I hope that's not an unfortunate trope that gets
developed... do we really want to teach our young girls "You can be
anything you want, as long as it's a woman who proves that you can be
anyone you want as a woman?" What I'm hoping, basically, is that more
films really and truly accept the premise that these films are trying to
put down. Can we get to the point where we've simply agreed on this, and
have a large number of films that simply have characters doing awesome
things, those characters are women, and we've accepted that as just
being normal? Because I think that would *really* be a progressive
message for future generations.

Or, you know, we could decide this film hit our "say that women can be
awesome" quota, and go back to making films where the primary characters
are always dudes.
