title: Siggraph 2008
date: 2008-08-06 22:41
author: Christine Lemmer-Webber
slug: siggraph-2008
---
Whee! [Siggraph](http://www.siggraph.org/), the world's leading graphics
conference, is rolling around the corner, and [this
year](http://www.siggraph.org/s2008/) is looking really good. Probably
won't surprise anyone that my interests are largely [Blender
oriented](http://www.blendernation.com/2008/06/19/blender-foundation-and-bbb-at-siggraph-2008/),
but I'm really enthusiastic about the festival in general. It looks like
it's running from the tenth to the fifteenth of this month, but I really
want to go on the thirteenth, because that's when [The Making of "Big
Buck Bunny": An Open-Source
Evolution](http://www.siggraph.org/s2008/attendees/caf/caftalks/4.php)
is going to present.

It's here in Chicago, and it looks like [a one day pass is a mere
\$45](http://www.siggraph.org/s2008/attendees/registration/fees.php).
That doesn't sound so bad.

Hm. The thirteenth is just next Wednesday? Maybe I *can't* attend that.
If not, at least it's good to see Blender and Big Buck Bunny gettin'
[represented](http://www.blendernation.com/2008/08/06/bbb-goes-hollywood/).
