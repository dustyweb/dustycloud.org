title: A Field Guide To Copyleft Perspectives
date: 2012-03-18 21:50
author: Christine Lemmer-Webber
slug: field-guide-to-copyleft
---
<div id="outline-container-1" class="outline-2">
<h2 id="sec-1">Intro</h2>
<div class="outline-text-2" id="text-1">


</div></div><p>
Licensing is a big deal in the software and cultural freedom
movements; there are a lot of licenses available in both domains
(probably <a href="http://en.wikipedia.org/wiki/License_proliferation">too many</a>), and people have strong opinions about what
licenses and license components are better or worse.  But in the truly
libre category of licenses, maybe the most controversial aspect of
licensing is that of <a href="http://en.wikipedia.org/wiki/Copyleft">copyleft</a>, a powerful copyright hack that uses
copyright itself in a sort of <a href="http://identi.ca/conversation/69035489#notice-71466181">judo move</a> to force those to make
derivatives to give their contributions back to the commons.
</p>
<p>
There are two primary copyleft licenses, the <a href="http://www.gnu.org/licenses/gpl.html">GNU GPL</a> for software (and
some other categories of functional) works (and the related <a href="http://www.gnu.org/licenses/agpl.html">AGPL</a> and
<a href="http://www.gnu.org/licenses/agpl.html">LGPL</a>) and <a href="http://creativecommons.org/licenses/by-sa/3.0/">CC BY-SA</a> for non-software (generally cultural) works.  But I
don't intend to go into details on copyleft or the licenses
themselves, there's plenty of resources about that already on the
internet.
</p>
<p>
What I'm more interested in exploring here is the <i>perspectives</i> on
copyleft.  Is copyleft good?  Is it bad?  A lot of people have
extremely strong opinions about it.  Actually that's an
understatement; if digital ink were made manifest, the amount spilled
over copyleft could fill at least one olympic sized swimming pool.
But despite all the heated debates about copyleft, I've never really
found a good breakdown about what those arguments are.  I actually
think it's not too hard to separate the arguments categorically, so
here's my attempt to do so.
</p>
<p>
Even though I'm on the overall-in-support side of things (I am
actually <i>conditionally</i> in <i>strategic support</i> of copyleft and think
the decision about whether to use copyleft or not should be weighed on
a case by case basis; more about that at the end) I'm going to start
by discussing the objections before I move to the support side.
Generally I think the objection side of things is a bit trickier (and
intellectually, maybe a bit more interesting to analyze) than the
support side, so I'll go to that first before I explain why one might
actually find copyleft to be a valuable tool.  (A slight amount more
caveat: I'm not claiming to not have bias here; I do.  But again, I'm
not completely on one side or the other, and I think the decision
about whether to apply copyleft to your project is best made by
understanding both the pros and the cons.)
</p>




<div id="outline-container-2" class="outline-2">
<h2 id="sec-2">Guide to objections</h2>
<div class="outline-text-2" id="text-2">


</div>

<div id="outline-container-2-1" class="outline-3">
<h3 id="sec-2-1">Objection 0: (some) Copyleft "infects" non-copyleft permissively licensed works</h3>
<div class="outline-text-3" id="text-2-1">


</div></div></div><p>
I'm marking this as objection 0 because it's not actually an objection
itself (some even argue it's a feature, and at the very least it's
mostly necessary, unless you're using file or package-based copyleft
like the MPL or LGPL). That is to say, on its own people aren't upset
about it, but combined with the other objections some people find it
particularly irritating: if you combine a copyleft work with a
non-copyleft permissively licensed work (again, unless the copyleft
license is the LGPL or MPL or similar), effectively the combined work
is under copyleft.  (This doesn't mean that you can't continue to
develop the non-copyleft permissively licensed work separately
without copyleft applying though.)
</p>
<p>
It should be noted though that the same thing is true with combining
a non-copyleft permissively licensed work with a proprietary work:
effectively the entire work is proprietized.  (Indeed, that's exactly
what copyleft licenses like the GPL are trying to prevent.)
</p>
<p>
Anyway, that wouldn't bother you if the terms of copyleft itself
didn't bother you, so let's move on to the reasons people find
copyleft itself objectionable.
</p>




<div id="outline-container-2-2" class="outline-3">
<h3 id="sec-2-2">Objection 1: Copyleft is non-free</h3>
<div class="outline-text-3" id="text-2-2">


</div></div><p>
The first objection is maybe the most classic objection to copyleft:
copyleft itself is non-free.  There are a few variations to this
argument but it generally goes like this: restrictions in licenses are
bad; possibly copyright as a system of restrictions is itself bad.
Since copyleft relies on copyright and restrictions to preserve the
commons, that means that it's also bad.  The most free license then is
one that provides as few restrictions as possible.
</p>
<p>
Sound confusing?  Let's put this another way and go back to the
copyleft as a "judo move" perspective.  If copyright were violence
(and a number of people in this camp believe that it really is), then
copyleft defends against proprietization with a
violence-in-retaliation move.  It might be defensive, it might even
just be returning the violent force of the oppressor against the
oppressor itself, but to this particular category of anti-copyleft
objection, that doesn't matter.  Any violence itself (or any
copyright restriction) is objectionable, even defensively, and the
fact that a copyleft license makes use of such force is offensive.
</p>
<p>
The trouble with this position is, if you're really arguing it, you'd
better be consistent about it and also object to the violence of
proprietization (which is surely worse than copyleft in its reduction
of freedoms through restrictions).  If you really are concerned with
user freedom, your whole ecosystem had better be free with completely
permissively licensed non-copyleft works to bring that dream alive.
If someone wants to proprietize your world, and legally they can, you
can't stop them directly.  Your only routes to bringing this
completely ultra-restriction-free world to life are to keep building
freely licensed works and tools (and encourage others to do so) and to
try and reduce the scope of or eliminate copyright on a legislative
level (a worthwhile pursuit, but certainly not an easy one, and one we
seem to be losing rather than gaining ground on at the moment).
</p>
<p>
In the software world you used to hear this argument a lot more,
particularly along operating system lines: back in the day it
especially used to be [Free/Open]BSD users arguing with GNU/Linux
users.  If you're completely running permissively licensed free
software and objecting to <b>both</b> copyleft <i>and</i> proprietary software
(like <a href="http://en.wikipedia.org/wiki/Theo_de_raadt">Theo de Raadt</a>), you have the moxie to back this position up by
sticking to your principles.  (And notably, even though I don't agree
with this position entirely, it's one I have a strong amount of
respect for.)
</p>
<p>
However, I think this position is on the decline, and instead we see
a different argument on the rise...
</p>




<div id="outline-container-2-3" class="outline-3">
<h3 id="sec-2-3">Objection 2: Copyleft is strategically suboptimal</h3>
<div class="outline-text-3" id="text-2-3">


</div></div><p>
The other argument (which I think we've been hearing more and more of)
is that copyleft is strategically a poor choice in comparison to
permissive licenses for free and open source software.
</p>
<p>
There are a few reasons you might make this argument; permissive
licenses are generally more interoperable with other licenses, but the
main reason given is that you'll get more developers and more users
on-board this way.  Some businesses are uncomfortable with the
obligations of copyleft; avoiding copyleft means that you'll get a
larger marketshare, and greater popularity means that it's more likely
that you'll have more people giving back to your project.  Maybe you
aren't even worried about contributions; maybe you're making a library
and you want as many users as possible even if you're the only active
contributor.
</p>
<p>
You might also not feel strongly about the freedom side of things at
all; you might write a library that you're totally okay with being
used by only-proprietary-programs; you just want developers to be able
to share code and give back to each other or think that you'll end up
with better software by following such a methodology, principles be
damned.  (However, many people who do take this side do feel strongly
about free and open source software, they just think this is an easier
strategy to iterate toward that goal.)
</p>
<p>
What I do think is true is that in the software world (but I don't
think quite as much in the culture world) we're seeing this attitude
on the rise: these days you often hear and see people take the route
of "release the code to the projects that aren't your core business,
but keep the core bits of your business proprietary if that's what
makes sense to you."  The move to this trend has been growing
simultaneously with the rise of interpreted languages like Python and
Ruby, the move to distributed revision control systems, and maybe most
importantly, the move to software as a service web applications.
This post by GitHub co-founder Tom Preston-Werner, 
"<a href="http://tom.preston-werner.com/2011/11/22/open-source-everything.html">Open Source (almost) Everything</a>", captures that mindset pretty well.
</p>
<p>
To say nothing of the culture side of things, the good news here is
that for a certain scope on the software side (libraries and
infrastructure specifically) this seems to be doing more than well
enough.  For libraries and certain parts of infrastructure, people do
seem interested and willing to contribute back even without copyleft.
And we're seeing an abundance of code crop up these days because of
it.  I think that's great, though I don't think it's actually
enough... but more on that below.
</p>
<p>
In short, arguments to not use copyleft for strategic reasons are
fairly common, probably even increasingly common, among many
developers.  And at least in certain situations, there seems to be
reason to back up such a choice.
</p>




<div id="outline-container-2-4" class="outline-3">
<h3 id="sec-2-4">Objection 3: Deceptive combination of the above</h3>
<div class="outline-text-3" id="text-2-4">


</div></div><p>
There's another sort of objection that's actually a combination of the
previous two, but in a way that's deceptive and potentially even
dishonest.  What I'm talking about is when anti-copyleft individuals
are arguing for not using copyleft for strategic reasons but mask the
argument to sound like a principled, freedom-oriented reason.  This
comic might help best explain what I mean (based on a true story):
</p>



<pre class="src src-fundamental">COPYLEFT COMIC
by Chris Lemmer-Webber
+-----------------------------+
|  Don't use that copyleft    |
|  license!  It's non-free!   |
|  It destroys your freedoms! |
|     /                       |
|   , ,               , .     |
|   O o               o O     |
|  \ C /               ~      |
|   '|'               /|\     |
|                 /           |
|   Oh no!  I like free!      |
|     Why isn't it free?      |
|                             |
+-----------------------------+
+-----------------------------+
|  I can't use it in this     |
|  proprietary program        |
|  with my proprietary        |
|  license!                   |
|     /                       |
|   \ /               , .     |
|   O o               o O     |
|  \ C /             __c      |
|   '|'                |\     |
|                 /           |
|  But your license is even   |
|  more restrictive point for |
|  point, and forbids even    |
|  basic distribution and     |
|  modification!              |
|                             |
+-----------------------------+
+-----------------------------+
|  What are you, some kind of |
|  software freedom zealot?   |
|     /                       |
|                             |
|   \ /               - _     |
|   O o               o O     |
|   c                  ~      |
|  &lt;'|'&gt;              /|\     |
|                             |
+-----------------------------+

To the extent possible under law, Chris Lemmer-Webber
has waived all copyright and related or neighboring rights to
Copyleft Comic via CC0.  Paste, alter wherever/however you like.
http://creativecommons.org/publicdomain/zero/1.0/
</pre>

<p>
Let me describe this perspective in another (non-comic) way: the
argument is that I'm reducing someone's freedom by using a copyleft
license that will infringe on their ability to integrate said program
with their proprietary application, that by choosing a copyleft
license one is reducing their "freedom to choose what license they
want to use".  Sorry, but as I said earlier, the reason why it's hard
to maintain the freedom-oriented anti-copyleft position is that you
also have to object to proprietary software <i>without</i> a mechanism to
protect your work from being proprietized (and this particular breed
of truly-freedom-oriented-anti-copyleft Theo de Raadt style
perspective seems to be on the decline, maybe because it is
hard... though as said, I do admire people who truly take this
perspective).  But if you're straight up looking to proprietize
software (or any other works) then it really isn't freedom you're
concerned with at all, it's strategy.  I actually think that many
people aren't maliciously trying to deceive people, they probably
don't realize they're doing this.  But a lot of people are, you hear
this perspective all the time, and the hypocrisy of it is really
annoying.
</p>
<p>
(And, by the way, if you're waving your finger at me over the edge of
your macbook about copyleft being nonfree while committing to your
GitHub account in-between working on your software as a service web
application and the game you're working for the iOS app store, sorry,
but I'm not going to take you seriously.)
</p>
<p>
Please don't deceptively use arguments about user freedoms when user
freedom isn't your primary concern, it diminishes those who are
actually concerned with principles and diminishes your own argument
when you had a perfectly good one already, one of strategy.
</p>





<div id="outline-container-3" class="outline-2">
<h2 id="sec-3">Some brief words on support</h2>
<div class="outline-text-2" id="text-3">



</div>

<div id="outline-container-3-1" class="outline-3">
<h3 id="sec-3-1">Support 1: Proprietary relicensing</h3>
<div class="outline-text-3" id="text-3-1">


</div></div></div><p>
On the support side, I think things are generally simpler to analyze.
Actually, there's one perspective on supporting copyleft that I think
is in decline but has traditionally played enough of a role that it's
worth observing: the financial incentive of proprietary relicensing.
The basic idea here is that the copyleft allows anyone to release free
work that integrates with or extend your own copylefted work, but if
they want to release something proprietary that integrates/expands
with your work, they need to relicense with you.
</p>
<p>
Over the last decade this strategy was very popular, but seems to be
rapidly on the decline for I suspect a couple of reasons: 1) it's not
generally as lucrative as organizations might like and 2) if you get
outside contributions and don't just throw code over the wall, you
generally need some sort of copyright assignment or contributor
agreement.  People seem less and less willing to sign such things
these days and furthermore they delay integrating contributions
(today's distributed collaboration systems have gotten people used to
being able to get their contributions integrated very quickly into a
codebase).
</p>
<p>
From my perspective, the decrease in this trend is probably not much
to be sad about, but it does probably help point to the perceived
decrease in copylefted works.
</p>




<div id="outline-container-3-2" class="outline-3">
<h3 id="sec-3-2">Support 2: Copyleft as a strategy for freedom</h3>
<div class="outline-text-3" id="text-3-2">


</div></div><p>
Now for the main reason for supporting copyleft: as a strategy (or
even as <a href="http://gondwanaland.com/mlog/2012/01/31/copyleft-regulates/">regulation</a>) for preserving user freedom.  I think I'm fairly
right in pinpointing this as strategy, I'm not sure I know of anyone
who seriously thinks that copyleft is a matter of principles (the FSF
directly says "Which license is best for a given library is a matter
of strategy, and it depends on the details of the situation" in the
article <a href="http://www.gnu.org/licenses/why-not-lgpl.html">Why you shouldn't use the Lesser GPL for your next library</a>)
and it's certainly not a requirement for a work to be considered
either free software or free culture.  The question really is then, if
we have preserving user freedoms in mind, is it a good idea?
</p>
<p>
Copyleft supporters tend to think yes, it is: going back to the judo
move metaphor, there's simply too much risk right now of being beaten
up otherwise, so some sort of form of self defense is necessary or at
least very useful.  By adding a requirement that others share alike,
we've helped to make sure that the commons is not commandeered by
interests that might not otherwise personally care about user freedom.
</p>





<div id="outline-container-4" class="outline-2">
<h2 id="sec-4">Some personal conclusions</h2>
<div class="outline-text-2" id="text-4">


</div></div><p>
So what do I think?  Actually, I already stated it: I'm in the
conditionally-consider-whether-or-not-copyleft-is-good camp.  I <b>am</b>
in the concerned-with-user-freedom camp, and I don't feel bad about
having a license condition that you're only violating if you're
proprietizing things.  So a more important question to me is: is
copyleft the most strategically beneficial licensing option?  And, as
I keep semi-saying, it depends.
</p>
<p>
I think it's worth recognizing that libraries are doing just fine
without copyleft.  In fact, it's now the case that almost everyone who
releases libraries does so under a permissive free and open source
software license.  And people do seem to be contributing back to those
libraries, as much or more than they would be if they were under
copyleft (mainly because the scope of people using them is higher and
because people seem to realize that you're lowering maintenance costs
by trying to give back your contributions into an actual codebase,
plus it feels great to have your code merged into a library you love).
So as for libraries, I think maybe copyleft isn't so necessary these
days as it used to be.
</p>
<p>
But a world where only libraries are free is also a world where
developers are free and users are not.  As someone who believes in
<i>user freedom</i>, that's not acceptable to me.  So if not libraries,
where <i>does</i> copyleft hold value?  And the answer is obvious:
applications.  Applications have traditionally been the areas that
have had the strongest copyleft.  They're also the area that's
receiving the least amount of attention from a free and open source
software perspective in emerging areas right now (web applications and
mobile applications).  Particularly I'm interested in the web world,
where we're winning on the library side and losing on the application
side.  What we do see is that free and open source web applications
still have a high proportion of copyleft licensing (think Wordpress
under the GPL and StatusNet under the AGPL).  I suspect copyleft has a
huge role to play here yet.
</p>
<p>
<b>An addendum:</b> I wrote this blogpost a while ago, but continued to
procrastinate on publishing it for some reason.  On that note, I've
just come back from <a href="https://us.pycon.org/2012/">PyCon</a>, which is an amazing conference, but one
generally that has a strong amount of the "release your libraries
under a permissive license, and snark on people who use copyleft" type
attitude (pretty much exactly in the manner of the
<a href="http://tom.preston-werner.com/2011/11/22/open-source-everything.html">Open Source (almost) Everything</a> article).  
Surprisingly, despite having a big logo of AGPL in our
<a href="http://pyvideo.org/video/725/40-mediagoblin-the-road-to-federation">poster session on MediaGoblin</a>, we only got one person who snarked at
us for the license choice (a pretty lame snarking at that, which was
"I think people who use copyleft are insecure", which sounded like
hyper-masculine chest thumping in licensing wars form).  What I wanted
to say in response to that person, but which I failed to do, was to
say: I think permissively licensed tools are still great, but I use
copyleft in the space that you probably would have proprietized it.  I
don't want to just "open source almost everything"... I want the whole
stack to be released as free software.  It's not just developer
freedom I'm concerned about, it's user freedom.  And I think that's
probably the difference.
</p>
<p>
<b>Another addendum:</b> It's been pointed out to me that maybe my position
on "libraries are doing just fine without copyleft" misses that, for
example, the state of Android device lockdown might be less abysmal if
that ecosystem were copylefted.  That's a fair point, though I'm
really honestly mostly a web developer and speaking from a web
developer space.  In the web world, I feel like the type of people who
are traditionally copyleft advocates completely fell asleep at the
wheel for a while, and the generation of (erk) "rails community" type
people took over.  And where they've driven us to is a place where the
whole ecosystem is so close to being free, but people stop right
before finishing the job.  And if I wrote copylefted libraries in this
space, for the most part, people will just not use it.  So why not
just be allies with those people, and in the space that they normally
lock things down, I can release things as copylefted free software web
applications?
</p>
