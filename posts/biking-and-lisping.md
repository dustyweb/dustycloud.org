title: Biking and Lisping
date: 2008-04-22 18:20
author: Christine Lemmer-Webber
slug: biking-and-lisping
---
Before I got my job at Google, I was reasonably fit. I also didn't have
a car, and traveled everywhere via biking and public transportation.
About three years later I'm quite overweight and haven't even owned a
bike for some time. There's probably a correlation there.

Besides weight gain there's also just the feel of biking that I've
missed. Well, since now's the best season for it, I decided to go out
and get one. I went to a place called [Working
Bikes](http://workingbikes.org/) in Chicago; they recycle old bikes to
create new working ones. I had no idea how furious the selling of bikes
is as soon as the location opens. Well, I got one, but the decision was
somewhat rushed. Its cheap, it works, but its pretty small and my butt
sure does hurt sitting on it.

I figured I'd just bike it home. That was a huge mistake, as it actually
took me several hours as working bikes is about thirteen miles from my
apartment, and I'm totally out of shape. (In my biking prime that would
have taken me no more than an hour and a half.) To top it off, I had no
phone on me, I forgot that you can take bikes on public transportation
here, and I was only wearing a t-shirt and it started raining (at least
closer to the lake on the bike path).

But in a way I've been thrown fully into the world of biking again and
I've forgotten that while the experience isn't fully unusual, the full
experience of biking takes a bit to get readjusted to again.

Interestingly enough, I've begun learning lisp again, partly due to some
frustrations I've had with Emacs and the realization that I just don't
know how to debug and extend it well enough. I was never super great at
lisp, but I wasn't bad either. Even for the parts I knew well though,
I'm finding that you never totally forget how to bike or code lisp, but
there's always a bit of relearning to do.

It's nice to return to these things again.
