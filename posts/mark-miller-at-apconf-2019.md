title: Mark S. Miller keynoting at ActivityPub Conf 2019
date: 2019-07-24 11:00
author: Christine Lemmer-Webber
tags: ocaps, markm, activitypub, foss
slug: mark-miller-at-apconf-2019
---
I am extremely pleased to announce that Mark S. Miller is keynoting at
[ActivityPub Conf
2019](https://dustycloud.org/blog/activitypub-conf-2019/)!

It's hard for me to understate how huge this is. Mark S. Miller works at
[Agoric](https://agoric.com/) which is leading the way on modern
application of object capabilities, which is convenient, since
exploration of how to apply object capabilities to federated social
networks is a major topic of interest on the fediverse.

But just leaving it at that would be leaving out too much. We can trace
Mark's work back the [Agoric papers](https://agoric.com/papers/) in 1988
which laid out the vision for a massive society and economy of computing
agents. (And yes, that's where the Agoric company got its name from.)

For 30 years Mark has been working towards that vision, and social
networks continued to intersect with its work. In the late 1990s Mark
was involved in a company working on the game Electric Communities
Habitat (it's hard to find information on it, but here's [a rare video
of it in action](https://www.youtube.com/watch?v=KNiePoNiyvE)).
(Although Mark Miller didn't work on it, Electric Communities Habitat
has its predecessor in [Lucasfilm's
Habitat](https://en.wikipedia.org/wiki/Habitat_(video_game)), which it
turns out was a graphical multiplayer game which ran on the Commodore
64!(!!!) You can see the entertaining [trailer for this
game](https://www.youtube.com/watch?v=VVpulhO3jyc)... keep in mind, this
was released in 1986!)

People who have read my blog before may know that I've talked about
building [secure social spaces as virtual
worlds](https://dustycloud.org/blog/spritely/): part of the reason I
know it is possible is that Electric Communities Habitat for the large
part built it and proved the ideas possible. Electric Communities the
company did not survive, but the ideas lived on in the [E programming
language](http://erights.org/), which I like to describe as "the most
interesting and important programming language you may have never heard
of".

While the oldschool design of the website may give you the impression
that the ideas there are out of date, time and time again I've found
that the answers to my questions about how to build things have all been
found on [erights.org](http://erights.org/) and in [Mark Miller's
dissertation](http://erights.org/talks/thesis/index.html).

Mark's work hasn't stopped there. Many good ideas in Javascript (such as
its promises system) were largely inspired from Mark's work on the E
programming language (Mark joined the standardization process of
Javascript to make it be possible to build ocap-safe systems on it),
and... well, I can go on and on.

Instead, I'm going to pause and say that I'm extremely excited that Mark
has agreed to come to ActivityPub Conf to help introduce the community
to the ideas in object capabilities. I hope the history I laid out above
helps make it clear that the work to coordinate cooperative behavior
amongst machines overlaps strongly with our work in the federated social
web of establishing cooperative behavior amongst communities of human
beings. I look forward to Mark helping us understand how to apply these
ideas to our space.

Has this post got you excited? At the time of me writing this, there's
still space at [ActivityPub Conf
2019](https://dustycloud.org/blog/activitypub-conf-2019/), and there's
still time (until Monday July 29th) to submit talks. See the [conference
announcement](https://dustycloud.org/blog/activitypub-conf-2019/) for
more details, and hope to see you there!

**EDIT:** I incorrectly cited Mark Miller originally as being involved
in Lucasfilm's Habitat; fixed and better explained its history.
