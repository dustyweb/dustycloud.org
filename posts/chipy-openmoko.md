title: ChiPy Talk: OpenMoko Phone
date: 2008-09-11 23:39
tags: python, phone, foss, chipy, openmoko
author: Christine Lemmer-Webber
slug: chipy-openmoko
---
I gave a talk at [ChiPy](http://chipy.org), the Chicago Python
usergroup, tonight; my work, [Imaginary
Landscape](http://imagescape.com), hosted. It was a really good meeting.
Ian Bicking gave a talk on how to write a web app without a framework
(which was also kind of a peek into how to build your own python web
framework), Peter Fein gave a talk on his [Factory
module](http://code.google.com/p/grassyknoll/source/browse/branches/unhork/grassyknoll/lib/Factory.py)
(a really neat approach to simplifying redundant code), and I gave a
talk on the [OpenMoko phone](http://openmoko.org). We always advertise
the current ChiPy meeting as going to be "our best meeting ever", but I
really do think this was one of the best ones. That's not just me being
conceited (although I might be a little bit, it's hard for me to tell),
I thought all the talks went well.

I did think this was the best talk I've given though. Honestly, I wasn't
going to do it this month because I didn't think I was prepared
enough... I was going to wait until I had thrown together some example
applications. But instead it was a bit more ad-hoc... I just described
the phone and the history of it, and then gave a brief tutorial of how
to make a phone call by entering lines into the interpreter one by one.
I promised that it would be less than ten lines of code, and it was (it
was seven), but I noted that if we hadn't already turned on the antenna
(and it was turned on before I opened the interpreter), I would have
needed an additional four, bringing the number to eleven. Well anyway,
when I finally entered the last line (with the phone number of an
audience member as an argument) there was silence for a moment... and
then when his phone rang, well that's when the room started buzzing with
excitement.

I've been meaning to write about the OpenMoko phone on here for a while,
and why I think it matters, but maybe I can just post the talk... Carl
Karsten was kind enough to record it, and we might try to upload it to
[blip.tv](http://blip.tv/) next week. I've been talking about how it
would be kind of nice to start a ChiPy videocast, a channel one could
watch with Miro and stuff. Sounds like that might happen.
