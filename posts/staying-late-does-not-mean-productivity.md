title: Staying late at work does not mean productivity
date: 2008-03-20 00:25
author: Christine Lemmer-Webber
slug: staying-late-does-not-mean-productivity
---
I stayed late at work today. No, I mean *really* late. I guess I felt
bad about how many times I pressed refresh on the UPS tracking page
today waiting for that damn laptop.

And what the hell did I do at work staying so late? Oh, watching goddamn
anime [intarwebs](http://www.youtube.com/watch?v=j7al5TKaR8M)
[memes](http://www.youtube.com/watch?v=_mdMb6bRXt4&feature=related).
What the hell is wrong with me?

Well, at least tomorrow should be interesting. The Eee will arrive, and
I'll be meeting a couple of people from PyCon.
