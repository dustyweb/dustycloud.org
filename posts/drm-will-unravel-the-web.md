title: DRM will unravel the Web
date: 2017-09-18 14:30
author: Christine Lemmer-Webber
tags: drm, web, standards
slug: drm-will-unravel-the-web
---
I'm a web standards author and I participate in the W3C. I am co-editor
of the [ActivityPub](https://www.w3.org/TR/activitypub/) protocol,
participate in a few other community groups and working groups, and I
consider it an honor to have been able to participate in the W3C
process. What I am going to write here though represents me and my
feelings alone. In a sense though, that makes this even more painful.
This is a blogpost I don't have time to write, but here I am writing it;
I am emotionally forced to push forward on this topic. The W3C has
[allowed DRM to move forward on the
web](https://www.networkworld.com/article/3225456/internet/w3c-drm-appeal-fails-votes-kept-secret.html)
through the [EME specification](https://www.w3.org/TR/encrypted-media/)
(which is, [to paraphrase Danny O'Brien from the
EFF](http://dustycloud.org/blog/memories-of-a-march-against-drm/), a
"DRM shaped hole where nothing else but DRM fits"). This threatens to
unravel the web as we know it. How could this happen? How did we get
here?

Like many of my generation, I grew up on the web, both as a citizen of
this world and as a developer. "Web development", in one way or another,
has principally been my work for my adult life, and how I have learned
to be a programmer. The web is an enormous, astounding effort of many,
many participants. Of course, Tim Berners-Lee is credited for much of
it, and deserves much of this credit. I've had the pleasure of meeting
Tim on a couple of occasions; when you meet Tim it's clear how deeply he
cares about the web. Tim speaks quickly, as though he can't wait to get
out the ideas that are so important to him, to try to help you
understand how wonderful and exciting this system it is that we can
build together. Then, as soon as he's done talking, he returns to his
computer and gets to hacking on whatever software he's building to
advance the web. You don't see this dedication to "keep your hands
dirty" in the gears of the system very often, and it's a trait I admire.
So it's very hard to reconcile that vision of Tim with someone who would
intentionally unravel their own work... yet by allowing the W3C to
approve DRM/EME, I believe that's what has happened.

I had an opportunity to tell Tim what I think about DRM and EME on the
web, and unfortunately I blew it. At TPAC (W3C's big
conference/gathering of the standards minds) last year, there was a
protest against DRM outside. I was too busy to take part, but I did talk
to a friend who is close to Tim and was frustrated about the protests
happening outside. After I expressed that I sympathized with the
protestors (and that I had even indeed [protested myself in
Boston](http://dustycloud.org/blog/memories-of-a-march-against-drm/)), I
explained my position to my friend. Apparently I was convincing enough
where they encouraged me to talk to Tim and offer my perspective; they
offered to flag them down for a chat. In fact Tim and I did speak over
lunch, but -- although we had met in person before -- it was my first
time talking to Tim one-on-one, and I was embarassed for that first
interaction would me to be talking about DRM and what I was afraid was a
sore subject for him. Instead we had a very pleasant conversation about
the work I was doing on ActivityPub and some related friends' work on
other standards (such as Linked Data Notifications, etc). It was a good
conversation, but when it was over I had an enormous feeling of regret
that has been on the back of my mind since.

Here then, is what I wish I had said.

Tim, I have [read your article on why the W3C is supporting
EME](https://www.w3.org/blog/2017/02/on-eme-in-html5/), and that I know
you have thought about it a great deal. I think you believe what you are
doing what is right for the web, but I believe you are making an
enormous miscalculation. You have fought long and hard to build the web
into the system it is... unfortunately, I think DRM threatens to undo
all that work so thoroughly that allowing the W3C to effectively
green-light DRM for the web will be, looking back on your life, your
greatest regret.

You and I both know the dangers of DRM: it creates content that is
illegal to operate on using any of the tooling you or I will ever be
able to write. The power of DRM is not in its technology but in the
surrounding laws; in the United States through the DMCA it is a criminal
offense to inspect how DRM systems work or to talk about these
vulnerabilities. DRM is also something that clearly cannot itself be
implemented as a standard; it relies on proprietary secrecy in order to
be able to function. Instead, EME defines a DRM-shaped hole, but we all
know what goes into that hole... unfortunately, there's no way for you
or I to build an open and interoperable system that can fit in that EME
hole, because DRM is antithetical to an interoperable, open web.

I think, from reading your article, that you believe that DRM will be
safely contained to just "premium movies", and so on. Perhaps if this
were true, DRM would still be serious but not as enormous of a threat as
I believe it is. In fact, we already know that [DRM is being used by
companies like John
Deere](https://www.wired.com/2015/04/dmca-ownership-john-deere/) to say
that you don't even own your own tractor, car, etc. If DRM can apply to
tractors, surely it will apply to more than just movies.

Indeed, there's good reason to believe that some companies will want to
apply DRM to every layer of the web. Since the web has become a full-on
"application delivery system", of course the same companies that apply
DRM to software will want to apply DRM to their web software. The web
has traditionally been a book which encourages being opened; I learned
much of how to program on the web through that venerable "view source"
right-click menu item of web browsers. However I fully expect with EME
that we will see application authors begin to lock down HTML, CSS,
Javascript, and every other bit of their web applications down with DRM.
(I suppose in a sense this is already happening with javascript
obfuscation and etc, but the web itself was at least a system of open
standards where anyone could build an implementation and anyone could
copy around files... with EME, this is no longer the case.) Look at the
prevelance of DRM in proprietary applications elsewhere... once the
option of a W3C-endorsed DRM-route exists, do you think these same
application developers will not reach for it? But I think if you develop
the web with the vision of it being humanity's greatest and most
empowering knowledge system, you must be against this, because if enough
of the web moves over to this model the assumptions and properties of
the web as we've known it, as an open graph to free the world, cannot be
upheld. I also know the true direction you'd like the web to go, one of
linked data systems (of which ActivityPub is somewhat quietly one). Do
you think such a world will be possible to build with DRM? I for one do
not see how it is possible, but I'm afraid that's the path down which we
are headed.

I'm sure you've thought of these things too, so what could be your
reason for deciding to go ahead with supporting DRM anyway? My suspicion
is it's two things contributing to this:

1.  Fear that the big players will pick up their ball and leave. I
    suspect there's fear of another WHATWG, that the big players will
    simply pick up their ball and leave.
2.  Most especially, and related to the above, I suspect the funding and
    membership structure of the W3C is having a large impact on this.
    Funding structures tend to have a large impact on decision making,
    as a kind of [Conway's
    Law](https://en.wikipedia.org/wiki/Conway%27s_law) effect. W3C is
    reliant on its ["thin
    gruel"](http://manu.sporny.org/2016/rebalancing/) of funding from
    member organizations (which means that large players tend to have a
    larger say in how the web is built today).

I suspect this is most of all what's driving the support for DRM within
the W3C. However, I know a few W3C staff members who are clearly not
excited about DRM, and two who have quit the organization over it, so
it's not that EME is internally a technology that brings excitement to
the organziation.

I suppose at this point, this is where I diverge with the things I could
have said in the past and did not say as an appeal to not allow the W3C
to endorse EME. Unfortunately, today [EME made it to
Recommendation](https://arstechnica.com/gadgets/2017/09/drm-for-html5-published-as-a-w3c-recommendation-after-58-4-approval/).
At the very least, I think the W3C could have gone forward with the
[Contributor Covenant proposed by the
EFF](https://www.eff.org/pages/objection-rechartering-w3c-eme-group#covenant),
but did not. This is an enormous disappointment.

What do we do now? I think the best we can do at this point, as
individual developers and users, is speak out against DRM and refuse to
participate in it.

And Tim, if you're listening, perhaps there's no chance now to stop EME
from becoming a Recommendation. But your voice can still carry weight. I
encourage you to join in speaking out against the threat DRM brings to
unravel the web.

Perhaps if we speak loud enough, and push hard enough, we can still save
the web we love. But today is a sad say, and from here I'm afraid it is
going to be an uphill battle.

**EDIT:** If you haven't yet read Cory Doctorow / the EFF's [open letter
to the
W3C](https://www.eff.org/deeplinks/2017/09/open-letter-w3c-director-ceo-team-and-membership),
you should.
