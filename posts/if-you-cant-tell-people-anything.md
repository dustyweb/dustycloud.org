title: If you can't tell people anything, can you show them?
date: 2020-08-29 12:05
author: Christine Lemmer-Webber
tags: foss, spritely
slug: if-you-cant-tell-people-anything
---

The other day I made a
[sadpost on the fediverse](https://octodon.social/@cwebber/104746037303599668)
that said: "simultaneously regularly feel like people don't take the
directions I'm trying to push seriously enough and that I'm not worth
taking seriously".
(Similarly, I've also joked that "imposter syndrome and a Cassandra
complex are a hell of a combo" before.)
I got a number of replies from people, both publicly and privately, and
the general summary of most of them are, "We do care!  The stuff you're
working on seems really cool and valuable!  I'll admit that I don't
really know what it is you're talking about but it sounds important!"
(Okay, and I just re-read, and it was only a portion of it that even said
the latter part, but of course, what do I emphasize in my brain?)
That was nice to hear that people care and are enthusiastic, and I did
feel much better, but it did also kind of feel like confirmation that
I'm not getting through to people completely either.

But then [jfred](https://mastodon.sdf.org/@jfred) made an
[interesting reply](https://mastodon.sdf.org/@jfred/104747234928467298):

> Yeah, that feels familiar. Impostor syndrome hits hard. You're
> definitely worth taking seriously though, and the projects you're
> working on are the most exciting ones I've been following.
> 
> As for people not taking the directions you're pushing
> seriously... I've felt the same at work, and I think part of it is
> that there's only so much one person can do. But also part of it is:
> [http://habitatchronicles.com/2004/04/you-cant-tell-people-anything/](http://habitatchronicles.com/2004/04/you-cant-tell-people-anything/)
>
> ...it's hard to get ideas across to someone until they can interact
> with it themselves

So first of all, what a nice post!
Second of all, it's kind of funny that jfred replied with this because
out of everyone, jfred is one of the people who's picked up and
understood what's happening in
[Spritely Goblins](https://gitlab.com/spritely/goblins)
in particular the most, often running or creating demos of things on
top of it using things I haven't even documented yet (so definitely
not a person I would say isn't taking me seriously or getting what the
work is doing).

But third, that link to [Habitat Chronicles](http://habitatchronicles.com/)
is right on point for a few reasons: first of all, Spritely is *hugely*
influenced by the various generations of Habitat, from the original
[first-ever-graphical-virtual-worlds Habitat](https://www.youtube.com/watch?v=VVpulhO3jyc)
(premiering on the Commodore 64 in the mid 1980s, of all things!)
to [Electric Communities Habitat](https://www.youtube.com/watch?v=KNiePoNiyvE),
especially because that's where the [E programming language](http://erights.org/)
came from, which I think it's safe to say has had a bigger influence on
[Spritely Goblins](https://gitlab.com/spritely/goblins) than anything
(except maybe
[this paper by Jonathan Rees](http://mumble.net/~jar/pubs/secureos/secureos.html),
which is the first time I realized that "oh, object capability security
is just normal programming flow").
But also, that blogpost in particular was so perfect about this subject:
[You can't tell people anything](http://habitatchronicles.com/2004/04/you-cant-tell-people-anything/)...!

In summary, the blogpost isn't saying that people aren't foolishly
incapable of understanding things, but that people in general don't
understand well by "being explained to".
What helps people understand is *experiences*:

> Eventually people can be educated, but what you have to do is find a
> way give them the experience, to put them in the
> situation. Sometimes this can only happen by making real the thing
> you are describing, but sometimes by dint of clever artifice you can
> simulate it.

This really congealed for me and helped me feel justified in an
approach I've been taking in the Spritely project.
In general, up until now I've spent most of my time between two
states: coding the backend super-engineering stuff, and coding
demos on top of it.
You might in the meanwhile see me post technobabble onto
[my fediverse](https://octodon.social/@cwebber/) or
[birdsite](https://twitter.com/dustyweb) accounts, but I'm not in
general trying too hard to write about the structurally interesting
things going on until it comes time to write documentation
(whether it be for [Goblins](https://docs.racket-lang.org/goblins/index.html),
or the [immutable storage](https://gitlab.com/dustyweb/magenc/blob/master/magenc/scribblings/intro.org)
and [mutable storage](https://gitlab.com/spritely/crystal/blob/master/crystal/scribblings/intro.org) writeups).
But in general, the way that I'm convinced people will get it is not
by talk but by first, demonstration, and second, use.

Aside from the few people that have picked up and played with Goblins
yet, I don't think I've hit a sufficient amount of "use" yet in Spritely.
That's ok, I'm not at that stage yet, and when I am, it'll be fairly clear.
(ETA: one year from now.)
So let's talk about demonstration.

The first demo I wrote was the
[Golem demo](https://gitlab.com/spritely/golem/blob/master/README.org),
that showed roughly that distributed but encrypted storage could be
applied to the fediverse.
Cute and cool, and that turned the heads of a few fediverse
implementers.

But let's face it, the best demo I've done yet was the
[Terminal Phase time travel demo](https://dustycloud.org/blog/goblins-time-travel-micropreview/).
And it didn't hurt that it had a cool looking animated GIF to go
with it:

![Time travel in Spritely Goblins shown through Terminal Phase](https://dustycloud.org/gfx/goodies/terminal-phase-goblins-time-travel.gif)

Prior to this demo, people would ask me, "What's this Goblins thing?"
And I'd try to say a number of things to them... "oh, its a
distributed, transactional, quasi-functional distributed programming
system safe to run in mutually suspicious networks that follows object
capability security and the classic actor model in the style of the E
programming language but written in Scheme!"
And I'd watch as their eyes glaze over because
*why wouldn't their eyes glaze over* after a statement like that, and
then I'd try to explain the individual pieces but I could tell
that the person would be losing interest by then and
*why wouldn't they lose interest* but even realizing that I'd kind of
feel despair settling in... 

But when you show them a pew pew space lasers game and oh wow why
is there time travel, *how did you add time travel*, is it using
functional reactive programming or something?
(Usually FRP systems are the only other ones where people have
seen these kinds of time travel demos.)
And I'd say nope!
It doesn't require that.
Mostly it looks like writing just straightahead code but you get
this kind of thing for free.
And the person would say, wow!
Sounds really cool!  How much work does it take to add the time
travel into the game?
And I just say: no extra work at all.
I wrote the whole game without testing anything about time travel or
even thinking about it, then later I just threw a few extra lines to
write the UI to expose the time travel part and it just worked.  And
that's when I see peoples' heads explode with wonder and the
connections start to be made about what Goblins might be able to do.

But of course, that's only a partial connection for two reasons.
One is that the time travel demo above only shows off a small,
minute part of the features of Goblins.
And actually, the least interesting of them!
It doesn't show off the distributed programming or asynchronous
programming parts, it doesn't show off the cool object capability
security that's safe to run in mutually suspicious networks.
But still: it gave a taste that something cool is happening here.
Maybe Chris hasn't just been blowing a bunch of time since
finishing the [ActivityPub](https://www.w3.org/TR/activitypub/)
standardization process
[about two and a half years ago](https://www.w3.org/blog/news/archives/6785).
(Yikes, two and a half years ago!?!)

To complete the rest of that demonstration of the other things
in the system requires a different kind of demo.
Terminal Phase was a demo to show off the synchronous half of Goblins,
but where Goblins really shines is in the asynchronous, distributed
programming stuff.
That's not ready to show off yet, but I'll give you the first taste
of what's in progress:

![Goblins chat GUI demo](https://dustycloud.org/misc/goblins-gui-integration.gif)

(Actually a bit more has progressed since I've recorded that GIF,
multiple chatrooms and etc, but not really worth bothering to show
off quite yet.)

Hmm, that's not really all that thrilling.  A chatroom that looks about
the same level of featureful, maybe less, than IRC?
Well, it could be more exciting if you hear that the
[full chat protocol implementation](https://gitlab.com/spritely/goblin-chat/-/blob/94207fd0f3290f8ec6b087cde167bc78a6329560/chat-backend.rkt)
is only about 250 lines of code, including authenticating users and
posts by users.
That's smaller even than its
[corresponding GUI code](https://gitlab.com/spritely/goblin-chat/-/blob/94207fd0f3290f8ec6b087cde167bc78a6329560/goblin-chat-gui.rkt),
which is less than 300 lines of code.
So the exciting thing there is how much heavy lifting Goblins takes
care of for you.

But that's hardly razzle-dazzle exciting.
In order for me to hint at the rest of what's happening here, we need
to put out an asynchronous programming demo that's as or more
interesting than the time travel demo.
And I expect to do that.
I hope soon enough to show off stuff that will make people go,
"Oh, what's *going on* here?"

But even that doesn't complete the connection for people,
because *showing* is one thing but to complete the loop, we need
people to *use* things.
We need to get this stuff in the hands of users to play with and
experiment themselves.
I have plans to do that... and not only that, make this stuff
*not* intimidating for newcomers.
When Spritely guides everyday people towards extending Spritely from
inside of Spritely as it runs, that's when it'll really click.

And once it clicks sufficiently, it'll no longer become exciting,
because people will just come to expect it.
A good example of that comes from the aforementioned
[You can't tell people anything](http://habitatchronicles.com/2004/04/you-cant-tell-people-anything/) article:

> Years ago, before Lucasfilm, I worked for [Project Xanadu](http://xanadu.com/)
> (the original hypertext project, way before this newfangled World
> Wide Web thing). One of the things I did was travel around the
> country trying to evangelize the idea of hypertext. People loved it,
> but nobody got it. Nobody. We provided lots of explanation. We had
> pictures. We had scenarios, little stories that told what it would
> be like. People would ask astonishing questions, like “who’s going
> to pay to make all those links?” or “why would anyone want to put
> documents online?” Alas, many things really must be experienced to
> be understood. We didn’t have much of an experience to deliver to
> them though — after all, the whole point of all this evangelizing
> was to get people to give us money to pay for developing the
> software in the first place! But someone who’s spent even 10 minutes
> using the Web would never think to ask some of the questions we got
> asked.

Eventually, if we succeed, the ideas in Spritely will no longer seem
exciting... because people will have internalized and come to expect
them.
Just like hyperlinks on the web today.

But to get there, in the meanwhile, we have to get people interested.
To become so successful as to be mundane, we have to first be
razzle-dazzle exciting.
And to that end, that's why I take the demo approach to Spritely.
Because it's hard to tell someone something... but showing them,
that's another matter.

**PS:** It's also not true that people don't get what I'm doing, and
that's even been reflected materially.
I've been lucky to be supported over the last few years from a
combination of
[a grant from Samsung's Stack Zero](https://dustycloud.org/blog/samsung-stack-zero-grant/)
and
[one from NLNet](https://dustycloud.org/blog/spritely-nlnet-grant/),
not to mention quite a few [donors on Patreon](https://www.patreon.com/cwebber).
I do recognize and appreciate that people are supporting me.
In some ways receiving this support makes me feel more seriously about the
need to demonstrate and prove that what I'm doing is real.
I hope I am doing and will continue to do a sufficient job, and hope that
the upcoming demos contribute to that more materially!

**PPS:** If, in the meanwhile, you're *already* excited, check out
the [Goblins documentation](https://docs.racket-lang.org/goblins/index.html).
The most exciting stuff is coming in the *next* major release
(which will be out soon), which is when the distributed programming
tools will be made available to users of the system for the first time.
But if you want to get a head start, the code you'll be writing will
mostly work the same between the distributed and non-distributed
(as in, distributed across computers/processes) asynchronous stuff,
so if you start reading the docs today, most of your code will already
just work on the new stuff once released.
And if you do start playing around, maybe drop by the
[#spritely channel on freenode](https://webchat.freenode.net/?channels=spritely)
and say hello!
