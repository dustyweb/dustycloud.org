title: That's embarrassing
date: 2008-03-19 13:09
author: Christine Lemmer-Webber
slug: embarrassing-rss
---
[A friend and former coworker](http://joelapenna.com/) pointed out that
my blog's RSS feed is still pointing to <http://example.com>.

I'm going to fix it as soon as I get off of work today. Like, the moment
I get off.
