title: Spritely's NLNet grant: Interface Discovery for Distributed Systems
date: 2020-05-13 13:54
author: Christine Lemmer-Webber
tags: spritely, grant, funding, interfaces, foss
slug: spritely-nlnet-grant
---

I've been putting off making this blogpost for a while because I kept
thinking, "I should wait to do it until I finish making some sort of
website for [Spritely](https://dustycloud.org/blog/spritely/) and make
a blogpost there!"
Which, in a sense is a completely reasonable thought because right now
Spritely's only "website" is a
[loose collection of repositories](https://gitlab.com/spritely),
but I'd like something that provides a greater narrative for what
Spritely is trying to accomplish.
But that also kind of feels like a distraction (or maybe I should just
make a very minimal website) when there's something important to
announce... so I'm just doing it here (where I've been making all the
other Spritely posts so far anyway).

Spritely is an [NLnet](https://nlnet.nl/) (in conjunction with the
European Commision / Next Generation Internet initative) grant
recipient!
Specifically, we have received a grant for "Interface Discovery for
Distributed Systems"!
I'll be implementing the work alongside Serge Wroclawski.

There are two interesting sub-phrases there: "Interface Discovery"
and "Distributed Systems".
Regarding "distributed systems", we should really say "mutually
suspicious open-world distributed systems".
Those extra words change some of the requirements; we have to assume
we'll be told about things we don't understand, and we have to assume
that many objects we interact with may be opaque to us... they might
lie about what kind of thing they are.

Choosing how to name interfaces then directly ties into something I
wrote about here more recently, namely
[content addressed vocabulary](https://dustycloud.org/blog/content-addressed-vocabulary/).

I wrote more ideas and details about the interfaces ideas
[email to cap-talk](https://groups.google.com/forum/#!topic/cap-talk/YBF3QMKgvXs)
so you can read more there if you like... but I think more details
about the interfaces thoughts than that can wait until we publish
a report about it (and publishing a report is baked into the grant).

The other interesting bit though is the "distributed" aspect; in order
to handle distributed computation and object interaction, we need to
correctly design our protocols.
Thankfully there is a lot of good prior art to work from, usually some
variant of "[CapTP](http://www.erights.org/elib/distrib/captp/index.html)"
(Capability Transport Protocol), as implemented in its original form by
[E](http://www.erights.org/), taking on a bit of a different form in
the [Waterken](http://waterken.sourceforge.net/) project, adapted in
[Cap'N Proto](https://capnproto.org/),
as well as with the new work happening over at [Agoric](https://agoric.com/).
Each of these variants of the core CapTP ideas have tried to tackle some
different use cases, and [Goblins](https://docs.racket-lang.org/goblins/index.html)
has its own needs to be covered.
Is there a possibility of convergence?
Possibly... I am trying to understand the work of and communicate with
the folks over at Agoric but I think it's a bit too early to be
conclusive about anything.
Regardless, it'll be a major milestone once Spritely Goblins is able
to actually live up to its promise of distributed computation, and work
on this is basically the next step to proceed on.

When I first [announced Spritely](https://dustycloud.org/blog/spritely/)
about a year and a half ago I included a section that said
"Who's going to pay for all this?" to which I then said,
"I don't really have a funding plan, so I guess this is kind of a
non-answer. However, I do have a
[Patreon account](https://www.patreon.com/cwebber) you could donate to."
To be honest, I was fairly nervous about it... so I want to express my
sincere and direct appreciation to [NLnet](https://nlnet.nl/) alongside
the European Commission / Next Generation Internet Initiative, along with
[Samsung Stack Zero](https://samsungnext.com/whats-next/category/podcasts/decentralization-samsung-next-stack-zero-grant-recipients/),
and all the folks donating on [Patreon](https://www.patreon.com/cwebber)
and [Liberapay](https://liberapay.com/cwebber/).
With all the above, and especially the new grant from NLnet, I
should have enough funding to continue working on Spritely through
a large portion of 2021.
I am determined to make good on the support I've received, and am
looking forward to put out more interesting demonstrations of this
technology over the next few months.
