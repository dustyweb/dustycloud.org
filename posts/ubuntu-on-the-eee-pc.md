title: Ubuntu on the Eee PC
date: 2008-03-23 23:05
author: Christine Lemmer-Webber
slug: ubuntu-on-the-eee-pc
---
This will be my final post regarding the Eee PC for some time to come, I
promise. I got Ubuntu installed on the Eee, and I thought I'd give my
impressions about that.

First of all, the results from a number of Google queries on how to do
the installation pointed me to some interesting and well written
articles, but I was having a lot of trouble until I actually visited the
Ubuntu wiki. The steps you want to take are definitely these:

-   [Make a bootable Ubuntu Live/Install USB
    key](https://help.ubuntu.com/community/Installation/FromUSBStick).
    This is worth doing even if you aren't using the Eee, especially if
    you follow the instructions on how to create a persistent USB
    installation. However, its critical for the Eee, since the Eee lacks
    any sort of optical drive.
-   Then follow the instructions for actually [installing Ubuntu on the
    Eee](https://help.ubuntu.com/community/EeePC) . I found the install
    to be pretty normal, but its definitely worth following the
    instructions on this page to optimize the life of your flash disk,
    for installing the wireless drivers, etc.

But yeah, it was all very, very easy once I found the right resources.
And now I've got a real operating system on here! It's way, way better
than running the default Xandros install.

The only problem I'm having is how apparent it is that many applications
just aren't developed to run at a resolution less than 800x600, and
since the model of the Eee that I have has a maximum resolution of
800x480, I sometimes have to to hold down alt to drag windows around.

Here's a list of applications I've tested which work really nicely on
the small resolution:

-   Firefox
-   Blender
-   zsnes
-   Emacs
-   GNOME Terminal
-   GIMP (had to modify the docks a bit, but once I did so all fit very
    nicely)
-   Pidgin
-   OpenOffice
-   Aisleriot Solitaire
-   Mines (minesweeper clone)
-   Chess
-   Ekiga

Applications that were unusable:

-   Gnome Blackjack: Required an unnecessarily large window
-   Wesnoth: While I could start this up in windowed mode (fullscreen
    would not work) it was totally unusable as I could not access the
    buttons from the main menu. Constantly moving around the window to
    play this game is just far too obnoxious of an idea to even
    seriously contemplate.

Other than that, a number of applications had preferences menus or
graphical wizards which required a lot of manual positioning to
navigate, but nothing too frustrating, and since these are things that
one needs to access very rarely, I don't really find myself bothered at
all. Now, the next generation of the Eee is both more expensive and more
powerful: for 400 bucks (50 dollars more than what I paid) you can get a
savvy 1024×600 resolution screen. My suspicion is that this will be good
enough for most people, though I think the current system I have is
still Good Enough For Me (tm).

Conclusions: Asus has definitely done the right thing here. Sure, they
could have shipped with a better distribution, but the fact that this
machine actually ships with and was designed for Linux means that I knew
I could buy it already knowing all the hardware would work with my
operating system and distribution of choice. Since the resolution is
good enough and not too unusual, and since it ships with a keyboard and
a touchpad, this means that finally Linux users have access to an
ultraportable device that isn't trying to reinvent the browser, email
clients and feed readers because of limitations or peculiarity of the
screen or input. For the most part, you know you can run the
distribution and applications that you already know and love. And not
reinventing the wheel is a great thing. And despite how great these
devices have done for the proliferation of free and open source
software, this just can't be said for the Nokia handhelds, the Zaurus,
or even a project I'm still a huge fan of, the OpenMoko phone.
