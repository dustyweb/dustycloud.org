title: Javascript beyond Javascript (with Guile?)
date: 2014-08-15 11:40
author: Christine Lemmer-Webber
tags: javascript, beyond-javascript, guile, lisp
slug: javascript-beyond-javascript
---
I'm learning more in my spare time / off normal hours about compilers
and graph theory and reading various books on lisp. I have an agenda
here, no idea if it'll happen; at the very worst I put a lot of tools in
my toolkit that I should have had. But there is another reason... Well,
this one's easiest to lay out point for point, so here goes:

> -   Python is still my favorite language to write in day to day, but I
>     guess that I keep feeling that any language that doesn't have a
>     way to transcompile *nicely* to Javascript (or isn't Javascript
>     itself) is unideal for writing really great web applications, for
>     a simple reason: modern web applications are highly interactive,
>     and I feel that a *really* stellar web application needs to share
>     some code between the backend and the frontend. This becomes more
>     obvious when writing dynamic templates or forms.
>
>     While Python probably has hands-down the nicest asynchronous
>     library in current development with asyncio, the above problem
>     makes it feel like you can only do so much in the web world with
>     it. ([The Javascript
>     Problem](http://www.haskell.org/haskellwiki/The_JavaScript_Problem)
>     is a good page on the Haskell wiki which puts this all pretty
>     nicely.)
>
>     There's a lot lost by not being able to interweave applications
>     between the frontend and the backend. Sadly, the best option in
>     Python right now is to just give up and write entirely separate
>     codebases in javascript that interact with your main codebase. But
>     clients on the web are becoming thicker and thicker these days
>     (even more so with technology like websockets and webrtc becoming
>     prevalent), so this feels both weak and redundant.
>
> -   You could say "just write javascript!" then, but let's face it, I
>     don't really like Javascript. The language *is* getting better by
>     leaps and bounds as time goes on, but a lot of the enthusiasm for
>     the language still feels like Stockholm syndrome to me. But we're
>     still *left with* Javascript. So what, then?
>
> -   Transcompiling actually is becoming an increasingly appealing
>     target. With asm.js this might not only be possible but actually
>     very optimal. One only need look at the [Unreal
>     demo](https://blog.mozilla.org/blog/2014/03/12/mozilla-and-epic-preview-unreal-engine-4-running-in-firefox/)
>     to see just how feasible transcompilation has become.
>
> -   So okay, let's say we're doing some kind of transcompiling. We
>     have two options, "transcompile wholesale", or "transcompile a
>     subset that's acceptable for javascript and local evaluation".
>     Obviously the former is desirable if possible, though the latter
>     is a lot easier.
>
> -   Indeed, transcompiling a subset has already been done, and well,
>     by [Clojurescript](http://clojurescript.net/). I'm not really
>     excited by anything too tied to the JVM, but still, pretty cool.
>     Maybe good enough to get me into a hybrid Clojure/Clojurescript
>     solution. But still... there's no asm.js target (maybe in the
>     future?), and it feels like you're tossing out *so much* with
>     Clojurescript, it really isn't the same language too much, just a
>     very similarly overlapping language. I haven't tried it though...
>     this is just from reading docs and watching talks :)
>
> -   We could say maybe we could transcompile something like Python,
>     but assuming we really want to have something like a template
>     engine, we probably want to be writing in a full fledged version
>     of the language, not a restricted subset. Python feels way too
>     huge to expect for people to load in their browsers. So, what's
>     more lightweight?
>
> -   Let's continue in the lisp direction. Is there a way we can get a
>     complete implementation in the browser, but of a language that's a
>     bit more lightweight? How about Scheme? Scheme is notoriously
>     simple to implement, maybe even too simple... and Javascript
>     shares a lot of overlap with Scheme in design...
>
> -   But which Scheme? How about
>     [Guile](https://www.gnu.org/software/guile/)? Yes, Guile, GNU's
>     extension language based on scheme.

Okay, wait, digression before we continue... I want to talk a bit more
about Guile, because up until a few months ago my impression of it was
still the somewhat-dismissive impression I had a few years ago. But I've
come back to looking at Guile again. Largely this is thanks to Dave
Thompson's [Sly project](https://gitorious.org/sly). There is some
[seriously](http://dthompson.us/guile-2d-is-now-named-sly.html)
[cool](http://dthompson.us/functional-reactive-programming-in-scheme-with-guile-2d.html)
stuff going on with Sly, and that made me want to look into what's
happening with Guile.

What I found is that Guile isn't the same thing it was a few years ago.
For evidence of all the cool things that are happening under the hood, I
point you to [wingolog](http://wingolog.org/) (warning: danger of being
a real time sink; linking to Andy Wingo's blog has been described as
"wingorolling" by a friend of mine). Guile isn't just an interpreter
anymore, it has a sophisticated ["compiler
tower"](https://www.gnu.org/software/guile/manual/html_node/Compiler-Tower.html#Compiler-Tower)
since Guile 2.0. With the levels of abstraction that exist in Guile,
could compiling to javascript/asm.js be a target? (What makes this feel
extra appealing/feasible: Andy Wingo also [works at his day
job](http://www.igalia.com/compilers) on improving javascript virtual
machines, so maybe...?)

Well, better to ask Guile hackers themselves if it's possible... and
I've pestered a number of them. The responses seem to be (and I may be
representing incorrectly):

> -   Getting the core language of Guile (which is of course scheme)
>     ported over is not the hard part.
> -   Making a subset of the language that works in Javascript is not
>     too hard, but isn't too interesting. It would be much better to
>     have the whole language work.
> -   Overall, the most tedious part of porting guile to target any
>     non-C target will be all of the library procedures currently
>     implemented in C... however, gradually the Guile team is reducing
>     the amount of C code in Guile and replacing it with Scheme code.
> -   The problem is the runtime; you probably would want to re-use some
>     higher level things like the allocator and garbage collector.
>     Emscripten might do garbage collection correctly (guile relies on
>     [Boehm GC](http://www.hboehm.info/gc/) to do garbage collection);
>     emscripten seems to provide a similar garbage collector? Tail
>     calls are also a problem until at least es6 is common, and even
>     then until they are optimized.
> -   Maybe someone could try transcompiling Guile with
>     [emscripten](https://github.com/kripken/emscripten/wiki) as a
>     test, but that probably wouldn't provide the right integration,
>     and regardless, this would probably be a bit uncomfortable because
>     emscripten relies on llvm, not gcc.
> -   Targeting asm.js brings things more low-level, but the asm.js
>     typedarray heap may not be appropriate; the guile-on-js heap and
>     the javascript heap should really be the same.

Okay, so, that's a good number of problems that need to be overcome.
Certainly it makes it feel like things are a ways off, but then again,
it seems like there's strong *interest*. And it *feels* feasible... as
an outsider to development. :)

But even assuming all the above are resolved, there are some other
problems that I think scheme will always face adoption-wise:

> -   *I* like lisp, and *I* like parentheses. I have emacs set up with
>     rainbow-delimiters, smartparens, and highlight-parentheses (set up
>     just to highlight the background, not the foreground, so it
>     doesn't clobber rainbow-delimiters). Lisp's greatest enemy has
>     never been its functional capability, but a general parenthesitis
>     in the general population. This the big one, so let's come back to
>     it in a moment.
> -   Lots of scheme (lisp generally, but *especially* scheme) feels
>     like it just is too in love with ideas that make it hard to
>     approach. "car" and "cdr" are not good names but form the very
>     basis of the language for [historical reasons where even prominent
>     lispers have had to backtrack to remember
>     why](http://www.iwriteiam.nl/HaCAR_CDR.html). Linked lists as the
>     core of lisp is great, but overuse of them is encouraged when
>     other data types are much more appropriate: see teaching
>     associative lists before hashmaps. While iteration tools are
>     provided, there's simply too much emphasis on recursion. Recursion
>     is powerful and awesome and necessary to solving many important
>     computer science problems, but rarely needed by say, a web
>     developer, and significantly harder to read and wrap one's mind
>     around than iteration.
> -   That said, these could be overcome with better introductory
>     material. There seems to be interest in the guile world for this,
>     thankfully!
> -   Module names often look like robot serial numbers ("srfi-13") than
>     anything human-readable and feel like they really need aliases.
> -   The Guile community is very small, and does not seem to be very
>     diverse. Outreach is greatly needed.

Returning to "parenthesitis", while I love lisp and all its parentheses,
admittedly it's not the most readable language ever. I can wake up
groggily at 5AM, be tossed a block of Python code, and even through
blurry eyes, I can get a feel for what's happening in the code just by
its structure. I can't say the same of any lisp.

But there's a way out of that situation! Guile's compiler stack is
nicely set up so that another syntax can be laid on top of that. Guile's
VM has semi-complete language implementations of ecmascript and some
other languages on top of it. Recently Arne Babenhauserheide has written
a [whitespace to lisp
preprocessor](http://www.drachentränen.de/light/english/wisp-lisp-indentation-preprocessor)
named wisp; I don't feel it really solves the problem by making things
so much more readable, but it's a nice demonstration.

The most promising resolution I think would be to implement a syntax
akin to [Julia](http://julialang.org/) on top of Guile. If you haven't
looked at Julia, it's a cool project: it has a python-like syntax with
lisp-like power, and many other interesting features, including
macros(!) in a language that is certified safe for those with
parenthesitis. In fact, the language is largely *built* on top of
scheme! So it might be nice to have a "sugarcoat" language that sits on
top of Guile.

If all the above were achieved and a well developed web framework were
written on top of Guile, it could be well positioned for writing web
applications that are a joy to write, both on the backend and frontend.

One more thing: the complaint about "don't use emscripten" because it's
built on top of llvm is an indication of how sorely needed a working
javascript/asm.js compiler target is in GCC. ARM, X86, SPARC... these
are all important compiler targets to have, but to advance user freedom
where the user is today, the browser is the most important target of
all.
