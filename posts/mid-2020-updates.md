title: Some updates: CapTP in progress, Datashards, chiptune experiments, etc
date: 2020-06-30 19:14
author: Christine Lemmer-Webber
tags: goblins, captp, datashards, chiptunes
slug: mid-2020-updates
---

([Originally written](https://www.patreon.com/posts/38809138) as a post
[for Patreon donors](https://www.patreon.com/cwebber).)

Hello... just figured I'd give a fairly brief update.  Since I wrote
my last post I've been working hard towards the distributed
programming stuff in Goblins.

In general, this involves implementing a protocol called
[CapTP](http://wiki.erights.org/wiki/CapTP), which
is fairly obscure... the idea is generally to apply the same "object
capability security" concept that Goblins already follows but on a
networked protocol level.  Probably the most prominent other
implementation of CapTP right now is being done by the
[Agoric](https://agoric.com/) folks,
[captp.js](https://github.com/Agoric/agoric-sdk/blob/master/packages/captp/lib/captp.js).
I've been in communication with them... could we achieve
interoperability between our implementations?  It could be cool, but
it's too early to tell.  Anyway it's one of those technical areas
that's so obscure that I decided to
[document my progress on the cap-talk mailing list](https://groups.google.com/forum/#!topic/cap-talk/xWv2-J62g-I%5B1-25%5D),
but that's becoming the length of a small novel... so I guess, beware
before you try to read that whole thing.  I'm far enough along where
the main things work, but not quite everything (CapTP supports such
wild things as distributed garbage collection...!!!!)

Anyway, in general I don't think that people get too excited by
hearing "backend progress is happening"; I believe that implementing
CapTP is even more important than standardizing ActivityPub was in the
long run of my life work, but I also am well aware that in general
people (including myself!) understand best by seeing an interesting
demonstration.  So, I do plan another networked demo, akin to the
[time-travel Terminal Phase demo](https://dustycloud.org/blog/goblins-time-travel-micropreview/),
but I'm not sure just how fancy it will be (yet).  I think I'll have
more things to show on that front in 1-2 months.

(Speaking of Goblins and games, I'm putting together a little library
called [Game Goblin](https://gitlab.com/spritely/game-goblin)
to make making games on top of Goblins a bit easier; it isn't quite
ready yet but thought I'd mention it.  It's currently going through
some "user testing".)

More work is happening on the Datashards front; Serge Wroclawski
(project leader for Datashards; I guess you could say I'm "technical
engineer") and I have started assembling more documentation and have
put together [some proto-standards documents](https://datashards.net/).
(Warning: WIP WIP WIP!!!)  We are exploring with a standards group
whether or not Datashards would be a good fit there, but it's too
early to talk about that since the standards group is still figuring
it out themselves.  Anyway, it's taken up a good chunk of time so I
figured it was worth mentioning.

So, more to come, and hopefully demos not too far ahead.

But let's end on a fun note.  In-between all that (and various things
at home, of course), I have taken a bit of what might resemble
"downtime" and I'm learning how to make ~chiptunes / "tracker music"
with [Milkytracker](https://milkytracker.titandemo.org/), which is
just a lovely piece of software.  (I've also been learning more about
[sound theory](http://www.drpetter.se/article_sound.html) and have
been figuring out how to compose some of my own samples/"instruments"
from code.)  Let me be clear, *I'm not very good at it*, but it's fun
to learn a new thing.  Here's a [dollhouse piano thing](https://dustycloud.org/misc/dollhouse.flac)
([XM file](https://dustycloud.org/misc/dollhouse.xm)), the
[start of a haunted video game level](https://dustycloud.org/misc/ecto-house.flac)
([XM file](https://dustycloud.org/misc/conversations-with-a-computer.xm)),
a [sound experiment representing someone interacting with a computer](https://dustycloud.org/misc/conversations-with-a-computer.flac)
([XM file](https://dustycloud.org/misc/conversations-with-a-computer.xm)),
and the
[mandatory demonstration that I've figured out how to do C64-like](https://dustycloud.org/misc/siddy-start.xm)
[phase modulation](https://en.wikipedia.org/wiki/Phase_modulation) and
[arpeggios](https://en.wikipedia.org/wiki/Arpeggio) ([XM file](https://dustycloud.org/misc/siddy-start.xm)).
Is any of that stuff... "good"?  Not really, all pretty amateurish,
but maybe in a few months of off-hour experiments it won't be... so
maybe some of my future demos / games won't be quite as quiet!  ;)

Hope everyone's doing ok out there...
