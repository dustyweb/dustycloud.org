title: "Big Buck Bunny" Released!
date: 2008-05-31 15:24
author: Christine Lemmer-Webber
slug: big-buck-bunny-released
---
[Big Buck Bunny](http://www.bigbuckbunny.org/) (previously Project
Peach), the world's second fully "open movie" (the first being
[Elephants Dream](http://www.elephantsdream.org/)), has been
[released](http://www.bigbuckbunny.org/index.php/download/)! Grab a
torrent and go!

It's quite a good little short. Much more accessible, much less
experimental, than Elephant's Dream. It's also totally gorgeous, and
gained enough attention that its [CGTalk
thread](http://forums.cgsociety.org/showthread.php?f=59&t=637109&page=1&pp=15)
got frontpaged. CGTalk's community, in the past, was somewhat
notoriously snarky toward Blender for just being a hobbyist's software,
but from the talk going on, it's clear that that attitude is changing.

Great news for Blender, great news for the free culture movement, and
great news for blenderheads like me! The direct collaboration between
the artists and developers means [great new
features](http://www.blender.org/development/release-logs/blender-246/)
making it into blender's codebase.

And if you like the movie? Like Blender? Like supporting free culture
productions like this one? Well then, [consider buying a
DVD](http://www.blender3d.org/e-shop/product_info.php?products_id=97) so
you can support the [Blender
Foundation](http://www.blender.org/blenderorg/blender-foundation/). It's
good for you, good for the public at large, and you'll feel good about
it at the end.

Now I'm looking forward to the next projects coming out of the Blender
Institute, including [Project Apricot](http://apricot.blender.org/), the
Blender-based Big Buck Bunny game, [Creature
Factory](http://www.artificial3d.com/blog/2008/03/teasing/), a blender
training DVD made by the well known Blender artist Andy Goralczyk, and
lastly the whatever-successor-to-peach movie they're coming out with
next (I hear it involves robots). Lots to look forward to, but in the
meantime my Big Buck Bunny DVD is on its way, as is the [ManCandy
FAQ](http://www.blender3d.org/e-shop/product_info.php?products_id=99), a
Blender character animation training DVD.

Mmmm. Mancandy.
