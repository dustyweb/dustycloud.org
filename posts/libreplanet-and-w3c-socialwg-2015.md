title: LibrePlanet and W3C Social Working Group 2015
date: 2015-03-29 09:32
author: Christine Lemmer-Webber
tags: libreplanet, travel, talks, personal, foss
slug: libreplanet-and-w3c-socialwg-2015
---
Great trip, spent 2 weeks doing the following:

-   Attending W3C Social Working Group. Good things have happened; felt
    much more clear by the second day than by the first day. Lessons
    learned in that raising issues (normally I'm reasonably quiet, a
    certain amount of imposter syndrome while participating in the
    group) was helpful, some people said they were thinking about the
    same things. Anyway, much work to do ahead.
-   Hanging out at Deb Nicholson's place with friends
-   Gave a talk at Libreplanet which I'm happy with
-   Had an awesome night of vegan salted caramel root beer floats (the
    best thing ever it turns out) and Hanabi with Aeva Palecek, [Amy
    Guy](http://www.rhiaro.co.uk/), and Jessica Tallon; all friends of
    federation, but it was nice getting to decompress without worrying
    about that stuff for a bit. Way better than a trip to the bar!
-   So many great people at Libreplanet. I'm kind of sad that I didn't
    get to spend a lot of time with everyone, but the up side is that I
    got to spend a lot of time with some particularly great people, and
    the main problem was there was just too many good people at once. A
    good problem.
-   Mako and Karen both gave good keynotes, and I liked a lot of talks
-   Lots of Veggie Galaxy, eaten
-   Then took a trip to hang out with [\@Bassam
    Kurdali](https://pumprock.net/bkurdali), Fateh, Libby Reinish,
    Tristen, at their respective places. It was nice to do this with
    [\@Tsyesika / Jessica Tallon](https://microca.st/Tsyesika) joining
    me.
-   Much of the week spent from the "Nerdodrome", Hampshire College's
    animation studio, where Bassam works. I spent most of the week
    working on a deployment system I'm specc'ing out. If it turns out to
    be something I use, I'll update with more info.
-   Got to spend some time exploring Western Mass, which was great.

The trip was really worth it. I spent a lot of time with people I care
about, and it felt very productive.

I'm not planning another conference for the remainder of 2015 though.
Amongst other things, my back and wrists are killing me right now, and
doing this morning's stretches was a painful wake-up. And I have a lot
of difficult personal things in the year ahead. Oh, and let's not forget
about how much work there is to do on MediaGoblin that I haven't gotten
to.

But! This trip really helped set some directions for me for the year
ahead, and I'm really grateful for that.

Looking forward to LibrePlanet 2016!
