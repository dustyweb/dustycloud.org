title: March 7, 2008: Projects in Progress
date: 2008-03-07 13:27
author: Christine Lemmer-Webber
slug: march-7-2008-projects-in-progress
---
And now for a post that *isn't* me whining about politics.

It's recently become apparent to me just how many projects I have
floating around. Some of them are right on the brink of completion, some
of them in the earliest stages. Since I'm trying to figure out exactly
what I'm going to be working on at the moment, I figure I'll list them
out here, along with priorities and completion levels, as I see them.

```
+------------+--------+------+-----------------------------------------+
| Project    | Comp   | Prio | Status & Description                    |
|            | letion | rity |                                         |
+============+========+======+=========================================+
| Blog       | 80%    | me   | For the most part, the blog is in a     |
|            |        | dium | good spot. It works, obviously, but it  |
|            |        |      | still lacks a drafts and                |
|            |        |      | save-preview-publish workflow.          |
|            |        |      | Currently I'm posting posts in my test  |
|            |        |      | environment to make sure they're        |
|            |        |      | working before I take them live.        |
|            |        |      |                                         |
|            |        |      | There are a couple of other features    |
|            |        |      | I'd like to add, such as tagging and    |
|            |        |      | "edit logging", but those can wait.     |
+------------+--------+------+-----------------------------------------+
| Image      | 0%     | high | I haven't even started on this, but     |
| gallery    |        |      | it's definitely critical for the        |
|            |        |      | success of my site. I do a lot of       |
|            |        |      | artwork, and one of the main reasons I  |
|            |        |      | wanted to even start this website was   |
|            |        |      | so that I could have a place to show    |
|            |        |      | off all the things I'm working on.      |
|            |        |      |                                         |
|            |        |      | In fact, I've had a few projects that   |
|            |        |      | I've been wanting to show off on here,  |
|            |        |      | but its not really possible until I get |
|            |        |      | the gallery up and running.             |
+------------+--------+------+-----------------------------------------+
| Web based  | 2%     | me   | A while ago I wrote a frustrated email  |
| media      |        | dium | at my work complaining about the awful, |
| player     |        |      | repetitive music we play on the radio   |
|            |        |      | there. I proposed that I would build a  |
|            |        |      | web-based music player where we could   |
|            |        |      | all upload music and collaboratively    |
|            |        |      | set playlists, vote for what songs we   |
|            |        |      | like, et cetera.                        |
|            |        |      |                                         |
|            |        |      | It sounds like an overly ambitious      |
|            |        |      | project, but I think it *will* happen,  |
|            |        |      | since I'm being told that, while I      |
|            |        |      | can't work on it on-hours at work,      |
|            |        |      | they'd be willing to fund for           |
|            |        |      | late-night hacking pizza, etc. One of   |
|            |        |      | my coworkers, Tamas, is down for        |
|            |        |      | collaborating with me on this one too.  |
|            |        |      |                                         |
|            |        |      | I've paper-prototyped the whole thing,  |
|            |        |      | I've learned the basics of gstreamer,   |
|            |        |      | and I already know how to do web        |
|            |        |      | development, so I expect that all will  |
|            |        |      | go well with it.                        |
+------------+--------+------+-----------------------------------------+
| Ble        | 60%    | low  | This is something I've just been doing  |
| nder-based |        |      | for fun in my relaxation time. I'm      |
| fanart     |        |      | doing a fanart for a [comic that I      |
|            |        |      | like](http://okk.keenspace.com). It's   |
|            |        |      | surprisingly coming along really well,  |
|            |        |      | but it doesn't take any sort of         |
|            |        |      | priority. Still, I don't want it to     |
|            |        |      | totally fall off the radar.             |
+------------+--------+------+-----------------------------------------+
| Getting my | 70%    | high | I'm actually able to make calls with    |
| Openmoko   |        |      | this every now and then... or I was,    |
| phone to   |        |      | until I updated to a broken image.      |
| work       |        |      | Maybe this wouldn't take as high of     |
| nicely     |        |      | priority except that I paid a good deal |
|            |        |      | of money for this thing and I continue  |
|            |        |      | to pay for service for it. Well, I knew |
|            |        |      | it was a developer version when I       |
|            |        |      | bought it. Mostly I wanted to support   |
|            |        |      | the project. However, I hear that there |
|            |        |      | are Python bindings coming soon, which  |
|            |        |      | would make this a whole lot cooler.     |
|            |        |      | Maybe I'll blog a bit more about my     |
|            |        |      | experiences thus far later.             |
+------------+--------+------+-----------------------------------------+
| Web-based  | 1%     | low  | This might raise in priority soon, but  |
| game       |        |      | my friend                               |
|            |        |      | [Jeff](http://blog.codexile.net) and I  |
|            |        |      | have worked on some game development in |
|            |        |      | the past, and are meeting up this       |
|            |        |      | Sunday to talk about some new           |
|            |        |      | development soon.                       |
|            |        |      |                                         |
|            |        |      | Currently I'm putting the priority here |
|            |        |      | as low because we haven't agreed on any |
|            |        |      | specifics, and it isn't clear how far   |
|            |        |      | this project is going to make it yet.   |
|            |        |      | Who knows? It might become              |
|            |        |      | significantly higher in priority soon.  |
+------------+--------+------+-----------------------------------------+
| Lingo      | ???    | ???  | My long dead comic,                     |
|            |        |      | [Lingo](http://lingocomic.com)... oh    |
|            |        |      | how I miss working on you. :(           |
|            |        |      |                                         |
|            |        |      | Unfortunately, I just don't know where  |
|            |        |      | I should go with this comic at the      |
|            |        |      | moment. I'm not as happy with the       |
|            |        |      | original storyline I sketched out for   |
|            |        |      | it, and overtime I've come to regret    |
|            |        |      | having named the characters after me    |
|            |        |      | and my friend.. it just seems far too   |
|            |        |      | egotistical.                            |
|            |        |      |                                         |
|            |        |      | That being said, I've sketched up some  |
|            |        |      | new ideas that I would actually be      |
|            |        |      | fairly enthusiastic about pursuing. I   |
|            |        |      | just really don't know what to do about |
|            |        |      | the characters, and that's really       |
|            |        |      | largely why nothing has happened since  |
|            |        |      | I've graduated.                         |
+------------+--------+------+-----------------------------------------+
| Blob       | 40%?   | low  | Jeff and I produced a [demo of          |
| bit/Gloopy |        |      | this](ht                                |
|            |        |      | tp://www.pyweek.org/e/the_sticky_bits/) |
|            |        |      | during the last                         |
|            |        |      | [PyWeek](http://pyweek.org/) sprint.    |
|            |        |      |                                         |
|            |        |      | I think if we revisited the collision   |
|            |        |      | code this could be a feasible project,  |
|            |        |      | but I don't feel any momentum for it at |
|            |        |      | the moment.                             |
+------------+--------+------+-----------------------------------------+
| PyStage    | 95%    | high | Now here's a project which I could      |
|            |        |      | almost kick out the door this weekend   |
|            |        |      | (and maybe I should).                   |
|            |        |      |                                         |
|            |        |      | PyStage is, as the name might suggest,  |
|            |        |      | an animation system for python. Not     |
|            |        |      | only that, but it *already works in its |
|            |        |      | present state*. In fact, I've already   |
|            |        |      | [given a talk about                     |
|            |        |      | it](htt                                 |
|            |        |      | p://video.google.com/videoplay?docid=-9 |
|            |        |      | 06354546937878632&q=chipy&total=278&sta |
|            |        |      | rt=0&num=10&so=0&type=search&plindex=0) |
|            |        |      | at [ChiPy](http://chipy.org). (I show   |
|            |        |      | up about 32 minutes into the video.)    |
|            |        |      |                                         |
|            |        |      | So why haven't I released it yet?       |
|            |        |      | Partly a fear that nobody else will     |
|            |        |      | find it useful, and partly because I    |
|            |        |      | haven't put up any sort of page for it. |
|            |        |      | Maybe I should just get over my fears   |
|            |        |      | and put up some page for it on Google   |
|            |        |      | Code or something.                      |
+------------+--------+------+-----------------------------------------+
```

Well that's about the summary of things. I guess there's a lot more that
I've put on my plate than I even realized. This isn't even considering
the volunteering I've been hoping to do. But this weekend, I'm going to
limit myself to working on the gallery code, finishing the blog code
here on dustycloud, and exploring that web game idea with Jeff a bit.
And next week I'll really, honestly kick PyStage out the door.

For serious.
