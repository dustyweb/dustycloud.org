title: Slew of posts ahead
date: 2008-04-22 18:19
author: Christine Lemmer-Webber
slug: slew-of-posts-ahead
---
I've been writing a lot on the go lately. Thus, I have a whole backlog
of posts to put up here, including some stuff from
[Flourish](http://www.flourishconf.com/flourish2008/). I'm going to be
posting them a bit sporadically as I clean them up, so just be aware the
dates I'm posting them on aren't necessarily when I wrote them, in case
that becomes confusing.

In the future I'll try to be more timely about posting just after
writing.
