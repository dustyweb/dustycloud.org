title: It arrives!  The Eee PC is here!
date: 2008-03-20 13:57
author: Christine Lemmer-Webber
slug: eepc-arrives
---
I'm bubbling with excitement. It just arrived. Oh man oh boy oh man oh
boy!

Morgan and I drove by the office and saw two UPS trucks passing to the
next house. I parked and *bolted* out of the car, bubbling with
questions about whether any packages for me arrived. The man calmly
explained that, yes, he left a few packages inside, as someone (Ric Lee,
the company founder) had let him in.

Sure enough, there it was. I've opened it and haven't played with it
that much, but let me say that even having seen it before, I didn't
realize how small it is until I actually held it in my hands. The
keyboard is indeed tiny, but manageable. My WPM was hindered a bit by
the size as I tested it out, but its definitely doable. Overall, the
design feels totally solid. My only regret is that I got a white one.
(White? I always prefer black technology, or at least something cool
like deep blue.) That's OK, I have a bunch of python stickers from PyCon
that I'll plaster it with. The Linux distribution it ships with has a
very Palm OS-like desktop by default, which is pretty lame, but since I
can install Ubuntu, I'm happy.

Now I'm off to meet [Doug Napoleone](http://www.dougma.com/) to talk
about next year's PyCon site. I might be volunteering more this time
around. We'll see how things go.

More impressions later as I continue to play with this thing.
