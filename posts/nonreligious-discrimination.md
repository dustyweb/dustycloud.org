title: Non-religious discrimination
date: 2012-04-27 20:01
tags: personal, hatecrime, freespeech, family
author: Christine Lemmer-Webber
slug: nonreligious-discrimination
---
Today I heard about a family member being bullied and discriminated
against for being an atheist. And at the moment, I am incredibly angry,
incredibly furious. I don't think I was prepared to be so angry about
this. I don't even think my life gave me an expectation or even much
thought that this could happen to someone I loved. And so I am feeling a
lot of anger that I don't know how to deal with, so I guess I will just
write it out.

Let me step back for a moment. I am an atheist who was raised as a
Christian. I came out as an atheist in early adulthood. I won't say my
family was very happy about it, but I never faced any serious
disrespect, and I haven't faced any discrimination about being an
atheist in my lifetime. To be honest, after coming out, I never gave it
too much thought. I mean, my father is even a religious teacher, and he
was understanding about it. Granted, he is about the most liberal,
tolerance-minded religion teacher possible; when I told him I was an
atheist, and explained my thoughts behind things, he said something like
"Well, that makes sense to me. I think you're approaching a belief
system and ethics in your own way." Maybe that is why I unsusbcribed
from the [atheist subreddit](http://www.reddit.com/r/atheism); it seemed
full of so many people who were just so angry at religious folks for
their intolerance against atheists or for their simplemindedness or
whatever. But I always thought of my father, a religious person who was
willing to and even interested in talking about my beliefs and
non-beliefs. My mother seemed a bit more disappointed, but she never
changed her attitude to me, and she has always supported me and what I
believe in. So that kind of bitterness that these other people held
atainst non-atheists never resonated with me. It seemed absurd and
reactionary.

And then today happened.

I don't use Facebook anymore, but Morgan occasionally tells me about
things she sees on there. Today I heard about my cousin-in-law's son
getting bullied and physically assulted for being an atheist. Worse yet,
it seems he's been bullied and physically assulted for being an atheists
by students at his *public school* and it seems at this point unlikely
that anyone is going to get in trouble for it.

The short version of it goes like this. The kid goes on a camping trip
for the fifth grade school outing. There's a campfire at night, and a
group of boys sit around and tell ghost stories. One kid declares that
there really are ghosts, but it's okay because if everyone around the
campfire believes in Jesus, he will protect them from the ghosts, and
that if anyone *didn't* believe in Jesus, they should leave the campfire
so that they don't endanger the safety of the circle. So my cousin's kid
gets up to leave the circle. The other boys notice. They chase after
him. They begin calling him names, and shoving him around. (Apparently,
they were forceful, but they didn't leave any marks. Regardless, that's
physical assult. And as you can imagine, being a kid and shoved around
and harassed by a group of other kids outdoors in the middle of the
night has got to be a pretty scary experience.)

The school doesn't call up my cousin(-in-law) and tell her. I'm not sure
if they didn't know, or if they didn't think it was important Anyway, my
cousin doesn't know until her kid gets home from school. And of course
he's scared.

They live in a very small, predominantly Christian town. She tells us
that normally she tells her children that it is best to just not talk
about their beliefs, and normally they do not. But of course, in this
case the issue was forced: he was forced to either hold to his beliefs
or lie about them. So he got up, and he walked away, as were the
instructions of the other children. And then he was assulted and
bullied.

We called Morgan's cousin on the phone. She tells us that of course, she
is angry. But she also wonders if she should blame herself for "doing
this to her children". But what has she done that any other parent does
not do? She has passed on her beliefs from herself and her children. And
in fact she has taught her children that even if they do not believe in
other religions, they should understand them. Her children are not
naive, and in fact they have read up on a lot of religions. But these
beliefs, even these non-beliefs, are their beliefs. And that's their
right. It isn't the first time her children have been bullied about such
things either, but it is the first time the bullying has turned violent.

What can she do? She lives in a very small and predominantly Christian
town. If she raises a fuss (which she might) the chance of the school
doing anything is fairly small. And even if they do, she does that at
risk of alienating her family from the rest of the community. And so
what is she to do?

In a moment like this, it is easy to see why so many atheists seem angry
much of the time. If the reverse had been true, with atheists beating up
a Christian for believing in Jesus-the-savior, of course there would be
recourse. There would be a mob. And thus, in retaliation of this
hypocrisy, it is tempting in this moment to join the tribes of the angry
atheists.

On the other hand, I think of my father, who I imagine would read this
and be just as angry and upset as I am. And so it is not true that all
Christians, in all communities, are this way. And if this happened in
someplace larger and more diverse and more liberal... if this happened
in Chicago, this would probably not be tolerated. And of course, this
would be bad, just as bad, if it were discrimination in any other
situation: whether the child were a Jew, a Christian, a Buddhist, a
Muslim. Any such discrimination is not okay. And certainly, many of the
groups listed above probably face similar fears. It is likely that our
cousin would also tell her children to not tell their schoolmates about
their beliefs if their were Muslim. And so, part of the problem is
simply something that is already generally known to be true: it can be a
frightening thing to be a minority, any minority, in a small,
tight-knit, and discriminatory community.

I do feel though that this action is likely to not be viewed with the
same seriousness (or as the same act as) "religious discrimination"
because by definition the religious belief expressed here is
non-religious. And so I will not join the crowd of atheists who feel
bitterly against all religious people. But today at least I have gained
an understanding of how someone can come to develop those feelings. And
I am certainly angry, and I feel very justified in being so.
