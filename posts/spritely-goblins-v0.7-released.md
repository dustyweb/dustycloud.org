title: Spritely Goblins v0.7 released!
date: 2020-09-13 17:20
author: Christine Lemmer-Webber
tags: spritely, goblins, racket, lisp, foss
slug: spritely-goblins-v0.7-released
---

I'm delighted to say that
[Spritely Goblins](https://docs.racket-lang.org/goblins/)
[v0.7](https://gitlab.com/spritely/goblins/-/tree/v0.7)
has been released!
This is the first release featuring
[CapTP support](https://docs.racket-lang.org/goblins/captp.html)
(ie, "capability-secure distributed/networked programming support"),
which is a huge milestone for the project!

Okay, caveat... there are still some things missing from the CapTP
stuff so far; you can only set up a bidirectional connection between
two machines, and can't "introduce" capabilities to other machines
on the network.
Also setting up connections is an extremely manual process.
Both of those should be improved in the next release.

But still!
Goblins can now be used to easily write distributed programs!
And Goblins' CapTP code even includes such wild features as
*distributed garbage collection!*

As an example
(also mentioned in a [recent blogpost](/blog/if-you-cant-tell-people-anything/)),
I recently wrote a short
[chat program demo](https://gitlab.com/spritely/goblin-chat).
Both the client and server "protocol" code were
[less than 250 lines of code](https://gitlab.com/spritely/goblin-chat/-/blob/master/chat-backend.rkt),
despite having such features as authenticating users during
subscription to the chatroom and verifying that messages claimed by
the chatroom came from the users it said it did.
(The [GUI code](https://gitlab.com/spritely/goblin-chat/-/blob/master/goblin-chat-gui.rkt),
by contrast, was a little less than 300 lines.)
I wrote this up without writing any network code at all and then
tested hooking together two clients over Tor Onion Services using
Goblins' CapTP support, and it Just Worked (TM):

![Goblins chat GUI demo](https://dustycloud.org/misc/goblins-chat-captp-onion-services.gif)

What's interesting here is that
*not a single line of code was added to the backend or GUI to accomodate networking*;
the [host](https://gitlab.com/spritely/goblin-chat/-/blob/master/onion-gui-server.rkt)
and [guest](https://gitlab.com/spritely/goblin-chat/-/blob/master/onion-gui-client.rkt)
modules merely imported the backend and GUI files *completely unchanged* and
did the network wiring there.
Yes, that's what it sounds like: in Goblins you can write distributed
asynchronous programs 

This is the really significant part of Goblins that's starting to
become apparent, and it's all thanks to the brilliant design of
[CapTP](http://erights.org/elib/distrib/captp/index.html).
Goblins continues to stand on the shoulders of giants; thank you to
everyone in the ocap community, but especially in this case
Michael FIG, Mark S. Miller, Kevin Reid, and Baldur Jóhannsson, all of
whom answered an enormous amount of questions (some of them very
silly) about CapTP.

There are more people to thank too (too many to list here), and you
can see some of them in this
[monster thread on the captp mailing list](https://groups.google.com/forum/#!topic/cap-talk/xWv2-J62g-I)
which started on May 18th (!!!) as I went through my journey of trying
to understand and eventually implement CapTP.
I actually started preparing a few weeks before which really means that
this journey took me about four and a half months to understand and
implement.
As it turns out, CapTP is a surprisingly simple protocol protocol
in its coneptualization once you understand what it's doing (though
implementing it is a bit more complex).
I do hope to try to build a guide for others to understand and
implement on their own systems... but that will probably wait until
Goblins is ported to another language (due to the realative simplicity
of the task due to the language similarities, the current plan is to
port to Guile next).

Anyway.
This is a big deal, a truly exciting moment for goblinkind.
If you're excited yourself, maybe join the
[#spritely channel on irc.freenode.net](https://webchat.freenode.net/?channels=fossandcrafts).

OH!
And also, I can't believe I nearly forgot to say this, but if you want
to hear more about Spritely in general (not just Goblins), we just
[released a Spritely-centric episode of FOSS and Crafts](https://fossandcrafts.org/episodes/9-what-is-spritely.html).
Maybe take a listen!
