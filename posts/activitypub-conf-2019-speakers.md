title: ActivityPub Conf 2019 Speakers
date: 2019-08-07 10:15
author: Christine Lemmer-Webber
tags: apconf, activitypub
slug: activitypub-conf-2019-speakers
---
Good news everyone! The speaker list for [ActivityPub Conf
2019](https://dustycloud.org/blog/activitypub-conf-2019/) is here! (In
this document, below, but also in
[ODT](https://dustycloud.org/misc/APConfSpeakers.odt) and
[PDF](https://dustycloud.org/misc/APConfSpeakers.pdf) formats.)

(Bad news everyone: registration is closed! We're now at 40 people
registered to attend. However, we do aim to be posting recordings of the
event afterwards if you couldn't register in time.)

But, just in case you'd rather see the list of speakers on a webpage
rather than download a document, here you go:

# Keynote: Mark Miller, “Architectures of Robust Openness”

*Description coming soon!* But we're very excited about [Mark Miller
keynoting](https://dustycloud.org/blog/mark-miller-at-apconf-2019/).

# Keynote: Chris Lemmer-Webber, “ActivityPub: past, present, future”

This talk gives an overview of ActivityPub: how did we get to this
point? Where are we now? Where do we need to go? We'll paint a chart
from past to a hopeful future with better privacy, richer interactions,
and more security and control for our users.

# Matt Baer, “Federated Blogging with WriteFreely”

We're building out one idea of what federated blogging could look like
with separate ActivityPub-powered platforms, WriteFreely and Read.as --
one for writing and and one for reading. Beyond the software, we're also
offering hosting services and helping new instances spring up to make
community-building more accessible, and get ActivityPub-powered software
into more hands. In this talk I'll go over our approach so far and where
we're headed next.

# Caleb James DeLisle, “The case for the unattributed message”

Despite it's significant contribution to internet culture, the archetype
of the anonymous image board has been largely ignored by protocol
designers. Perhaps the reason for this is because it's all too easy to
conflate unattributed speech with unmoderated speech, which has shown
itself to be a dead end. But as we've seen from Twitter and Facebook,
putting a name on everything hasn't actually worked that well at
improving the quality of discourse, but what it does do is put already
marginalized people at greater risk.

What I credit as one of the biggest breakthroughs of the fediverse has
been the loose federation which allows a person to choose their
moderator, completely side stepping the question of undemocratic
censorship vs. toxic free speech. Now I want to start a conversation
about how we might marry this powerful moderation system to a forum
which divorces the expression of thought from all forms of identity.

# Cristina DeLisle, “OSS compliance with privacy by default and design”

Privacy is becoming more and more central in shaping the future of tech
and the data protection legislation has contributed significantly to
making this happen. Privacy by default and design are core principles
that are fundamental to how software should be envisioned. The GDPR that
came into the spotlight has a strong case to become a standard even
outside European borders, influencing the way we protect personal data.
However its impact might be, its implementation is still in its infancy.
OSS has found itself facing the situation and one aspect which is
particularly interesting on the tech side is how to incorporate the
principles of privacy by default and design into the software that we
build.

This talk is going to be an overview of how the GDPR has impacted FOSS
communities, what do we mean by privacy by default and by design, how
could we envision them applied in our OSS. It will bring examples from
which we might find something interesting to learn from, regardless if
we are looking at them as mistakes, best practices or just ways of doing
things.

# Michael Demetriou, “I don't know what I'm talking about: a newbie's introduction to ActivityPub”

I have just started my development journey in ActivityPubLand and I hope
to have a first small application ready before ActivityPubConf. I was
thinking that since I have close to zero experience with ActivityPub
development, I could document my first month of experience, describe the
onboarding process and point out useful resources and common pitfalls.
In the end I can showcase what I've done during this period.

# Luc Didry, “Advice to new fediverse administrators and developers”

Hosting an ActivityPub service is not like hosting another service… and
it's the same for developing ActivityPub software. Here is some advice
based on Framasoft's experience (we host a Mastodon instance and develop
two ActivityPub software: PeerTube and Mobilizon – the last one is not
yet out), errors and observations.

# Maloki, “Is ActivityPub paving the way to web 3.0?”

A talk about how we're walking away from Web 2.0, and paving the way to
Web 3.0 with ActivityPub development. We'll discuss what this could mean
for the future of the web, we'll look at some of the history of the web,
and also consider the social implications moving forward.

# Pukkamustard, “The Semantic Social Network”

ActivityPub uses JSON-LD as serialization. This means
[\@context]{.title-ref} field all over the place. But really there is
more behind this: ActivityPub speaks Linked Data. In this talk we would
like to show what this means and how this can be used to do cool things.
We might even convince you that the Fediverse is a huge distributed
graph that could be queried in very interesting ways - that the
Fediverse is a Semantic Social Network.

# Schmittlauch, “Decentralised Hashtag Search and Subscription in Federated Social Networks”

Hashtags have become an important tool for organising topic-related
posts in all major social networks, even having managed to spark social
movements like #MeToo. In federated social networks, unfortunately so
far the view on all posts of a hashtag is fragmented between instances.

For a student research paper I came up with an architecture for search
and subscription of hashtag-posts in federated social networks. This
additional backend for instances augments the Fediverse with a little
bit of P2P technology.

As this architecture is still at a conceptual stage, after presenting my
work I'd like to gather ideas and feedback from various Fediverse
stakeholders: What do global hashtags mean for marginalised people and
moderation, are they more a tool of empowerment or of harassment? How
can this concept be represented in the ActivityPub protocol? And what
stories do server devs have to tell about common attack scenarios?

# Serge Wroclawski, “Keeping Unwanted Messages off the Fediverse”

Spam, scams and harassment pose a threat to all social networks,
including the Fediverse. In this talk, we discuss a multilayered
approach to mitigating these threats. We explore spam mitigation
techniques of the past as well as new techniques such as OcapPub and
Postage.
