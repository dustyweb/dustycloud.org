title: Vote for Amy Guy on the W3C TAG (if you can)
date: 2020-12-21 15:07
author: Christine Lemmer-Webber
tags: standards, w3c
slug: vote-for-amy-guy
---

My friend [Amy Guy](https://rhiaro.co.uk/) is
[running for election](https://rhiaro.co.uk/2020/11/tag-statement)
on the [W3C TAG](https://www.w3.org/2001/tag/)
(Technical Architecture Group).
The TAG is an unusual group that sets a lot of the direction of the
future of standards that you and I use everyday on the web.
[Read their statement on running](https://rhiaro.co.uk/2020/11/tag-statement),
and if you can, ie if you're one of those unusual people labeled
as "AC Representative", please consider
[voting for them](https://www.w3.org/2002/09/wbs/33280/tagelect-2020/).
(Due to the nature of the W3C's organizational and funding structure,
only paying W3C Members tend to qualify... if you know you're working
for an organization that has paying membership to the W3C, find out
who the AC rep is and strongly encourage *them* to vote for Amy.)

So, why vote for Amy?
Quite simply, they're running on a platform of putting the needs of
users first.
Despite all the good intents and ambitions of those who have done
founding work in these spaces, this perspective tends to get
increasingly pushed to the wayside as engineers are pressured to shift
their focus on the needs of their immediate employers and large
implementors.
I'm not saying that's bad; sometimes this even does help advance the
interest of users too, but... well we all know the ways in which it
can end up not doing so.
And I don't know about you, but the internet and the web have felt an
awful lot at times like they've been slipping from those early ideals.
Amy's platform shares in a growing zeitgeist (sadly, still in the
wispiest of stages) of thinking and reframing from the perspective of
user empowerment, privacy, safety, agency, autonomy.
Amy's platform reminds me of
[RFC 8890: The Internet Is For End Users](https://tools.ietf.org/html/rfc8890).
That's a perspective shift we desperately need right now... for the
internet and the web both.

That's all well and good for the philosophical-alignment angle.
But what about the "Technical" letter in TAG?
Amy's standing there is rock-solid.
And *I know* because I've had the pleasure of working side-by-side
with Amy on several standards (including
[ActivityPub](https://www.w3.org/TR/activitypub/), of which we are
co-authors.

Several times I watched with amazement as Amy and I talked about some
changes we thought were necessary and Amy just got *in the zone*, this
look of intense hyperfocus (really, someone should record the Amy Spec
Editing Zone sometime, it's quite a thing to see), and they refactored
huge chunks of the spec to match our discussion.
And Amy knows, and deeply *cares*, about so many aspects of the W3C's
organization and structure.

So, if you can vote for, or know how to get your organization to vote
for, an AC rep... well, I mean do what you want I guess, but if you
want someone who will help... for great justice, vote Amy Guy to the
W3C TAG!
