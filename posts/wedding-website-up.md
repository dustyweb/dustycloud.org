title: Wedding website is up
date: 2009-02-28 14:44
tags: dustycloud, wedding
author: Christine Lemmer-Webber
slug: wedding-website-up
---
Long time in coming, but <http://wedding.dustycloud.org> is actually up
now. Probably the best looking website I've designed (note: by that I
mean graphic design, and I don't do the graphic design for any of the
[PCF](http://pculture.org) projects; that stuff is done by an incredibly
talented fellow, Morgan Knutson). Complete with the sketchy mess that I
try to pass off as my style.

I've got a few bits to put up left. Will update with more information as
I go.
