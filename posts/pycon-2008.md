title: Pycon 2008!
date: 2008-03-12 21:06
author: Christine Lemmer-Webber
slug: pycon-2008
---
So I said I was going to have the gallery done by the end of the
weekend, and that didn't happen, because smash brothers was released for
the Wii!\[1\] But I did say I'd have pystage released by the end of this
coming weekend, and that might happen, because I might give a lightning
talk about it at PyCon! But that will only happen if I finish things
tonight!

By the way, have I mentioned how excited I am about PyCon? This year is
extra cool because:

-   It's in Chicago
-   I'm going
-   [We did work for PyCon at my
    job](http://www.imagescape.com/pycon.html), and even released our
    [first open source
    application](http://www.imagescape.com/library/software/jobboard.html),
    which is [running on the pycon site right
    now](http://us.pycon.org/2008/jobboard/) (the company has open
    sourced other software before, but it's all been developer-oriented,
    whereas this is an actual application).

I'm extremely stoked. There are a lot of really great looking talks to
look forward to. Also, cool people like are going to be there, including
[Phil Hassey](http://www.philhassey.com/). I've already informed that
guy that I plan on stalking him. (Does it count as stalking if you
inform the person in advance that you are planning on doing so?)

\[1\] (To be fair, Jeff and I did bet together and do some concept work
for the game, but we also played a lot of smash brothers)
