title: Life Update October 2012
date: 2012-10-09 00:45
author: Christine Lemmer-Webber
tags: personal, life update
slug: life_update_october_2012
---
I'm long overdue for another life update. I guess the last update I gave
was [in June](http://dustycloud.org/blog/june-2012-life-update) with a
supplemental post to update that I was [leaving CC to focus on
MediaGoblin](http://dustycloud.org/blog/leaving-cc-to-work-on-mediagoblin).
So, 4 months. That doesn't seem like a long time when I put it in
month-numbers. In my mind though, it feels like a universe away. So many
changes have happened in my life at once that it feels impossible to
record them in one entry. Which I guess is why I intended to blog
earlier to avoid this situation, but of course, I didn't do such a
thing. Oh well, "life update" blogposts are fairly self-indulgent;
interesting mostly as a record to myself and to keep the scattered few
family and friends who have passing interest informed.

I don't think I can really intelligently list everything off, but let's
start with bullet points and see where we go from there.

> -   First of all, I wrapped up my work at Creative Commons. Well, kind
>     of. I'm still somewhat involved as a contractor (details on that
>     still being worked out though, so even that is vague), but anyway,
>     there certainly was a significant "wrapping up" phase that
>     happened during that exiting period of two months, during which a
>     lot of the other things I'll be talking about happened
>     consecutively. Largely I did a lot of work to try to put the tech
>     team in as best of a place as I could and push forward various
>     agendas I care about very much so personally forward (CC 4.0 stuff
>     especially). There was also a lot of Liberated Pixel Cup wrapup
>     stuff, but that actually kind of deserves its own entry, so I
>     guess I'll list that next.
>
> -   Liberated Pixel Cup's contest submissions on both art and code
>     wrapped up and we got tons of amazing results. Way more than we
>     anticipated, and way more than we prepared for. We did end up
>     [wrapping up the art
>     judging](http://lpc.opengameart.org/content/liberated-pixel-cup-art-winners-announced)
>     but very much so significantly after when I would have liked to
>     seen it wrapped up. Part of this was because of how overwhelmed we
>     were by so many high quality entries, and thus a lot of judges
>     fell through (not their fault necessarily given we didn't prepare
>     them, didn't know to prepare them, for the volume of stuff).
>     Another part of it was because Bart and I, the main organizers of
>     the project, both had several large disruptions to our project;
>     him with some family medical emergencies, me with being "homeless"
>     for two weeks (more on that below) and settling into a new place
>     and launching a major campaign all at once. The code side of
>     things still needs to be judged, and I'll be returning some focus
>     to organizing that shortly. Anyway, failure on our part at all
>     largely comes in another way from a large amount of success, so
>     that's a weird situation of pride and guilt that I'm feeling right
>     now. It's good to have one of my major dreams come true and proven
>     right, anyway.
>
> -   Kind of a weird off-note but right before the move I made a major
>     change to my mail setup. For years I had been using a terrible
>     pop + fetchmail + local spamasssassin + gnus setup that I had
>     cobbled together before I understand how any of those things
>     worked from mailing lists and wiki pages and all sorts of cargo to
>     build cults from. Problem: I could only check mail from my
>     desktop, and when traveling, I always had the stress-inducing
>     process of having to ssh into my desktop from wherever and open up
>     gnus. I finally decided I was tired of that, and in a long and
>     painful process that I really should have documented but didn't, I
>     moved my mail over from gnus and nnml with some hacky elisp over
>     to [offlineimap](http://offlineimap.org/) and the incredible
>     [mu4e](http://www.djcbsoftware.nl/code/mu/mu4e.html). mu4e is a
>     real pleasure... I even added a small extension called
>     [mu4e-uqueue](https://gitorious.org/mu4e-uqueue) to make iterating
>     through my mail a bit easier. Definitely happy with the change.
>
> -   So yes, about the move. First, about leaving, which I have more to
>     say about than probably makes sense. A couple of years ago we had
>     the misfortune of [moving to
>     DeKalb/Deklabbs](http://dustycloud.org/blog/moved-to-dekalb.html)
>     and a couple of months ago we had the good fortune to finally move
>     out. DeKalb wasn't so bad for Morgan (and she had given me the
>     option to live someplace closer to Chicago or in far west Chicago,
>     but I didn't want her to have such a long commute) mostly because
>     she had a community there. But I didn't... I was mostly friendless
>     and depressed, which isn't good when you work from home. I had
>     such a lack of community and sense of connection (excepting a
>     university LUG that I attended sporadically) that I realized there
>     were only a few things I would feel at all like I missed in
>     DeKalb: the food co-op, the coffee shop, and most especially the
>     restaurant Pita Pete's, which I ate at almost every other day (and
>     I made sure the exact last thing we did before driving out of town
>     for the final time was to get one last delicious seitan wrap).
>     Going out to eat was in some ways one of the few connections I had
>     to other people living in that town, so we did it quite a bit. On
>     the last day there I was in a "finally, I'm getting the fuck out
>     of this town" kind of mood. I went to the coffee shop, got a final
>     coffee, and the woman behind the counter asked me if I'd come
>     back. I said I didn't think so, I didn't think I'd miss anything,
>     except maybe this place a little bit and Pita Pete's, but not
>     really that much anyway. She told me that I should come back and
>     visit, they'd miss me (not really sure that's just one of those
>     things you say or not) and I asked her if she was still there as a
>     college student (largely because I had the "so, when are you gonna
>     get the fuck out of this town too?" type attitude on the mind) and
>     she said she used to be, but she stayed around because she loved
>     DeKalb. So, something about that moment felt significant, that
>     there really wasn't an intrinsic terribleness to the area... it
>     was really just a lack of connection to anything on my part.
>
> -   So enough whining about a place I don't even have to live at
>     anymore... we had someplace new and exciting to move to...
>     Madison! But before we could do that we had a two week space of
>     non-residence between our leases. Technically, we were without a
>     home, so were "homeless" in one sense, but that seems degrading to
>     people who are actually homeless, since our situation was the
>     opposite of any sort of hardship. (Whatever, I'm rambling.
>     Whatever to that too, this whole post is a big ramble.) Quite the
>     opposite: we decided to do something we really haven't done much
>     as a couple and do some vacation traveling.
>
>     We had a two part-trip, first in Boston, then in New York. In
>     Boston Morgan and I stayed with our good friend Deb Nicholson. For
>     Morgan, this was pure vacation. For me, it was kind of a
>     "work-cation"; I spent a lot of time hanging out at the FSF and
>     meeting with various free softwareish people (a few highlights
>     were meeting friends Mo and Ray of Fedora hackingness for lunch
>     and Bassam Kurdali and Fateh Slavitskaya of Tube for dinner, as
>     well as hanging out with a lot of friends from the FSF). Will
>     Kahn-Greene also came down and Will, Deb and I gathered to discuss
>     the MediaGoblin campaign, how we'd go about it, and whether we'd
>     go a Kickstarter type route or do things through the FSF (whom had
>     mentioned they would likely be interested in doing such a thing).
>     After laying out a long list of requirements that the FSF would
>     have to add for our campaign to work with them, we all agreed on
>     that route forward.
>
>     At some point, Deb's partner Ernie asked me when I was going to
>     stop working and start vacationing. Actually, hanging out with
>     free software people, and even doing the MediaGoblin stuff while
>     lurking at the FSF offices, had me in a better mood than I had
>     been in ages. Guess that's how I roll.
>
>     Nonetheless, we also did some wonderful hanging out and seeing
>     some touristy things with Deb, who as always, is a great host and
>     excellent friend. Anyway, Boston was great times.
>
> -   In-between Boston and New York, we thought we'd try to play it
>     cheap by going to a smaller town in-between and just relaxing and
>     reading and keeping things simple. So we MegaBus'ed it to
>     Hartford.
>
>     When I told people in Boston that we'd be having a few days in
>     Hartford, we got a lot of "Oh god, why would you do that?" and
>     stories about Hartford's insurance industry lobbying to kill all
>     taxes, thus completely not investing in any infrastructure, and
>     better hope you won't be stabbed, blah blah. I figured these were
>     exaggerations from township rivalry. It couldn't be any more
>     boring than DeKalb, anyway. I was wrong. I won't go into details,
>     but Hartford kind of feels like one huge ghetto. [Sad city
>     Hartford](http://sadcityhartford.blogspot.com/) indeed. Anyway, we
>     took the fastest trip out of there we could, which meant buying
>     extra bus tickets and spending the extra money I didn't want to
>     spend in New York, but there you go.
>
> -   New York was great though. I didn't really do any work, so that
>     was real vacation for me. Some high points were museums, walking
>     around Central Park, [weird but delightful
>     films](http://www.meetthefokkens.com/), [live
>     puppetry](http://en.wikipedia.org/wiki/Avenue_Q), and meeting
>     MediaGoblin contributors [Aaron
>     Williamson](http://copiesofcopies.org/webl/) and [Sam
>     Kleinman](http://www.tychoish.com/). But maybe most of all I
>     really enjoyed hanging out with [Karen
>     Sandler](http://identi.ca/kaz) and her husband. The original plans
>     to [record my interview on Free as in
>     Freedom](http://dustycloud.org/blog/interviewed-on-faif) while I
>     was in-person didn't work out, but on the up side we got lunch on
>     Karen's rooftop, which is had a crazy amazing view, and
>     unsurprisingly really great conversation.
>
>     Oh, and we didn't bring back bedbugs. Which is great, given that's
>     a huge phobia of mine, and we were staying in New York.
>
> -   We moved to Madison. Madison is, I will say, completely and
>     totally amazing. For some reason I was afraid of moving to another
>     college town after DeKalb, but there is simply no resemblance
>     between towns here. Madison has a great tech community, amazing
>     food, lots of interesting things going on and people, and so on.
>     Also, [we live between two
>     lakes](https://maps.google.com/maps?f=q&source=s_q&hl=en&geocode=&q=1208+Jenifer+Street,+Madison,+WI&aq=0&oq=1208+jenifer&sll=43.084993,-89.40642&sspn=0.243722,0.465546&vpsrc=0&ie=UTF8&hq=&hnear=1208+Jenifer+St,+Madison,+Wisconsin+53703&t=m&z=16)
>     and are three houses away from the town's amazing [Willy Street
>     Food Co-op](http://www.willystreet.coop/).
>
>     If you ever plan on stopping through Madison, maybe consider
>     [contacting me](http://dustycloud.org/contact/)... we have
>     crash-space, and an extra desk for people to work from.
>
>     Anyway, Madison is amazing. I'm very happy with the move.
>
> -   Some bad news on Morgan's health. I won't go into details, but
>     some things we thought were fixed weren't. Lots of stress.
>
> -   Last Saturday my mom got married. I really like the guy she
>     married, and the wedding was beautiful. Plus, I didn't screw up my
>     part in the ceremony too badly. Morgan and I danced for five hours
>     straight at the reception having a wonderful time and regretting
>     it the next day.
>
> -   Most significantly of all though is the MediaGoblin campaign. This
>     will get a post of its own shortly (hopefully we launch this week)
>     but what I will say now is that I've been working insanely hard on
>     it. The video, the website (with commissioned help from Jef van
>     Schendel), etc are all coming together well, and things are mostly
>     coordinated with the FSF.
>
>     In the meanwhile, I am really fried. I have put as much of myself
>     as is possible into the campaign over the last month and a half,
>     pretty much working as much around the clock as my body and mind
>     will let me. I guess I can't complain though if I am living the
>     dream?
>
>     Or hopefully at least I will be! It really depends on how the
>     fundraiser comes out. Really hoping for the best. As said, I've
>     poured myself into it... and I think at least that the results are
>     really good and I've tried my damndest.
>
>     I'm feeling quite confident that this campaign is the right thing
>     at the right time in many different ways, but most especially in
>     the "more socially important than ever" type way. Here's hoping
>     everything goes right. You'll certainly hear about it here when
>     things launch, which is fairly imminent.

So those are the things on my mind these days. More news about the
campaign comin' up shortly.
