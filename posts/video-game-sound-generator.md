title: Video Game Sound Generator
date: 2008-06-11 19:28
tags: sfxr, foss, music
author: Christine Lemmer-Webber
slug: video-game-sound-generator
---
Check out [sfxr](http://www.cyd.liu.se/~tompe573/hp/project_sfxr.html),
an awesome awesome awesome video game noise generator.

I've seriously been sitting here for the last half hour just goofing
around with it. I legally shouldn't be allowed to enjoy this so much. If
only the real world sounded this beautiful.

Hm. I wonder how hard it would be to use this to generate noises/notes
for some home-brewed chiptune-like beats... but probably nobody in the
world would like listening to that stuff but me.

**Update:** Looks like the same person is working on an unbelievably
cool looking music editor/synthesizer called
[musagi](http://www.cyd.liu.se/~tompe573/hp/project_musagi.html) which
seems extremely well suited for such means. I hope this gets released as
free and open source software some day.

**Another update:** Sorry to keep spazzing about the subject, but you
should seriously check out this dude's [article on how sound
works](http://www.cyd.liu.se/~tompe573/hp/article_sound.html). As
someone who's never studied the subject, I find myself surprised to be
mostly understanding this. Really an illuminating read.
