title: The return of Lingo
date: 2008-03-27 00:41
author: Christine Lemmer-Webber
slug: return-of-lingo
---
A while ago I gave a summary of the [huge number of
projects](/blog/view_post/march-7-2008-projects-in-progress/) I have
going on. I've been struggling to figure out what my primary focus
should be.

Well, over the last several days I've been going on a lot of walks.
Walking is one of my favorite activities, and this weather is the most
perfect time of the year for me to take a stroll. I find that I do my
best thinking on walks, and over the last few days I've been walking
until I accumulate enough thoughts, then I sit down, pull out my laptop
and type them all up.

Well, one of the things on my list of projects in progress was my old
comic [Lingo](http://lingocomic.com), which never really got that far in
the first place, but which I've always felt very attached to anyway. But
I really couldn't continue developing the comic. First of all, I want to
take the comic in a different direction than I was originally planning.
Second, I really regretted attaching myself and my friend Jay to the two
main characters; it felt both egotistical and limiting. I recently had a
nice talk about this with one of my friends, and she gave me her
assessment that it's alright for the characters to be based off of real
life people but to develop in their own directions.

Well with that in mind I went on another walk today; this time the focus
of my thoughts was how to bring Lingo back to life. I sat down on a
bench and typed up an outline of the story, and I think what I wrote
should keep me going for at least another thirty comics. Afterwards I
sketched out some new character designs for some of the forthcoming
characters and drew out some layout drafts.

I'm glad to say that not only do I now know where I want to bring the
comic, I wrote the script in a way that allows me to continue where I
left off (even though I didn't get too far) without having to start from
scratch. This is a huge relief, and the decision of what to do here was
one of the major roadblocks I was facing.

So, I've decided that for off-work hours projects, Lingo will take
number one priority. I'm going to try to turn this into a weekly comic.

By next Friday there will be a new comic up. I hope you look forward to
it as much as I do.
