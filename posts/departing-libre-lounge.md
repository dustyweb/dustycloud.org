title: Departing Libre Lounge
date: 2020-05-13 14:13
author: Christine Lemmer-Webber
tags: libre lounge, foss, podcasts
slug: departing-libre-lounge
---

Over the last year and a half I've had a good time presenting on
[Libre Lounge](https://librelounge.org/) with my co-host Serge Wroclawski.
I'm very proud of the topics we've decided to cover, of which there
are quite a few good ones in [the archive](https://librelounge.org/archive/),
and the audience the show has had is just the best.

However, I've decided to depart the show...  Serge and I continue to be
friends (and are still working on a number of projects together, such as
[Datashards](https://datashards.net/) and the
[recently announced grant](https://dustycloud.org/blog/spritely-nlnet-grant/)),
but in terms of the podcast I think we'd like to take things in
different creative directions.

This is probably not the end of me doing podcasting, but if I start
something up again it'll be a bit different in its structure... and
you can be sure you'll hear about it here and on my
[fediverse account](https://octodon.social/@cwebber/) and over at
[the birdsite](https://twitter.com/dustyweb/).

In the meanwhile, I look forward to continuing to tuning into Libre
Lounge, but as a listener.

Thanks for all the support, Libre Loungers!
