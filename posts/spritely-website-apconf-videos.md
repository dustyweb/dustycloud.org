title: Spritely website launches, plus APConf video(s)!
date: 2020-09-30 14:20
author: Christine Lemmer-Webber
tags: spritely, activitypub, apconf
slug: spritely-website-apconf-videos
---

**Note:** This originally appeared
[as a post on my Patreon account](https://www.patreon.com/posts/42200964)...
thanks to all who have donated to support my work!

Hello, hello!  [Spritely's website](https://spritelyproject.org/) has
finally launched!  Whew... it's been a lot of work to get it to this
state!  Plus check out our new logo:

![Spritely logo](https://spritelyproject.org/static/images/spritely-logo-transparent-500px.png)

Not bad, eh?  Also with plenty of cute characters on the Spritely site
(thank you to [David Revoy](https://www.peppercarrot.com/) for taking
my loose character sketches and making them into such beautiful
paintings!)

But those cute characters are there for a reason!  Spritely is quite
ambitious and has quite a few subprojects.  Here's a
[video that explains how they all fit together](https://conf.tube/videos/watch/18aa2f92-36cc-4424-9a4f-6f2de946fbd2).
Hopefully that makes things more clear!

Actually that video is from [ActivityPub Conference 2020](https://conf.activitypub.rocks/),
the [talks](https://conf.activitypub.rocks/#talks) of which have now
[all have their videos live](https://conf.tube/video-channels/apconf_channel/videos)!
I also moderated the [intro keynote panel about ActivityPub authors/editors](https://conf.tube/videos/watch/6289920f-4e35-4141-ab6f-379b357849ec).
Plus there's an easter egg, the [ActivityPub Conference Opening Song](https://conf.tube/videos/watch/285e7580-8281-4ae4-842e-81c687237c69)! :)

But I can't take credit for APConf 2020... organization and support are thanks to Morgan Lemmer-Webber, Sebastian Lasse, and [FOSSHost](https://conf.tube/videos/watch/285e7580-8281-4ae4-842e-81c687237c69) for hosting the website and BigBlueButton instance and [conf.tube](https://conf.tube/) for generously hosting all the videos.  There's a [panel about the organization of APConf](https://conf.tube/videos/watch/87bc99dd-b1b8-4fc3-b034-dca805388179) you can watch if you're interested in more of that!  (And of course, all the other great videos too!)

So... what about that week I was going to work on Terminal Phase?
Well... I'm still planning on doing it but admittedly it hasn't
happened yet.  All of the above took more time than expected.
However, today I *am* working on my talk about Spritely Goblins for
RacketCon, and as it turns out, extending Terminal Phase is a big part
of that talk.  But I'll announce more soon when the Terminal Phase
stuff happens.

Onwards and upwards!
