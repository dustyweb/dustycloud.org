title: Mushiki Love
date: 2008-12-27 18:28
tags: food, personal, miro, diet, blender
author: Christine Lemmer-Webber
slug: mushiki-love
---
Miro 2.0 is shaping up pretty fast, but I'm actually working on the
[Miro Guide](http://miroguide.org) presently. New versions of both
should be launching pretty close to each other, if not at the same time.
I'm pretty confident in a super-awesome-release. But between that, the
holidays, the upcoming wedding, and my efforts to improve my
[Blender](http://blender.org) skills, things are pretty busy.

Anyway, that has nothing to do with the title of this post. I am going
to go on a bit of a random rant.

I've recently been playing with food dehydration, fermentation,
pickling, and other forms of food preservation that don't require
refrigeration. (No reason other than it's just a really interesting
thing to learn about.) In the process of experimenting on how to make my
own vegetarian teriyaki jerky using
[tofu](http://en.wikipedia.org/wiki/Tofu),
[tempeh](http://en.wikipedia.org/wiki/Tempeh), and
[seitan](http://en.wikipedia.org/wiki/Seitan) (the tofu and tempeh
turned out to be the most interesting... seitan was a bit too brittle
for my taste, though it was the one that *looked* most like beef jerky)
I ended up wandering the aisles of the local asian grocery store to
refresh my supply of those ingredients. I ended up impulsively picking
up a bamboo steamer (a [Mushiki](http://en.wikipedia.org/wiki/Mushiki)).
I didn't know how it worked... I just bought it. It was only 6 bucks. It
may have been the best impulsive 6 bucks I ever spent.

I had a pot that it fit perfectly over. I put some water on to boil,
chopped up some vegetables, tossed in some extra firm tofu and
vegetarian fake duck (really just seasoned, canned Seitan). Put it over
the pot to steam for 5 minutes. I was surprised at how fast and
effortless it all was. Anyway, put the food into a bowl and poured some
teriyaki sauce over top. Mixed it up, dug in.

I was *totally astonished* at how delicious the vegetables were. I have
*never* enjoyed vegetables so much in all my life. It wasn't a complex
meal, it was healthy, and it was totally delicious. And oddly enough, I
was full.

The next night I chopped up a banana and an apricot, threw in a
raspberry and a cherry, and steamed it all for 5 minutes. I almost fell
over. *It was the most delicious desert I had ever eaten.* No added
sugar or anything.. was just fantastic on its own.

Since then I have also steamed and eaten: a leek bun, a red bean bun,
and some edamame. All fantastic.

Not really much more to this post than that. I am just astounded that I
have never played with this form of cooking until now.
