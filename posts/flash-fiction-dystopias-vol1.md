title: Flash Fiction Dystopias Volume 1
date: 2013-06-03 10:25
author: Christine Lemmer-Webber
slug: flash-fiction-dystopias-vol1
---
I thought I'd have some fun and try my hand at some flash fiction
dystopian futures. This will be an exercise in brevity writing about
topics I think about without taking them too seriously. Without further
ado:

> HTML5 DRM goes through; Netflix sweeps into the web and over the next
> few years takes over the majority of video and audio distribution on
> the web, becoming the web's first super-monopoly on media. The DRM
> standard is easily worked around, but that was just a front for a
> legal excuse to sue anyone who does. Netflix eventually starts sending
> out lawsuits to anyone who doesn't have a subscription; I mean come
> on, do they really believe you haven't been watching any of these
> popular shows? Seems unlikely.
>
> Monsanto does military contracting and creates a disease (or
> non-dispersable airborne chemical, or a plain old buildup in toxins
> from embedding pesticides inside of food, take your pick) that wipes
> out half the population; children are particularly susceptible. In
> further military contracting, they had already built several lines of
> genetically engineered embryos that are immune. After the war,
> future-parents are offered the purchase of said embryos, but you
> basically have a choice between one of 5 different sets of DNA. Also
> Monsanto has control on the patents so attempts to genetically
> engineer your own immune children are seen as piracy; said children
> are confiscated and disposed of.
>
> Genetic patents on cancer and cancer cures means you get charged both
> for the cure to the disease and for infringing on the company's
> patents for having the disease in the first place.
>
> Google Glass comes out, free software alternatives struggle to develop
> for its infrastructure, especially on the backend. The pressure to own
> a set and stream your life through Google's datacenters is far greater
> than the pressure to own a cell phone ever was (and so is the social
> ostracization). EEG keyboards also come out; computation has moved
> into the point of being an additional processor for your brain. Free
> alternatives start to get good, but aren't quite there, and the
> authors never bother to successfully advocate to non-developers;
> occasionally people try to move over but discover that the only people
> they can communicate with in the free alternative are free software
> developers, and get tired of it. The singularity happens, and is
> wholly owned and operated by Google, Inc.
>
> Automated cars come out with no free alternatives, are a remarkable
> improvement in automotive safety, but mean total surveillance on
> movement and become an effective tool (along with Google Glass) in an
> emerging police state. Bicyclists suddenly become the only group that
> have true autonomy, and begin to realize it. Unfortunately that leads
> 5% of the biking population to become really smug about things and the
> popular image of bicyclists on the road becomes so low that pushing
> for legislation to improve conditions for bicyclists to make it a more
> feasible daily urban mode of transport never take off.
>
> Full computing, data, and software user freedom is eventually
> achieved, but it turns out it doesn't matter because it happens at
> approximately the same time that modern civilization becomes
> unsustainable due to resource depletion and catastrophic climate
> change.
