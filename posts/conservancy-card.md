title: telnet postcard.sfconservancy.org 2333
date: 2019-12-19 12:11
author: Christine Lemmer-Webber
tags: spritely, conservancy, foss, racket
slug: conservancy-card
---
![](https://dustycloud.org/tmp/conservancy-card-terminal-snow.gif){.align-center}

Try running "telnet postcard.sfconservancy.org 2333" in your terminal...
you won't regret it, I promise! You should get to see a fun "season's
greetings postcard" animated live in your terminal. My friends at
[Software Freedom Conservancy](https://sfconservancy.org/) are [running
a supporters drive](https://sfconservancy.org/supporter/) and I was
happy to do this to help (if you were a supporter in the past, you
should be getting a matching postcard in the mail!)

As you probably guessed, this is built on top of the same tech stack as
[Terminal Phase](https://dustycloud.org/blog/terminal-phase-prototype/).
Speaking of, at the time of writing we're getting quite close to that...
only \$40/month to go! Maybe we'll make it by the end of the year?

In the meanwhile, if you want to show your support for Conservancy and
convince your friends to join, there are some [matching
banners](https://sfconservancy.org/supporter/banner/) you can put up on
your website! And if you want to run the animated postcard yourself or
generate any of its associated images, well... [it's free
software](https://gitlab.com/dustyweb/conservancy-postcard)!
