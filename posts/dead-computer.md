title: UPDATE computer SET status='dead';
date: 2008-06-22 23:20
tags: backups
author: Christine Lemmer-Webber
slug: dead-computer
---
The title of this post is actually less me trying to make a bad SQL joke
as it is me ripping off [my favorite Dinosaur Comics
comic](http://www.qwantz.com/archive/000153.html). But yeah. Lots of
deadness of non-funness. Anyway, this is one of those boring personal
life posts that people probably don't care about, but it at least has a
fairly simple moral to the story.

So I noticed from work on Friday that my ssh connection kept resetting
to my home machine. Every once in a while, RCN is flaky, so I figured
that's what it was until I realized that emacsclient wasn't connecting
to my existing session and that my uptime was only five minutes. It was
raining, so I figured that maybe the power went out several times.

Well, I got home and quickly realized that my computer wasn't turning
on. At all. I took off the case and noticed that, while the power supply
fan was running, the processor fan wasn't. Cycling the power supply and
then hitting the power button would lead to the processor fan spinning
for a couple of seconds and then stopping. So I tried reseating the ram.
No luck. I tried with individual sticks of ram. Again, no luck. So it
didn't seem to be the ram. My couple of years of diagnosing and
repairing machines at my [previous job](http://google.com) have made me
pretty decent at fixing machines (though I mostly got by at that job
with using my software skills to overcome my lack of hardware skills),
but I'm pretty rusty. But anyway, at this point I figured there was a
good chance that it was the powersupply, despite the fact that the fan
was running, as it might not be supplying enough power to power up the
machine entirely (which I've seen happen before). So I tried replacing
the powersupply. Nope, no luck. Luckily I had made a backup of my data a
couple of weeks ago, so I wasn't really worried there. Even so, most
likely I figured the drive wasn't damaged, so I probably hadn't even
lost anything within that group of time.

At this point, I figure it's probably the motherboard, but it's not
within my budget to replace that right now. So I asked Morgan, my
wonderful fiance, if she'd mind if I "moved in" to her computer until I
was able to fix mine. She was fine with it. I figured this would also be
good motivation for me to *actually* start regular backups on her
computer, which I've been promising to do for a while. About six months
ago, I installed a second drive on Morgan's computer so she could easily
run Ubuntu. She hasn't touched the windows drive much since then, except
to pull data over. So it shouldn't be too hard for me to move over.

So I pull out my drive, and with one of those nice little PATA-to-USB
adapters I started rsync'ing my data over to my home directory.

Except, bad news. That drive I installed Ubuntu on for Morgan was a
rather old one, and apparently moving several gigs of data over was just
too much for it to handle. Halfway through the transfer, it died.
Completely died. Like, I couldn't even 'dd' the drive to an image so I
could try to inspect and recover its contents. I deliver the bad news to
Morgan. She takes it pretty well, since most of her data was still
mirrored on the windows machine, but she probably did lose a paper or
two. At any rate, I was pretty clearly an ass for not backing up her
drives sooner. So I promised to back up her Windows drive right then, to
be sure that no more freak accidents could happen where she would lose
*all* of her data.

So I took the [Western Digital
Passport](http://www.amazon.com/Western-Digital-WDXMS2500TN-Passport-Portable/dp/B000RY2PLQ)
I had bought expressly for this purpose several months ago. Fired up
rsync, and... well, it didn't die, but it kept clicking and then
disconnecting during the middle of transfer. Thankfully, rsync is
inherently well designed for this scenario, but... dammit.

So I went out and bought two new drives, one to replace the dead Linux
drive, and one to put in an enclosure for regular backups. Those drives
are fine, but I'm pretty disturbed by the multiple system failures I
experienced. The moral of the story is pretty obvious. Backups are
critical. Losing your computer is a frustrating experience. Losing your
data can be a traumatic experience. Thankfully, not much data was lost,
though that small amount of data that Morgan did lose was totally
preventable.

I'm also planning on writing a tutorial on how to do easy backups with
rsync and either an external drive or another remote computer. I'm also
thinking about starting to back up to an entirely remote location, like
either a family member's house or maybe even something like Amazon S5.
But if I did that I'd want to encrypt all my files with something like
GPG. I'm not sure about that yet, but I think it's generally a good idea
in case some sort of disaster were ever to strike us. Hopefully such a
thing won't happen, but with the kind of luck I've had this weekend, I'm
all about preparing for the worst.
