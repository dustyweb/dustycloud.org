title: Terminal Phase v1.1 and Spritely Goblins v0.6 releases!
date: 2020-03-05 10:50
author: Christine Lemmer-Webber
tags: spritely, goblins, terminal phase
slug: terminal-phase-1.1-and-goblins-0.6
---

Hello all!
I just did a brand new release of both:

- [Terminal Phase v1.1!](https://gitlab.com/dustyweb/terminal-phase/-/tree/v1.1)
- [Goblins v0.6!](https://gitlab.com/spritely/goblins/-/tree/v0.6)

So some highlights from each.

# Terminal Phase

Okay, this is flashier, even if less important than Goblins.
But the main thing is that I added the
[time travel debugging](https://dustycloud.org/blog/goblins-time-travel-micropreview/)
feature, which is so flashy I feel the need to show that gif again here:

![Time travel in Spritely Goblins shown through Terminal Phase](https://dustycloud.org/gfx/goodies/terminal-phase-goblins-time-travel.gif)

Aside from time travel, there aren't many new features, though I plan
on adding some in the next week (probably powerups or a boss fight),
so another release should be not far away.

And oh yeah, since it's a new release, now is a good time to thank the
current [supporters](https://www.patreon.com/cwebber):

![Terminal Phase Credits](https://dustycloud.org/gfx/goodies/terminal-phase-credits-2020-03-05.gif)

But yeah, the main thing that was done here is that Terminal Phase was
updated for the new release of
[Goblins](https://gitlab.com/spritely/goblins), so let's talk about that!

# Goblins

For those who aren't aware, [Spritely Goblins](https://gitlab.com/spritely/goblins)
is a transactional actor model library for [Racket](https://racket-lang.org/).

v0.6 has resulted in a number of changes in semantics.

But the big deal is that
[Goblins finally has decent documentation](https://docs.racket-lang.org/goblins/),
including a fairly [in-depth tutorial](https://docs.racket-lang.org/goblins/tutorial.html)
and [documentation about the API](https://docs.racket-lang.org/goblins/api.html).
I've even documented how you, in your own programs, can
[play with Goblins' time travel features](https://docs.racket-lang.org/goblins/tutorial.html#%28part._.Time_travel__snapshotting_and_restoring%29).

So, does this mean you should start using it?
Well, it's still in alpha, and the most exciting feature (networked,
distributed programming) is still on its way.
But I think it's quite nice to use already (and I'm using it for Terminal Phase).

Anyway, that's about it... I plan on having a new video explaining
more about how Goblins works out in the next few days, so I'll
announce that when it happens.

If you are finding this work interesting, a reminder that this work is
powered by
[people like you](https://www.patreon.com/cwebber).

In the meanwhile, hope you enjoy the new releases!
