title: Minimalist bundled and distributed bugtracker w/ orgmode
date: 2015-10-11 09:20
author: Christine Lemmer-Webber
tags: orgmode, hacking, foss, emacs
slug: minimalist-distributed-bugtracker
---
*Thinking out loud here... this isn't a new idea but maybe here's a
solid workflow...*

"Distributed" as in the project's existing DVCS.

-   Check a TODO.org orgmode file right into your project's git repo
-   Accept additions/adjustments to TODO.org via patches on your mailing
    list
-   As soon as a bug is "accepted", it's committed to the project.
-   When a bug is finished, it's closed and archived.
-   Contributors are encouraged to submit closing tasks in the orgmode
    tree as part of their patch.
-   Bug commentary happens on-list, but if users have useful information
    to contribute to someone working on a bug, they can submit that as a
    patch.

I think this would be a reasonably complete but very emacs user oriented
bugtracker solution, so maybe in addition:

-   A script can be provided which renders a static html copy for
    browsing open/closed bugs.
-   A "form" can be provided on that page to email the list about new
    discovered bugs, and formats the submission as an orgmode TODO
    subsection. This way maintainers can easily file the bug into the
    tracker file if they deem appropriate.

I think this would work. Lately I've been hacking on a project that's
mostly just me so far, so I just have an orgmode file bundled with the
repo, but I must say that it's rather *nice* to just hack an orgmode
file and have your mini-bugtracker distributed with your project. I've
done this a few times but as soon as the project grows to multiple
contributors, I move everything over to some web based bugtracker UI.
But why *not* distribute all bugs with the project itself? My main
thinking is that there's a tool-oriented barrier to entry, but maybe the
web page render can help with that.

I've been spending more time working on more oldschool projects that
just take bugs submitted on mailing lists as a contribution project.
They seem to do just fine. So I guess it entirely depends on the type of
project, but this may work well for some.

And yes, there are a lot of obvious downsides to this too; [paultag
points out a few](https://twitter.com/paultag/status/653217639658090497)
:)
