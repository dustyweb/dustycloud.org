title: Free Speech Is Illegal
date: 2008-06-04 23:03
author: Christine Lemmer-Webber
slug: free-speech-is-illegal
---
If you've read this blog before, I'm sure you're expecting me to talk
about Obama's awesome win. But no doubt you've already heard about that.

What you might not have heard is that [82 people were arrested for
opposing torture at Guantanamo
Bay](http://paceebene.org/blog/ken-butigan/witness-against-torture-trial-diary-entry-6).
(One of them was my good friend Ken Butigan.) To top it off, the
location they were holding their protest at was the US Supreme court. So
much for being a temple of justice and liberty.

The Washington Post [also ran a
story](http://www.washingtonpost.com/wp-dyn/content/article/2008/05/29/AR2008052903103.html).
