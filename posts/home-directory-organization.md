title: Home Directory Organization
date: 2008-05-11 10:35
author: Christine Lemmer-Webber
slug: home-directory-organization
---
I've Been thinking about the way I currently organize my home directory,
and I'm not sure it's quite the best. It's actually a lot more messy
than this in reality, but here goes:

  Directory          Contents
  ------------------ ------------------------------------------------------------------------------------------------------------------------------------------------------------------
  \~/bin/            What you'd expect... local scripts & executables.
  ---------------    ------------------------------------------------------
  \~/backgrounds/    Backgrounds for my desktop
  ---------------    ------------------------------------------------------
  \~/college/        College notes, essays, etc. Has subfolders of individual classes.
  ---------------    ------------------------------------------------------
  \~/docs/           Writings and documentation, not by me.
  ---------------    ------------------------------------------------------
  \~/devel/          Code-related projects I've created or have been involved with. I also have a \~/code directory that I'm planning on deprecating.
  ---------------    ------------------------------------------------------
  \~/elisp/          Code I use to extend emacs. Some of it my own creation (and symlinked to \~/devel). Mostly stuff I've downloaded
  ---------------    ------------------------------------------------------
  \~/gfx/            Graphics of my own creation, or that I've been involved with.
  ---------------    ------------------------------------------------------
  \~/mail/           My local mail spools...
  ---------------    ------------------------------------------------------
  \~/music/          Third party music. I don't make music, so this is all other peoples' creations.
  ---------------    ------------------------------------------------------
  \~/Plans/          "Muse" files from [Planner Mode](http://www.emacswiki.org/cgi-bin/wiki/PlannerMode). Personal notes and documentation, todo lists, etc.
  ---------------    ------------------------------------------------------
  \~/programs/       Programs I've downloaded (and usually compiled from source) that I don't do any development for.
  ---------------    ------------------------------------------------------
  \~/proj/           Some projects that are too wide encompassing to fit in a specific folder.
  ---------------    ------------------------------------------------------
  \~/python-local/   Common python libraries I use. Normal setuptools-y directory. I'm planning on moving this into \~/programs/ soon.
  ---------------    ------------------------------------------------------
  \~/records/        Stuff I need to remember, like how much my rent bill is and what my landlord's address is. Some of this is encrypted with [GPG](http://www.gnupg.org/).
  ---------------    ------------------------------------------------------
  \~/svn/            Some svn repositories I've set up. It's not one big repository in here, but several... one for \~/college, one for \~/devel, one for \~/gfx... you get the idea.
  ---------------    ------------------------------------------------------
  \~/tmp/            What you'd expect... files I'm just downloading temporarily. I clear this out now and then.
  ---------------    ------------------------------------------------------
  \~/vids/           Videos... pretty much all made by others, except for a few blender animations I hacked together.
  ---------------    ------------------------------------------------------
  \~/words/          Writings, by me.

Well, that's the way it is *ideally*, at least. Now, this probably looks
reasonably clean, but of course, looks can be deceiving. There's a
fundamental problem with this design.

I'd like to have a distinction with projects I'm involved with and
projects totally made by others. This isn't an issue of "fear of
influence" or whatever. I think it would be pretty hard to be part of
the free software/open source/free culture movements and such and be
afraid to be influenced by or collaborate with others. Its just really
convenient to separate the things I'm working on from the rest.

So anyway, how to reorganize things? The easy way would be to either
have an all encompassing \~/projects directory that could have separate
subdirectories for \~/code \~/words \~/gfx & etc. Or maybe I should do
the reverse and keep all of my personal projects at toplevel of my home
directory, and keep a \~/media directory for all files that aren't mine?

How do other people organize their home directories?
