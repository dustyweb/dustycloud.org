title: Things I've been doing lately
date: 2010-03-06 14:23
tags: personal, foss, creative-commons, pycon, blender
author: Christine Lemmer-Webber
slug: things-ive-been-doing-lately
---
I've actually been writing quite a lot and doing some rather interesting
things. Unfortunately, I haven't really found much time to update here,
so here's a brief recap of stuff:

-   I've done some blogging over on the [Creative Commons Labs
    blog](http://labs.creativecommons.org/). I plan to do more soon, but
    for now there's a post called [Understanding the State of Sanity via
    Whiteboards and Ascii
    Art](http://labs.creativecommons.org/2009/12/18/understanding-the-state-of-sanity-via-whiteboards-and-ascii-art/)
    and [CC.Engine and Web
    Non-Frameworks](http://labs.creativecommons.org/2010/01/13/cc-engine-and-web-non-frameworks/).
-   I've participated in the [PyCon video](http://pycon.blip.tv/)
    recording crew again.
-   I've given a number of talks at [ChiPy](http://chipy.org), but the
    only one I am going to bother linking to is this this talk on [Git
    and GitPython](http://carlfk.blip.tv/file/3107979/).
-   I suggested that the [Durian Project](http://durian.blender.org)
    should [run a
    sprint](http://durian.blender.org/news/community-modeling-sprint/)
    (I'm credited as "cwebb" in that post). They did, and it was a
    [stellar
    success](http://durian.blender.org/news/modeling-sprint-a-stellar-success/),
    by their own words. Sadly, I was recording video at PyCon and was
    thus unable to participate. I'd like to write more about why this is
    significant soon beyond what is immediately obvious... hopefully
    I'll find some time.
-   I've been coding a whole bunch of things. One of the things maybe
    that I'll get up soon is a simple cgit-like git web interface called
    [wsgit](http://gitorious.org/wsgit). Although,
    [Will](http://bluesock.org/~willg/) has been working on that more
    recently than I have, because he's awesome. (He's also been working
    on curating [Python Miro
    Community](http://python.mirocommunity.org)... have you seen that?)
    I've been working on a number of other smaller wsgi applications
    that hopefully I can better announce (and get to a publicly usable
    state) soon.
-   I've been hanging out at [Pumping Station
    One](http://pumpingstationone.com), local Chicago hackerspace.
-   I won a [weekend challenge on the BlenderArtists
    forums](http://blenderartists.org/forum/showthread.php?t=168549).

As for that last one, there's no "real" prize for winning a Blender
weekend challenge other than suggesting the title of the next contest,
but this may be one of the thing I am most proud of anyway, because I
think the final product came out really well. It is the first piece I've
finished in Blender that I feel really happy with. More importantly, I
redid the piece a bit after the contest. We got it printed and Morgan
framed it, and we gave it to [my father for
Christmas](http://dustycloud.org/gfx/goodies/dad_and_zugg.png). This was
important to me, as my father is the one who got me interested in
cartooning and animation at an early age in the first place.

![image](http://dustycloud.org/gfx/goodies/zugg_scene-hd-cropped-small.png)

There's an even much bigger thing I've been working on that's almost
ready for public viewing and consumption, but it's not ready for viewing
yet. But I promise I'll blog here when it is instead of waiting for an
overwhelming blogpost. :)

In the meanwhile, [my identi.ca account is where it's
at](http://identi.ca/cwebber).
