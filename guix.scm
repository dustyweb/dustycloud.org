(use-modules (guix packages)
             (guix licenses)
             (guix git-download)
             (guix build-system gnu)
             (gnu packages)
             (gnu packages autotools)
             (gnu packages guile)
             (gnu packages guile-xyz)
             (gnu packages pkg-config)
             (gnu packages texinfo))

(define haunt-from-git
  (package
   (inherit haunt)
   (version "git")
   (source (origin
            (method git-fetch)
            (uri (git-reference
                  (url "https://gitlab.com/dustyweb/haunt.git")
                  (commit "7243f98c6eda325288bb3c92c55e26303c38f7dc")))
            (sha256
             (base32
              "1px7yxv3a6g4nnd4p7xjxdbsvaf0b6x6n8x9xqv7a9j51h3wjsps"))))
   (arguments
    `(#:modules ((ice-9 match) (ice-9 ftw)
                 ,@%gnu-build-system-modules)
      #:tests? #f                       ; test suite is non-deterministic :(
                                        ; (or it was when I first wrote this
                                        ; comment?)
      #:phases (modify-phases %standard-phases
                 ;; this one is custom
                 (add-before 'configure 'bootstrap
                   (lambda _
                     (zero? (system* "./bootstrap"))))
                 ;; This one came straight from haunt's definition
                 ;; in Guix
                 (add-after 'install 'wrap-haunt
                   (lambda* (#:key inputs outputs #:allow-other-keys)
                     ;; Wrap the 'haunt' command to refer to the right
                     ;; modules.
                     (let* ((out  (assoc-ref outputs "out"))
                            (bin  (string-append out "/bin"))
                            (site (string-append
                                   out "/share/guile/site"))
                            (guile-reader (assoc-ref inputs "guile-reader"))
                            (deps `(,@(if guile-reader
                                          (list guile-reader)
                                          '())
                                    ,(assoc-ref inputs "guile-commonmark"))))
                       (match (scandir site)
                         (("." ".." version)
                          (let ((modules (string-append site "/" version))
                                (compiled-modules (string-append
                                                   out "/lib/guile/" version
                                                   "/site-ccache")))
                            (wrap-program (string-append bin "/haunt")
                              `("GUILE_LOAD_PATH" ":" prefix
                                (,modules
                                 ,@(map (lambda (dep)
                                          (string-append dep
                                                         "/share/guile/site/"
                                                         version))
                                        deps)))
                              `("GUILE_LOAD_COMPILED_PATH" ":" prefix
                                (,compiled-modules
                                 ,@(map (lambda (dep)
                                          (string-append dep "/lib/guile/"
                                                         version
                                                         "/site-ccache"))
                                        deps))))
                            #t)))))))))
   (native-inputs
    `(("autoconf" ,autoconf)
      ("automake" ,automake)
      ("libtool" ,libtool)
      ,@(package-native-inputs haunt)))))

(package
 (name "spritely-site")
 (version "git")
 (source #f)
 (build-system gnu-build-system)
 (synopsis #f)
 (description #f)
 (license gpl3+)
 (home-page "https://spritelyproject.org")
 (inputs
  `(("guile" ,guile-3.0)
    ("haunt" ,haunt-from-git)
    ("guile-lib" ,guile-lib)
    ("guile-reader" ,guile-reader)
    ("guile-sjson" ,guile-sjson)
    ("guile-syntax-highlight" ,guile-syntax-highlight)
    ("guile-commonmark" ,guile-commonmark))))
