(use-modules (dustycloud templates)
             (dustycloud atom-feed-hack)
             (haunt asset)
             (haunt html)
             (haunt site)
             (haunt page)
             (haunt post)
             (haunt utils)
             (haunt reader commonmark)
             (haunt reader skribe)
             (haunt reader)
             (haunt builder blog)
             (haunt builder rss)
             (haunt builder assets)

             (web uri)

             (commonmark)
             (markdown)
             (flat-files)
             (html-reader))

(define dustycloud-haunt-theme
  (theme #:name "Dustycloud Brainstorms"
         #:layout
         (lambda (site title body)
           (base-tmpl
            site body
            #:title title))
         #:post-template post-tmpl
         #:collection-template collection-tmpl
         #:pagination-template pagination-tmpl))

(define (archive-page site posts)
  (make-page
   "archive/index.html"
   (archive-tmpl site posts)
   sxml->html))

(define (custom-build-post-uri site post blog-prefix)
  (build-uri (site-scheme site)
             #:host (site-domain site)
             #:path (string-append blog-prefix "/"
                                   (post-slug post)
                                   "/")))

(define builders
  (list (blog #:theme dustycloud-haunt-theme
              #:prefix "/blog"
              #:posts-per-page 10)
        archive-page
        (flat-files "pages"
                    #:templates flat-templates)
        (atom-feed #:blog-prefix "/blog"
                   #:file-name "/blog/index.xml"
                   #:build-post-uri custom-build-post-uri)
        (static-directory "static/css" "etc/css")
        (static-directory "static/images" "etc/images")
        (static-directory "static/js" "etc/js")
        (static-directory "static/flier" "etc/flier")))

(define (indexified-slug post)
  (string-append (post-slug post) "/index"))

(site #:title "Dustycloud Brainstorms"
      #:domain "dustycloud.org"
      #:default-metadata
      '((author . "Christine Lemmer-Webber")
        (email . "cwebber@dustycloud.org"))
      #:readers (list skribe-reader
                      commonmark-reader*
                      html-reader*
                      sxml-reader)
      #:builders builders
      #:make-slug indexified-slug)
