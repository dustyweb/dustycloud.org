compile_and_run: html serve

html:
	guix environment -l guix.scm -- haunt build

serve:
	guix environment -l guix.scm -- haunt serve --watch

upload: html
	rsync --delete --recursive --verbose \
	   site/ dustycloud:/srv/dustycloud/git_checkout/output

serve-any: html
	guix environment -l guix.scm -- haunt serve --watch --host=any

.PHONY: html serve upload serve-any
