All original content in this repository is dual licensed under
Creative Commons Attribution-ShareAlike 3.0 Unported
and the GNU General Public License, version 3 or (at your option) any
later version published by the Free Software Foundation.

It's mostly written by Christine Lemmer-Webber.  There's a chance of
other contributors, and I try to acknowledge them where appropriate. :)
